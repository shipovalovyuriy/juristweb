package kz.novelty.jurist;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.net.URLEncoder;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.zip.GZIPOutputStream;

public class UpdateBuilderHandler extends javax.servlet.http.HttpServlet {
    interface handler {
        void process(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException;
    }

    HashMap<String, handler> handlers = new HashMap<String, handler>();

    public UpdateBuilderHandler() {
        super();
        handlers.put("getChanges", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
                process_getChanges(req, resp, out);
            }
        });
        handlers.put("buildUpdate", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
                process_buildUpdate(req, resp, out);
            }
        });
        handlers.put("doLogin", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
                process_doLogin(req, resp, out);
            }
        });
        handlers.put("doLogout", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
                process_doLogout(req, resp, out);
            }
        });
        handlers.put("getUserInfo", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
                process_getUserInfo(req, resp, out);
            }
        });
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String rtype = req.getParameter("requestType");
        handler handler = handlers.get(rtype);
        OutputStream out = resp.getOutputStream();
        OutputStream outOrig = out;
        String encoding = req.getHeader("Accept-Encoding");
        if (encoding != null && encoding.indexOf("gzip") >= 0) {  // gzip broser?
            resp.setHeader("Content-Encoding", "gzip");
            out = new GZIPOutputStream(out);
        }
        if (handler == null) {
            resp.setStatus(500);
            //resp.sendError(500, " UNKNOWN REQUEST " + rtype);
            out.write((" UNKNOWN REQUEST " + rtype).getBytes());
            out.flush();
            out.close();
        } else try {
            handler.process(req, resp, out);
        } catch (Exception e) {
            e.printStackTrace();
            resp.setStatus(500);
            //resp.sendError(500, e.getMessage());
            out.write(e.getMessage().getBytes());
            out.flush();
            out.close();
        }
        out.flush();
        out.close();
        if (outOrig != out) outOrig.close();
    }



    public void setUserInfo(HttpServletRequest req, HttpServletResponse resp, UserInfo user) {
        HttpSession session = req.getSession(true);
        Cookie sessionCookie = new Cookie("JSESSIONID", session.getId());
        sessionCookie.setPath("/");
        resp.addCookie(sessionCookie);
        session.setAttribute("upduser", user);
        String sess = session.getId();
        if (user == null) {
            Connector.Instance().removeUserSessionInfo(sess, "Завершение сеанса");
        } else {
            Connector.Instance().addUserSessionInfo(sess, user);
        }
    }

    /**
     * возвращает информацию о текущем пользователе
     *
     * @param req
     * @param resp
     * @return
     */
    public UserInfo getUserInfo(HttpServletRequest req, HttpServletResponse resp) {
        HttpSession session = req.getSession(true);
        String sess = session.getId();
        UserInfo user = (UserInfo) session.getAttribute("upduser");
        return user;
    }

    public void print(OutputStream stream, String s) throws IOException {
        if (s == null) s = "null";
        int len = s.length();
        for (int i = 0; i < len; i++) {
            char c = s.charAt(i);
            if ((c & 65280) != 0) {
                ResourceBundle lStrings = ResourceBundle.getBundle("javax.servlet.LocalStrings");
                String errMsg = lStrings.getString("err.not_iso8859_1");
                Object errArgs[] = new Object[1];
                errArgs[0] = new Character(c);
                errMsg = MessageFormat.format(errMsg, errArgs);
                throw new CharConversionException(errMsg);
            }
            stream.write(c);
        }
    }

    protected void process_getChanges(final HttpServletRequest req, final HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
        if(getUserInfo(req,resp)==null) throw  new ServletException("access denied");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String dateFrom = req.getParameter("date_from");
        String dateTill = req.getParameter("date_till");
        if(dateFrom==null || "".equals(dateFrom)){
            dateFrom = sdf.format(new Date());
        }
        if(dateTill==null || "".equals(dateTill)){
            dateTill = sdf.format(new Date());
        }
        String sql =    "select \n" +
                "    id1,\n" +
                "    doc_type,\n" +
                "    group_concat(title) as title,\n" +
                "    group_concat(op_type) as oper_type,\n" +
                "    max(confirm) as confirm, \n" +
                "    max(id) as id, \n" +
                "    max(op_date) as oper_date, \n" +
                "    max(included) as included, \n" +
                "    group_concat(distinct changed_by) as changed_by\n" +
                "from (\n" +
                "    select  \n" +
                "            id1, \n" +
                "            modifiedTable as doc_type, \n" +
                "            case modifiedTable \n" +
                "                 when 'documents' then (select shortName from properties where id=id1 ) \n" +
                "                 when 'trees' then (select name from trees where id=id1 ) \n" +
                "                 when 'attachments' then (select name from attachments where id=id1 ) \n" +
                "            end as title,\n" +
                "            case operation \n" +
                "                 when 0 then 'add' \n" +
                "                 when 1 then 'edit' \n" +
                "                 when 2 then 'del' \n" +
                "                 else concat('',operation)  \n" +
                "            end as op_type, \n" +
                "            0 as confirm, \n" +
                "            max(id) as id, \n" +
                "            max(dateCreating) as op_date, \n" +
                "            max(included) as included, \n" +
                "            group_concat(distinct user) as changed_by\n" +
                "    from updates \n" +
                "    where dateCreating>=? AND dateCreating<=?\n" +
                "    group by id1, doc_type, op_type\n" +
                ") t\n" +
                "group by id1, doc_type\n" +
                "order by id1";
        String res = (String) Connector.Instance().executeQuery(sql, new Object[]{dateFrom, dateTill}, Connector.Instance().QueryToJsonHandler, Boolean.TRUE);
        out.write(res.getBytes("UTF-8"));
    }
    protected void process_buildUpdate(final HttpServletRequest req, final HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
        if(getUserInfo(req,resp)==null) throw  new ServletException("access denied");
        String id = req.getParameter("ids");
        final String userAgent = req.getHeader("USER-AGENT").toLowerCase();
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String ids = req.getParameter("ids");
        String vids = req.getParameter("v_ids");
        String dateFrom = req.getParameter("date_from");
        String dateTill = req.getParameter("date_till");
        if(dateFrom==null || "".equals(dateFrom)){
            dateFrom = sdf.format(new Date());
        }
        if(dateTill==null || "".equals(dateTill)){
            dateTill = sdf.format(new Date());
        }
        UpdateBuilder builder = new UpdateBuilder();

        byte[] data = null;
        try {
            data = builder.build(dateFrom, dateTill, ids, vids);
        } catch (Exception e) {
            throw new ServletException(e);
        }

        resp.setContentType("application/octet-stream");
        try {
            SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
            String fileName = URLEncoder.encode(String.format("upd_%s.sps", df.format(new Date())), "UTF-8");
            if (userAgent != null && (userAgent.contains("chrome") || userAgent.contains("msie"))) {
                resp.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            } else {
                resp.setHeader("Content-Disposition", "attachment; filename*=utf-8''" + fileName);
            }
        } catch (UnsupportedEncodingException e) {
            resp.setHeader("Content-Disposition", "attachment; filename=upd.sps");
        }
        out.write(data);
    }

    protected void process_getUserInfo(final HttpServletRequest req, final HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
        UserInfo user = getUserInfo(req, resp);
        if (user != null) {
            String data = "";
            if (user.errorText != null && !"".equals(user.errorText)) {
                data = String.format("{error:'%1$s'}", user.errorText);
            } else {
                String sql = "select ur.role_name from user_roles ur where ur.user_name=?";
                String roles = (String) Connector.Instance().executeQuery(sql, new Object[]{user.userName}, Connector.Instance().QueryToJsonHandler, Boolean.TRUE);
                data = String.format("{name:'%1$s',locMode:%4$s, id:%2$d,roles:%3$s}", user.userName, user.userId, roles,
                        Connector.Instance().isEmbeddedLoc(true) ? "true" : "false");
            }
            out.write(data.getBytes("UTF-8"));
        }
    }

    protected void process_doLogout(final HttpServletRequest req, final HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
        setUserInfo(req, resp, null);
        req.getSession().invalidate();
    }


    protected void process_doLogin(final HttpServletRequest req, final HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
        final String login = req.getParameter("login");
        final String pwd = req.getParameter("pwd");
        final String host = req.getRemoteHost();
        if (!Connector.Instance().isEmbeddedLoc(true)) {
            Connector.Instance().executeCall("SELECT * FROM users u, user_roles r where u.user_name=r.user_name and r.role_name = ? and u.user_name=? and u.user_pass=?", new Connector.CallHandler() {
                public void onCall(CallableStatement st, Object... params) throws SQLException {
                    st.setString(1, "apsadmin");
                    st.setString(2, login);
                    st.setString(3, pwd);
                    ResultSet rs = st.executeQuery();
                    if(rs.next()){
                        setUserInfo(req, resp, new UserInfo(1, login, host));
                    }
                }
            });
        }
        UserInfo user = getUserInfo(req, resp);
        if (user == null) {
            String data = "{error:'Неправильный пароль или имя пользователя'}";
            out.write(data.getBytes("UTF-8"));
        } else {
            String data = "{name:'" + user.userName + "',id:" + user.userId + "}";
            out.write(data.getBytes("UTF-8"));
        }

    }

}
