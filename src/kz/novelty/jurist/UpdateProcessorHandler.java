package kz.novelty.jurist;

/**
 * User: Malik.N
 */
public interface UpdateProcessorHandler {
    boolean isApplyable(int updType);
    boolean apply(byte[] data, long currentUpdateID, StringBuilder log);
}
