package kz.novelty.jurist;

import net.sf.json.JSONObject;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * User: Malik.N
 */
/**
 * класс для работы с таблицей
 */
public class UpdateEntityHandler {
    /**
     * таблица, с которой работаем
     */
    String tableName;
    /**
     * название ключевого поля
     */
    String keyField;
    /**
     * список полей, которые обновляются
     */
    List<String> fields;
    /**
     * поля типа дата, которые нужно конвертировать в формат базы
     */
    List<String> dateFields;
    /**
     * кешированный update запрос
     */
    String updateSql;
    /**
     * кешированный insert запрос
     */
    String insertSql;
    /**
     * кешированный select запрос на проверку сущестования записи в таблице
     */
    String existSql;
    /**
     * кешированный delete запрос
     */
    String deleteSql;
    /**
     * схема, с которой работаем
     */
    String scheme;

    protected UpdateEntityHandler(String tableName, String keyField, String[] fields) {
        this(tableName, keyField, Arrays.asList(fields));
    }

    protected UpdateEntityHandler(String tableName, String keyField, String[] fields, String[] dateFields) {
        this(tableName, keyField, Arrays.asList(fields), Arrays.asList(dateFields));
    }

    protected UpdateEntityHandler(String tableName, String keyField) {
        this(tableName, keyField, new ArrayList<String>(), new ArrayList<String>());
    }

    protected UpdateEntityHandler(String tableName, String keyField, List<String> fields) {
        this(tableName, keyField, fields, new ArrayList<String>());
    }

    protected UpdateEntityHandler(String tableName, String keyField, List<String> fields, List<String> dateFields) {
        this.tableName = tableName;
        this.keyField = keyField;
        this.fields = fields;
        this.dateFields = dateFields;
        init();
        initFormats();
    }

    /**
     * Для доп действий
     */
    protected void init() {

    }

    /**
     * инициирует внутренние структуры и объекты
     */
    protected void initFormats() {
        StringBuilder formatUpd = new StringBuilder();
        StringBuilder formatIns = new StringBuilder();
        String InsertFormat2 = Connector.Instance().getParamStr(fields.size());
        for (int i = 0; i < fields.size(); i++) {
            String field = fields.get(i);
            if (i > 0) {
                formatUpd.append(",");
                formatIns.append(",");
            }
            formatIns.append(field);
            formatUpd.append(field).append(" = ?");
        }
        this.scheme = Connector.Instance().getDocumentScheme()+".";
        this.updateSql = String.format(" update %2$s set %3$s  where %1$s = ?", keyField, this.scheme + tableName, formatUpd.toString());
        this.insertSql = String.format(" insert into %2$s (%1$s,%3$s) values (?,%4$s)", keyField, this.scheme + tableName, formatIns.toString(), InsertFormat2);
        this.existSql = String.format(" select %1$s from %2$s where %1$s = ?", keyField, this.scheme + tableName);
        this.deleteSql = String.format(" delete from %2$s where %1$s = ?", keyField, this.scheme + tableName);

    }

    /**
     * проверяет существует ли запись в базе
     * @param id
     * @param data
     * @return
     * @throws SQLException
     */
    public boolean exists(long id, JSONObject data) throws SQLException {
        Object[] params = new Object[]{id};
        params = onExistParams(params, data);
        return (Boolean) Connector.Instance().executeQuery(existSql, params, new DBConnector.QueryHandler() {
            public Object onQuery(ResultSet rs, Object... params) throws SQLException {
                if (rs.next()) {
                    return Boolean.TRUE;
                }
                return Boolean.FALSE;
            }
        });
    }

    /**
     * событие перед вызовом запроса exists
     * @param params подготовленне параметры
     * @param data доп объект
     * @return измененные параметры
     */
    private Object[] onExistParams(Object[] params, JSONObject data) {
        return params;
    }

    /**
     * Вызывается для каждого поля указанного в конструкторе перед вставкой или изменением данных в бд
     * @param id первичный ключ
     * @param data объект хранящий данные
     * @param st запрос
     * @param idx индекс поля в списке полей
     * @param field имя поля
     * @throws SQLException
     */
    public void beforeInsertOrUpdate(final Long id, JSONObject data, CallableStatement st, int idx, String field) throws SQLException {
        st.setNull(idx, Types.VARCHAR);

    }

    /**
     * Событие вызывается после добавление или изменения записи в бд
     * @param id первичный ключ
     * @param data объект с исходными данными
     * @param wasNew true если была добавлена запись
     */
    public void afterInsertOrUpdate(final long id, JSONObject data, boolean wasNew) {

    }

    /**
     * конвертирует строку в формат даты базы
     * @param date
     * @return
     */
    public String getDateField(String date) {
        return date;
    }

    /**
     * изменить запись в бд
     * @param id перичный ключ
     * @param data объект с исходными данными
     * @throws SQLException
     */
    public void update(final long id, final JSONObject data) throws SQLException {
        Connector.Instance().executeCall(updateSql, new DBConnector.CallHandler() {
            public void onCall(CallableStatement st, Object... params) throws SQLException {
                int i;
                for (i = 0; i < fields.size(); i++) {
                    String field = fields.get(i);
                    if(data.get(field)==null){
                        beforeInsertOrUpdate(id, data, st, i + 1, field);
                    }else{
                        String value = data.getString(field);
                        if (dateFields.contains(field)) {
                            value = getDateField(value);
                        }
                        st.setString(i + 1, value);
                    }
                }
                st.setLong(i + 1, id);
                st.execute();
            }
        });
        afterInsertOrUpdate(id, data, false);
    }

    /**
     * Добавить запись в бд
     * @param id перичный ключ
     * @param data объект с исходными данными
     * @throws SQLException
     */
    public void insert(final Long id, final JSONObject data) throws SQLException {
        Connector.Instance().executeCall(insertSql, new DBConnector.CallHandler() {
            public void onCall(CallableStatement st, Object... params) throws SQLException {
                if(id==null){
                    st.setNull(1, Types.BIGINT);
                } else{
                    st.setLong(1, id);
                }
                int i;
                for (i = 0; i < fields.size(); i++) {
                    String field = fields.get(i);
                    if(data.get(field)==null){
                        beforeInsertOrUpdate(id, data, st, i + 2, field);
                    }else{
                        String value = data.getString(field);
                        if (dateFields.contains(field)) {
                            value = getDateField(value);
                        }
                        st.setString(i + 2, value);
                    }
                }
                st.execute();

            }
        });
        afterInsertOrUpdate(id, data, true);
    }

    /**
     * удалить запись в бд
     * @param id перичный ключ
     * @throws SQLException
     */
    public void delete(final long id) throws SQLException {
        Connector.Instance().executeCall(deleteSql, new DBConnector.CallHandler() {
            public void onCall(CallableStatement st, Object... params) throws SQLException {
                st.setLong(1, id);
                st.execute();
            }
        });

    }

    /**
     * сохранить доп данные в памяти
     * @param binaryMap массив смещений буфера
     * @param data буфер
     */
    public void setBinaryMap(Map<String, int[]> binaryMap, byte[] data) {

    }
}
