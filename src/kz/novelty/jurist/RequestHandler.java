package kz.novelty.jurist;

import SevenZip.LZMAHelper;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.net.URLEncoder;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPOutputStream;

/**
 * User: developer
 * Date: 13.08.11
 * Time: 14:38
 */
public class RequestHandler extends javax.servlet.http.HttpServlet {
    interface handler {
        void process(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException;
    }

    HashMap<String, handler> handlers = new HashMap<String, handler>();

    public RequestHandler() {
        super();
        handlers.put("DocInfo", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
                process_docInfo(req, resp, out);
            }
        });
        handlers.put("DocLoad", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
                process_docLoad(req, resp, out);
            }
        });
        handlers.put("DocLoadPartial", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
                process_docLoadPartial(req, resp, out);
            }
        });
        handlers.put("DocList", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
                process_docList(req, resp, out);
            }
        });
        handlers.put("DocListRespCorr", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
                process_docListRespCorr(req, resp, out);
            }
        });
        handlers.put("loadAttachment", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
                process_loadAttachment(req, resp, out);
            }
        });
        handlers.put("loadDictionary", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
                process_dictionary(req, resp, out);
            }
        });
        handlers.put("doLogin", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
                process_doLogin(req, resp, out);
            }
        });
        handlers.put("doLogout", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
                process_doLogout(req, resp, out);
            }
        });
        handlers.put("getUserInfo", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
                process_getUserInfo(req, resp, out);
            }
        });
        handlers.put("proc", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
                process_proc(req, resp, out);
            }
        });
        handlers.put("doSendUserRequest", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
                process_doSendUserRequest(req, resp, out);
            }
        });
        handlers.put("setActivationNumber", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
                process_setActivationNumber(req, resp, out);
            }
        });
        handlers.put("applyUpdates", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
                process_applyUpdates(req, resp, out);
            }
        });
        handlers.put("getMonList", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
                process_getMonList(req, resp, out);
            }
        });
        handlers.put("getMonDetail", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
                process_getMonDetail(req, resp, out);
            }
        });
        handlers.put("endUserSession", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
                process_endUserSession(req, resp, out);
            }

        });
        handlers.put("addActionLog", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
                process_addActionLog(req, resp, out);
            }

        });

    }

    /**
     * получает имя схемы где хранятся документы
     *
     * @param req
     * @param resp
     * @return
     * @throws SQLException
     */
    public String getDocumentSchemeName(HttpServletRequest req, HttpServletResponse resp) throws SQLException {
        String schemeName = (String) Connector.Instance().executeQuery("select getDocumentScheme() as schm", new Object[]{}, new Connector.QueryHandler() {
            public Object onQuery(ResultSet rs, Object... params) throws SQLException {
                if (rs.next()) {
                    return rs.getString("schm");
                }
                return "";
            }
        });
        return schemeName;
    }


    public void setUserInfo(HttpServletRequest req, HttpServletResponse resp, UserInfo user) {
        HttpSession session = req.getSession(true);
        Cookie sessionCookie = new Cookie("JSESSIONID", session.getId());
        sessionCookie.setPath("/");
        resp.addCookie(sessionCookie);
        session.setAttribute("user", user);
        String sess = session.getId();
        if (user == null) {
            Connector.Instance().removeUserSessionInfo(sess, "Завершение сеанса");
        } else {
            Connector.Instance().addUserSessionInfo(sess, user);
        }
    }

    /**
     * возвращает информацию о текущем пользователе
     *
     * @param req
     * @param resp
     * @return
     */
    public UserInfo getUserInfo(HttpServletRequest req, HttpServletResponse resp) {
        return  getUserInfo(req, resp, false);
    }

    public UserInfo getUserInfo(HttpServletRequest req, HttpServletResponse resp, boolean instantiateUserError) {
        HttpSession session = req.getSession(true);
        String sess = session.getId();
//        UserInfo user = (UserInfo) session.getAttribute("user");
        UserInfo user = Connector.Instance().getUserSessionInfo(sess);
        if(user == null && instantiateUserError){
            String error = Connector.Instance().getUserSessionError(sess);
            if("".equals(error) || null == error) return null;
            user = new UserInfo(0,"", req.getRemoteHost());
            user.errorText = error;
        }
        return user;
    }

    public void print(OutputStream stream, String s) throws IOException {
        if (s == null) s = "null";
        int len = s.length();
        for (int i = 0; i < len; i++) {
            char c = s.charAt(i);
            if ((c & 65280) != 0) {
                ResourceBundle lStrings = ResourceBundle.getBundle("javax.servlet.LocalStrings");
                String errMsg = lStrings.getString("err.not_iso8859_1");
                Object errArgs[] = new Object[1];
                errArgs[0] = new Character(c);
                errMsg = MessageFormat.format(errMsg, errArgs);
                throw new CharConversionException(errMsg);
            }
            stream.write(c);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String rtype = req.getParameter("requestType");
        handler handler = handlers.get(rtype);
        OutputStream out = resp.getOutputStream();
        OutputStream outOrig = out;
        String encoding = req.getHeader("Accept-Encoding");
        if (encoding != null && encoding.indexOf("gzip") >= 0) {  // gzip browser?
            resp.setHeader("Content-Encoding", "gzip");
            out = new GZIPOutputStream(out);
        }
        UserInfo userInfo = getUserInfo(req, resp);
        Connector con = Connector.Instance();
        boolean emb = userInfo == null && (con.isEmbeddedLoc(true) && con.isEmbedAuth() || (con.isEmbeddedLoc(false) && new UpdateProcessor().hasLicensePermission()));
        if (emb && !Connector.Instance().isEmbeddedNet()) {
            setUserInfo(req, resp, new UserInfo(-1, "USER", req.getRemoteHost()));
        }
        if (handler == null) {
            resp.setStatus(500);
            //resp.sendError(500, " UNKNOWN REQUEST " + rtype);
            out.write((" UNKNOWN REQUEST " + rtype).getBytes());
            out.flush();
            out.close();
        } else try {
            handler.process(req, resp, out);
        } catch (Exception e) {
            e.printStackTrace();
            resp.setStatus(500);
            //resp.sendError(500, e.getMessage());
            out.write(e.getMessage().getBytes());
            out.flush();
            out.close();
        }
        out.flush();
        out.close();
        if (outOrig != out) outOrig.close();
    }

    /*
   * Все сравнения в процедуре идут с учетом регистра символов
   * */
    protected void truncateAndWriteHTML(long iOffset, String data, OutputStream out, long max_size, int size) throws IOException {
        String newData = data.substring(0);
        long count = newData.length();
        if (count == max_size) {
            final String TagCl_P = "</P>";
            final String TagOp_P = "<P ";
            final String TagOp_Table = "<TABLE";
            final String TagCl_Table = "</TABLE>";
            int pos = newData.lastIndexOf(TagOp_Table);// tag open table
            boolean parseEnd = false;
            if (pos >= 0) {
                int endPos = newData.indexOf(TagCl_Table, pos);
                if (endPos == -1 || endPos < pos) { // no tag close table
                    newData = newData.substring(0, pos);
                    endPos = newData.lastIndexOf(TagCl_P);
                    newData = newData.substring(0, endPos + TagCl_P.length());
                    parseEnd = true;
                } else { // has closed table tag
                    int lastEndTag = endPos;
                    endPos = newData.indexOf("<DIV class=sps_comment", lastEndTag);
                    if (endPos == -1) endPos = newData.indexOf("<DIV", lastEndTag);
                    if (endPos == -1) endPos = newData.indexOf(TagOp_P, lastEndTag);
                    if (endPos == -1) {
                        pos = lastEndTag + TagCl_Table.length();
                    } else {
                        pos = endPos;
                    }
                }
            }
            if (!parseEnd) {
                if (pos == -1) pos = 0;
                int endPos = newData.lastIndexOf(TagOp_P);
                if (endPos > pos) { // нашли тег Р
                    int lastEndPos = newData.indexOf(TagCl_P, endPos);
                    if (lastEndPos == -1) { // не нашли закрывающий тег Р
                        pos = endPos;
                    } else {
                        pos = lastEndPos + TagCl_P.length();
                    }
                }
                newData = newData.substring(0, pos);
            }
        }
        print(out, "<!--" + (iOffset + newData.length()) + "|" + size + "-->");
        out.write(newData.getBytes("UTF-8"));
    }

    protected void process_docLoadPartial(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
        String id = req.getParameter("id");
        String offset = req.getParameter("offset");
        if (offset == null || "".equals(offset)) offset = "0";
        long iOffset = Long.parseLong(offset);
        if (iOffset < 0) iOffset = 0;
        long MAX_SIZE = 1048576;//1Mb
        resp.setContentType("text/html; charset=UTF-8");
        String docScheme = getDocumentSchemeName(req, resp);
        String data = "";
        UserInfo user = getUserInfo(req, resp);
        String isDenied = execute_proc("isDocumentDenied", id, user);
        if (!"0".equals(isDenied)) {
            id = isDenied;
        }
        {
            data = (String) Connector.Instance().executeQuery("select body from " + docScheme + ".documents where id=?", new Object[]{/*iOffset + 1,MAX_SIZE,*/id}, new Connector.QueryHandler() {
                public Object onQuery(ResultSet rs, Object... params) throws SQLException {
                    String val = "";
                    if (rs.next()) {
                        try {
                            ByteArrayOutputStream bs = new ByteArrayOutputStream();
                            bs.reset();
                            LZMAHelper.Decompress(rs.getBinaryStream("body"), bs);
                            val = bs.toString("utf-8");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    return val;
                }
            });
            int size = data.length();
            if (data.length() > MAX_SIZE) {
                int cnt = (int) (iOffset + MAX_SIZE);
                if (cnt > data.length()) cnt = data.length();
                data = data.substring((int) iOffset, cnt);
            }
            truncateAndWriteHTML(iOffset, data, out, MAX_SIZE, size);
        }
    }
    private static final String PRINT_HEADER_HEAD  = "<style type=\"text/css\">\n" +
            ".doc-body, .doc-body p {\n" +
            "    text-indent: 20px;\n" +
            "    margin: 0;" +
            "    -webkit-margin-before: 0px;\n" +
            "    -webkit-margin-after: 0px;\n" +
            "    font-size: 12px;\n" +
            "    %background%" +
            "}" +
            ".sps_comment {\n" +
            "    background-color: #D3D3D3;\n" +
            "    -webkit-print-color-adjust: exact; \n" +
            "    %comment_display%" +
            "}" +
            "    @media screen {\n" +
            "        thead.header{\n" +
            "        display:none;\n" +
            "        }\n" +
            "    }\n" +
            "    @media print {\n" +
            "        thead.header td,thead.header th,thead.header tr{\n" +
            "            text-align: left;\n" +
            "            font-size: 13px;\n" +
            "            font-weight: normal;\n" +
            "            font-style: italic;\n" +
            "            color: black;\n" +
            "            margin-right:100px;\n" +
            "        }\n" +
            "       thead.header {\n" +
            "           display: table-header-group;\n" +
            "       }\n" +
            "    }\n" +
            "</style>\n";
    private static final String PRINT_HEADER_BODY  = "<BODY><table border=\"0\" width=\"100%\"><thead class=\"header\"><tr><th style=\"width:100%\">Источник: СПС &laquo;Правовой консультант&raquo; <b>http://docs.wfin.kz</b></th></tr></thead>\n" +
            "<tbody><tr><td width=\"100%\" class='doc-body'>";
    private static final String PRINT_HEADER_BODY_END  = "</td></tr></tbody></table></BODY>";

    protected void process_docLoad(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
        String id = req.getParameter("id");
        String mode = req.getParameter("mode");
        String docScheme = getDocumentSchemeName(req, resp);
        UserInfo user = getUserInfo(req, resp);
        String isDenied = execute_proc("isDocumentDenied", id, user);
        String data = "";
        if (!"0".equals(isDenied)) {
            id = isDenied;
        }
        final boolean modePrint = "0".equals(mode);
        final boolean modeWord = "1".equals(mode);
        final boolean showComment = "1".equals(req.getParameter("comment"));
        if (modeWord) {
            resp.setContentType("application/octet-stream");
            resp.setHeader("Content-Disposition", "attachment; filename=\"Report.doc\"");
        } else {
            resp.setContentType("text/html; charset=UTF-8");
        }
        data = (String) Connector.Instance().executeQuery("select d.body,p.status from " + docScheme + ".documents d," + docScheme + ".properties p where p.id=d.id and d.id=?", new Object[]{id}, new Connector.QueryHandler() {
            public Object onQuery(final ResultSet rs, Object... params) throws SQLException {
                String val = "";
                if (rs.next()) {
                    try {
                        final int status = rs.getInt("status");
                        final ByteArrayOutputStream bs = new ByteArrayOutputStream(){
                            @Override
                            public synchronized void write(byte[] b, int off, int len) {
                                try {
                                    if(modePrint || modeWord){
                                        String tmp = new String(b,off,len,"UTF-8");
                                        int idx = tmp.indexOf("<BODY>");
                                        if( idx > 0){
                                            tmp = tmp.replace("<BODY>", PRINT_HEADER_BODY);
                                        }
                                        idx = tmp.indexOf("</BODY>");
                                        if( idx > 0){
                                            tmp = tmp.replace("</BODY>", PRINT_HEADER_BODY_END);
                                        }
                                        idx = tmp.indexOf("</HEAD>");
                                        if( idx > 0){
                                            String header = PRINT_HEADER_HEAD.replace("%background%", "").replace("%comment_display%", showComment ? "" : "display:none;");
                                            tmp = tmp.replace("</HEAD>", header);
                                        }
                                        out.write(tmp.getBytes("UTF-8"));
                                    }else{
                                            out.write(b, off, len);
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        };
                        LZMAHelper.Decompress(rs.getBinaryStream("body"), bs);
                        //val = bs.toString("utf-8");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return val;
            }
        });
        //out.write(data.getBytes("UTF-8"));
    }

    protected void process_loadAttachment(final HttpServletRequest req, final HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
        String id = req.getParameter("id");
        final String userAgent = req.getHeader("USER-AGENT").toLowerCase();
        String docScheme = getDocumentSchemeName(req, resp);
        Object r = Connector.Instance().executeQuery("select ext,name,data from " + docScheme + ".attachments where id=?", new Object[]{id}, new Connector.QueryHandler() {
            public Object onQuery(ResultSet rs, Object... params1) throws SQLException {
                if (rs.next()) {
                    String name = rs.getString("name");
                    String ext = "." + rs.getString("ext");
                    if (name.endsWith(ext)) {
                        ext = "";
                    }
                    InputStream data = rs.getBinaryStream("data");
                    if (data != null) {
                        if (".html".equals(ext)) {
                            resp.setContentType("text/html");
                        } else if (".pdf".equals(ext)) {
                            resp.setContentType("application/pdf");
                        } else if (".xls".equals(ext)) {
                            resp.setContentType("application/xls");
                        } else if (".rtf".equals(ext)) {
                            resp.setContentType("application/rtf");
                        } else if (".doc".equals(ext)) {
                            resp.setContentType("application/msword");
                        } else {
                            resp.setContentType("application/octet-stream");
                        }
                        try {
                            String fileName = URLEncoder.encode(name + ext, "UTF-8");
                            if (userAgent != null && (userAgent.contains("chrome") || userAgent.contains("msie"))) {
                                resp.setHeader("Content-Disposition", "attachment; filename=" + fileName);
                            } else {
                                resp.setHeader("Content-Disposition", "attachment; filename*=utf-8''" + fileName);
                            }
                        } catch (UnsupportedEncodingException e) {
                            resp.setHeader("Content-Disposition", "attachment; filename=" + name + ext);
                        }
                        try {
                            LZMAHelper.Decompress(data, out);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    return 1;
                }
                return null;
            }
        });
        if (r == null) throw new ServletException("Файл не найден");
    }

    protected void process_docInfo(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
        String id = req.getParameter("id");
        try {
            StringBuilder struct = new StringBuilder();
            String docScheme = getDocumentSchemeName(req, resp);
            String data = (String) Connector.Instance().executeQuery("select p.*  from " + docScheme + ".properties p where p.id=?", new Object[]{id}, Connector.Instance().QueryToJsonHandler, Boolean.TRUE, struct);
            if ("[]".equals(data)) {
                data = (String) Connector.Instance().executeQuery("select name, ext from " + docScheme + ".attachments p where p.id=?", new Object[]{id}, Connector.Instance().QueryToJsonHandler, Boolean.TRUE, struct);
            }
            resp.setContentType("text/json; charset=UTF-8");
            print(out, "(");
            out.write(data.getBytes("UTF-8"));
            print(out, ")");
        } catch (SQLException e) {
            e.printStackTrace();
            throw new ServletException(e);
        }
    }

    protected void process_dictionary(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
        String name = req.getParameter("TableName");
        String[] allowedDicts = new String[]{"trees", "classificators"};
        boolean found = false;
        for (int i = 0, l = allowedDicts.length; i < l; i++) {
            found = found || name.equals(allowedDicts[i]);
            if (found) break;
        }
        if (!found) {
            throw new ServletException(name + " не найден в списке справочников");
        }
        String grantedTrees = "";
        StringBuilder struct = new StringBuilder();
        if ("trees".equals(name)) {
            UserInfo user = getUserInfo(req, resp);
            grantedTrees = (String) Connector.Instance().executeQuery("select getGrantedTreeList(?) as ids ", new Object[]{user == null ? 0 : user.userId}, new Connector.QueryHandler() {
                public Object onQuery(ResultSet rs, Object... params) throws SQLException {
                    if (rs.next()) return rs.getString(1);
                    return "0";
                }
            });
        }
        String docScheme = getDocumentSchemeName(req, resp);
        String data = (String) Connector.Instance().executeQuery("select * from " + docScheme + "." + name + " p ", new Object[]{}, Connector.Instance().QueryToJsonHandler, Boolean.FALSE, struct);
        resp.setContentType("text/json; charset=UTF-8");
        print(out, "{ 'metaData': {'root':'rows', 'fields':");
        print(out, struct.toString());
        print(out, "}, 'rows':");
        out.write(data.getBytes("UTF-8"));
        print(out, ", 'highlight':'");
        out.write(grantedTrees.getBytes("UTF-8"));
        print(out, "'}");
    }

    protected void process_docListRespCorr(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
        String doc_id = req.getParameter("id");
        String docScheme = getDocumentSchemeName(req, resp);
        String data = (String) Connector.Instance().executeQuery(String.format("select * from (\n" +
                "    select t.resp_id as id, t.*, p.fullName from %1$sproperties p,%1$srespondent_correspondent t \n" +
                "    where t.resp_id=p.id and corr_id = ? order by p.fullName ) a\n" +
                "union \n" +
                "select * from (\n" +
                "    select t.corr_id as id,t.*, p.fullName from %1$sproperties p,%1$srespondent_correspondent t \n" +
                "    where t.corr_id=p.id and resp_id = ? order by p.fullName \n" +
                ") b", docScheme + "."),
                new Object[]{doc_id, doc_id}, Connector.Instance().QueryToJsonHandler, Boolean.TRUE);
        resp.setContentType("text/json; charset=UTF-8");
        print(out, "{ 'docs':");
        out.write(data.getBytes("UTF-8"));
        print(out, "}");

    }

    protected void process_docList(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
        String offset = req.getParameter("offset");
        String rowcount = req.getParameter("count");
        String tree_id = req.getParameter("tree_id");
        String sortMode = req.getParameter("sortMode");
        String mode = req.getParameter("mode");
        String incSearchStr = req.getParameter("incSearchStr");
        sortMode = sortMode.replace("(", "").replace(")", "").replace("--", "");
        int rcount = "".equals(rowcount) ? 1000 : Integer.parseInt(rowcount);
        int roffset = "".equals(offset) ? 0 : Integer.parseInt(offset);
        String searchName = "and (LOWER(p.fullname) like ? or LOWER(p.shortname) like ? )";
        if ("" == incSearchStr || incSearchStr == null) incSearchStr = "%";
        incSearchStr = incSearchStr.toLowerCase();
        if ("%".equals(incSearchStr)) {
            searchName = " and ? = '%' and ? = '%' ";
        } else {
            incSearchStr = "%" + incSearchStr;
        }
        if (mode == null || "".equals(mode)) {
            mode = "browse";
        }
        StringBuilder struct = new StringBuilder();
        UserInfo user = getUserInfo(req, resp);
        String docScheme = getDocumentSchemeName(req, resp);
        String q1 = "";
        Object[] q1par = null;
        String sql = "";
        Object[] sqlPar = null;
        if ("bookmark".equals(mode)) {
            long user_id = 0;
            if (user != null) user_id = user.userId;
            q1 = String.format("select count(*) from %1$sproperties p,bookmark_tree t,bookmark_docs t2 where p.isDocument=1 and t.user_id=? and t2.tree_id=t.id and t2.doc_id=p.id and t2.tree_id=? %2$s", docScheme + ".", searchName);
            q1par = new Object[]{user_id, tree_id, incSearchStr, incSearchStr};
            sql = String.format("select p.* from %1$sproperties p,bookmark_tree t,bookmark_docs t2 where p.isDocument=1 and t.user_id=? and t2.tree_id=t.id and t2.doc_id=p.id and t2.tree_id=? %2$s order by %3$s limit ? offset ? "
                    , docScheme + ".", searchName, sortMode);
            sqlPar = new Object[]{user_id, tree_id, incSearchStr, incSearchStr, rcount, roffset};
        } else {
            q1 = String.format("select count(*) from %1$sproperties p,%1$stree_document t where p.isDocument=1 and t.doc_id=p.id and t.tree_id=? %2$s", docScheme + ".", searchName);
            q1par = new Object[]{tree_id, incSearchStr, incSearchStr};
            sql = String.format("select p.* from %1$sproperties p,%1$stree_document t where p.isDocument=1 and t.doc_id=p.id and t.tree_id=? %2$s order by %3$s limit ? offset ? "
                    , docScheme + ".", searchName, sortMode);
            sqlPar = new Object[]{tree_id, incSearchStr, incSearchStr, rcount, roffset};
        }
        long tcount = (Long) Connector.Instance().executeQuery(q1, q1par, new Connector.QueryHandler() {
            public Object onQuery(ResultSet rs, Object... params) throws SQLException {
                if (rs.next()) return rs.getLong(1);
                return 0;
            }
        }, Boolean.FALSE, struct);
        String data = (String) Connector.Instance().executeQuery(sql, sqlPar, Connector.Instance().QueryToJsonHandler, Boolean.TRUE, struct);
        resp.setContentType("text/json; charset=UTF-8");
        print(out, String.format("{ mode:'%4$s', facet_counts:{facet_dates:{},facet_fields:{},facet_queries:{},facet_ranges:{}},responseHeader:{params:{rows:%d,start:%d,sort:'%s'}}, 'response': {'start':'%2$d', 'numFound':",
                rcount, roffset, sortMode, mode));
        print(out, "" + tcount);
        print(out, ", 'docs':");
        out.write(data.getBytes("UTF-8"));
        print(out, "}}");
    }

    protected void process_getUserInfo(final HttpServletRequest req, final HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
        UserInfo user = getUserInfo(req, resp);
        Connector conn = Connector.Instance();
        String embMode = conn.isEmbeddedLoc(true) ? "true" : "false";
        String error = user != null ? user.errorText : Connector.Instance().getUserSessionError(req.getSession().getId());
        String data = "";
        if(error != null && !"".equals(error)){
            data = String.format("{error:'%1$s',netMode:%2$s}", error,conn.isEmbeddedNet() ? "true" : "false");
        }else if (user != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
            String sql = "select r.role_id , r.role from user_roles ur, roles r where ur.role_id=r.role_id and ur.user_id=?";
            String roles = (String) conn.executeQuery(sql, new Object[]{user.userId}, conn.QueryToJsonHandler, Boolean.TRUE);
            Date validTill = null;
            if(conn.isEmbeddedLoc(false)){
                validTill = new UpdateProcessor().getLicenseEndDate(Connector.Instance().getLastLicense());
            }else{
                validTill = (Date) conn.executeQuery("select valid_till from users where user_id=?", new Object[]{user.userId}, new DBConnector.QueryHandler() {
                    public Object onQuery(ResultSet rs, Object... params) throws SQLException {
                        if(rs.next()){
                            return rs.getDate("valid_till");
                        }
                        return null;
                    }
                });
            }
            data = String.format("{name:'%1$s', locMode:%4$s, locMode1:%6$s, netMode:%5$s, id:%2$d, roles:%3$s, valid_till:'%7$s'}",
                    user.userName,
                    user.userId,
                    roles,
                    embMode,
                    conn.isEmbeddedNet() ? "true" : "false",
                    conn.isEmbeddedLoc(false) ? "true" : "false",
                    validTill==null ? "" : dateFormat.format(validTill));
        } else {
            boolean localMode = conn.isEmbeddedLoc(false);
            embMode = localMode ? "true" : "false";
            UpdateProcessor upd = new UpdateProcessor();
            boolean activated = localMode && upd.hasLicensePermission();
            String outFormat = "{locMode:%s, activated:%s}";
            if (localMode) {
                String serial = upd.getSystemUUID();
                outFormat = outFormat.replace("}", ",serial:'" + serial + "'}");

            }
            data = String.format(outFormat, embMode, activated ? "true" : "false");
        }
        out.write(data.getBytes("UTF-8"));
    }

    protected void process_doLogout(final HttpServletRequest req, final HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
        setUserInfo(req, resp, null);
        req.getSession().invalidate();
    }

    private String execute_proc(final String proc, final String par, final UserInfo user) throws SQLException {
        final StringBuilder res = new StringBuilder();
        Connector.Instance().executeCall("call execute_proc(?,?,?,?)", new Connector.CallHandler() {
            public void onCall(CallableStatement st, Object... params) throws SQLException {
                st.setLong(1, user != null ? user.userId : 0);
                st.setString(2, proc);
                st.setString(3, par);
                st.registerOutParameter(4, Types.LONGVARCHAR);
                ResultSet rs = st.executeQuery();
                //st.executeUpdate();
                String result = st.getString(4);
                res.append(result);
            }
        });
        if ("getDefaultPage".equals(proc)) {
            String docScheme = getDocumentSchemeName(null, null);
            String data = "";
            data = (String) Connector.Instance().executeQuery("select body from " + docScheme + ".documents where id=?", new Object[]{res.toString()}, new Connector.QueryHandler() {
                public Object onQuery(ResultSet rs, Object... params) throws SQLException {
                    String val = "";
                    if (rs.next()) {
                        try {
                            ByteArrayOutputStream bs = new ByteArrayOutputStream();
                            LZMAHelper.Decompress(rs.getBinaryStream("body"), bs);
                            val = bs.toString("utf-8");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    return val;
                }
            });
            res.delete(0, res.length());
            res.append(data);
        }
        return res.toString();
    }

    protected void process_proc(final HttpServletRequest req, final HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
        final String proc = req.getParameter("proc");
        final String par = req.getParameter("par");
        final UserInfo user = getUserInfo(req, resp);
        String result = execute_proc(proc, par, user);
        // для списка пользователей проставим признак активности
        if ("getUsers".equals(proc)) {
            Matcher matcher = Pattern.compile("<l>(.*?)</l>").matcher(result);
            HashSet<String> users = new HashSet<String>();
            // список пользователей из результата функции
            while (matcher.find()) {
                String login = matcher.group(1);
                users.add(login);
            }
            // ищем среди этих пользователей активные сессии
            for (Iterator<UserInfo> sessions = Connector.Instance().getUserSessionList().iterator(); sessions.hasNext(); ) {
                UserInfo sess = sessions.next();
                if (sess != null && users.contains(sess.userName)) {
                    result = result.replace(String.format("<l>%s</l>", sess.userName), String.format("<l>%s</l><ls>1</ls><lsess>%s</lsess>", sess.userName, sess.getSession()));
                }
            }
        }
        out.write(result.getBytes("UTF-8"));
    }

    protected void process_applyUpdates(final HttpServletRequest req, final HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
        String type = req.getParameter("t");
        new UpdateProcessor().applyUpdates();
        // инкрементальная индексация
        if(Connector.Instance().isEmbedded()){
            Connector.Instance().showTrayMessage("inc_ind");
        }else{
            String url = req.getRequestURL().toString().replaceAll("RequestHandler", "dataimport?command=delta-import");
            byte[] res = URLRequester.executeRequestAsByte(url, null, false);
        }
        if ("exit".equals(type)) {
            Connector.Instance().showTrayMessage("exit");
        }
    }

    protected void process_setActivationNumber(final HttpServletRequest req, final HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
        final String id = req.getParameter("id");
        new UpdateProcessor().setActivationNumber(id);
    }

    protected void process_doSendUserRequest(final HttpServletRequest req, final HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
        final String html = req.getParameter("html");
        final UserInfo user = getUserInfo(req, resp);
        boolean mailSend = Connector.Instance().sendMail("sale@wfin.kz", "Заявка на подписку из СПС «Правовой консультант on-line»", html);
        String res = "";
        if (mailSend) {
            res = "Заявка отправлена";
        } else {
            res = "Произошла ошибка при отправке заявки. Попробуйте позже.";
        }
        out.write(res.getBytes("UTF-8"));
    }

    protected void process_doLogin(final HttpServletRequest req, final HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
        final String login = req.getParameter("login");
        final String pwd = req.getParameter("pwd");
        final String host = req.getRemoteHost();
        // если локальная версия то проверим лицензию
        if (!Connector.Instance().isEmbeddedLoc(true) || Connector.Instance().isEmbeddedNet() && (new UpdateProcessor().hasLicensePermission())) {
            Connector.Instance().executeCall("call doLogin(?,?,?)", new Connector.CallHandler() {
                public void onCall(CallableStatement st, Object... params) throws SQLException {
                    st.registerOutParameter(3, Types.NUMERIC);
                    st.setString(1, login);
                    st.setString(2, pwd);
                    ResultSet rs = st.executeQuery();
                    //st.executeUpdate();
                    long userId = st.getLong(3);
                    if (userId > 0) {
                        setUserInfo(req, resp, new UserInfo(userId, login, host));
                    }
                }
            });
        }
        UserInfo user = getUserInfo(req, resp);
        if (user == null) {
            String data = "{error:'Неправильный пароль или имя пользователя или срок лицензии истек'}";
            //resp.sendError(500, "Неправильный пароль или имя пользователя");
            out.write(data.getBytes("UTF-8"));
        } else {
            String data = "{name:'" + user.userName + "',id:" + user.userId + "}";
            out.write(data.getBytes("UTF-8"));
        }

    }

    private void process_getMonList(HttpServletRequest req, HttpServletResponse resp, OutputStream out) throws SQLException, IOException {
        resp.setContentType("text/plain; charset=UTF-8");
        String sql = "select \n" +
                "    l.update_id,\n" +
                "    l.install_date,\n" +
                "    (select count(*) from update_stat s where status = 1 and  s.update_id = l.update_id) as cnt_old,\n" +
                "    (select count(*) from update_stat s where status = 2 and  s.update_id = l.update_id) as cnt_early,\n" +
                "    (select count(*) from update_stat s where status = 3 and  s.update_id = l.update_id) as cnt_new,\n" +
                "    (select count(*) from update_stat s where status = 4 and  s.update_id = l.update_id) as cnt_edited\n" +
                "from\n" +
                "    update_log l\n" +
                "    order by install_date desc";
        String res = (String) Connector.Instance().executeQuery(sql, new Object[0], Connector.Instance().QueryToJsonHandler);
        print(out, res);
    }

    private void process_getMonDetail(HttpServletRequest req, HttpServletResponse resp, OutputStream out) throws SQLException, IOException {
        resp.setContentType("text/plain; charset=UTF-8");
        String id = req.getParameter("id");
        String scheme = Connector.Instance().getDocumentScheme();
        String sql = "select doc_id, \n" +
                "       s.status, \n" +
                "       p.fullName\n" +
                "  from update_stat s, \n" +
                "       "+scheme+".properties p\n" +
                " where s.doc_id = p.id\n" +
                "   and update_id = ?\n" +
                "order by s.status";
        String res = (String) Connector.Instance().executeQuery(sql, new Object[]{id}, Connector.Instance().QueryToJsonHandler);
        out.write(res.getBytes("UTF-8"));
    }

    private void process_endUserSession(HttpServletRequest req, HttpServletResponse resp, OutputStream out) {
        resp.setContentType("text/plain; charset=UTF-8");
        String id = req.getParameter("id");
        String msg = "Ваша сессия закрыта администратором.";
        Connector.Instance().removeUserSessionInfo(id, msg);
        Connector.Instance().setUserSessionError(id, msg);
    }

    private void process_addActionLog(HttpServletRequest req, HttpServletResponse resp, OutputStream out) {
        resp.setContentType("text/plain");
        String type = req.getParameter("type");
        String msg = req.getParameter("msg");
        Connector connector =Connector.Instance();
        connector.addLogMessage(type, getUserInfo(req, resp), msg);
    }

}
