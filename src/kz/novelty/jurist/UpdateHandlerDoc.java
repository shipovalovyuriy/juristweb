package kz.novelty.jurist;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.CallableStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static kz.novelty.jurist.DBConnector.*;

/**
 * User: Malik.N
 */
public class UpdateHandlerDoc implements UpdateProcessorHandler {
    Map<String, UpdateEntityHandler> entityHandlers = new HashMap<String, UpdateEntityHandler>();

    StringBuilder log;
    private long currentUpdateID;

    public UpdateHandlerDoc() {
        // регистрируем обработчики
        // обработчик типов классификатора
        entityHandlers.put("classificators", new UpdateEntityHandler("classificators", "id", new String[]{"name", "type"}));
        // обработчик дерева классификатора
        entityHandlers.put("trees", new UpdateEntityHandler("trees", "id", new String[]{"class_id", "parent_id", "name", "sort_index"}));
        // обработчик словарей терминов
        entityHandlers.put("terms", new UpdateEntityHandler("terms", "id", new String[]{"word", "definition"}));
        // тоже слова и определения. сноски к документу
        entityHandlers.put("definitions", new UpdateEntityHandler("terms", "id", new String[]{"word"}) {
            @Override
            public void afterInsertOrUpdate(final long id, JSONObject data, boolean wasNew) {
                final String sql = String.format("insert into %sdefinition_document(wordId,docId,parId)values(?,?,?)", Connector.Instance().getDocumentScheme() + ".");
                try {
                final JSONArray arr = data.getJSONArray("doc_paragraph");
                Connector.Instance().executeBatch(new DBConnector.BatchCallHandler() {
                    @Override
                    public void OnBatch() throws SQLException {
                        for (Object anArr : arr) {
                            final JSONObject object = (JSONObject) anArr;
                                executeCall(sql, new CallHandler() {
                                    public void onCall(CallableStatement st, Object... params) throws SQLException {
                                        st.setLong(1, id);
                                        st.setLong(2, object.getLong("id"));
                                        st.setLong(3, object.getLong("name"));
                                        st.execute();
                                    }
                                });
                        }

                    }
                });
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
        /**
         *сохраняет в таблицу вложения, которые встречаются в документах
         */
        entityHandlers.put("attachments", new UpdateEntityHandler("attachments", "id", new String[]{"size", "name", "description", "MIMEtype", "ext", "dateCreated", "md5", "compression", "data"}) {
            public Map<String, int[]> binaryMap;
            public byte[] data;

            @Override
            public void setBinaryMap(Map<String, int[]> binaryMap, byte[] data) {
                super.setBinaryMap(binaryMap, data);
                this.binaryMap = binaryMap;
                this.data = data;
            }

            @Override
            public void beforeInsertOrUpdate(Long id, JSONObject data, CallableStatement st, int idx, String field) throws SQLException {
                if ("data".equals(field)) {
                    int[] off = binaryMap.get("" + id);
                    ByteInStream is = new ByteInStream(this.data, off[0], off[1]);
                    st.setBinaryStream(idx, is);
                } else {
                    super.beforeInsertOrUpdate(id, data, st, idx, field);
                }
            }
        });
        /**
         * Object that holds variable
         */
        abstract class CallIDhandler implements CallHandler {
            public long id;
            public JSONObject obj;
            public String tmpVal;

            public void setId(long id) {
                this.id = id;
            }

            public void setObj(JSONObject obj) {
                this.obj = obj;
            }

            public void setTmpVal(String tmpVal) {
                this.tmpVal = tmpVal;
            }
        }

        /**
         * Object that holds variable
         */
        abstract class CallIDBatch extends BatchCallHandler {
            public long id;
            public JSONObject obj;
            public String tmpVal;

            public void setId(long id) {
                this.id = id;
            }

            public void setObj(JSONObject obj) {
                this.obj = obj;
            }

            public void setTmpVal(String tmpVal) {
                this.tmpVal = tmpVal;
            }
        }

        UpdateEntityHandler docHandler = new UpdateEntityHandler("properties", "id", new String[]{"fullName", "shortName", "publishDate", "regDate", "regNumber", "comment", "priority", "isDocument", "isActive", "lastModified", "draft", "status", "price", "propsMD5", "bodyMD5", "treesMD5", "codesMD5"}, new String[]{"publishDate", "regDate", "lastModified"}) {
            public byte[] data;
            public Map<String, int[]> binaryMap;

            @Override
            public void setBinaryMap(Map<String, int[]> binaryMap, byte[] data) {
                this.binaryMap = binaryMap;
                this.data = data;
            }

            CallIDhandler binIns = new CallIDhandler() {
                public void onCall(CallableStatement st, Object... params) throws SQLException {
                    st.setLong(1, id);
                    st.execute();
                }
            };

            CallIDhandler binUpd = new CallIDhandler() {
                public void onCall(CallableStatement st, Object... params) throws SQLException {
                    int[] off = binaryMap.get("" + id);
                    ByteInStream is = new ByteInStream(data, off[0], off[1]);
                    st.setBinaryStream(1, is);
                    st.setString(2, obj.getString("compression"));
                    st.setLong(3, id);
                    st.execute();

                }
            };
            CallIDhandler delId = new CallIDhandler() {
                public void onCall(CallableStatement st, Object... params) throws SQLException {
                    st.setLong(1, id);
                    st.execute();
                }
            };
            CallIDhandler hCodes = new CallIDhandler() {
                public void onCall(CallableStatement st, Object... params) throws SQLException {
                    st.setLong(1, id);
                    st.setString(2, tmpVal);
                    st.execute();
                }
            };

            public JSONArray getArray(JSONObject obj, String key) {
                JSONArray arr = obj.optJSONArray(key);
                if (arr == null) {
                    arr = new JSONArray();
                    Object val=obj.opt(key);
                    if(val != null){
                        arr.add(obj.get(key));
                    }
                }
                return arr;
            }
            String scheme = Connector.Instance().getDocumentScheme()+".";
            CallIDBatch batch = new CallIDBatch() {
                @Override
                public void OnBatch() throws SQLException {
                    Integer view = obj.optInt("stat_view");
                    Integer status = obj.getInt("status");
                    if (binaryMap.get("" + id) != null) {
                        binIns.setId(id);
                        binIns.setObj(obj);
                        binUpd.setId(id);
                        binUpd.setObj(obj);
                        //Connector.Instance().executeCallIgnoreException(String.format("insert into %sdocuments(id)values(?)", scheme), binIns);
                        executeCallIgnoreException(String.format("replace into %sdocuments (body,compression,id) values(?, ?, ?)", scheme), binUpd);
                    }
                    delId.setId(id);
                    executeCallIgnoreException(String.format("delete from %scodes where docid = ?", scheme), delId);
                    executeCallIgnoreException(String.format("delete from %srespondent_correspondent where corr_id = ?", scheme), delId);
                    executeCallIgnoreException(String.format("delete from %srespondent_correspondent where resp_id = ?", scheme), delId);
                    executeCallIgnoreException(String.format("delete from %stree_document where doc_id = ?", scheme), delId);

                    JSONArray codes = getArray(obj, "codes");
                    hCodes.setId(id);
                    for (Object code1 : codes) {
                        String code = (String) code1;
                        hCodes.setTmpVal(code);
                        executeCallIgnoreException(String.format("INSERT INTO %scodes(docId,code)values(?,?)", scheme), hCodes);
                    }
                    JSONArray respondents = getArray(obj, "respondents");
                    for (Object respondent1 : respondents) {
                        String respondent = (String) respondent1;
                        hCodes.setTmpVal(respondent);
                        executeCallIgnoreException(String.format("replace INTO %srespondent_correspondent(corr_id,resp_id)values(?,?)", scheme), hCodes);
                    }
                    JSONArray correspondents = getArray(obj, "correspondents");
                    for (Object correspondent : correspondents) {
                        String respondent = (String) correspondent;
                        hCodes.setTmpVal(respondent);
                        executeCallIgnoreException(String.format("replace INTO %srespondent_correspondent(resp_id,corr_id)values(?,?)", scheme), hCodes);
                    }
                    JSONArray trees;
                    trees = getArray(obj, "classificators");
                    for (Object tree1 : trees) {
                        String tree = (String) tree1;
                        hCodes.setTmpVal(tree);
                        executeCallIgnoreException(String.format("replace INTO %stree_document(doc_id,tree_id)values(?,?)", scheme), hCodes);
                    }
                    boolean wasNew = "1".equals(tmpVal);
                    if (status == 0) {
                        // 1 утративший силу
                        // 2 не введенный в действие
                        // 3 Новый действующий
                        // 4 Измененный действующий
                        status = wasNew ? 3 : 4;
                    }
                    if(Integer.valueOf(1).equals(view)){
                        final Integer finalStatus = status;
                        executeCallIgnoreException("replace INTO update_stat (update_id, doc_id, status) VALUES (?, ?, ?)", new CallHandler() {
                            public void onCall(CallableStatement st, Object... params) throws SQLException {
                                st.setLong(1, currentUpdateID);
                                st.setLong(2, id);
                                st.setLong(3, finalStatus);
                                st.execute();
                            }
                        });
                    }

                }
            };
            @Override
            public void afterInsertOrUpdate(final long id, final JSONObject obj, boolean wasNew) {
                batch.setObj(obj);
                batch.setId(id);
                batch.setTmpVal(wasNew ? "1" : "0");
                Connector.Instance().executeBatchIgnoreException(batch);
            }
        };

        entityHandlers.put("documents", docHandler);
        entityHandlers.put("doc_params", docHandler);
    }

    /**
     * проверяет можем ли обработать обновление
     * @param updType тип пакета обновления
     * @return true если покет может быть обработан
     */
    public boolean isApplyable(int updType) {
        return 4 == updType;
    }

    /**
     * обработка пакета
     * @param buff сам пакет обновления
     * @param currentUpdateID номер пакета обновления
     * @param log журнал обработки
     * @return
     */
    public boolean apply(byte[] buff, long currentUpdateID, StringBuilder log) {
        this.log = log;
        this.currentUpdateID = currentUpdateID;
        try {
            byte[] data = LZMAHelper.decompress(buff);
            ByteArrayInputStream is = new ByteArrayInputStream(data);
            DataInputStream dis = new DataInputStream(is);
            int jsonSize = dis.readInt();
            JSONObject json = readJson(4, jsonSize, data);
            dis.reset();
            dis.skipBytes(4 + jsonSize);
            int binarySize = dis.readInt();
            Map<String, Map<String, int[]>> binaryMap = readBinaryData(8 + jsonSize, binarySize, data);
            return processJson(json, binaryMap, data);
        } catch (Exception e) {
            e.printStackTrace();
            appendLogAndTray(e.getMessage());
        }

        return false;
    }

    /**
     *легковесный класс для быстрой работы с буфером
     */
    private class ByteInStream extends ByteArrayInputStream {
        private ByteInStream(byte[] buf, int offset, int length) {
            super(buf, offset, length);
        }

        public int getPos() {
            return pos;
        }

        public byte[] getBuf() {
            return buf;
        }
    }

    /**
     * Соберем информацию о смещеннях данных в буфере
     * @param offset смещение откуда читать данные
     * @param binarySize количество байт для чтение
     * @param data буфер данных
     * @return массив смещений
     * @throws IOException
     */
    private Map<String, Map<String, int[]>> readBinaryData(int offset, int binarySize, byte[] data) throws IOException {
        Map<String, Map<String, int[]>> binaryMap = new HashMap<String, Map<String, int[]>>();
        ByteInStream is = new ByteInStream(data, offset, binarySize);
        DataInputStream dis = new DataInputStream(is);
        while (is.available() > 0) {
            int nameSize = dis.readInt();
            String name = new String(is.getBuf(), is.getPos(), nameSize);
            Map<String, int[]> map = new HashMap<String, int[]>();
            binaryMap.put(name, map);
            long n = is.skip(nameSize);
            int jsonSize = dis.readInt();
            String json = new String(is.getBuf(), is.getPos(), jsonSize);
            n = is.skip(jsonSize);
            int streamSize = dis.readInt();
            JSONArray jsonObject = (JSONArray) JSONSerializer.toJSON(json);
            for (int i = 0; i < jsonObject.size(); i++) {
                JSONObject object = (JSONObject) jsonObject.get(i);
                int size = object.getInt("s");
                String id = object.getString("i");
                int pos = is.getPos();
                n = is.skip(size);
                map.put(id, new int[]{pos, size});
            }
            // просто так
            if( streamSize != 0 || n!=0) {
                n = 0;
                streamSize = (int) n;
            }
            is.skip(streamSize);
        }
        return binaryMap;
    }

    /**
     * сериализация в JSON из строки
     * @param offset смещение в массиве байт
     * @param jsonSize размер строки
     * @param data строка для десериализации
     * @return JSON объект c данными
     * @throws UnsupportedEncodingException
     */
    private JSONObject readJson(int offset, int jsonSize, byte[] data) throws UnsupportedEncodingException {
        String json = new String(data, offset, jsonSize, "utf-8");
        return (JSONObject) JSONSerializer.toJSON(json);
    }

    /**
     * показать сообщение в трей если поддерживается
     * @param msg сообщение для добавления в журнал
     */
    private void appendLogAndTray(String msg) {
        if (log != null) log.append("\n").append(msg);
        Connector.Instance().showTrayMessage(msg);
        // wait message display
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * обработка ссответсвующего сущностей и сохрание их  базе
     * @param arr массив сущностей
     * @param handler обработчик сущностей
     * @param binaryMap массив смещений для чтения вложений
     * @param data данные для чтения вложений
     * @return true если обработано
     * @throws SQLException
     */
    private boolean processEntities(JSONArray arr, UpdateEntityHandler handler, Map<String, int[]> binaryMap, byte[] data) throws SQLException {
        handler.setBinaryMap(binaryMap, data);
        int cnt = arr.size();
        for (int i = 0; i < cnt; i++) {
            Object anArr = arr.get(i);
            JSONObject object = (JSONObject) anArr;
            String operation = object.getString("operation");
            Long id = object.getLong("id");
            if (operation.contains("2")) {
                handler.delete(id);
            } else {
                if (handler.exists(id, object)) {
                    handler.update(id, object);
                } else {
                    handler.insert(id, object);
                }
            }
            System.out.println(String.format(handler.tableName + " documents done %d of %d", i, cnt));
        }
        return true;
    }

    /**
     * обработать пакет обновления
     * @param json объект с данными
     * @param binaryMap информация о вложениях
     * @param data информация о вложениях
     * @return
     */
    private boolean processJson(JSONObject json, Map<String, Map<String, int[]>> binaryMap, byte[] data) {
        java.util.Iterator keys = json.keys();
        while (keys.hasNext()) {
            String key = (String) keys.next();
            UpdateEntityHandler handler = entityHandlers.get(key);
            if (handler != null && (json.get(key) instanceof JSONArray)) {
                try {
                    processEntities(json.getJSONArray(key), handler, binaryMap.get(key), data);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return true;
    }


}
