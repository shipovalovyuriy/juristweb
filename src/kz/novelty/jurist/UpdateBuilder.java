package kz.novelty.jurist;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import javax.servlet.ServletException;
import java.io.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: Malik.N
 * Date: 09.03.13
 * Time: 16:08
 */
public class UpdateBuilder extends BaseUpdateBuilder{
    DesUtil des = new DesUtil();
    CertUtil certUtil = new CertUtil();

    @Override
    public BaseDesUtil getDes() {
        return des;
    }

    @Override
    protected BaseCertUtil getCertUtil() {
        return certUtil;
    }

    public byte[] build(String from, String till, String ids, String visible_ids) throws Exception {
        byte[] data;
        final Map<String, BuildTablehandler> handlers = new HashMap<String, BuildTablehandler>();
        final List<String> visibleList = Arrays.asList(visible_ids.split(","));

        BuildTablehandler handlerClass = new BuildTablehandler() {
            Connector.QueryHandler queryHandler = new Connector.QueryHandler() {
                public Object onQuery(ResultSet rs, Object... params) throws SQLException {
                    while (rs.next()) {
                        JSONObject jtemp = new JSONObject();
                        jtemp.put("operation", 1);
                        jtemp.put("id", rs.getLong(1));
                        jtemp.put("name", rs.getString(2));
                        jtemp.put("type", rs.getString(3));
                        res.add(jtemp);
                    }
                    return 1;
                }
            };
            public void process(final String table, long id, String operation) throws ServletException, IOException, SQLException {
                Connector.Instance().executeQuery("select id, name, type from classificators where id=?",
                        new Object[]{id}, queryHandler);
            }
        };
        handlers.put("classificators", handlerClass);

        handlers.put("attachments", new BuildTablehandler() {
            Connector.QueryHandler queryHandler = new Connector.QueryHandler() {
                public Object onQuery(ResultSet rs, Object... params) throws SQLException {
                    while (rs.next()) {
                        JSONObject jtemp = new JSONObject();
                        jtemp.put("id", rs.getLong(1));
                        jtemp.put("operation", 1);
                        jtemp.put("size", rs.getString(2));
                        jtemp.put("name", rs.getString(3));
                        jtemp.put("description", rs.getString(4));
                        jtemp.put("MIMEtype", rs.getString(5));
                        jtemp.put("ext", rs.getString(6));
                        jtemp.put("dateCreated", rs.getString(7));
                        jtemp.put("md5", rs.getString(8));
                        jtemp.put("compression", rs.getString(9));
                        byte[] data = rs.getBytes(10);
                        addBuff(data, rs.getLong(1));
                        res.add(jtemp);
                    }
                    return 1;
                }
            };
            public void process(final String table, final long id, String operation) throws ServletException, IOException, SQLException {
                    Connector.Instance().executeQuery("select id,size,name,description,MIMEtype,ext,dateCreated,md5,compression,data from attachments where id=?",
                        new Object[]{id}, queryHandler);
            }
        });
        BuildTablehandler docHandler;
        docHandler = new BuildTablehandler() {
            Connector.QueryHandler accumHandler = new Connector.QueryToJsonObjectAccumulatedHandler();
            String currentTable = "";
            long currentID = 0;
            Object[] params;
            Connector.QueryHandler queryHandler = new Connector.QueryToJsonObjectHandler() {
                @Override
                public Object onRowObject(ResultSet rs, Object row) throws SQLException {
                    JSONObject jtemp = (JSONObject) row;
                    jtemp.remove("body");
                    jtemp.put("stat_view", visibleList.contains("" + currentID) ? "1" : "0");
                    if ("documents".equals(currentTable)) {
                        byte[] data = rs.getBytes("body");
                        addBuff(data, currentID);
                    }
                    jtemp.put("operation", 1);
                    //collect codes
                    String sql = "select * from (select code as codes from codes where docId = ?) t";
                    Connector.Instance().executeQuery(sql, params, accumHandler, jtemp);
                    //collect respondents
                    sql = "select * from (select resp_id as respondents from respondent_correspondent where corr_id!=resp_id and corr_id = ? ) t";
                    Connector.Instance().executeQuery(sql, params, accumHandler, jtemp);
                    //collect correspondents
                    sql = "select * from (select corr_id as correspondents from respondent_correspondent where corr_id!=resp_id and resp_id = ? ) t ";
                    Connector.Instance().executeQuery(sql, params, accumHandler, jtemp);
                    //collect classificators
                    sql = "select * from ( SELECT distinct trees.id as classificators FROM trees INNER JOIN tree_document ON trees.id=tree_document.tree_id WHERE tree_document.doc_id = ? ) t";
                    Connector.Instance().executeQuery(sql, params, accumHandler, jtemp);
                    return row;
                }
            };
            public void process(final String table, final long id, String operation) throws ServletException, IOException, SQLException {
                params = new Object[]{id};
                currentTable = table;
                currentID = id;
                Connector.Instance().executeQuery("select p.id,p.fullName,p.shortName,p.publishDate,p.regDate," +
                        " p.regNumber,p.comment,p.priority,p.isDocument,p.isActive,p.lastModified,p.draft," +
                        " p.status,p.price,p.propsMD5,p.bodyMD5,p.treesMD5,p.codesMD5 ,d.body,d.compression " +
                        " from properties p left join documents d on d.id=p.id where p.id = ? ",
                        params, queryHandler, res);
                System.out.println(res.size());
            }
        };
        handlers.put("documents", docHandler);
        handlers.put("doc_params", docHandler);

        handlers.put("trees", new BuildTablehandler() {
            public void process(final String table, final long id, String operation) throws ServletException, IOException, SQLException {
                Connector.Instance().executeQuery("select class_id, parent_id, name, sort_index from trees where id=?", new Object[]{id}, new Connector.QueryHandler() {
                    public Object onQuery(ResultSet rs, Object... params) throws SQLException {
                        while (rs.next()) {
                            JSONObject jtemp = new JSONObject();
                            jtemp.put("id", id);
                            jtemp.put("operation", 1);
                            jtemp.put("class_id", rs.getLong(1));
                            jtemp.put("parent_id", rs.getLong(2));
                            jtemp.put("name", rs.getString(3));
                            jtemp.put("sort_index", rs.getString(4));
                            res.add(jtemp);
                        }
                        return 1;
                    }
                });
            }
        });
        handlers.put("terms", new BuildTablehandler() {
            public void process(final String table, final long id, String operation) throws ServletException, IOException, SQLException {
                Connector.Instance().executeQuery("select word, definition from terms where id=?", new Object[]{id}, new Connector.QueryHandler() {
                    public Object onQuery(ResultSet rs, Object... params) throws SQLException {
                        while (rs.next()) {
                            JSONObject jtemp = new JSONObject();
                            jtemp.put("id", id);
                            jtemp.put("operation", 1);
                            jtemp.put("word", rs.getLong(1));
                            jtemp.put("definition", rs.getString(2));
                            res.add(jtemp);
                        }
                        return 1;
                    }
                });
            }
        });
        handlers.put("definitions", new BuildTablehandler() {
            public void process(final String table, final long id, String operation) throws ServletException, IOException, SQLException {
                Connector.Instance().executeQuery("SELECT a.word, b.docId, b.parId "
                        + "FROM definitions a "
                        + "LEFT JOIN definition_document b ON b.wordId = a.id "
                        + "WHERE id = ?", new Object[]{id}, new Connector.QueryHandler() {
                    public Object onQuery(ResultSet rs, Object... params) throws SQLException {
                        JSONObject jtemp = new JSONObject();
                        jtemp.put("id", id);
                        jtemp.put("operation", 1);
                        JSONArray arr = new JSONArray();
                        while (rs.next()) {
                            jtemp.put("word", rs.getString(1));
                            if (rs.getObject("docId") != null) {
                                JSONObject obj = new JSONObject();
                                obj.put("id", rs.getLong("docId"));
                                obj.put("name", rs.getString("parId"));
                                arr.add(obj);
                            }
                        }
                        jtemp.put("doc_paragraph", arr);
                        res.add(jtemp);
                        return 1;
                    }
                });
            }
        });
        String[] paramList = (from + "," + till + "," + ids).split(",");
        Connector.Instance().executeQuery("select * from(\n" +
                "select \n" +
                "    max(id) as id, \n" +
                "    doc_type,\n" +
                "    group_concat(op_type) as oper_type,\n" +
                "    id1,\n" +
                "    max(op_date) as oper_date, \n" +
                "    max(included) as included\n" +
                "from (\n" +
                "    select  \n" +
                "            id1, \n" +
                "            modifiedTable as doc_type, \n" +
                "            case operation \n" +
                "                 when 0 then 'add' \n" +
                "                 when 1 then 'edit' \n" +
                "                 when 2 then 'del' \n" +
                "                 else concat('',operation)  \n" +
                "            end as op_type, \n" +
                "            max(id) as id, \n" +
                "            max(dateCreating) as op_date, \n" +
                "            max(included) as included\n" +
                "    from updates \n" +
                "    where dateCreating>=? AND dateCreating<=?\n" +
                "    group by id1, doc_type, op_type\n" +
                ") t\n" +
                "group by id1, doc_type\n" +
                ")t2\n" +
                "where id in (" + Connector.Instance().getParamStr(paramList.length - 2) + ")\n" +
                "order by doc_type,id1\n", paramList, new Connector.QueryHandler() {
            public Object onQuery(ResultSet rs, Object... params1) throws SQLException {
                int i = 0;
                while (rs.next()) {
                    String strTable = rs.getString(2);
                    String operation = rs.getString(3);
                    long id = rs.getLong("id1");
                    BuildTablehandler handler = handlers.get(strTable);
                    if (handler != null) {
                        try {
                            if (operation.contains("del")) {
                                handler.process_del(id, operation);
                            } else {
                                handler.process(strTable, id, operation);
                            }
                        } catch (ServletException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    i++;
                }
                return i;
            }
        });
        JSONObject jo = new JSONObject();
        jo.put("status", "ok");
        jo.put("dateFrom", from);
        jo.put("dateTo", from);
        // stream for binary data
        ByteArrayOutputStream binary = new TinyByteArrayOutStream();
        // putting all to json
        String[] roots = new String[]{"documents", "terms", "attachments", "definitions", "trees", "classificators"};
        for (String root : roots) {
            if (handlers.get(root) != null) {
                BuildTablehandler handler = handlers.get(root);
                jo.put(root, handler.res);
                if (handler.stream.size() > 0) {
                    handler.flushToStream(root, binary);
                }
            }
        }
        byte[] json = jo.toString().getBytes("UTF-8");
        ByteArrayOutputStream out = new TinyByteArrayOutStream();
        DataOutputStream dout = new DataOutputStream(out);
        //put header size
        dout.writeInt(json.length);
        // put header
        dout.write(json);
        // prepare binary part
        binary.flush();
        // put binary size
        dout.writeInt(binary.size());
        // put binary data
        dout.write(binary.toByteArray(), 0, binary.size());
        // flush all
        dout.flush();
        // return all data
        data = out.toByteArray();
        // compress
        byte[] zipped = LZMAHelper.compress(new ByteArrayInputStream(data, 0, out.size()));
        // add sign and license
        byte[] signed = sign(zipped);
        // crypt whole packet
        return crypt(signed);
    }

    /**
     * Упаковщик данныъ по таблицам
     */
    abstract class BuildTablehandler {
        //list of elements
        JSONArray res = new JSONArray();
        // list of binary id and size
        JSONArray binary = new JSONArray();
        // buffer for binary data
        ByteArrayOutputStream stream = new TinyByteArrayOutStream();

        abstract void process(String table, long id, String operation) throws ServletException, IOException, SQLException;

        /**
         * Put binary data to stream
         *
         * @param name binary block name
         * @param out stream where data will be written
         */
        void flushToStream(String name, OutputStream out) {
            try {
                DataOutputStream dout = new DataOutputStream(out);
                byte[] nameArr = name.getBytes("UTF-8");
                // put name of buffer
                dout.writeInt(nameArr.length);
                dout.write(nameArr);
                //put size info in JSON
                byte[] json = binary.toString().getBytes("UTF-8");
                dout.writeInt(json.length);
                dout.write(json);
                // putting data
                dout.writeInt(stream.size());
                dout.write(stream.toByteArray(), 0, stream.size());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /**
         * collect buff list
         *
         * @param buff binary data
         * @param id unique identifier of binary data
         */
        void addBuff(byte[] buff, long id) {
            try {
                if (buff == null || buff.length == 0) return;
                stream.write(buff);
                JSONObject jtemp = new JSONObject();
                jtemp.put("s", buff.length);
                jtemp.put("i", id);
                binary.add(jtemp);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        void process_del(long id, String operation) throws ServletException, IOException, SQLException {
            JSONObject jtemp = new JSONObject();
            jtemp.put("operation", 2);
            if (operation.contains("del")) {
                jtemp.put("operation", 2);
            }
            jtemp.put("id", id);
            res.add(jtemp);
        }
    }


}
