package kz.novelty.jurist;

import java.util.Date;

public class UserInfo {
    long userId;
    String userName;
    String errorText;
    Date loginTime;
    String session;
    final String host;

    public UserInfo(long userId, String userName, String host) {
        this.userId = userId;
        this.userName = userName;
        this.host = host;
        this.loginTime = new Date();
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public void setErrorText(String errorText) {
        this.errorText = errorText;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getHost() {
        return host;
    }

}
