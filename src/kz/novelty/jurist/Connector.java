package kz.novelty.jurist;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;

/**
 * User: developer
 * Date: 13.08.11
 * Time: 14:52
 */
public class Connector extends DBConnector{
    private static Connector instance = new Connector();
    private String embeddedmode = null;
    private Map<String, String> embeddedDrives = null;
    private Map<String, UserInfo> userSessions = null;
    private byte[] licenseData;
    private boolean licensePermission;

    private class SessionError {
        String error;
        int showCount;

        public SessionError(String error) {
            this.error = error;
        }
    }
    private Map<String, SessionError> userSessionsErrors = null;
    private List<String> sysInfo = null;
    private Properties smtpProps = null;
    private List<String> trayMessages = null;
    private long maxUserSessions = Long.MAX_VALUE;
    private AtomicLong userSessionsCount = null;

    @SuppressWarnings("unchecked")
    private Connector() {
        super();
        userSessionsErrors = new HashMap<String, SessionError>();
        userSessionsCount = new AtomicLong(0);
        try {
            InitialContext ctx = new InitialContext();
            // определеяем режим запуска
            try {
                embeddedmode = (String) ctx.lookup("embeddedMode");
            } catch (NamingException e) {
                try {
                    embeddedmode = (String) ctx.lookup("java:comp/env/embeddedMode");
                } catch (NamingException ignored) {
                }
            }
            // список меток CD дисков
            try {
                embeddedDrives = (Map<String, String>) ctx.lookup("embeddedDrives");
            } catch (NamingException e) {
                try {
                    embeddedDrives = (Map<String, String>) ctx.lookup("java:comp/env/embeddedDrives");
                } catch (NamingException ignored) {
                }
            }
            // список информации о системе
            try {
                sysInfo = (List<String>) ctx.lookup("sys_info");
            } catch (NamingException e) {
                try {
                    sysInfo = (List<String>) ctx.lookup("java:comp/env/sys_info");
                } catch (NamingException ignored) {
                    e.printStackTrace();
                }
            }
            // список информации о системе
            try {
                trayMessages = (List<String>) ctx.lookup("tray_messages");
            } catch (NamingException e) {
                try {
                    trayMessages = (List<String>) ctx.lookup("java:comp/env/tray_messages");
                } catch (NamingException ignored) {
                }
            }
            // список для хранения сессий пользователей
            try {
                userSessions = (Map<String, UserInfo>) ctx.lookup("embeddedUsers");
            } catch (NamingException e) {
                try {
                    userSessions = (Map<String, UserInfo>) ctx.lookup("java:comp/env/embeddedUsers");
                } catch (NamingException ignored) {
                    userSessions = new ConcurrentHashMap<String, UserInfo>();
                }
            }
            // настройки почты
            // Get system properties
            //Properties properties = System.getProperties();
            smtpProps = new Properties();
            // Setup mail server
            smtpProps.put("mail.user", System.getProperty("interconsult.mail.user", "user"));
            smtpProps.put("mail.password", System.getProperty("interconsult.mail.pass", "password"));
            smtpProps.put("mail.transport.protocol", System.getProperty("interconsult.mail.protocol", "smtp"));
            smtpProps.put("mail.smtp.host", System.getProperty("interconsult.mail.host", "mailhost"));
            smtpProps.put("mail.smtp.port", System.getProperty("interconsult.mail.port", "587"));
            smtpProps.put("mail.smtp.auth", System.getProperty("interconsult.mail.auth", "false"));
            smtpProps.put("mail.smtp.starttls.enable", System.getProperty("interconsult.mail.tls", "false"));
            if (!"".equals(System.getProperty("interconsult.mail.ssl"))) {
                smtpProps.put("mail.smtp.socketFactory.port", System.getProperty("interconsult.mail.ssl", "465"));
                smtpProps.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            }
        } catch (NamingException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
    }

    public static Connector Instance() {
        return instance;
    }

    /**
     * если режим локального сервера
     * @return
     */
    public boolean isEmbeddedLoc(boolean includeCDmode) {
        return (includeCDmode && "CD".equals(embeddedmode)) || "LOC".equals(embeddedmode) || isEmbeddedNet();
    }

    /**
     * Локальная версия в многопользовательском режиме
     * @return
     */
    public boolean isEmbeddedNet() {
        return "NET".equals(embeddedmode);
    }
    public boolean isUpdateBuilderMode() {
        return "builder".equals(embeddedmode);
    }

    /**
     * Режим самостоятельного сервера
     * @return true if embedded mode on
     */
    public boolean isEmbedded() {
        return isEmbeddedLoc(true) || isEmbeddedNet();
    }

    /**
     * Если однопользовательский режим, то авторизация не нужна
     * @return true для CD версии, если метка диска совпадет с переменной
     */
    public boolean isEmbedAuth() {
        boolean res = false;
        if ("CD".equals(embeddedmode)) {
            String label = System.getProperty("interconsult.cd.label", "");
            System.out.println("cd label" +label);
            res = embeddedDrives.containsValue(label);
        }

        return res;
    }

    public void showTrayMessage(String msg) {
        if(trayMessages != null){
            try{
                trayMessages.add(msg);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public List<String> getSysInfo() {
        return sysInfo;
    }

    static final String ENCODING = "UTF-8";

    /*
   * Отправка сообщения на email.
   * */
    public boolean sendMail(String receipient, String subject, String messageBody) {
        // Recipient's email ID needs to be mentioned.

        // Sender's email ID needs to be mentioned
        String from = System.getProperty("interconsult.mail.from", "robot@wfin.kz");

        // Get the default Session object.
        Session session = Session.getInstance(smtpProps, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(smtpProps.getProperty("mail.user"), smtpProps.getProperty("mail.password"));
            }
        });

        try {
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(receipient));

            // Set Subject: header field
            message.setSubject(subject, ENCODING);

            // Send the actual HTML message, as big as you like
            BodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(messageBody, "text/html; charset=" + ENCODING + "");
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);
            message.setContent(multipart);

            // Send message
            Transport.send(message);
        } catch (MessagingException mex) {
            mex.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * устанавливается максимальное количество пользовательских сессий
     * @param maxUserSessions
     */
    public void setMaxUserSessions(long maxUserSessions) {
        if(maxUserSessions < this.maxUserSessions ){
            this.maxUserSessions = maxUserSessions;
        }
    }

    /**
     * список пользователей
     * @return
     */
    public Collection<UserInfo> getUserSessionList(){
        return userSessions.values();
    }

    /**
     * Возврщает информацию о сессии пользователя
     * @param id
     * @return
     */
    public UserInfo getUserSessionInfo(String id){
        return userSessions.get(id);
    }

    public void setUserSessionError(String id, String error){
        userSessionsErrors.remove(id);
        if(error != null && !"".equals(error)) {
            userSessionsErrors.put(id, new SessionError(error));
        }
    }
    /**
     * получить ошибки сессии
     * @param id номер сессии
     * @return текст ошибки ( сессия истекла, другой пользователь зашел, ограничение числа пользователей)
     */
    public String getUserSessionError(String id){
        SessionError sessionError = userSessionsErrors.get(id);
        if(sessionError != null){
            sessionError.showCount++;
            if ( sessionError.showCount > 3 ) userSessionsErrors.remove(id);
            return sessionError.error;
        }
        return "";
    }

    public  void addUserSessionInfo(String id, UserInfo user){
        user.setSession(id);
        addLogMessage("login", user, user.getHost());
        userSessionsCount.getAndIncrement();
        userSessionsErrors.remove(id);
        checkConcurrentSessions(user);
        userSessions.put(id, user);
        checkMaxUsersCount(maxUserSessions);
    }

    /**
     * Блокировка одновременных сессий
     * @param user
     */
    private void checkConcurrentSessions(UserInfo user) {
        Collection<UserInfo> users = getUserSessionList();
        ArrayList<UserInfo> sessions = new ArrayList<UserInfo>(users.size());
        for (Iterator<UserInfo> iterator = users.iterator(); iterator.hasNext(); ) {
            UserInfo next = iterator.next();
            if(next.userName.equals(user.userName)){
                sessions.add(next);
            }
        }
        // завершаем одновременные сессии
        for (UserInfo session : sessions) {
            String msg = "Сессия заблокирована. Другой пользователь под вашим логином.";
            setUserSessionError(session.getSession(), msg);
            removeUserSessionInfo(session.getSession(), msg);
        }
    }

    /**
     * проверка на максимальное кол сессий
     * @param maxUserSessions макс кол пользователей
     */
    private void checkMaxUsersCount(long maxUserSessions) {
        // если число не ограничено или не локальная версия, то все ок
        if ( Long.MAX_VALUE == maxUserSessions || !isEmbedded() ) return;
        if ( userSessionsCount.get() <= maxUserSessions ) return;
        Collection<UserInfo> users = getUserSessionList();
        ArrayList<UserInfo> sessions = new ArrayList<UserInfo>(users.size());
        for (Iterator<UserInfo> iterator = users.iterator(); iterator.hasNext(); ) {
            UserInfo next = iterator.next();
            sessions.add(next);
        }
        // сортируем по времени входа
        Collections.sort(sessions,new Comparator<UserInfo>() {
            public int compare(UserInfo u1, UserInfo u2) {
                return u1.getLoginTime().compareTo(u2.getLoginTime());
            }
        });
        // завершаем первые сессии, до числа одновременных сессий
        for (long i = 0, len = userSessionsCount.get() - maxUserSessions; i <= len; i++) {
            UserInfo user = sessions.get((int) i);
            String msg = "Ваша сессия завершена (достигнуто максимальное число одновременных сессий)";
            setUserSessionError(user.getSession(), msg);
            removeUserSessionInfo(user.getSession(), msg);
        }
    }

    /**
     *
     * @param id
     */
    public  void removeUserSessionInfo(String id, String msg){
        UserInfo user =userSessions.get(id);
        if(user != null){
            addLogMessage("logout", user, msg);
        }
        userSessionsCount.getAndDecrement();
        userSessions.remove(id);
    }

    /**
     * берем текущее имя схемы для работы с  документами
     * @return
     */
    public String getDocumentScheme()  {
        String schemeName = null;
        try {
            schemeName = (String) executeQuery("select getDocumentScheme() as schm", new Object[]{}, new QueryHandler() {
                public Object onQuery(ResultSet rs, Object... params) throws SQLException {
                    if (rs.next()) {
                        return rs.getString("schm");
                    }
                    return "";
                }
            });
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return schemeName;

    }

    /**
     * берем кэшированное значение
     * @return
     */
    public boolean hasCachedLicensePermission() {
        return licensePermission;
    }

    /**
     *
     * @param licensePermission
     */
    public void setCachedLicensePermission(boolean licensePermission) {
        this.licensePermission = licensePermission;
    }

    /**
     * сбрасываем кэши лицензий
     */
    public void flushLicenseBuf() {
        licenseData = null;
        this.licensePermission = false;
    }

    /**
     * получаем информацию о текущей лицензии
     * @return
     */
    public byte[] getLastLicense(){
        if( licenseData != null && licenseData.length > 0) return licenseData;
        try {
            Connector.Instance().executeQuery("select uuid,last_update_id,rights_info from update_info limit 1", new Object[]{}, new QueryHandler() {
                public Object onQuery(ResultSet rs, Object... params) throws SQLException {
                    if (rs.next()) {
                        licenseData = rs.getBytes(3);
                        return licenseData;
                    } else {

                    }
                    return licenseData;
                }
            });
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return licenseData;
    }




    public void addLogMessage(String type, UserInfo user, String descr){
        addLogMessage(type, user, descr, 0);
    }

    public void addLogMessage(final String type, final UserInfo user, final String descr, double docId){
        if(isUpdateBuilderMode()) return;
        executeCallIgnoreException("call addActionLog(?,?,?,?)", new DBConnector.CallHandler() {
            public void onCall(CallableStatement st, Object... params) throws SQLException {
                st.setString(1, type);
                st.setLong(2, user == null ? 0 : user.userId);
                st.setString(3, descr);
                st.setString(4, user == null ? "" : user.getSession());
                st.execute();
            }
        });

    }
}
