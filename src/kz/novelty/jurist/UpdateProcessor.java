package kz.novelty.jurist;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * User: Malik.N
 */
public class UpdateProcessor extends BaseUpdateBuilder {
    private String url;
    List<UpdateProcessorHandler> handlers;
    DesUtil des;
    CertUtil certUtil;
    private long currentUpdateID;

    public UpdateProcessor() {
        this(System.getProperty("sps.update.url", "http://localhost:9090/updateServer/UpdateServerHandler"));
    }

    public UpdateProcessor(String url) {
        this.url = url;
        this.handlers = new ArrayList<UpdateProcessorHandler>();
        handlers.add(new UpdateHandlerDoc());
        this.des = new DesUtil();
        this.certUtil = new CertUtil();
    }

    @Override
    public BaseDesUtil getDes() {
        return des;
    }

    @Override
    protected BaseCertUtil getCertUtil() {
        return certUtil;
    }


    /**
     * сохранить код активации
     *
     * @param id код активации
     * @throws SQLException
     */
    public void setActivationNumber(final String id) throws SQLException {
        long act_id = (Long) Connector.Instance().executeQuery("select update_info_id from update_info limit 1", new Object[]{}, new DBConnector.QueryHandler() {
            public Object onQuery(ResultSet rs, Object... params) throws SQLException {
                if (rs.next()) {
                    return rs.getLong(1);
                }
                return (long) 0;
            }
        });
        String sql = "update update_info set uuid = ?";
        if (act_id <= 0) {
            sql = "insert into update_info (uuid)values(?)";
        }
        Connector.Instance().executeCall(sql, new DBConnector.CallHandler() {
            public void onCall(CallableStatement st, Object... params) throws SQLException {
                st.setString(1, id);
                st.execute();
            }
        });
        JSONObject object = new JSONObject();
        String info = getSystemDataForServer(object);

        List<String[]> params = new ArrayList<String[]>();
        params.add(new String[]{"requestType", "activate"});
        params.add(new String[]{"info", info});
        appendLogAndTray("Активация продукта", null);
        byte[] license = URLRequester.executeRequestAsByte(url, params);
        if (license == null || license.length < 50) {
            appendLogAndTray("Активация продукта прошла неуспешно", null);
            return;
        }
        boolean res = checkLicensePermission(license, Connector.Instance().getSysInfo(), true);
        if (res) {
            saveLicenseInfo(license);
            appendLogAndTray("Активация продукта прошла успешно", null);
        }
    }

    /**
     * Сохранить шифрованную лицензию
     *
     * @param license
     * @throws SQLException
     */
    public void saveLicenseInfo(final byte[] license) throws SQLException {
        long act_id = (Long) Connector.Instance().executeQuery("select update_info_id from update_info limit 1", new Object[]{}, new DBConnector.QueryHandler() {
            public Object onQuery(ResultSet rs, Object... params) throws SQLException {
                if (rs.next()) {
                    return rs.getLong(1);
                }
                return (long) 0;
            }
        });
        String sql = "update update_info set rights_info = ?";
        if (act_id > 0) {
            Connector.Instance().executeCall(sql, new DBConnector.CallHandler() {
                public void onCall(CallableStatement st, Object... params) throws SQLException {
                    st.setBytes(1, license);
                    st.execute();
                }
            });
            Connector.Instance().flushLicenseBuf();
        }
    }

    /**
     * Скачать и установить обновления
     *
     * @throws SQLException
     */
    public void applyUpdates() throws SQLException {
        final StringBuilder log = new StringBuilder();
        JSONObject obj = new JSONObject();
        JSONArray arr = getUpdateListJson(obj, log);
        List<String[]> params = new ArrayList<String[]>();
        params.add(new String[]{"requestType", "download"});
        params.add(new String[]{"uuid", obj.getString("uuid")});
        params.add(new String[]{"id", "0"});
        int j = 0;
        int successCnt = 0;
        int cnt = arr.size();
        Object[] newArr = arr.toArray();
        Arrays.sort(newArr, new Comparator<Object>() {
            public int compare(Object o1, Object o2) {
                return Long.valueOf(((JSONArray) o1).getLong(0)).compareTo(((JSONArray) o2).getLong(0));
            }

        });
        for (int i = 0; i < cnt; i++) {
            JSONArray upd = (JSONArray) newArr[i];
            String id = upd.getString(0);
            String name = upd.getString(1);
            String visible = "";
            if (upd.size() > 4) {
                visible = upd.getString(4);
            }
            j++;
            params.set(2, new String[]{"id", id});
            appendLogAndTray(String.format("Скачивание обновления %d из %d", j, cnt), log);
            byte[] response = URLRequester.executeRequestAsByte(url, params);
            if (response != null && response.length > 0) {
                appendLogAndTray(String.format("Установка обновления %d из %d", j, cnt), log);
                if (processUpdate(response, log, id, name, visible)) {
                    successCnt++;
                    appendLogAndTray(String.format("Обновление %d установлено", j, cnt), log);
                }
            }
        }
        if (cnt == 0) {
            appendLogAndTray("Новых обновлений нет", log);
        } else {
            if (successCnt == cnt) {
                appendLogAndTray(String.format("База находится в актуальном состоянии. Успешно установлены %d обновлений", successCnt), log);
            } else {
                appendLogAndTray(String.format("Успешно установлены %d из %d обновлений", successCnt, cnt), log);
            }
        }
    }


    private void appendLogAndTray(String msg, StringBuilder log) {
        if (log != null) log.append("\n").append(msg);
        Connector.Instance().showTrayMessage(msg);
        // wait message display
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Расшифровать информацию и лицензии
     *
     * @param license шифрованные данные
     * @return объект лицензии
     * @throws Exception если не прошли проверку цифровой подписи и прочее
     */
    protected JSONObject getLicenseObject(byte[] license) throws Exception {
        if (license == null) return null;
        int size = license.length;
        byte[] dec = des.decrypt(license, 0, size);
        int[] sizeInfo = checkSign(dec, false);
        final int unsignedLength = sizeInfo[0];
        final ByteArrayInputStream uis = new ByteArrayInputStream(dec, 0, unsignedLength);
        byte[] uncompressed = LZMAHelper.decompress(uis);
        String info = new String(uncompressed, "utf-8");
        //{"id":1,"valid_from":"2013-03-01","valid_till":"2013-03-30","update_id":1,"previous_id":0}
        JSONObject obj = (JSONObject) JSONSerializer.toJSON(info);
        System.out.println(info);
        return obj;
    }

    /**
     * проверить текущую лицензию. Игнорируются сроки действия
     *
     * @return
     */
    public boolean hasLicensePermission() {
        return hasLicensePermission(false);
    }

    /**
     * проверить текущую лицензию.
     *
     * @param checkDates Игнорировать сроки действия или нет
     * @return
     */
    public boolean hasLicensePermission(final boolean checkDates) {
        Boolean res = Connector.Instance().hasCachedLicensePermission();
        if (res) return res;
        byte[] data = Connector.Instance().getLastLicense();
        if (data != null && data.length > 0) {
            List<String> systemInfo = Connector.Instance().getSysInfo();
            res = checkLicensePermission(data, systemInfo, checkDates);
            Connector.Instance().setCachedLicensePermission(res);
        }
        return res;
    }

    /**
     * Проверить лицензию на этом компьютере
     *
     * @param license    шифрованная информация о лицензии
     * @param systemInfo информация о текущей системе
     * @param checkDates проверять даты или нет
     * @return true если лицензия валидна для текущей системы, false иначе
     */
    public boolean checkLicensePermission(byte[] license, List<String> systemInfo, boolean checkDates) {
        try {
            JSONObject obj = getLicenseObject(license);
            return checkLicensePermission(obj, systemInfo, checkDates);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public Date getLicenseEndDate(byte[] license){
        try {
            JSONObject obj = getLicenseObject(license);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date till = dateFormat.parse(obj.getString("valid_till"));
            return till;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }
    /**
     * Проверить лицензию на этом компьютере
     *
     * @param obj        информация о лицензии
     * @param systemInfo информация о текущей системе
     * @return true если лицензия валидна для текущей системы, false иначе
     */
    public boolean checkLicensePermission(JSONObject obj, List<String> systemInfo, boolean checkDates) {
        if (obj == null) return false;
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date from = dateFormat.parse(obj.getString("valid_from"));
            Date till = dateFormat.parse(obj.getString("valid_till"));
            String sUser_count = obj.optString("user_count");
            long user_count = 1;
            try {
                user_count = Long.parseLong(sUser_count);
            } catch (NumberFormatException e) {
            }
            Connector.Instance().setMaxUserSessions(user_count);
            Date now = new Date();
            if (checkDates && (now.after(till) || now.before(from))) {
                return false;
            }
            JSONArray allowedList = obj.getJSONArray("allowed");
            for (Object anAllowedList : allowedList) {
                JSONObject allowed = (JSONObject) anAllowedList;
                String allowString = allowed.getString("name") + "=" + allowed.getString("value");
                if (systemInfo.contains(allowString)) {
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * проверим права для обновления
     *
     * @param license информация о лицензии
     * @throws Exception
     */
    @Override
    protected void onLicenseCheck(byte[] license) throws Exception {
        JSONObject obj = getLicenseObject(license);
        if (!checkLicensePermission(obj, Connector.Instance().getSysInfo(), true)) {
            throw new Exception("Обновление не разрешается ставить на этом компьютере");
        }
        JSONObject licenseInfo = getSystemUpdateInfo(null);
        long last_update_id = licenseInfo.getLong("last_id");
        if (obj.getLong("update_id") <= last_update_id) {
            throw new Exception("Обновление уже установлено ");
        }
        if (obj.getLong("previous_id") != last_update_id) {
            throw new Exception("Не установлено предыдушее обновление");
        }
        setCurrentUpdateID(obj.getLong("update_id"));
        saveLicenseInfo(license);
    }

    /**
     * обработать обновление
     *
     * @param data    бинарные данные
     * @param log     журнал установки
     * @param id
     * @param name
     * @param visible
     */
    private boolean processUpdate(byte[] data, final StringBuilder log, final String id, final String name, String visible) {
        try {
            int size = data.length;
            ByteArrayInputStream is = new ByteArrayInputStream(data);
            DataInputStream ds = new DataInputStream(is);
            // check headers
            if (ds.readInt() != 1229) {
                appendLogAndTray("Неверный тип обновления", log);
                return false;
            }
            int updType = ds.readInt();
            boolean handled = false;
            for (UpdateProcessorHandler handler : handlers) {
                if (handler.isApplyable(updType)) {
                    handled = true;
                    break;
                }
            }
            if (handled == false) {
                appendLogAndTray("Неверный тип обновления", log);
                return false;
            }
            byte[] dec = des.decrypt(data, 8, size - 8);
            int[] sizeInfo = checkSign(dec);
            final int unsignedLength = sizeInfo[0];
            final ByteArrayInputStream uis = new ByteArrayInputStream(dec, 0, unsignedLength);
            byte[] uncompressed = LZMAHelper.decompress(uis);
            boolean res = false;
            for (UpdateProcessorHandler handler : handlers) {
                if (handler.isApplyable(updType)) {
                    res = res || handler.apply(uncompressed, this.currentUpdateID, log);
                }
            }
            // добавим статистику
            Connector.Instance().executeBatch(new DBConnector.BatchCallHandler() {
                @Override
                public void OnBatch() throws SQLException {
                    final Boolean exists = (Boolean) executeQuery("select name from update_log where update_id = ? ", new Object[]{id}, new DBConnector.QueryHandler() {
                        public Object onQuery(ResultSet rs, Object... params) throws SQLException {
                            final boolean exists = rs.next();
                            return exists;
                        }
                    });
                    final String sql = exists ? "update update_log set log = ? where update_id = ?" : "insert INTO update_log (update_id, name, log,install_date)VALUES(?,?,?,now())";
                    executeCall(sql, new DBConnector.CallHandler() {
                        public void onCall(CallableStatement st, Object... params) throws SQLException {
                            if (exists) {
                                st.setString(1, log.toString());
                                st.setString(2, id);
                            } else {
                                st.setString(1, id);
                                st.setString(2, name);
                                st.setString(3, log.toString());
                            }
                            st.execute();
                            log.setLength(0);
                        }
                    });

                }
            });
            // если публичное обновление, то пометим как успешный
            if ("0".equals(visible)) {
                Connector.Instance().executeCallIgnoreException("update update_info set last_update_id = ?", new DBConnector.CallHandler() {
                    public void onCall(CallableStatement st, Object... params) throws SQLException {
                        st.setLong(1, currentUpdateID);
                        st.execute();
                    }
                });
            }
            return res;
            //String json = new String(uncompressed);
        } catch (Exception e) {
            e.printStackTrace();
            appendLogAndTray(e.getMessage(), log);
            return false;
        }
    }

    /**
     * получить текущий ключ активации
     *
     * @return
     */
    public String getSystemUUID() {
        try {
            return (String) Connector.Instance().executeQuery("select uuid from update_info limit 1", new Object[]{}, new DBConnector.QueryHandler() {
                public Object onQuery(ResultSet rs, Object... params) throws SQLException {
                    if (rs.next()) {
                        return rs.getString(1);
                    }
                    return "";
                }
            });
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * собрать информацию о системе и активации
     *
     * @param object
     * @return
     * @throws SQLException
     */
    public JSONObject getSystemUpdateInfo(JSONObject object) throws SQLException {
        final JSONObject obj = object != null ? object : new JSONObject();
        Connector instance = Connector.Instance();
        List<String> sysInfo = instance.getSysInfo();
        obj.remove("info");
        for (int i = 0; i < sysInfo.size(); i++) {
            String s = sysInfo.get(i);
            obj.accumulate("info", s);
        }
        instance.executeQuery("select uuid,last_update_id,rights_info from update_info limit 1", new Object[]{}, new DBConnector.QueryHandler() {
            public Object onQuery(ResultSet rs, Object... params) throws SQLException {
                if (rs.next()) {
                    obj.put("uuid", rs.getString(1));
                    obj.put("last_id", rs.getLong(2));
                } else {
                    throw new SQLException("Продукт не активирован");
                }
                return null;
            }
        });
        // соберем журнал действий пользователя
        if (instance.isEmbedded()) {
            JSONArray arr = (JSONArray) instance.executeQuery("select * from (select action_log_id as id,action_type as type, action_date as dt,doc_id, descr, session as s from action_log) t order by dt",
                    new Object[]{}, new DBConnector.QueryToJsonObjectHandler() {
                @Override
                public Object onRowObject(ResultSet rs, Object row) throws SQLException {
                    JSONObject jtemp = (JSONObject) row;
                    // запомним последний идентификатор
                    obj.put("log_last_id", jtemp.getString("id"));
                    return row;
                }
            });
            obj.put("log", arr);
        }
        return obj;
    }

    /**
     * Очищаем логи чтоб не копить их
     * @param object
     */
    private void cleanLogAfterSend(JSONObject object) {
        final String id = object.optString("log_last_id");
        if (id == null || "".equals(id)) return;
        Connector.Instance().executeCallIgnoreException("delete from action_log where action_log_id <= ?", new DBConnector.CallHandler() {
            public void onCall(CallableStatement st, Object... params) throws SQLException {
                st.setString(1, id);
                st.execute();
            }
        });
    }


    /**
     * получить список обновлений
     *
     * @param object
     * @param log
     * @return
     * @throws SQLException
     */
    public List<String> getUpdateList(JSONObject object, StringBuilder log) throws SQLException {
        List<String> updateList = new ArrayList<String>();
        JSONArray arr = getUpdateListJson(object, log);
        for (int i = 0; i < arr.size(); i++) {
            JSONArray array = arr.getJSONArray(i);
            updateList.add(array.getString(0));
        }
        return updateList;
    }

    /**
     * собрать информацию о системе и активации
     *
     * @param object доп сведения
     * @return base64 данные
     * @throws SQLException
     */
    public String getSystemDataForServer(JSONObject object) throws SQLException {
        final JSONObject obj = getSystemUpdateInfo(object);
        byte[] data = new byte[0];
        try {
            data = des.crypt(LZMAHelper.compress(obj.toString().getBytes("utf-8")));
            return certUtil.byteToBase64(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public JSONArray getUpdateListJson(JSONObject object, StringBuilder log) throws SQLException {
        try {
            String info = getSystemDataForServer(object);
            List<String[]> params = new ArrayList<String[]>();
            params.add(new String[]{"requestType", "getList"});
            params.add(new String[]{"info", info});
            appendLogAndTray("Получение списка обновлений", log);
            byte[] response = URLRequester.executeRequestAsByte(url, params);
            if (response == null) {
                appendLogAndTray("Сервер обновлений не доступен", log);
                return new JSONArray();
            }
            JSONArray arr = (JSONArray) JSONSerializer.toJSON(new String(response, "utf-8"));
            cleanLogAfterSend(object);
            System.out.println(new String(response));
            return arr;
        } catch (Exception e) {
            e.printStackTrace();
            Connector.Instance().showTrayMessage(e.getMessage());
            throw new SQLException(e);
        } finally {

        }
    }

    /**
     * установить номер текущего обновления
     *
     * @param currentUpdateID
     */
    public void setCurrentUpdateID(long currentUpdateID) {
        this.currentUpdateID = currentUpdateID;
    }
}
