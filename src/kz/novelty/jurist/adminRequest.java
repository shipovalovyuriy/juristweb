package kz.novelty.jurist;

import SevenZip.*;
import SevenZip.LZMAHelper;
import com.google.common.base.Joiner;
import net.sf.json.JSON;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.noggit.JSONParser;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.sql.*;
import java.text.MessageFormat;
import java.util.*;
import java.util.logging.Logger;

import static kz.novelty.jurist.Connector.Instance;

/**
 * Created by аип on 02.02.2016.
 */
@WebServlet(name = "adminRequest",urlPatterns="/adminPanel")



public class adminRequest extends HttpServlet {
    private Connection con;
    private Statement st;
    private ResultSet res;
    StringBuilder struct = new StringBuilder();
    interface handler {
        void process(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException;
    }

    HashMap<String, handler> handlers = new HashMap<String, handler>();

    public adminRequest (){
        super();
        handlers.put("getCategories", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, OutputStream out) throws ServletException, IOException, SQLException {
                process_getCategory(req,resp,out);
            }
        });
        handlers.put("getClasses", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, OutputStream out) throws ServletException, IOException, SQLException {
                process_getClasses(req,resp,out);
            }
        });
        handlers.put("getTerms", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, OutputStream out) throws ServletException, IOException, SQLException {
                process_getTerms(req,resp,out);
            }
        });
        handlers.put("addTerm", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, OutputStream out) throws ServletException, IOException, SQLException {
                process_addTerm(req,resp,out);
            }
        });
        handlers.put("delTerms", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, OutputStream out) throws ServletException, IOException, SQLException {
                process_delTerm(req,resp,out);
            }
        });
        handlers.put("getAllCategories", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, OutputStream out) throws ServletException, IOException, SQLException {
                process_getAllCat(req,resp,out);
            }
        });
        handlers.put("getDocs", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, OutputStream out) throws ServletException, IOException, SQLException {
                process_getDocs(req,resp,out);
            }
        });
        handlers.put("getDoc", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, OutputStream out) throws ServletException, IOException, SQLException {
                process_docLoad(req,resp,out);
            }
        });
        handlers.put("getDocInfo", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, OutputStream out) throws ServletException, IOException, SQLException {
                process_docInfo(req,resp,out);
            }
        });
        handlers.put("getDocTree", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, OutputStream out) throws ServletException, IOException, SQLException {
                process_docTree(req,resp,out);
            }
        });
        handlers.put("searchDocs", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, OutputStream out) throws ServletException, IOException, SQLException {
                process_searchDocs(req,resp,out);
            }
        });
        handlers.put("getUserList", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, OutputStream out) throws ServletException, IOException, SQLException {
                process_getUserList(req,resp,out);
            }
        });
    }

    public void process_getUserList( HttpServletRequest req, HttpServletResponse resp,OutputStream out) throws IOException, SQLException {

        JSONArray data = (JSONArray) Connector.Instance().executeQuery("SELECT * FROM jurist.users c join jurist.user_roles b on b.user_id = c.user_id where role_id = 1", new Object[]{}, new DBConnector.QueryToJsonObjectHandler(), Boolean.TRUE, struct);
        resp.setContentType("application/json; charset=UTF-8");
        out.write(data.toString().getBytes("UTF-8"));

    }

    public void process_getTerms( HttpServletRequest req, HttpServletResponse resp,OutputStream out) throws SQLException, IOException {

        String title = req.getParameter("title");
        JSONArray data = (JSONArray) Connector.Instance().executeQuery("SELECT * FROM test.terms where word like '%"+title+"%';", new Object[]{}, new DBConnector.QueryToJsonObjectHandler(), Boolean.TRUE, struct);
        resp.setContentType("application/json; charset=UTF-8");
        out.write(data.toString().getBytes("UTF-8"));

    }
    public void process_addTerm(HttpServletRequest req, HttpServletResponse resp, OutputStream out){

    }
    public void process_delTerm(HttpServletRequest req, HttpServletResponse resp,OutputStream out){

    }
    public void process_searchDocs(HttpServletRequest req, HttpServletResponse resp,OutputStream out) throws SQLException, IOException {

        String id = req.getParameter("id");
        String shortName = req.getParameter("shortName");
        String regDate = req.getParameter("regDate");
        String regDateUntil = req.getParameter("regDateUntil");
        String code = req.getParameter("code");

       List<String> strings = new ArrayList<String>();

        if ((id != null)&&(id !="")) {strings.add("b.id like "+id);}
        if ((shortName != null)&&(shortName != "")) {strings.add("b.fullName like '%"+shortName+"%'");}
        if ((code != null)&&(code != "")) {strings.add("c.code like '"+code+"'");}
        if ((regDate != null)&&(regDateUntil != null)) {strings.add("b.publishDate between '"+regDate+"'");strings.add("'"+regDateUntil+"'");}
        if ((regDateUntil == null)&&(regDate != null)) {strings.add("b.publishDate >= '"+regDate+"'");}
        if ((regDateUntil != null)&&(regDate == null)) {strings.add("b.publishDate <= '"+regDateUntil+"'");}

        String criteria = Joiner.on(" and ").join(strings);


        String query = "select * from test.properties b join test.codes c on c.docId = b.id where "+criteria;

        JSONArray data = (JSONArray) Connector.Instance().executeQuery(query, new Object[]{}, new DBConnector.QueryToJsonObjectHandler(), Boolean.TRUE, struct);
        resp.setContentType("application/json; charset=UTF-8");
        out.write(data.toString().getBytes("UTF-8"));


    }
    public void process_getCategory(HttpServletRequest req, HttpServletResponse resp , OutputStream out) throws SQLException, IOException {

        ResultSet res = null;
        String class_id = req.getParameter("id");
        JSONArray data = (JSONArray) Connector.Instance().executeQuery("select *  from test.trees where class_id = "+class_id+"", new Object[]{}, new DBConnector.QueryToJsonObjectHandler(), Boolean.TRUE, struct);
        resp.setContentType("application/json; charset=UTF-8");
        out.write(data.toString().getBytes("UTF-8"));

    }
    public void process_getAllCat(HttpServletRequest req, HttpServletResponse resp , OutputStream out) throws SQLException, IOException {

        ResultSet res = null;
        JSONArray data = (JSONArray) Connector.Instance().executeQuery("select *  from test.trees", new Object[]{}, new DBConnector.QueryToJsonObjectHandler(), Boolean.TRUE, struct);
        resp.setContentType("application/json; charset=UTF-8");
        out.write(data.toString().getBytes("UTF-8"));

    }

    public void process_getClasses(HttpServletRequest req,HttpServletResponse resp, OutputStream out) throws SQLException, IOException {
        ResultSet res = null;
        JSONArray data = (JSONArray) Connector.Instance().executeQuery("select *  from test.classificators", new Object[]{}, new DBConnector.QueryToJsonObjectHandler(), Boolean.TRUE, struct);
        resp.setContentType("application/json; charset=UTF-8");
        out.write(data.toString().getBytes("UTF-8"));
    }

    public void process_getDocs(HttpServletRequest req,HttpServletResponse resp, OutputStream out) throws SQLException, IOException {

        ResultSet res = null;
        String tree_id = req.getParameter("id");
        JSONArray data = (JSONArray) Connector.Instance().executeQuery("select * from test.properties b join test.tree_document c on c.doc_id = b.id where c.tree_id ="+tree_id+"", new Object[]{}, new DBConnector.QueryToJsonObjectHandler(), Boolean.TRUE, struct);
        resp.setContentType("application/json; charset=UTF-8");
        out.write(data.toString().getBytes("UTF-8"));

    }

    protected void process_docLoad(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
        String id = req.getParameter("id");
        String data = (String) Connector.Instance().executeQuery("select body from test.documents where id="+id, new Object[]{}, new Connector.QueryHandler() {
            public Object onQuery(ResultSet rs, Object... params) throws SQLException {
                String val = "";
                if (rs.next()) {
                    try {
                        ByteArrayOutputStream bs = new ByteArrayOutputStream();
                        bs.reset();
                        LZMAHelper.Decompress(rs.getBinaryStream("body"), bs);
                        val = bs.toString("utf-8");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return val;
            }

        });
        out.write(data.getBytes("UTF-8"));
    }

    protected void process_docInfo(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
        String id = req.getParameter("id");
        try {
            JSONArray data = (JSONArray) Connector.Instance().executeQuery("select p.*  from test.properties p where p.id=?", new Object[]{id},new DBConnector.QueryToJsonObjectHandler(), Boolean.TRUE, struct);
            resp.setContentType("application/json; charset=UTF-8");
            out.write(data.toString().getBytes("UTF-8"));
        } catch (SQLException e) {
            e.printStackTrace();
            throw new ServletException(e);
        }
    }
    protected void process_docTree(HttpServletRequest req, HttpServletResponse resp, final OutputStream out)throws ServletException, IOException, SQLException {

        String id = req.getParameter("id");
        ResultSet res = null;
        JSONArray data = (JSONArray) Connector.Instance().executeQuery("SELECT * FROM test.trees a\n" +
                "join test.tree_document b on a.id = b.tree_id\n" +
                "where b.doc_id ="+id+";", new Object[]{}, new DBConnector.QueryToJsonObjectHandler(), Boolean.TRUE, struct);
        resp.setContentType("application/json; charset=UTF-8");
        out.write(data.toString().getBytes("UTF-8"));

    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        String rtype = req.getParameter("requestType");
        handler handler = handlers.get(rtype);
        OutputStream out = resp.getOutputStream();
        if (handler==null){
                resp.setStatus(500);
                //resp.sendError(500, " UNKNOWN REQUEST " + rtype);
                out.write((" UNKNOWN REQUEST " + rtype).getBytes());
                out.flush();
                out.close();
        }else
        try {
            handler.process(req,resp,out);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        out.flush();
        out.close();


    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

            doPost(req,resp);

    }

    public void print(OutputStream stream, String s) throws IOException {
        if (s == null) s = "null";
        int len = s.length();
        for (int i = 0; i < len; i++) {
            char c = s.charAt(i);
            if ((c & 65280) != 0) {
                ResourceBundle lStrings = ResourceBundle.getBundle("javax.servlet.LocalStrings");
                String errMsg = lStrings.getString("err.not_iso8859_1");
                Object errArgs[] = new Object[1];
                errArgs[0] = new Character(c);
                errMsg = MessageFormat.format(errMsg, errArgs);
                throw new CharConversionException(errMsg);
            }
            stream.write(c);
        }
    }


}
