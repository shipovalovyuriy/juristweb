package kz.novelty.jurist;

import SevenZip.LZMAHelper;
import org.apache.solr.handler.dataimport.Context;
import org.apache.solr.handler.dataimport.Transformer;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Map;

public class TransformerSpsDoc extends Transformer {
    private static final ThreadLocal<ByteArrayOutputStream> streamThreadLocal = new ThreadLocal<ByteArrayOutputStream>(){
        @Override
        protected ByteArrayOutputStream initialValue()
        {
            return new ByteArrayOutputStream();
        }
    };
    @Override
    public Object transformRow(Map<String, Object> stringObjectMap, Context context) {
        String doc = "";
        ByteArrayOutputStream bs = streamThreadLocal.get();
        byte[] zipBody = (byte[]) stringObjectMap.get("zipBody");

        if(zipBody==null || zipBody.length == 0) return stringObjectMap;

        ByteArrayInputStream bis = new ByteArrayInputStream(zipBody);
        try {
            bs.reset();
            LZMAHelper.Decompress(bis, bs);
            String body=bs.toString("utf-8");
            body = body.replaceAll("\\<.*?>"," ").replaceAll("\\&.*?\\;", " ").replaceAll("  ", " ");
            stringObjectMap.put("body", body);
            byte[] b = new byte[0];
            stringObjectMap.put("zipBody", b);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stringObjectMap;
    }
}
