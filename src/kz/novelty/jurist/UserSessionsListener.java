package kz.novelty.jurist;
/**
 * Created with IntelliJ IDEA.
 * User: Malik.N
 * Date: 04.02.13
 * Time: 14:43
 */

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

@WebListener
public class UserSessionsListener implements HttpSessionListener {

    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        httpSessionEvent.getSession().setMaxInactiveInterval(3600); // 1 час
    }

    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
        String id = httpSessionEvent.getSession().getId();
        Connector.Instance().removeUserSessionInfo(id, "Завершение сеанса");
    }
}
