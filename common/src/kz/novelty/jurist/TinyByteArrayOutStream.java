package kz.novelty.jurist;

import java.io.ByteArrayOutputStream;

/**
 * Created with IntelliJ IDEA.
 * User: Malik.N
 * Date: 11.03.13
 * Time: 14:57
 * To change this template use File | Settings | File Templates.
 */
public class TinyByteArrayOutStream extends ByteArrayOutputStream {
    public TinyByteArrayOutStream() {
        super();
    }

    public TinyByteArrayOutStream(int size) {
        super(size);
    }

    @Override
    public synchronized byte[] toByteArray() {
        return buf;
    }
}
