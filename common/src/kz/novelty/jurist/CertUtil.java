package kz.novelty.jurist;

import java.io.FileInputStream;
import java.security.*;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

public class CertUtil extends BaseCertUtil{
    protected String getPass(){
        return "1q2w3e";
    }

    protected String getAlias(){
        return System.getProperty("cert.alias");
    }

    protected KeyStore getStore(){
        try {
            KeyStore keystore = KeyStore.getInstance("PKCS12");
            // Loading the keystore
            keystore.load(new FileInputStream(System.getProperty("cert.file")), getPass().toCharArray());
            return keystore;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


}
