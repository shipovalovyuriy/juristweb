package kz.novelty.jurist;

import javax.net.ssl.*;
import java.io.*;
import java.net.*;
import java.util.List;
import java.util.Map;
import java.util.Set;

/*
String[][] parameters = new String[][]{
                new String[]{"var1","value1"},
                new String[]{"var2","value2"}
        };
* String res = URLRequester.executeRequest(localhost, SERVLET_NAME, UI_PATH, Arrays.asList(parameters));
*
* */
public class URLRequester {
    private static String localhost = "localhost";
    private static URLRequester instance;
    private static final String ACCESS_METHOD = "http://";

    public static URLRequester getInstance() {
        if (instance == null) {
            instance = new URLRequester();
        }
        return instance;
    }

    private URLRequester() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    public void checkClientTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }

                    public void checkServerTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }
                }
        };
        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
        }
    }

    public static byte[] executeRequestAsByte(String path, List<String[]> params) {
        return executeRequestAsByte(path, params, false);
    }
    public static byte[] executeRequestAsByte(String path, List<String[]> params, boolean ignoreResponse) {
        String request = (!path.startsWith("http") ? ACCESS_METHOD : "") + path.replaceAll(" ", "%20");
        try {
            return getInstance().sendPostRequest(request, params, null,ignoreResponse);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static byte[] executeRequestAsByte(String host, String cmd, String pathCGI, List<String[]> params) {
        return getInstance().executeRequestAsByte(host, cmd, pathCGI, params, null);
    }

    public InputStream executeRequestAsStream(String host, String cmd, String pathCGI, List<String[]> params, StringBuffer cookieBuffer, Map<String, String> headers) {
        if (cmd == null || "".equals(cmd.trim())) return null;
        cmd = cmd.replaceAll(" ", "%20");
        String request = ACCESS_METHOD + host + pathCGI + cmd;
        try {
            return sendPostRequestAsStream(request, params, cookieBuffer, headers, false);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            return null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public InputStream executeRequestAsStream(String host, String cmd, String pathCGI, List<String[]> params, StringBuffer cookieBuffer) {
        if (cmd == null || "".equals(cmd.trim())) return null;
        cmd = cmd.replaceAll(" ", "%20");
        String request = ACCESS_METHOD + host + pathCGI + cmd;
        try {
            return sendPostRequestAsStream(request, params, cookieBuffer, false);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            return null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public byte[] executeRequestAsByte(String host, String cmd, String pathCGI, List<String[]> params, StringBuffer cookieBuffer) {
        if (cmd == null || "".equals(cmd.trim())) return new byte[0];
        cmd = cmd.replaceAll(" ", "%20");
        String request = ACCESS_METHOD + host + pathCGI + cmd;
        try {
            return sendPostRequest(request, params, cookieBuffer, false);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            return "Time Out Error Occured".getBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new byte[0];
    }

    public static String executeRequest(String host, String cmd, String pathCGI, List<String[]> params, StringBuffer cookieBuffer) {
        if (cmd == null || "".equals(cmd.trim())) return "";
        try {
            byte[] out = getInstance().executeRequestAsByte(host, cmd, pathCGI, params, cookieBuffer);
            return new String(out);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String executeRequest(String host, String cmd, String pathCGI, List<String[]> params) {
        return executeRequest(host, cmd, pathCGI, params, null);
    }

    /**
     * would be executed with GET method
     *
     * @param cmd
     * @return
     */
    public static String executeRequest(String cmd) {
        return executeRequest(cmd, null);
    }

    /**
     * would be executed with POST method
     *
     * @param cmd
     * @param params
     * @return
     */
    public static String executeRequest(String cmd, List<String[]> params) {
        return executeRequest(cmd, "/cgi-bin/", params);
    }

    public static String executeRequest(String cmd, String pathCGI, List<String[]> params) {
        return executeRequest(localhost, cmd, pathCGI, params);
    }


    private boolean useProxy;
    private String proxyAdress;
    private int proxyPort;

    /**
     * acquire connection
     *
     * @param urlLocation request string
     * @return created connection Object
     * @throws java.io.IOException
     */
    private HttpURLConnection getConnection(String urlLocation) throws IOException {
        URL url = new URL(urlLocation);
        HttpURLConnection connection = null;
        if (useProxy) {
            connection = (HttpURLConnection) url.openConnection(new Proxy(java.net.Proxy.Type.HTTP, new InetSocketAddress(proxyAdress, proxyPort)));
        } else {
            connection = (HttpURLConnection) url.openConnection();
        }
        if (connection instanceof HttpsURLConnection) {
            ((HttpsURLConnection) connection).setHostnameVerifier(new HostnameVerifier() {
                public boolean verify(String s, SSLSession sslSession) {
                    return true;
                }
            });
        }
        connection.setDoInput(true);
        connection.setDoOutput(true);
        // setting these timeouts ensures the client does not deadlock indefinitely
        // when the server has problems.
        int timeoutMs = 600000; // 10 minutes
        connection.setConnectTimeout(timeoutMs);
        connection.setReadTimeout(timeoutMs);
        populateConnectionWithStandartProperties(connection);
        return connection;
    }

    /**
     * set common default settings
     *
     * @param connection set default parameters to given connection
     */
    private void populateConnectionWithStandartProperties(HttpURLConnection connection) {
        connection.setRequestProperty("Accept", "text/html");
        connection.setRequestProperty("Accept-Language", "en-US");
        connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.8.1.12) Gecko/20080201 Firefox Java");
//       connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        connection.setRequestProperty("Pragma", "no-cache");
        connection.setInstanceFollowRedirects(false);
    }

    /**
     * Reads coockies
     *
     * @param connection source connection
     * @return cookies
     */
    private String getResponseCookies(HttpURLConnection connection) {
        Map responseHeaders = connection.getHeaderFields();
        java.util.List responseCookies = (java.util.List) responseHeaders.get("Set-Cookie");
        String allCookies = "";
        if (responseCookies != null) {
            for (int i = 0; i < responseCookies.size(); i++) {
                allCookies = allCookies + responseCookies.get(i) + " ";
            }
        }
        return allCookies;
    }

    /**
     * make request
     *
     * @param urlLocation
     * @param parameters
     * @param cookieBuffer
     * @param cookieBuffer
     * @return
     * @throws java.io.IOException
     */
    private byte[] sendPostRequest(String urlLocation, java.util.List<String[]> parameters, StringBuffer cookieBuffer, boolean ignoreResponse) throws IOException {
        InputStream in = sendPostRequestAsStream(urlLocation, parameters, cookieBuffer, ignoreResponse);
        if(in==null) return new byte[0];
        // Read and display data from url
        byte buff[] = new byte[1024];
        int i;
        ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream(1024);
        while ((i = in.read(buff)) != -1) {
            arrayOutputStream.write(buff, 0, i);
        }
        in.close();
        return arrayOutputStream.toByteArray();

    }

    private InputStream sendPostRequestAsStream(String urlLocation, java.util.List<String[]> parameters, StringBuffer cookieBuffer, boolean ignoreResponse) throws IOException {
        return sendPostRequestAsStream(urlLocation, parameters, cookieBuffer, null, ignoreResponse);
    }

    private InputStream sendPostRequestAsStream(String urlLocation, java.util.List<String[]> parameters, StringBuffer cookieBuffer, Map<String, String> headers, boolean ignoreResponse) throws IOException {
        HttpURLConnection connection = getConnection(urlLocation);
        if (parameters == null || parameters.size() == 0) {
            connection.setRequestMethod("GET");
        } else {
            connection.setRequestMethod("POST");
        }
        connection.setRequestProperty("Referer", urlLocation);
//       connection.setRequestProperty("Cookie", "your cookies may be here");
        String data = "";
        if (parameters != null) {
            for (int i = 0; i < parameters.size(); i++) {
                String param[] = (String[]) parameters.get(i);
                if (i != 0) {
                    data = data + "&";
                }
                data = data + param[0] + "=" + URLEncoder.encode(param[1], "UTF-8");
            }

        }
        if (parameters != null && data.length() != 0) {
            connection.setRequestProperty("Content-Length", Integer.toString(data.length()));
        }
        connection.connect();
        if (parameters != null && data.length() != 0) {
            PrintWriter out = new PrintWriter(connection.getOutputStream());
            out.write(data);
            out.flush();
        }
        if (cookieBuffer != null) {
            String resCookie = getResponseCookies(connection);
            cookieBuffer.append(resCookie);
        }
        if (headers != null) {
            Map<String, List<String>> responseHeaders = connection.getHeaderFields();
            Set<Map.Entry<String, List<String>>> s = responseHeaders.entrySet();
            for (Map.Entry<String, List<String>> o : s) {
                StringBuilder bf = new StringBuilder();
                boolean first = true;
                for (String stringListEntry : o.getValue()) {
                    bf.append(stringListEntry);
                    if (!first) bf.append("\n");
                    first = false;
                }
                headers.put(o.getKey(), bf.toString());
            }
        }
        try {
            if(ignoreResponse) return null;
            return new MyInputStream(connection);
        }catch (IOException e){
            e.printStackTrace();
            InputStream eis = connection.getErrorStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(eis));
            String response = "";
            String line;
            while ((line = br.readLine()) != null){
                response += line;
            }
            throw new IOException(response);
        }
    }

    class MyInputStream extends InputStream {
        InputStream io;
        HttpURLConnection connection;

        @Override
        public int read() throws IOException {
            return io.read();
        }

        public MyInputStream(HttpURLConnection connection) throws IOException {
            super();
            this.connection = connection;
            this.io = connection.getInputStream();
        }

        @Override
        public int read(byte[] b) throws IOException {
            return io.read(b);
        }

        @Override
        public int read(byte[] b, int off, int len) throws IOException {
            return io.read(b, off, len);
        }

        @Override
        public long skip(long n) throws IOException {
            return io.skip(n);
        }

        @Override
        public int available() throws IOException {
            return io.available();    //To change body of overridden methods use File | Settings | File Templates.
        }

        @Override
        public void close() throws IOException {
            io.close();
            connection.disconnect();
        }

        @Override
        public void mark(int readlimit) {
            io.mark(readlimit);
        }

        @Override
        public void reset() throws IOException {
            io.reset();
        }

        @Override
        public boolean markSupported() {
            return io.markSupported();
        }
    }

}