package kz.novelty.jurist;

import java.io.*;
import java.security.PublicKey;

/**
 * User: Malik.N
 */
public abstract class BaseUpdateBuilder {

    public abstract BaseDesUtil getDes();

    protected abstract BaseCertUtil getCertUtil();

    /**
     * зашифивровать данные
     * @param data даннеы
     * @return зашифрованные Данные
     * @throws Exception
     */
    public byte[] crypt(byte[] data) throws Exception {
        return crypt(data, 0);
    }
    public byte[] crypt(byte[] data, int magicMode) throws Exception {
        byte[] encoded = getDes().crypt(data);//outStream.toByteArray();
        java.io.ByteArrayOutputStream outStreamEncoded = new java.io.ByteArrayOutputStream(512);
        DataOutputStream dsEncoded = new DataOutputStream(outStreamEncoded);
        // magic
        putMagic(dsEncoded, magicMode);
        dsEncoded.write(encoded);
        dsEncoded.close();
        return outStreamEncoded.toByteArray();
    }

    /**
     * записать заголовки для буера
     *
     * @param dsEncoded поток для вывода
     * @param magicMode
     * @throws IOException
     */
    protected void putMagic(DataOutputStream dsEncoded, int magicMode) throws IOException {
        // magic
        dsEncoded.writeInt(1229);
        // one more magic
        dsEncoded.writeInt(4);
    }

    /**
     * возвращает данные о лицензии
     * @return
     */
    protected byte[] getLicense() {
        return new byte[0];
    }

    /**
     * подписать данные и лицензию
     * @param data
     * @return
     * @throws Exception
     * @see BaseUpdateBuilder#getLicense
     */
    protected byte[] sign(byte[] data) throws Exception {
        byte[] license = getLicense();
        return sign(data, license);
    }

    /**
     * подписать данные и лицензию
     * @param data данные для подписи
     * @param license лицензия. если null, то не участвует в подписи
     * @return даные с подписью и лицензией. Если license null, то лицензия в буфере не участвует
     * @throws Exception
     */
    protected byte[] sign(byte[] data, byte[] license) throws Exception {
        BaseCertUtil util = getCertUtil();
        byte[] signature = util.signData(data, license);
        int size = signature == null ? 0 : signature.length;
        byte[] key = util.getPublicKeyToBytes();
        int sizeInfo = license == null ? 8 : (12 + license.length) ;
        ByteArrayOutputStream stream = new TinyByteArrayOutStream(data.length + size + key.length + sizeInfo);
        DataOutputStream ds = new DataOutputStream(stream);
        ds.write(data);
        if (null != license) {
            ds.write(license);
        }
        ds.writeInt(key.length);
        ds.write(key);
        ds.write(signature);
        // put license size
        if (null != license) {
            ds.writeInt(license.length);
        }
        //write signature size
        ds.writeInt(size + key.length);
        return stream.toByteArray();
    }

    /**
     * Проверяет подпись информации о лицензии
     * @param license информация о лицензии
     */
    protected void onLicenseCheck(byte[] license) throws Exception {

    }

    /**
     * проверяет подпись данных с учетом информации о лицензии
     * @param dec  разшифрованные данные
     * @return размер данных и размер области лицензии
     * @throws Exception
     */
    protected int[] checkSign(byte[] dec) throws Exception {
        return checkSign(dec, true);
    }

    /**
     *
     * @param dec расшифрованные данные
     * @param hasCert указывает содержит ли буфер информацию о лицензии
     * @return размер данных и размер области лицензии
     * @throws Exception
     */
    protected int[] checkSign(byte[] dec, boolean hasCert) throws Exception {
        int size = dec.length;
        ByteArrayInputStream is = new ByteArrayInputStream(dec, 0, dec.length);
        DataInputStream ds = new DataInputStream(is);
        int sizeInfo = (hasCert ? 12 : 8);
        ds.skip(dec.length - (sizeInfo - 4));
        int certSize = 0;
        if (hasCert) {
            certSize = ds.readInt();
        }
        int signSize = ds.readInt();
        if (signSize == 0) throw new Exception("Data not signed.");
        size -= certSize + signSize + sizeInfo;
        is.reset();
        is.skip(size);
        byte[] licenseData = null;
        int bytesRead = 0;
        if (hasCert) {
            licenseData = new byte[certSize];
            bytesRead = ds.read(licenseData);
            if (bytesRead != licenseData.length) throw new Exception("Signature verification error.");
        }
        int keySize = ds.readInt();
        byte[] keyData = new byte[keySize];
        byte[] signature = new byte[signSize - keySize];

        bytesRead = ds.read(keyData);
        if (bytesRead == 0) throw new Exception("Signature verification error.");

        bytesRead = ds.read(signature);
        if (bytesRead == 0) throw new Exception("Signature verification error.");

        BaseCertUtil util = getCertUtil();
        PublicKey pubKey = util.getPublicKeyFromBytes(keyData);
        boolean res = util.verifySign(dec, 0, size, signature, pubKey, licenseData);
        if (!res) throw new Exception("Signature is not valid.");
        if (hasCert) {
            onLicenseCheck(licenseData);
        }
        return new int[]{size, certSize};
    }


}
