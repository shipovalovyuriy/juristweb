package kz.novelty.jurist;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.spec.AlgorithmParameterSpec;

public class DesEncrypter {

    Cipher ecipher;
    Cipher dcipher;
    // Iteration count
    int iterationCount = 1;

    // Takes a 7-byte quantity and returns a valid 8-byte DES key.
    // The input and output bytes are big-endian, where the most significant
    // byte is in element 0.
    private byte[] addParity(byte[] in) {
        byte[] result = new byte[8];

        // Keeps track of the bit position in the result
        int resultIx = 1;

        // Used to keep track of the number of 1 bits in each 7-bit chunk
        int bitCount = 0;

        // Process each of the 56 bits
        for (int i = 0; i < 56; i++) {
            // Get the bit at bit position i
            boolean bit = (in[6 - i / 8] & (1 << (i % 8))) > 0;

            // If set, set the corresponding bit in the result
            if (bit) {
                result[7 - resultIx / 8] |= (1 << (resultIx % 8)) & 0xFF;
                bitCount++;
            }

            // Set the parity bit after every 7 bits
            if ((i + 1) % 7 == 0) {
                if (bitCount % 2 == 0) {
                    // Set low-order bit (parity bit) if bit count is even
                    result[7 - resultIx / 8] |= 1;
                }
                resultIx++;
                bitCount = 0;
            }
            resultIx++;
        }
        return result;
    }

    public DesEncrypter(byte[] rawBytes, byte[] salt) {
        try {
            byte[] keyBytes = addParity(rawBytes);
            SecretKey key = new SecretKeySpec(keyBytes, "DES");
//		    System.out.println("----------: "+convertToHex(keyBytes));
            ecipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
            dcipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
            AlgorithmParameterSpec paramSpec = new IvParameterSpec(salt);
            ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);
            dcipher.init(Cipher.DECRYPT_MODE, key, paramSpec);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

//        } catch (javax.crypto.NoSuchPaddingException e) {
//        } catch (java.security.NoSuchAlgorithmException e) {
//        } catch (java.security.InvalidKeyException e) {
//        }
    }

    public byte[] encrypt(byte[] in) {
        try {
            // Encode the string into bytes using utf-8
            //byte[] utf8 = str.getBytes("UTF8");

            // Encrypt
            byte[] enc = ecipher.doFinal(in);

            // Encode bytes to base64 to get a string
            return enc;//new sun.misc.BASE64Encoder().encode(enc);
        } catch (javax.crypto.BadPaddingException e) {
        } catch (IllegalBlockSizeException e) {
        }
        return null;
    }

    public byte[] decrypt(byte[] dec) {
        return decrypt(dec, 0, dec.length);
    }

    public byte[] decrypt(byte[] dec, int offset, int length) {
        try {
            byte[] utf8 = dcipher.doFinal(dec, offset, length);
            return utf8;
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String decryptStr(String str) {
        try {
            // Decode base64 to get bytes
            byte[] dec = new sun.misc.BASE64Decoder().decodeBuffer(str);

            // Decrypt
            byte[] utf8 = dcipher.doFinal(dec);

            // Decode using utf-8
            return new String(utf8, "UTF8");
        } catch (javax.crypto.BadPaddingException e) {
        } catch (IllegalBlockSizeException e) {
        } catch (UnsupportedEncodingException e) {
        } catch (java.io.IOException e) {
        }
        return null;
    }

    private String convertToHex(byte[] data) {
        StringBuffer buf = new StringBuffer();
        int counter = 0;
        for (int i = 0; i < data.length; i++) {
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                if ((0 <= halfbyte) && (halfbyte <= 9)) {
                    buf.append((char) ('0' + halfbyte));
                } else {
                    buf.append((char) ('a' + (halfbyte - 10)));
                }
                halfbyte = data[i] & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }
}
