package kz.novelty.jurist;

import java.io.DataOutputStream;

public abstract class BaseDesUtil {
    public byte[] crypt(byte[] data) throws Exception {
        DesEncrypter des = new DesEncrypter(CreateKeyBytes(), getSalt());
        byte[] encoded = des.encrypt(data);//outStream.toByteArray();
        return encoded;
    }

    public byte[] decrypt(byte[] data, int offset, int length) throws Exception {
        DesEncrypter des = new DesEncrypter(CreateKeyBytes(), getSalt());
        byte[] encoded = des.decrypt(data, offset, length);//outStream.toByteArray();
        return encoded;
    }

    protected abstract byte[] getSalt();

    protected abstract byte[] CreateKeyBytes();
}
