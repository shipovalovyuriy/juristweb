package kz.novelty.jurist;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

/**
 * User: Malik.N
 */
public abstract class BaseCertUtil {
    protected abstract String getPass();
    protected abstract String getAlias();
    protected abstract KeyStore getStore();

    /**
     * Алгоритм RSA
     */
    public static final String RSA_ALG = "SHA1withRSA";

    /**
     * Подписать данные без учета информации о лицензии
     * @param store хранилище сертификатов
     * @param buf данные для подписи
     * @see kz.novelty.jurist.BaseCertUtil#getAlias()
     * @see kz.novelty.jurist.BaseCertUtil#getPass()
     * @return цифровую подпись
     */
    private byte[] generateSign(KeyStore store, byte[] buf) {
        return generateSign(store, buf, null);
    }

    /**
     * Подписать данные
     * @param store хранилище сертификатов
     * @param buf данные для подписи
     * @param license данные о лицензии. Если null, то не участвует в подписи
     * @see kz.novelty.jurist.BaseCertUtil#getAlias()
     * @see kz.novelty.jurist.BaseCertUtil#getPass()
     * @return цифровую подпись
     */
    private byte[] generateSign(KeyStore store, byte[] buf, byte[] license) {
        try {
            PrivateKey privkey = (PrivateKey) store.getKey(getAlias(), getPass().toCharArray());
            Signature sgn = Signature.getInstance(RSA_ALG);
            sgn.initSign(privkey);
            sgn.update(buf);
            if(license != null){
                sgn.update(license);
            }
            return sgn.sign();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnrecoverableKeyException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * проверить подпись указанных данных
     * @param buf данные для проверки подписи
     * @param offset смещения с начала данных
     * @param length длина данных
     * @param signBuf подпись
     * @param pubKey публичный ключ
     * @param license информация о лицензии. Если null то не проверяется для подписи
     * @return true если подпись совпадает с данными
     * @throws Exception
     */
    public Boolean verifySign(byte[] buf, int offset,int length, byte[] signBuf, PublicKey pubKey,byte[] license) throws Exception {
        Signature sgn = Signature.getInstance(RSA_ALG);
        sgn.initVerify(pubKey);
        sgn.update(buf, offset, length);
        if(license != null){
            sgn.update(license);
        }
        return sgn.verify(signBuf);
    }

    /**
     * Сохранить публичный ключ как бинарные данные
     * @return
     */
    public byte[] getPublicKeyToBytes(){
        try {
            KeyStore store = getStore();
            X509Certificate oPublicCertificate = null;
            oPublicCertificate = (X509Certificate) store.getCertificate(getAlias());
            PublicKey pubKey = oPublicCertificate.getPublicKey();
            return pubKey.getEncoded();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Получить публичный ключ из буфера данных
     * @param encKey
     * @return
     */
    public PublicKey getPublicKeyFromBytes(byte[] encKey){
        try {
            X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(encKey);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PublicKey pubKey = keyFactory.generatePublic(pubKeySpec);
            return pubKey;
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * подписать данные
     * @param data данные для подписи
     * @param license данные о лицензииы
     * @return цифровую подпись
     * @see kz.novelty.jurist.BaseCertUtil#getStore()
     */
    public byte[] signData(byte[] data, byte[] license){
        KeyStore store = getStore();
        return generateSign(store, data, license);
    }

    public MessageDigest getDigest(String algorithm) {
        try {
            return MessageDigest.getInstance(algorithm);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e.getMessage());
        }
    }
    public byte[] sha256(byte data[]) {
        return getSha256Digest().digest(data);
    }

    public byte[] sha256(InputStream data)
            throws IOException {
        return digest(getSha256Digest(), data);
    }

    public byte[] sha256(String data) {
        try {
            return sha256(data.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }
    private MessageDigest getSha256Digest() {
        return getDigest("SHA-256");
    }
    public String toHex(byte[] data){
        BigInteger bi = new BigInteger(1, data);
        String result = bi.toString(16);
        if (result.length() % 2 != 0) {
            return "0" + result;
        }
        return result;
    }
    private static byte[] digest(MessageDigest digest, InputStream data)
            throws IOException {
        byte buffer[] = new byte[1024];
        for (int read = data.read(buffer, 0, 1024); read > -1; read = data.read(buffer, 0, 1024))
            digest.update(buffer, 0, read);

        return digest.digest();
    }

    public String sha256Hex(byte data[]) {
        return toHex(sha256(data));
    }

    public String sha256Hex(InputStream data)
            throws IOException {
        return toHex(sha256(data));
    }

    public String sha256Hex(String data) {
        return toHex(sha256(data));
    }

    public String byteToBase64(byte[] data){
        BASE64Encoder enc = new BASE64Encoder();
        return enc.encode(data);
    }

    public byte[] byteFromBase64(String data){
        BASE64Decoder enc = new BASE64Decoder();
        try {
            return enc.decodeBuffer(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static void copyStream(InputStream is, OutputStream out) throws IOException {
        int read = 0;
        final byte[] bytes = new byte[1024];
        while ((read = is.read(bytes)) != -1) {
            out.write(bytes, 0, read);
        }
    }

}
