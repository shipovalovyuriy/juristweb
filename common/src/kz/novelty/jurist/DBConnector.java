package kz.novelty.jurist;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.*;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: Malik.N
 * Date: 14.03.13
 * Time: 17:53
 * To change this template use File | Settings | File Templates.
 */
public class DBConnector {
    protected DataSource dataSource = null;
    protected Logger logger = null;

    public DBConnector() {
        this("jdbc/NoveltyDBJurist");
    }
    public DBConnector(String jndiName) {
        try {
            logger = Logger.getLogger(DBConnector.class.getName());
            InitialContext ctx = new InitialContext();
            dataSource = (DataSource) ctx.lookup(jndiName);
        } catch (NamingException e) {
            e.printStackTrace();
        }

    }
    public Connection GetConnection() throws SQLException {
        return dataSource.getConnection();
    }

    public void CloseConnection(Connection aConnection) {
        try {
            if (aConnection != null) {
                aConnection.close();
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
    }

    public interface QueryHandler {
        Object onQuery(ResultSet rs, Object... params) throws Exception;
    }

    public interface CallHandler {
        void onCall(CallableStatement st, Object... params) throws SQLException;
    }

    public static abstract class BatchCallHandler{
        private Connection conn;
        private DBConnector instance;
        public abstract void OnBatch() throws SQLException;
        final public void executeCallIgnoreException(String sql, CallHandler handler, Object... params) {
            try {
                instance.internalExecuteCall(conn, sql, handler, params);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        final public void executeCall(String sql, CallHandler handler, Object... params) throws SQLException {
            instance.internalExecuteCall(conn, sql, handler, params);
        }

        final public Object executeQuery(String sql, Object[] sqlParams, QueryHandler handler, Object... params) throws SQLException {
            return instance.internalExecuteQuery(conn, sql, sqlParams, handler, params);
        }
    }

    private void internalExecuteCall(Connection conn, final String sql, final CallHandler handler, final Object... params) throws SQLException {
        CallableStatement st = null;
        try {
            st = conn.prepareCall(sql);
            //st.setFetchSize(Integer.MIN_VALUE);
            handler.onCall(st, params);
        } catch (SQLException err) {
            logger.log(Level.WARNING, "call error " + sql, err);
            throw err;
        } finally {
            if (st != null) st.close();
        }
    }

    private Object internalExecuteQuery(Connection conn, String sql, Object[] sqlParams, QueryHandler handler, Object... params) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = conn.prepareStatement(sql);
            st.setFetchSize(Integer.MIN_VALUE);
            int i = 0;
            for (Object sqlParam : sqlParams) {
                st.setObject(++i, sqlParam);
            }
            rs = st.executeQuery();
            return handler.onQuery(rs, params);
        } catch (SQLException err) {
            logger.log(Level.WARNING, "query error " + sql, err);
            throw err;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) rs.close();
            if (st != null) st.close();
        }
        return null;
    }

    public void executeCallIgnoreException(String sql, CallHandler handler, Object... params) {
        try {
            executeCall(sql, handler, params);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void executeCall(final String sql, final CallHandler handler, final Object... params) throws SQLException {
        Connection conn = GetConnection();
        try {
            internalExecuteCall(conn, sql, handler, params);
        } finally {
            CloseConnection(conn);
        }
    }

    /**
     * Игнорирует ошибки
     * @param handler
     * @see DBConnector#executeBatch(kz.novelty.jurist.DBConnector.BatchCallHandler)
     */
    public void executeBatchIgnoreException(BatchCallHandler handler) {
        try {
            executeBatch(handler);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Выполнить группу операций на одном соединении
     * @param handler
     * @throws SQLException
     */
    public void executeBatch(BatchCallHandler handler) throws SQLException {
        handler.conn = GetConnection();
        handler.instance = this;
        try{
            handler.OnBatch();
        }finally {
            CloseConnection(handler.conn);
        }
    }

    public String getParamStr(int count){
        String res = "";
        for (int i = 0; i < count; i++) {
            res += ( i > 0 ?",": "" ) + "?";
        }
        return res;
    }

    public Object executeQuery(String sql, Object[] sqlParams, QueryHandler handler, Object... params) throws SQLException {
        Connection conn = GetConnection();
        try {
            return internalExecuteQuery(conn, sql, sqlParams, handler, params);
        } catch (SQLException err) {
            logger.log(Level.WARNING, "query error " + sql, err);
            throw err;
        } finally {
            CloseConnection(conn);
        }
    }

    /**
     * возвращает JSON массив объектов
      */
    public static class QueryToJsonObjectHandler implements QueryHandler {
        public Object onRowObject(ResultSet rs, Object row) throws SQLException {
            return row;
        }

        public Object onQuery(ResultSet rs, Object... params) throws SQLException {
            JSONArray arr = null;
            arr = params != null && params.length > 0 && params[0] instanceof JSONArray ? (JSONArray) params[0] : new JSONArray();
            ResultSetMetaData meta = rs.getMetaData();
            List<String> columns = new Vector<String>(meta.getColumnCount());
            for (int i = 1, len = meta.getColumnCount(); i <= len; i++) {
                String colName = meta.getColumnName(i);
                columns.add(colName);
            }
            int len = columns.size();
            while (rs.next()) {
                JSONObject row = new JSONObject();
                for (int i = 0; i < len; i++) {
                    String colName = columns.get(i);
                    row.put(colName, rs.getString(colName));
                }
                onRowObject(rs, row);
                arr.add(row);
            }
            return arr;
        }
    };

    /**
     * возвращает JSON объект где каждое поле может быть массивом
     */
    public static class QueryToJsonObjectAccumulatedHandler implements QueryHandler {
        public Object onQuery(ResultSet rs, Object... params) throws SQLException {
            JSONObject arr = null;
            arr = params != null && params.length > 0 && params[0] instanceof JSONObject ? (JSONObject) params[0] : new JSONObject();
            ResultSetMetaData meta = rs.getMetaData();
            List<String> columns = new Vector<String>(meta.getColumnCount());
            for (int i = 1, len = meta.getColumnCount(); i <= len; i++) {
                String colName = meta.getColumnName(i);
                columns.add(colName);
            }
            while (rs.next()) {
                for (int i = 0, len = columns.size(); i < len; i++) {
                    String colName = columns.get(i);
                    arr.accumulate(colName, rs.getString(colName));
                }
            }
            return arr;
        }
    };
    /**
     * возращает вручную заполненную JSON строку
     */
    public QueryHandler QueryToJsonHandler = new QueryHandler() {
        public String getJsonRowFormat(List<String> columns, boolean asJsObject) throws SQLException{
            int j = 0;
            String rowFormat = "";
            for (int i = 1, len = columns.size(); i <= len; i++) {
                String colName = columns.get(i);
                rowFormat += (i > 1 ? "," : "") + (asJsObject ? (colName + ":") : "") + "'%" + (++j) + "$s'";
            }
            return rowFormat;
        }
        public String getJsonRow(List<String> columns, ResultSet rs, boolean asJsObject) throws SQLException{
            return getJsonRow(columns,rs,getJsonRowFormat(columns,asJsObject),new String[columns.size()],asJsObject, 0);
        }

        public String getJsonRow(List<String> columns, ResultSet rs,String rowFormat, String[] values, boolean asJsObject, final int i) throws SQLException{
            String defaultValue = null;
            String row = "";
            int j = 0;
            for (String colName : columns) {
                String s1 = rs.getString(colName);
                if (s1 != null) s1 = s1.replaceAll("''", "\\\"").replaceAll("'", "\\\\\'");
                values[j++] = s1;
            }
            if (i > 0) {
                row += ",";
            } else {
                if (null == defaultValue || defaultValue.isEmpty()) defaultValue = values[0];
            }
            row += (asJsObject ? "{" : "[") + String.format(rowFormat, (Object[])values) + (asJsObject ? "}" : "]");
            row = row.replaceAll("\"", "\\\"");
            while (row.contains("\n")) row = row.replaceAll("\n", " ");
            while (row.contains("\r")) row = row.replaceAll("\r", " ");
            return row;
        }

        public Object onQuery(ResultSet rs, Object... params)
                throws SQLException {
            StringBuilder data = new StringBuilder(1000);
            data.append("[");
            int j = 0;
            ResultSetMetaData meta = rs.getMetaData();
            List<String> columns = new Vector<String>(meta.getColumnCount());
            String s = "[";
            boolean asJsObject = params != null && params.length > 0 && params[0] instanceof Boolean && params[0] == Boolean.TRUE;
            StringBuilder structBuilder = params != null && params.length > 1 && params[1] instanceof StringBuilder ? (StringBuilder) params[1] : null;
            String rowFormat = "";
            for (int i = 1, len = meta.getColumnCount(); i <= len; i++) {
                String colName = meta.getColumnName(i);
                columns.add(colName);
            }
            for (int i = 0, len = columns.size(); i < len; i++) {
                String colName = columns.get(i);
                s += i > 0 ? "," : "";
                s += "{name:'" + colName + "'}";
                rowFormat += (i > 0 ? "," : "") + (asJsObject ? (colName + ":") : "") + "'%" + (++j) + "$s'";
            }
            s += "]";
            String[] values = new String[columns.size()];
            int i = 0;
            String defaultValue = null;
            while (rs.next()) {
                String row = getJsonRow(columns, rs, rowFormat, values,asJsObject, i++);
                data.append(row);
            }
            data.append("]");
            if (structBuilder != null) structBuilder.append(s);
            return data.toString();
        }
    };


}
