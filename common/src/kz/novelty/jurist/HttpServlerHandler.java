package kz.novelty.jurist;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.*;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.zip.GZIPOutputStream;

public class HttpServlerHandler<E> extends HttpServlet {
    static public interface handler {
        void process(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException;
    }

    protected HashMap<String, handler> handlers = new HashMap<String, handler>();

    public HttpServlerHandler() {
        super();
    }

    protected void beforeProcess(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }
    protected String getParameter(HttpServletRequest request, String param) throws IOException, ServletException {
        if(isMultipartContent(request)){
            Part part = request.getPart(param);
            ByteArrayOutputStream stream = new TinyByteArrayOutStream();
            InputStream filecontent = part.getInputStream();
            int read = 0;
            final byte[] bytes = new byte[1024];

            while ((read = filecontent.read(bytes)) != -1) {
                stream.write(bytes, 0, read);
            }
            if (stream != null) {
                stream.close();
            }
            return new String(stream.toByteArray(),0, stream.size());
        }else{
            return request.getParameter(param);
        }
    }

    public void setUserInfo(HttpServletRequest req, HttpServletResponse resp, E user) {
        HttpSession session = req.getSession(true);
        Cookie sessionCookie = new Cookie("JSESSIONID", session.getId());
        sessionCookie.setPath("/");
        resp.addCookie(sessionCookie);
        session.setAttribute("user", user);
        String sess = session.getId();
    }

    /**
     * возвращает информацию о текущем пользователе
     *
     * @param req
     * @param resp
     * @return
     */
    public E getUserInfo(HttpServletRequest req, HttpServletResponse resp) {
        HttpSession session = req.getSession(true);
        String sess = session.getId();
        E user = (E) session.getAttribute("user");
        return user;
    }

    public static boolean isMultipartContent(HttpServletRequest request) {
        if (!"post".equals(request.getMethod().toLowerCase()))
            return false;
        String contentType = request.getContentType();
        if (contentType == null)
            return false;
        return contentType.toLowerCase().startsWith("multipart/");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String rtype = getParameter(req,"requestType");
        handler handler = handlers.get(rtype);
        OutputStream out = resp.getOutputStream();
        OutputStream outOrig = out;
        String encoding = req.getHeader("Accept-Encoding");
        if (encoding != null && encoding.indexOf("gzip") >= 0) {  // gzip broser?
            resp.setHeader("Content-Encoding", "gzip");
            out = new GZIPOutputStream(out);
        }
        beforeProcess(req, resp);
        if (handler == null) {
            resp.setStatus(500);
            //resp.sendError(500, " UNKNOWN REQUEST " + rtype);
            out.write((" UNKNOWN REQUEST " + rtype).getBytes());
            out.flush();
            out.close();
        } else try {
            handler.process(req, resp, out);
        } catch (Exception e) {
            e.printStackTrace();
            resp.setStatus(500);
            //resp.sendError(500, e.getMessage());
            out.write(e.getMessage().getBytes());
            out.flush();
            out.close();
        }
        out.flush();
        out.close();
        if (outOrig != out) outOrig.close();
    }

    public void print(OutputStream stream, String s) throws IOException {
        if (s == null) s = "null";
        int len = s.length();
        for (int i = 0; i < len; i++) {
            char c = s.charAt(i);
            if ((c & 65280) != 0) {
                ResourceBundle lStrings = ResourceBundle.getBundle("javax.servlet.LocalStrings");
                String errMsg = lStrings.getString("err.not_iso8859_1");
                Object errArgs[] = new Object[1];
                errArgs[0] = new Character(c);
                errMsg = MessageFormat.format(errMsg, errArgs);
                throw new CharConversionException(errMsg);
            }
            stream.write(c);
        }
    }



}
