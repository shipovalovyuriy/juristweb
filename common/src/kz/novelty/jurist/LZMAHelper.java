package kz.novelty.jurist;


import java.io.InputStream;
import java.io.OutputStream;

public class LZMAHelper {
    public static void compress(InputStream in, OutputStream out) throws Exception {
        SevenZip.Compression.LZMA.Encoder encoder = new SevenZip.Compression.LZMA.Encoder();

        //params
        int DictionarySize = 1 << 21;
        int Lc = 3;
        int Lp = 0;
        int Pb = 2;
        int Fb = 128;
        boolean FbIsDefined = false;
        boolean eos = true;
        int Algorithm = 2;
        int MatchFinder = 1;

        if (!encoder.SetAlgorithm(2)) {
            throw new Exception("Incorrect compression mode");
        }
        if (!encoder.SetDictionarySize(DictionarySize)) {
            throw new Exception("Incorrect dictionary size");
        }
//        if (!encoder.SeNumFastBytes(Fb)) {
//            throw new Exception("Incorrect -fb value");
//        }
        if (!encoder.SetMatchFinder(MatchFinder)) {
            throw new Exception("Incorrect -mf value");
        }
        if (!encoder.SetLcLpPb(Lc, Lp, Pb)) {
            throw new Exception("Incorrect -lc or -lp or -pb value");
        }

        encoder.SetEndMarkerMode(eos);
        encoder.WriteCoderProperties(out);
        long fileSize;
        if (eos) {
            fileSize = -1;
        } else {
            fileSize = in.available();
        }
        for (int i = 0; i < 8; i++) {
            out.write((int) (fileSize >>> (8 * i)) & 0xFF);
        }
        encoder.Code(in, out, -1, -1, null);

        out.flush();
        //outStream.close();
        in.close();
    }

    public static void decompress(InputStream in, OutputStream out) throws Exception {
        int propertiesSize = 5;
        byte[] properties = new byte[propertiesSize];
        if (in.read(properties, 0, propertiesSize) != propertiesSize) {
            throw new Exception("input .lzma file is too short");
        }
        SevenZip.Compression.LZMA.Decoder decoder = new SevenZip.Compression.LZMA.Decoder();
        if (!decoder.SetDecoderProperties(properties)) {
            throw new Exception("Incorrect stream properties");
        }
        long outSize = 0;
        for (int i = 0; i < 8; i++) {
            int v = in.read();
            if (v < 0) {
                throw new Exception("Can't read stream size");
            }
            outSize |= ((long) v) << (8 * i);
        }
        if (!decoder.Code(in, out, outSize)) {
            throw new Exception("Error in data stream");
        }

        out.flush();
        //out.close();
        in.close();
    }

    public static byte[] compress(byte[] data) throws Exception {
        java.io.InputStream inStream = new java.io.ByteArrayInputStream(data);
        return compress(inStream);
    }

    public static byte[] compress(java.io.InputStream inStream) throws Exception {
        java.io.ByteArrayOutputStream outStream = new java.io.ByteArrayOutputStream(512);
        LZMAHelper.compress(inStream, outStream);
        return outStream.toByteArray();

    }
    public static byte[] decompress(byte[] data) throws Exception {
        java.io.InputStream inStream = new java.io.ByteArrayInputStream(data);
        return decompress(inStream);
    }

    public static byte[] decompress(java.io.InputStream inStream) throws Exception {
        java.io.ByteArrayOutputStream outStream = new java.io.ByteArrayOutputStream(512);
        LZMAHelper.decompress(inStream, outStream);
        return outStream.toByteArray();

    }
}
