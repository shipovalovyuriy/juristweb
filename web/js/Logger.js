﻿/* $Id: Logger.js 2631 2011-05-25 14:44:21Z nurbol $ */
var Logger = function () {
    return {
        timers: [],
        logType : 5,// текущий уровень лога
        logDebug : 1, // отладочный лог
        logTracer : 2, // лог триггера
        logTimer : 4, // лог таймера
        message: function (obj, logType) {
            var consoleFound = (window.console) ? true : false;
            if (consoleFound && (this.logType & logType) ) {
                console.log(obj);
            }
        }
        , timerStart: function (msg) {
            var date1=new Date();
            this.timers.push(date1.getTime());
            this.message('Timer ' + msg+' ' + date1, this.logTimer);
        }
        , timerStop: function (msg) {
            var timeEnd = new Date().getTime();
            var timer = this.timers.pop();
            if (timer) {
                this.message('Timer ' + msg + ' time: ' + (timeEnd - timer), this.logTimer);
            } else {
                this.message('Timer ' + msg + ' notimer ', this.logTimer);
            }
        }

    }
} ();
