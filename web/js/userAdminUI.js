var AdminUIList = {};

function UserAdminUI(user) {
    this.user = user;
    this.comboTree = createComboBoxRemoteTree('trees', 'name', 'id', 'id', 'parent_id', 'sort_index');
    this.treeStore = this.comboTree.store;
    this.id = Ext.id();
    AdminUIList[this.id] = this;
}

UserAdminUI.prototype.hideUI = function () {
    if(this.pWindow)this.pWindow.hide();
};

UserAdminUI.prototype.showUI = function () {
    this.buildUI();
    this.pWindow.show();
    this.pWindow.doLayout();
};

UserAdminUI.prototype.showUser = function () {
    this.panel.ownerCt.setActiveTab(1);
};

function getAdminUI(id) {
    return AdminUIList[id];
}

function getRoleTreeRenderer(self) {
    var selfID = self.id;
    var comboTree = self.comboTree;
    return function (value, metaData, record, rowIndex, colIndex, store) {
        var data = record.json || record.data;
        var node = comboTree.findNodeByIdInIndex(value)
        var tmp = node && node.text || value;
        //tmp = "<a href='#' class='lnk_style_default' name='" + data.role_id + "' onclick='onRoleNameClick(this,\""+selfID+"\");return false'>" + value + "</a>";
        return tmp;
    }
}

function getStoreRenderer(self, locStore) {
    var selfID = self.id;
    var store1 = locStore;
    return function (value, metaData, record, rowIndex, colIndex, store) {
        var data = record.json || record.data;
        for (var i = 0, il = store1.data.items.length; i < il; i++) {
            var rec = store1.data.items[i].data;
            if (rec.id == value) {
                return rec.value;
            }
        }
        return value;
    }
}

function passwordRenderer(value, metaData, record, rowIndex, colIndex, store) {
    return '***';
}

UserAdminUI.prototype.createXmlStore = function (name, fields, groupable) {
    // create the Data Store
    var storeCons = groupable === true ? Ext.data.GroupingStore : Ext.data.Store;
    var store = new storeCons({
        // destroy the store if the grid is destroyed
        autoDestroy: true,
        proxy: new Ext.data.HttpProxy({ url: 'RequestHandler', timeout: 300000 }),
        baseParams: {
            proc: name,
            par: '',
            requestType: 'proc'
        },
        reader: new Ext.ux.XmlReader({
            totalProperty: "total",
            record: 'row',
            fields: fields || [
                {name: 'id', type: 'string'},
                {name: 'value', type: 'string'}
            ]
        })
    });
    return store;
};

UserAdminUI.prototype.refreshUI = function () {
    this.roleGrid.store.reload({});
    this.roleTreeGrid.store.reload({});
    this.userGrid.store.reload({});
};

UserAdminUI.prototype.onRecordSelect = function (grid, rowIndex, columnIndex, e) {
    for (var i = 0; i < grid.selModel.selections.items.length; i++) {
        var selectRec = grid.selModel.selections.items[i];
        var data = selectRec.json || selectRec.data;
        var thisRef = grid.extraData.thisRef;
        var selId = data.id || data.user_id;
        var store = grid.extraData.refreshStore;
        var lastRole = store.lastOptions && store.lastOptions.params && store.lastOptions.params.par;
        if (selId != lastRole) {
            store.load({ params: { par: selId } });
        }
        if(grid.form){
            grid.form.loadFormRecord(selectRec);
        }
    }
}

UserAdminUI.prototype.delRecord = function (scope) {
    var extraData = scope.extraData;
    var store = extraData.store;
    var grid = store.extraData.grid;
    var methodDel = store.extraData.methodDel;
    if (!isStrNullOrEmpty(methodDel) && grid.selModel && grid.selModel.selections.items && grid.selModel.selections.items.length > 0 && confirm('Вы действительно хотите удалить запись?')) {
        var recs = [];
        var selectRec = grid.selModel.selections.items[0];
        recs.push(selectRec && selectRec.data);
        var lastRole = store.lastOptions && store.lastOptions.params && store.lastOptions.params.par || '';
        var obj = {obj: {count: recs.length, parent: lastRole, row: recs}};
        var xml = json2xml(obj);
        callRequestHandler("proc", {proc: methodDel, par: xml}, CallClassMethodAfterAjaxReturn, CallClassMethodAfterAjaxReturn, {thisRef: this, thisMethod: 'onRecordSave', store: store});
        return;
    }
    for (var i = 0; i < grid.selModel.selections.items.length; i++) {
        var selectRec = grid.selModel.selections.items[i];
        grid.stopEditing();
        store.remove(selectRec);
        grid.startEditing(0, 0);
    }
};

UserAdminUI.prototype.addRecord = function (scope) {
    var extraData = scope.extraData;
    var store = extraData.store;
    var grid = store.extraData.grid;
    var ObjType = store.recordType;
    var obj = new ObjType({id: 0, value: 'Нажмите чтоб выбрать', login: 'Нажмите чтоб выбрать'});
    grid.stopEditing();
    store.insert(0, obj);
    grid.fireEvent('rowmousedown', grid, 0 ,{button:0});
    if(grid.colModel.isCellEditable(0, 0)){
        grid.startEditing(0, 0);
        grid.fireEvent('cellclick', grid, 0, 0)
    }else{
        grid.startEditing(0, 1);
        grid.fireEvent('cellclick', grid, 0, 1)
    }
};

UserAdminUI.prototype.onRecordSave = function (res, scope) {
    var store = scope.store;
    var lastRole = store.lastOptions && store.lastOptions.params && store.lastOptions.params.par;
    store.load({ params: { par: lastRole} });
};

UserAdminUI.prototype.onSessionEnd = function (res, scope) {

};

UserAdminUI.prototype.closeUserSession = function (scope, event, record) {
    var extraData = scope.extraData;
    var store = extraData.store;
    var grid = store.extraData.grid;
    if (grid.selModel && grid.selModel.selections.items && grid.selModel.selections.items.length > 0 && confirm('Вы действительно хотите завершить сессию пользователя?')) {
        var selectRec = grid.selModel.selections.items[0];
        callRequestHandler("endUserSession", {id:selectRec && selectRec.data && selectRec.data.session }, CallClassMethodAfterAjaxReturn, CallClassMethodAfterAjaxReturn, {thisRef: this, thisMethod: 'onSessionEnd', store: store});

    }

};

UserAdminUI.prototype.saveRecord = function (scope, event, record) {
    var extraData = scope.extraData;
    var store = extraData.store;
    var grid = store.extraData.grid;
    var methodSave = store.extraData.methodSave;
    var saveSelectedOnly = store.extraData.saveSelectedOnly;
    var lastRole = store.lastOptions && store.lastOptions.params && store.lastOptions.params.par || '';
    var recs = [];
    if(record != null) {
        recs.push(record);
    }else if (!saveSelectedOnly) {
        for (var i = 0, il = store.data.items.length; i < il; i++) {
            var rec = store.data.items[i].data;
            recs.push(rec);
        }
    } else {
        for (var i = 0; i < grid.selModel.selections.items.length; i++) {
            var selectRec = grid.selModel.selections.items[i].data;
            recs.push(selectRec);
        }
    }
    var obj = {obj: {count: recs.length, parent: lastRole, row: recs}};
    var xml = json2xml(obj);
    callRequestHandler("proc", {proc: methodSave, par: xml}, CallClassMethodAfterAjaxReturn, CallClassMethodAfterAjaxReturn, {thisRef: this, thisMethod: 'onRecordSave', store: store});
};

UserAdminUI.prototype.getGridButtons = function (store, canAdd, canDel, canSave, customForm) {
    var res = [];
    if (canAdd)res.push({icon: 'images/plus.png', handler: this.addRecord, scope: this, tooltip: 'Добавить', tooltipType: 'title', extraData: {store: store}});
    if (canDel)res.push({icon: 'images/minus.png', handler: this.delRecord, scope: this, tooltip: 'Удалить', tooltipType: 'title', extraData: {store: store}});
    if (canSave)res.push({icon: 'images/save_small.png', handler: this.saveRecord, scope: this, tooltip: 'Сохранить', tooltipType: 'title', extraData: {store: store}});
    if (customForm) {
        var self = this;
        var obj = {extraData: {store: store}};
        customForm.on('save', function (scope, record) {
            var data = record.data;
            for(var val in data ){
                if(!data.hasOwnProperty(val)) continue;
                if (data[val] instanceof Date) {
                    data[val] = data[val].format('Y-m-d');//2011-12-22
                }
            }
            self.saveRecord(obj, null, data);
        });
    }
    return res;
};

UserAdminUI.prototype.validateUsersEdit = function (e) {
    if (e.value instanceof Date) {
        e.value = e.value.format('Y-m-d');//2011-12-22
    }
};

UserAdminUI.prototype.buildUserUI = function () {
    var storeUserTypes = this.createXmlStore("getUserTypes");
    var editorUserTypes = createComboBoxLocal(storeUserTypes, "value", "id", true, null, false);
    var formEditorUserTypes = createComboBoxLocal(storeUserTypes, "value", "id", true, null, false);
    var editorUserTypesFilter = createComboBoxLocal(storeUserTypes, "value", "id", true, null, false);
    var comboRoles = createComboBoxLocal(this.storeRoles, "value", "id", true, null, false);
    this.storeUserTypes = storeUserTypes;
    var store = this.createXmlStore('getUsers', [
        {name: 'user_id', mapping: 'u'},
        {name: 'login', mapping: 'l'},
        {name: 'online', mapping: 'ls'},
        {name: 'session', mapping: 'lsess'},
        {name: 'pass'},
        {name: 'user_type_id', mapping: 'ut'},
        {name: 'valid_from', mapping: 'vf'},
        {name: 'valid_till', mapping: 'vt'},
        {name: 'email', mapping: 'e'},
        {name: 'name', mapping: 'n'},
        {name: 'serial', mapping: 's'},
        {name: 'rnn', mapping: 'r'},
        {name: 'company', mapping: 'c'}
    ], true);
    store.remoteSort = true;
    var columns = [];
    var dateFormat = 'd.m.Y';
    var dbDateFormat = 'Y-m-d';
    columns.push({ header: 'Онлайн', width: 50, dataIndex: 'online', sortable: true, hidden: false/*, editor:new Ext.form.TextField({})*/ /*, renderer: getRoleNameRenderer(this)*/ });
    columns.push({ header: 'Ник пользователя', width: 140, dataIndex: 'login', sortable: true, hidden: false, editor: new Ext.form.TextField({}) /*, renderer: getRoleNameRenderer(this)*/ });
    columns.push({ header: 'Пароль', width: 60, dataIndex: 'pass', sortable: true, hidden: false, editor: new Ext.form.TextField({inputType: 'password'}), renderer: passwordRenderer });
    columns.push({ header: 'Тип пользователя', width: 100, dataIndex: 'user_type_id', sortable: true, hidden: false, editor: editorUserTypes, renderer: getStoreRenderer(this, storeUserTypes) });
    columns.push(new Ext.grid.DateColumn({ header: 'Активен с', width: 70, dataIndex: 'valid_from', sortable: true, hidden: false, format: dateFormat, editor: new Ext.form.DateField({ format: dateFormat, altFormats: altDateFormats })}));
    columns.push(new Ext.grid.DateColumn({ header: 'Активен до', width: 70, dataIndex: 'valid_till', sortable: true, hidden: false, format: dateFormat, editor: new Ext.form.DateField({ format: dateFormat, altFormats: altDateFormats })}));
    columns.push({ header: 'E-mail пользователя', width: 140, dataIndex: 'email', sortable: true, hidden: false, editor: new Ext.form.TextField({}) });
    columns.push({ header: 'Имя пользователя', dataIndex: 'name', sortable: true, hidden: false, editor: new Ext.form.TextField({}) });
    columns.push({ header: 'Кодовое слово', width: 90, dataIndex: 'serial', sortable: true, hidden: false, editor: new Ext.form.TextField({}) });
    columns.push({ header: 'РНН', width: 90, dataIndex: 'rnn', sortable: true, hidden: false, editor: new Ext.form.TextField({}) });
    columns.push({ header: 'Название компании', dataIndex: 'company', sortable: true, hidden: false, editor: new Ext.form.TextField({}) });
    var users_per_page = 50;
    var filters = new Ext.ux.grid.GridFilters({local: false, menuFilterText: 'Фильтры',
        filters: [
            {type: 'combo', dataIndex: 'user_type_id', editor: editorUserTypesFilter},
            {type: 'string', dataIndex: 'login'},
            {type: 'string', dataIndex: 'name'},
            {type: 'date', dateFormat: dbDateFormat, dataIndex: 'valid_from'},
            {type: 'date', dateFormat: dateFormat, dataIndex: 'valid_till'},
            {type: 'string', dataIndex: 'email'},
            {type: 'string', dataIndex: 'name'},
            {type: 'string', dataIndex: 'rnn'},
            {type: 'string', dataIndex: 'company'},
            {type: 'string', dataIndex: 'serial'}
        ],
        buildQuery: function (filters, store, options) {
            var p = {};
            var limit = options.params && options.params.limit || users_per_page;
            var start = options.params && options.params.start || 0;
            var sort = options.params && options.params.sort || 'login';
            var dir = options.params && options.params.dir || 'asc';
            var cnt = filters.length;
            var tmp = '';
            for (var i = 0, len = filters.length; i < len; i++) {
                var f = filters[i];
                var fldName = f.field;
                var fldValue = f.data.value;
                var fldCmp = f.data.comparison || 'like';
                var fldType = f.data.type;
                if (typeof fldValue == 'string') {
                    fldValue = fldValue.replace(new RegExp("&", "g"), "&amp;").replace(new RegExp("'", "g"), "");
                    if (fldCmp == 'like') fldValue = fldValue + '%';
                }
                tmp += "<p type='" + fldType + "' cmp='" + fldCmp + "' name='" + fldName + "'>" + fldValue + "</p>";
                var root = [this.paramPrefix, '[', i, ']'].join('');
                p[root + '[field]'] = f.field;
                var dataPrefix = root + '[data]';
                for (var key in f.data)
                    p[[dataPrefix, '[', key, ']'].join('')] = f.data[key];
            }
            p.par = "<filter><cnt>" + cnt + "</cnt><start>" + start + "</start><limit>" + limit + "</limit><sort>" + sort + "</sort><dir>" + dir + "</dir>" + tmp + "</filter>";
            return p;
        }});

    // form edit fields
    var fields = [];
    fields.push({fieldLabel: 'Ник пользователя', name: 'login'});
    fields.push({fieldLabel: 'E-mail пользователя', name: 'email', vtype: 'email'});
    fields.push({fieldLabel: 'Пароль', name: 'pass',inputType: 'password'});
    fields.push({fieldLabel: 'Имя пользователя', name: 'name'});
    formEditorUserTypes.fieldLabel = 'Тип пользователя';
    formEditorUserTypes.name = 'user_type_id';
    fields.push(formEditorUserTypes);
    fields.push({xtype: 'datefield', fieldLabel: 'Активен с', name: 'valid_from',format: dateFormat,altFormats: altDateFormats});
    fields.push({xtype: 'datefield', fieldLabel: 'Активен до', name: 'valid_till',format: dateFormat,altFormats: altDateFormats});
    fields.push({fieldLabel: 'Кодовое слово', name: 'serial'});
    fields.push({fieldLabel: 'РНН', name: 'rnn'});
    fields.push({fieldLabel: 'Название компании', name: 'company'});

    this.userForm = createGridEditFromPanel(fields);
    var userGridButtons = this.getGridButtons(store, true, true, true, this.userForm);
    userGridButtons.push({icon: 'images/delete.png', handler: this.closeUserSession, scope: this, tooltip: 'Завершить сессию пользователя', tooltipType: 'title', extraData: {store: store}});

    this.userGrid = createGridEditablePanel(columns, store, userGridButtons, [filters], users_per_page);
    this.userGrid.clicksToEdit = 2;
    this.userGrid.form = this.userForm;
    this.userGrid.on('validateedit', this.validateUsersEdit, this);

    var storeRole = this.createXmlStore('getUserRoles');
    var columnsTree = [];
    columnsTree.push({ header: 'Роль', dataIndex: 'value', sortable: true, hidden: false, editor: comboRoles, renderer: getStoreRenderer(this, this.storeRoles) });
    this.userRoleGrid = createGridEditablePanel(columnsTree, storeRole, this.getGridButtons(storeRole, true, true, true));
    storeRole.extraData = storeRole.extraData || {};
    storeRole.extraData.grid = this.userRoleGrid;
    storeRole.extraData.methodSave = "setUserRoles";
    storeRole.extraData.saveSelectedOnly = false;
    this.userGrid.extraData.thisRef = this;
    this.userGrid.extraData.refreshStore = storeRole;
    store.extraData = store.extraData || {};
    store.extraData.grid = this.userGrid;
    store.extraData.methodSave = "setUser";
    store.extraData.methodDel = "delUser";
    store.extraData.saveSelectedOnly = true;
    this.userGrid.addListener('cellclick', this.onRecordSelect, this);
    this.storeUserTypes.load({});
};

UserAdminUI.prototype.buildRoleUI = function () {
    var store = this.createXmlStore('getRoles');
    var columns = [];
    columns.push({ header: 'Имя роли', dataIndex: 'value', sortable: true, hidden: false, editor: new Ext.form.TextField({}) /*, renderer: getRoleNameRenderer(this)*/ });
    this.storeRoles = store;
    this.roleGrid = createGridEditablePanel(columns, store, this.getGridButtons(store, true, false, true));
    this.roleGrid.clicksToEdit = 2;
    var storeRoleTree = this.createXmlStore('getRolesTree');
    var columnsTree = [];
    columnsTree.push({ header: 'Каталог', dataIndex: 'value', sortable: true, hidden: false, editor: this.comboTree, renderer: getRoleTreeRenderer(this) });
    this.roleTreeGrid = createGridEditablePanel(columnsTree, storeRoleTree, this.getGridButtons(storeRoleTree, true, true, true));
    storeRoleTree.extraData = storeRoleTree.extraData || {};
    storeRoleTree.extraData.grid = this.roleTreeGrid;
    storeRoleTree.extraData.methodSave = "setRolesTree";
    storeRoleTree.extraData.saveSelectedOnly = false;
    this.roleGrid.extraData.thisRef = this;
    this.roleGrid.extraData.refreshStore = storeRoleTree;
    store.extraData = store.extraData || {};
    store.extraData.grid = this.roleGrid;
    store.extraData.methodSave = "setRole";
    store.extraData.saveSelectedOnly = true;
    this.roleGrid.addListener('cellclick', this.onRecordSelect, this);
};

UserAdminUI.prototype.buildUI = function () {
    if (!this.panel) {
        this.buildRoleUI();
        this.buildUserUI();
        this.panel = new Ext.Panel({
            border: false,
            autoScroll: false,
            //autoHeight: true,
            layout: 'fit',
            region: 'center',
            collapsible: false,
            hideCollapseTool: true,
            //layout: 'border',
            listeners: {
                afterlayout: onAfterPanelLayout
            },
            extraData: {id: null},
            items: [ new Ext.TabPanel({activeTab: 0, region: 'center', border: false, //autoHeight: true,
                layoutOnTabChange: true,
                autoScroll: false,
                //tbar:[new Ext.Button({border:false,text:'Обратно на информацию о пользователе', ref:'../btnShowUser', handler: this.showUser, scope:this})],
                listeners: {afterlayout: onAfterPanelLayout},
                items: [
                    createPanelSimple(
                        createPanelSimple(
                            [
                            createPanelSimple(this.userGrid, {region: 'north', layout: 'fit', height: 200}),
                            createPanelSimple(this.userForm, {region: 'center', layout: 'fit', height: 300, width: 300}),
                            createPanelSimple(this.userRoleGrid, {region: 'east', layout: 'fit', width: 100, /*autoHeight: true, */height: 300})
                            ],
                            {
                                border: false, autoHeight: false, autoScroll: false,
                                //height: 500,
                                layout: 'border',
                                region: 'center',
                                listeners: {afterlayout: onAfterPanelLayout},
                                defaults: {collapsible: false, split: true}
                            }
                        ),
                        {
                            listeners: {afterlayout: onAfterPanelLayout},
                            title: 'Управление пользователями',
                            layout: 'fit'
                        }),
                    createPanelSimple(
                        createPanelSimple(
                            [
                            createPanelSimple(this.roleGrid, {region: 'west', layout: 'fit', width: 300}),
                            createPanelSimple(this.roleTreeGrid, {region: 'center', layout: 'fit'})
                            ],
                            {
                                border: false, autoHeight: false, autoScroll: false,
                                height: 400,
                                layout: 'border',
                                region: 'center',
                                listeners: {afterlayout: onAfterPanelLayout},
                                defaults: {collapsible: false, split: true}
                            }
                        ),
                        {
                            listeners: {afterlayout: onAfterPanelLayout}, title: 'Управление ролями', layout: 'fit'}
                    )
                ]})
            ],
            defaults: {
                collapsible: false,
                split: false,
                bodyStyle: 'padding-left:0px'
            }
        });
        // окно где показывается панель
        var window = new Ext.Window({
            autoHeight: false, height: 550, width: 700, closable: true, closeAction: 'hide', maximizable: true, modal: true, layout: 'fit'
            //, bbar: panelToolbar
            , extraData: { thisRef: this }, listeners: {
                /*close: function (scope) {
                 scope.extraData.thisRef.closeWindow('no');
                 },*/
                afterlayout: onAfterPanelLayout
            }

        });
        this.pWindow = window;
        window.add(this.panel);
        window.show();
        window.doLayout();
        window.hide();
    }
    this.comboTree.store.load({});
    return this.panel;
};