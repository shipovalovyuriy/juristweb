// $Id$

/**
 * @see http://wiki.apache.org/solr/SolJSON#JSON_specific_parameters
 * @class Manager
 * @augments AjaxSolr.AbstractManager
 */
jQuery.extend({
   postJSON: function( url, data, callback) {
       if(jQuery.ajaxSettings.contentType=="application/x-www-form-urlencoded"){
           jQuery.ajaxSettings.contentType="application/x-www-form-urlencoded; charset=UTF-8";
       }
      return jQuery.post(url, data, callback, "json");
   }
});
AjaxSolr.Manager = AjaxSolr.AbstractManager.extend(
  /** @lends AjaxSolr.Manager.prototype */
  {
  executeRequest: function (servlet) {
    var self = this;
    if (this.proxyUrl) {
      jQuery.post(this.proxyUrl, { query: this.store.string() }, function (data) { self.handleResponse(data); }, 'json');
    }
    else {
      //jQuery.getJSON(this.solrUrl + servlet + '?' + this.store.string() + '&wt=json&json.wrf=?', {}, function (data) { self.handleResponse(data); });
       var par=this.store.objectForPost();
        //jQuery.postJSON(this.solrUrl + servlet + '?wt=json&json.wrf=?', par, function (data) { self.handleResponse(data); });
        jQuery.postJSON(servlet + '?wt=json&json.wrf=?', par, function (data) { self.handleResponse(data); });
    }
  }
});
