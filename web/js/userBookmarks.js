function UserBookmarkUI(navigateUI){
    this.navigateUI = navigateUI;
    this.listeners={};
    this.lastDocs = [];
}

UserBookmarkUI.prototype.addHistory = function (sender, id, title, html){
    var target='#open_doc_history';
    if($(target).find('a[docid='+id+']').length==0){
        var a = $('<div/>').append($('<a href="#"/>').
                text(title.substring(0,50)+"...").
                attr('title',title).
                attr('docid',id).
                click(this.navigateUI.documentUI.getDocumentOpenHandler(id, true)));
        var i = $(target).find('a').length;
        if( i % 2 == 1) a.addClass('azure');
        $(target).prepend(a);
    }
    this.lastDocs.unshift({id:id, fullName:title, shortName:title, html:html, pDate:new Date()});
};

UserBookmarkUI.prototype.bookmarkLoadListHandler = function (sender, tree_id, offset, sortMode, incSearch){
    this.navigateUI.loadList(tree_id, offset, sortMode,incSearch,'bookmark');
};

UserBookmarkUI.prototype.historyLoadListHandler = function (sender, tree_id, offset, sortMode, incSearch){
    this.showHistory(offset);
};

// показываем список дерева
UserBookmarkUI.prototype.showHistory = function (offset){
    var offs = offset || 0;
    var rows = 20;
    var docs = this.lastDocs.slice(offs,offs+rows);
    var data = {mode:'history',
        facet_counts:{facet_dates:{},facet_fields:{},facet_queries:{},facet_ranges:{}},
        responseHeader:{params:{rows:this.lastDocs.length,start:offs,sort:'publishDate desc'}},
        'response': {'start':offs, 'numFound':docs.length, docs:docs }};
    this.navigateUI.manager.handleResponse(data);
    this.navigateUI.selTreeId = null;
};
// кликаем по дереву
UserBookmarkUI.prototype.onTreeClick = function (node){
    if(node && node.id=='-1'){
        this.showHistory();
    }
    var tree_id =node && node.id;
    if(Number(tree_id)>0){
        this.bookmarkLoadListHandler(this.navigateUI,tree_id,0,null,'');
    }
};
// Drug & Drop когда перенесли ветку куда то
UserBookmarkUI.prototype.movenode = function (tree, node, oldParent, newParent, index){
    var parNode = newParent;
    var obj = {obj:{id:node.id,parent:parNode.id,name:node.text}};
    var xml = json2xml(obj);
    callRequestHandler("proc", { proc:'editBookmark',par:xml }, CallClassMethodAfterAjaxReturn,CallClassMethodAfterAjaxReturn,{thisRef:this,thisMethod:'afterCreateNode', node:parNode });
};
//обновление ветки когда закончили ее изменять
UserBookmarkUI.prototype.afterCreateNode = function (res,scope){
    if(getResponseErrorText(res)){
        alert('Вы не авторизованы. Авторизуйтесь');
        return;
    }
    var node = scope.node;
    var tree = node.ownerTree;
//    var root = tree.getRootNode();
    tree.loader.load(node);
};
// создать новую папку
UserBookmarkUI.prototype.createNode = function (sender){
    var tree = sender.extraData.tree;
    var node = tree.getSelectionModel().getSelectedNode();
    if(node==null || node.id=='-1') return;
    callRequestHandler("proc",{proc:'addBookmark',par:node.id}, CallClassMethodAfterAjaxReturn,CallClassMethodAfterAjaxReturn,{thisRef:this,thisMethod:'afterCreateNode',node:node});
};
// удалить папку
UserBookmarkUI.prototype.deleteNode = function (sender) {
    var tree = sender.extraData.tree;
    var node = tree.getSelectionModel().getSelectedNode();
    if (node == null || node.id == '-1') return;
    var parNode = node.parentNode;
    callRequestHandler("proc", {proc: 'delBookmark', par: node.id}, CallClassMethodAfterAjaxReturn, CallClassMethodAfterAjaxReturn, {thisRef: this, thisMethod: 'afterCreateNode', node: parNode});
};

// показать хелп
UserBookmarkUI.prototype.showNodeHelp = function (sender) {

}
// Переимновать папку
UserBookmarkUI.prototype.renameNode = function (sender) {
    var tree = sender.extraData.tree;
    var node = tree.getSelectionModel().getSelectedNode();
    if (node == null || node.id == '-1') return;
    var parNode = node.parentNode;
    var caption = node.text;
    caption = prompt('Введите новое имя', caption);
    if(isStrNullOrEmpty(caption)) return;
    var obj = {obj: {id: node.id, parent: parNode.id, name: caption}};
    var xml = json2xml(obj);
    callRequestHandler("proc", { proc: 'editBookmark', par: xml }, CallClassMethodAfterAjaxReturn, CallClassMethodAfterAjaxReturn, {thisRef: this, thisMethod: 'afterCreateNode', node: parNode });
};

// обновить дерево
UserBookmarkUI.prototype.refresh = function (sender){
    var tree = sender;
    if(sender.topToolbar){
        if(tree.topToolbar.btnTreeRefresh){
            tree.topToolbar.btnTreeRefresh.extraData =tree.topToolbar.btnTreeRefresh.extraData||{};
            tree.topToolbar.btnTreeRefresh.extraData.tree = tree;
        }
        if(tree.topToolbar.btnTreeNew){
            tree.topToolbar.btnTreeNew.extraData =tree.topToolbar.btnTreeNew.extraData||{};
            tree.topToolbar.btnTreeNew.extraData.tree = tree;
        }
        if(tree.topToolbar.btnTreeRename){
            tree.topToolbar.btnTreeRename.extraData =tree.topToolbar.btnTreeRename.extraData||{};
            tree.topToolbar.btnTreeRename.extraData.tree = tree;
        }
        if(tree.topToolbar.btnTreeDel){
            tree.topToolbar.btnTreeDel.extraData =tree.topToolbar.btnTreeDel.extraData||{};
            tree.topToolbar.btnTreeDel.extraData.tree = tree;
        }
        if(tree.topToolbar.btnTreeHelp){
            tree.topToolbar.btnTreeHelp.extraData =tree.topToolbar.btnTreeHelp.extraData||{};
            tree.topToolbar.btnTreeHelp.extraData.tree = tree;
        }
    }else{
        tree = sender.extraData.tree;
    }
    var root = tree.getRootNode();
    var node = root.item(0);
    if(node){
        tree.loader.load(node);
    }
};
// когда закончили переименовывать
UserBookmarkUI.prototype.onEditComplete = function (treeEditor, o, n){
    var node=treeEditor.editNode;
    var caption = o;
    var parNode = node.parentNode;
    var obj = {obj:{id:node.id,parent:parNode.id,name:o}};
    var xml = json2xml(obj);
    callRequestHandler("proc", { proc:'editBookmark',par:xml }, CallClassMethodAfterAjaxReturn,CallClassMethodAfterAjaxReturn,{thisRef:this,thisMethod:'afterCreateNode', node:parNode });
};
// динамическая подгрузка дерева
UserBookmarkUI.prototype.createTreeLoader = function (){
    return new Ext.tree.TreeLoader({url: 'RequestHandler',nodeParameter:'par',baseParams:{proc: 'getBookmarks',par: '',requestType: 'proc'},
            listeners:{
                /*load:function(sender,node,resp){
                    node.expandChildNodes(false);
                }*/
            }
    });
};

UserBookmarkUI.prototype.createTreePanel = function (classes){
    var childNodes = [{
        text: 'Закладки',
        leaf: false,
        loaded:false,
        editable:false,
        id:'0'
    }];
    if(classes){
         childNodes.push({
             text: 'История',
             leaf: false,
             allowDrag:false,
             allowDrop:false,
             loaded:true,
             editable:false,
             id:-1
         });
    }
    var treeConfig = {
        border: false,
        //title:'Классификатор',
        region:'center',
        rootVisible: false,
        autoScroll: true,
        //autoHeight: true,
        enableDD:true,
        loader: this.createTreeLoader(),
        root:new Ext.tree.AsyncTreeNode({
            text: 'treeRoot',
            id: '-1',
            expanded:true,
            allowDrop:false,
            children: childNodes
        })
        ,tbar : [ classes ||' ', '->',
            {icon:'images/refresh_16.png',ref:'btnTreeRefresh',handler:this.refresh,scope:this,tooltipType:'title',tooltip: 'Обновить'},
            {icon:'images/create_16.png',ref:'btnTreeNew',handler:this.createNode,scope:this,tooltipType:'title',tooltip: 'Создать'},
            {icon:'images/rename_16.png',ref:'btnTreeRename',handler:this.renameNode,scope:this,tooltipType:'title',tooltip: 'Переимновать'},
            {icon:'images/delete.png',ref:'btnTreeDel',handler:this.deleteNode,scope:this,tooltipType:'title',tooltip: 'Удалить'},
            {icon:'images/help_16.png',ref:'btnTreeHelp',handler:this.showNodeHelp,scope:this,tooltipType:'title',tooltip: 'Вы можете перетаскивать закладки из одного в другой. '}
        ], expandNodeById: function (id) {
            var rootNode = this.getRootNode();
            var path = [];
            var node = this.getNodeById(id);
            if (node == null) {
                var tnode = this.findNodeById(rootNode.attributes, id, path);
                if (tnode != null) {
                    var cNode = rootNode;
                    for (var i = path.length - 2; i >= 0; i--) {
                        cNode = this.getNodeById(path[i]);
                        cNode.expand(true, false);
                    }
                    node = this.getNodeById(id);
                }
            }
            if (node != null) {
                node.ensureVisible();
                node.expand();
                node.select();
            }

        }
        , findNodeByText: function (nodeAttr, text, path) {
            var children =nodeAttr.children;
            /*children.sort(function (a, b) {
             return a.text < b.text ? -1 : 1;
             });*/
            for (var i = 0, len = children.length; i < len; i++) {
                if (children[i].text.search(text)>=0) {
                    path.push(children[i].id);
                }
                var child = this.findNodeByText(children[i], text, path);
            }
        }
        , findNodeById: function (nodeAttr, id, path) {
            if (nodeAttr.id == id) return nodeAttr;
            for (var i = 0, len = nodeAttr.children.length; i < len; i++) {
                if (nodeAttr.children[i].id == id) {
                    path.push(nodeAttr.id);
                    return nodeAttr.children[i];
                } else {
                    var child = this.findNodeById(nodeAttr.children[i], id, path);
                    if (child != null) {
                        path.push(nodeAttr.id);
                        return child;
                    }
                }
            }
        }
    };
    var tree = new Ext.tree.TreePanel(treeConfig);
    tree.nodeAction = 0;
    tree.on('render',this.refresh,this);
    tree.on('movenode',this.movenode,this);
    var treeEditor = new Ext.tree.TreeEditor(tree);
    treeEditor.on('complete',this.onEditComplete,this);
    //var treeSorter = new Ext.tree.TreeSorter(tree, { dir: "asc" });
    return tree;
};

UserBookmarkUI.prototype.afterDocumentBookmark = function (res,scope){
        if(getResponseErrorText(res)){
            alert(getResponseErrorText(res));
            return;
        }
    var res = getResponseText(res);
    if (res == '1') alert('Документ добавлен в закладки');
    else alert(res);
};
UserBookmarkUI.prototype.onDocumentBookmark = function (sender, answer){
    if('no' == answer) return;
    var id = sender.extraData.docid;
    var tree = sender.extraData.tree;
    var tree_id = tree && tree.getSelectionModel() && tree.getSelectionModel().getSelectedNode() && tree.getSelectionModel().getSelectedNode().id;
    if(tree_id ==null || tree_id=='0') return;
    var obj = {obj:{id:tree_id,docid:id}};
    var xml = json2xml(obj);
    callRequestHandler("proc", { proc:'bookmarkDocument',par:xml }, CallClassMethodAfterAjaxReturn,CallClassMethodAfterAjaxReturn,{thisRef:this,thisMethod:'afterDocumentBookmark'});
};

UserBookmarkUI.prototype.bookmarkDocument = function (sender,id){
    var msgWindow = null;
    if(!msgWindow){
        var tree = this.createTreePanel();
        msgWindow = new ParamMsgBox('Добавить документ в закладки',[],this,'onDocumentBookmark',{docid:id,tree:tree},tree);
    }
    msgWindow.extraData.docid = id;
    msgWindow.requestValues();

};
UserBookmarkUI.prototype.buildUI = function (){
    var classes = this.navigateUI.buildControlClasses( {navigatePnl:this.navigateUI.navigatePnl} );
    classes.extraData.classes = this.navigateUI.cClasses;
    this.navigateUI.cClasses.extraData.classes = classes;
    classes.extraData.treeStore = this.navigateUI.navStore;
    classes.setValue(0);
    this.navTree = this.createTreePanel(classes);
    this.navTree.on('click', this.onTreeClick, this);
    this.navigateUI.navigatePnl.add(this.navTree);
    this.navigateUI.addLoadListHandler('history',this.historyLoadListHandler,this);
    this.navigateUI.addLoadListHandler('bookmark',this.bookmarkLoadListHandler,this);
    this.navigateUI.documentUI.on('history',this.addHistory, this);
    this.navigateUI.documentUI.on('bookmark',this.bookmarkDocument, this);
};
