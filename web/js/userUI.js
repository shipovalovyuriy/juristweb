function UserUI(lnkId) {
    // ссылка на объект управления пользователями. Для администратора
    this.userAdminUI = null;
    // поле куда покажется логин пользователя
    this.lnkId = lnkId;
    this.user = null;
    this.panel = null;
    this.userAdminUI = null;
    this.listeners = {};
}
// событие на кнопку выйти
UserUI.prototype.onLogout = function (res, scope, extraParams, o) {
    this.setUserInfo(null);
};
/*
 * событие при входе в систему
 * */
UserUI.prototype.onLogin = function (resp, scope, extraParams, o) {
    if (getResponseErrorText(resp)) {
        alert(getResponseErrorText(resp));
        return;
    }
    var res = getResponseText(resp);
    if (isStrNullOrEmpty(res)) {
        alert('Неверное имя пользователя или пароль');
        return;
    }
    var data = eval("(" + res + ")");
    if (data.error) {
        alert(data.error);
        return;
    }
    //this.setUserInfo(data);
    this.loginPnl.fldPass.setValue('');
    this.checkUser();
};
// нажатие на кнопку выйти
UserUI.prototype.doLogout = function () {
    var documentUI = navigateUI.documentUI;
    documentUI.closeDocument();
    callRequestHandler("doLogout", {},
        CallClassMethodAfterAjaxReturn, CallClassMethodAfterAjaxReturn, {thisRef: this, thisMethod: 'onLogout'});
    return false;
};
//нажатие на кнопку Войти
UserUI.prototype.doLogin = function () {
    callRequestHandler("doLogin", {login: this.loginPnl.fldLogin.getValue(), pwd: this.loginPnl.fldPass.getValue()},
        CallClassMethodAfterAjaxReturn, CallClassMethodAfterAjaxReturn, {thisRef: this, thisMethod: 'onLogin'});

};

UserUI.prototype.getPatches = function () {
    callRequestHandler("applyUpdates", {},
        CallClassMethodAfterAjaxReturn, CallClassMethodAfterAjaxReturn, {thisRef: this, thisMethod: 'onErrorAlert'});
    return false;
};

UserUI.prototype.onErrorAlert = function (res, scope, extraParams, o) {
    if (getResponseErrorText(res)) {
        alert(getResponseErrorText(res));
    }

};

UserUI.prototype.askActNumber = function (sender) {
    var val = sender && $(sender).attr('serial');
    var act_number = prompt('Введите код активации ', val);
    if (!isStrNullOrEmpty(act_number)) {
        callRequestHandler("setActivationNumber", {id: act_number},
            CallClassMethodAfterAjaxReturn, CallClassMethodAfterAjaxReturn, {thisRef: this, thisMethod: 'onErrorAlert'});
    }
    return false;
};

UserUI.prototype.doUserAdmin = function () {
    if (this.userAdminUI != null) {
        this.userAdminUI.refreshUI();
        this.userAdminUI.showUI();
        this.panel.setActiveTab(2);
    }
    return false;
};
//добавить событие в список
UserUI.prototype.on = function (event, fn, scope) {
    var clb = {fn: fn, scope: scope};
    this.listeners[event] = this.listeners[event] || [];
    this.listeners[event].push(clb);
};

//оповестить о событии
UserUI.prototype.fire = function (event) {
    var listeners = this.listeners[event];
    if (listeners && listeners.length > 0) {
        for (var i = 0, il = listeners.length; i < il; i++) {
            var clb = listeners[i];
            clb.fn.call(clb.scope, this);
        }
    }
};
var user_license_notification = null;
function notifyUser(title, message) {
    user_license_notification = user_license_notification || $("#user_notification").notify();
    return user_license_notification.notify("create", "default", { title: title || 'Внимание', text: message});
}

// установить текущего пользователя
UserUI.prototype.setUserInfo = function (user, netMode) {
    var userChange = this.user != user;
    var sameUser = (this.user && this.user.name) == (user && user.name);
    if (user && !isStrNullOrEmpty(user.name)) {
        this.user = user;
        navigateUI.setDisabledNavigate(false);
        $('#' + this.lnkId + ' .userName').text(user.name);
        $('#' + this.lnkId + ' .logout').css('display', '');
        this.panel.setActiveTab(1);
        this.panel.lUserName.setText(user.name + (user.valid_till == null ? '' : (" (действует до " + user.valid_till + ")" )));
        var roles = user.roles;
        var rolesTxt = '';
        var hasAdminRole = false;
        for (var i = 0; i < roles.length; i++) {
            if (roles[i].role_id == 1) hasAdminRole = true;
            if (i > 0) rolesTxt += ",";
            rolesTxt += roles[i].role;
        }
        if (user.locMode && !user.netMode) {
            $('#userDiv').hide();
            if (user.activated) $('#' + this.lnkId + ' .get_updates').show();
        }
        this.panel.lUserRoles.setText(rolesTxt);
        if (hasAdminRole && this.userAdminUI == null) {
            this.userAdminUI = new UserAdminUI();
            var pnl = this.userAdminUI.buildUI();
            $('#' + this.lnkId + ' .admin').css('display', '');
            /* var pnl1 = createColumnPanel([createLabel(''),new Ext.Button({border:false,text:'Управление пользователями', ref:'../btnUserAdmin', handler: this.doUserAdmin, scope:this}),createLabel('')],[0.35,0.5,0.25]);
             this.userAdminPnl=pnl;
             this.userAdminBtn=pnl1;
             this.panel.add(pnl);
             this.userInfoPnl.add( pnl1);
             this.panel.doLayout();*/
        }
        this.pWindow.hide();
        if (!sameUser && user.valid_till) {
            var msg = user.name + " , добро пожаловать в СПС «Правовой консультант»!\nСрок действия лицензии до «" + user.valid_till + "».";
            notifyUser(null, msg);
            //$("#user_license_notification").css('right',"0px").show().text(msg).hide( "drop", { direction: "up" }, 10000 );
            //2014100710000058 — ОНЛ_сылки после авторизации
            var documentUI = navigateUI.documentUI;
            if(documentUI.docId){
                var docId = documentUI.docId;
                documentUI.docId = null;
                documentUI.openDocument(docId, null, false);
            }
        }
    } else {
        $('#' + this.lnkId + ' .logout').css('display', 'none');
        $('#' + this.lnkId + ' .admin').css('display', 'none');
        if (netMode === true) {
            navigateUI.setDisabledNavigate(true);
        }
        if (user && user.locMode) {
            if (!user.activated) {
                $('#' + this.lnkId + ' .activate').attr('serial', user.serial).show();
            }
            if (user.activated) $('#' + this.lnkId + ' .get_updates').hide();
        }
        this.user = null;
        if (this.userAdminUI != null) {
            this.userAdminUI.hideUI();
        }
        this.userAdminUI = null;
        if (this.userAdminPnl) {
            this.panel.remove(this.userAdminPnl);
            this.userAdminPnl = null;
        }
        if (this.userAdminBtn) {
            this.userInfoPnl.remove(this.userAdminBtn);
            this.userAdminBtn = null;
        }
        $('#' + this.lnkId + ' .userName').html(METADATA.login_content);
        this.panel.setActiveTab(0);
    }
    if (userChange && !sameUser) this.fire('change');
};
// событие при получении информации об авторизованном пользователе
UserUI.prototype.onGetUserInfo = function (resp) {
    var data = null;
    if (!getResponseErrorText(resp)) {
        var res = getResponseText(resp);
        if (!isStrNullOrEmpty(res)) {
            data = eval("(" + res + ")");
            if (data.error) {
                this.setUserInfo(null, data.netMode);
                alert(data.error);
                return;
            }
        }
    }
    this.setUserInfo(data);
};
// получить информацию о текущем авторизованном пользователе
UserUI.prototype.checkUser = function () {
    callRequestHandler("getUserInfo", {}, CallClassMethodAfterAjaxReturn, CallClassMethodAfterAjaxReturn, {thisRef: this, thisMethod: 'onGetUserInfo'});
};
// событие при смене пароля
UserUI.prototype.onChangePass = function (resp, scope, extraParams, o) {
    if (getResponseErrorText(resp)) {
        alert(getResponseErrorText(resp));
        return;
    }
    var res = getResponseText(resp);
    var dom = parseXml(res);
    var result = getXMLNodeText(selectNode(dom.documentElement, 'success'));
    if (result == "1") {
        alert('Пароль изменен');
        this.doChangePassCancel();
    } else {
        alert('Ошибка при сменен пароля');
    }
};
// сменить пароль
UserUI.prototype.changePassword = function (login, pass, newpass) {
    var xml = '<chg><login>' + login + '</login><pwd>' + pass + '</pwd><newpwd>' + newpass + '</newpwd></chg>';
    callRequestHandler("proc", {proc: 'changePassword', par: xml}, CallClassMethodAfterAjaxReturn, CallClassMethodAfterAjaxReturn, {thisRef: this, thisMethod: 'onChangePass'});
};
//кнопка сменить пароль
UserUI.prototype.doChangePass = function () {
    var login = this.user.name;
    var pass = this.changePassPnl.fldPassOld.getValue();
    var newpass = this.changePassPnl.fldPassNew.getValue();
    var newpass2 = this.changePassPnl.fldPassNew2.getValue();
    if (newpass != newpass2) {
        alert('Подтверждение пароля не совпадает с новым паролем');
        return;
    }
    this.changePassword(login, pass, newpass2);
};
//отменить смену пароля
UserUI.prototype.doChangePassCancel = function () {
    this.changePassPnl.setVisible(false);
    this.userInfoPnl.btnChangePass.ownerCt.setVisible(true);
};
//показать кнопки смены пароля
UserUI.prototype.showChangePass = function () {
    this.changePassPnl.setVisible(true);
    this.userInfoPnl.btnChangePass.ownerCt.setVisible(false);
};
//показать окно
UserUI.prototype.showUI = function (sender) {
    this.buildUI();
    this.pWindow.show();
    this.pWindow.doLayout();
    //alert('Не реализовано');
    return false;
};
// построить пользовательский интерфейс
UserUI.prototype.buildUI = function () {
    if (!this.panel) {
        // панель для авторизации
        var self = this;
        this.loginPnl = new Ext.Panel({
            border: false,
            autoScroll: true,
            autoHeight: true,
            region: 'center',
            collapsible: false,
            hideCollapseTool: true,
            //layout: 'border',
            listeners: {
                afterlayout: onAfterPanelLayout
            },
            closable: true,
            extraData: {id: null},
            minSize: 400,
            minWidth: 400,
            items: [createColumnPanel([createLabel(''), createLabel('Авторизация', 0.5, 'font-size:16px;font-weight:bold;')], [0.35, 0.65]),
                createColumnPanel([createLabel(''), createLabel('Имя пользователя', null, 'text-align:right;padding-right:5px'), new Ext.form.TextField({ref: '../fldLogin'}), createLabel('')], [0.05, 0.30, 0.5, 0.15], 25),
                createColumnPanel([createLabel(''), createLabel('Пароль', null, 'text-align:right;padding-right:5px'), new Ext.form.TextField({ref: '../fldPass', inputType: 'password', enableKeyEvents: true, listeners: {keypress: function (sender, e) {
                    if (e.getKey() == e.ENTER) {
                        self.doLogin();
                    }
                }}}), createLabel('')], [0.05, 0.30, 0.5, 0.15], 25),
                createColumnPanel([createLabel(''), new Ext.Button({border: false, text: 'Войти', ref: '../btnLogin', handler: this.doLogin, scope: this}), createLabel('')], [0.35, 0.5, 0.25])
            ],
            defaults: {
                collapsible: false,
                split: false,
                bodyStyle: 'padding:0px'
            }
        });
        // панель для смены пароля
        this.changePassPnl = createPanelSimple([
            createColumnPanel([createLabel(''), createLabel('Старый пароль'), new Ext.form.TextField({ref: '../fldPassOld', inputType: 'password'}), createLabel('')], [0.05, 0.30, 0.6, 0.05]),
            createColumnPanel([createLabel(''), createLabel('Новый пароль'), new Ext.form.TextField({ref: '../fldPassNew', inputType: 'password'}), createLabel('')], [0.05, 0.30, 0.6, 0.05]),
            createColumnPanel([createLabel(''), createLabel('Повтор нового пароля'), new Ext.form.TextField({ref: '../fldPassNew2', inputType: 'password'}), createLabel('')], [0.05, 0.30, 0.6, 0.05]),
            createColumnPanel([createLabel(''), new Ext.Button({border: false, text: 'Сменить', ref: '../btnChangePassAprove', handler: this.doChangePass, scope: this}),
                new Ext.Button({border: false, text: 'Отменить', ref: '../btnChangePassCancel', handler: this.doChangePassCancel, scope: this}), createLabel('')], [0.35, 0.3, 0.3, 0.15])
        ]);
        this.changePassPnl.setVisible(false);
        // панель информации об авторизованном пользователе
        this.userInfoPnl = new Ext.Panel({
            border: false,
            autoScroll: true,
            autoHeight: true,
            region: 'center',
            collapsible: false,
            hideCollapseTool: true,
            //layout: 'border',
            listeners: {
                afterlayout: onAfterPanelLayout
            },
            closable: true,
            extraData: {id: null},
            minSize: 400,
            minWidth: 400,
            items: [createColumnPanel([createLabel('Имя пользователя'), createLabel('', 0, '', {ref: '../../lUserName'}), createLabel('')], [0.35, 0.50, 0.15], 25),
                createColumnPanel([createLabel('Роли'), createLabel('', 0, '', {ref: '../../lUserRoles'}), createLabel('')], [0.35, 0.50, 0.15], 25),
                createColumnPanel([createLabel(''), new Ext.Button({border: false, text: 'Выйти', ref: '../btnLogout', handler: this.doLogout, scope: this}), createLabel('')], [0.35, 0.5, 0.25]),
                createColumnPanel([createLabel(''), new Ext.Button({border: false, text: 'Смена пароля', ref: '../btnChangePass', handler: this.showChangePass, scope: this}), createLabel('')], [0.35, 0.5, 0.25]),
                this.changePassPnl
            ],
            defaults: {
                collapsible: false,
                split: false,
                bodyStyle: 'padding-left:5px'
            }
        });
        // все панел воедино
        this.panel = new Ext.Panel({ autoHeight: false, region: 'south', height: 100, layout: 'card', activeItem: 0,
            hideCollapseTool: true, collapsible: false, collapseMode: 'mini',
            autoScroll: true, border: false, items: [this.loginPnl, this.userInfoPnl] });
        this.panel.getActiveTab = function () {
            return this.layout.activeItem;
        };
        this.panel.setActiveTab = function (tab) {
            this.layout.setActiveItem(tab);
        };
        // окно где показывается панель
        var window = new Ext.Window({
            autoHeight: false, height: 200, width: 400, closable: true, closeAction: 'hide', maximizable: true, modal: true, layout: 'fit'
            //, bbar: panelToolbar
            , extraData: { thisRef: this }, listeners: {
                /*close: function (scope) {
                 scope.extraData.thisRef.closeWindow('no');
                 },*/
                afterlayout: onAfterPanelLayout
            }
        });
        this.pWindow = window;
        window.add(this.panel);
        window.show();
        window.doLayout();
        window.hide();
    }
    return this.panel;
};