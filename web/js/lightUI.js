﻿function domReady() {
    // исправления для корректной работы в разных браузерах
        //Ext.net.DirectEvent.timeout = 180000;
        /* !!! remove after adding IE9 support to the ExtJS */
        var ieVer = /msie (\d+)/i.exec(navigator.userAgent);
        ieVer = ieVer ? parseInt(ieVer[1], 10) : 0;
        Ext.isIE6 = Ext.isIE && ieVer <= 6;
        Ext.isIE7 = Ext.isIE && ieVer == 7;
        Ext.isIE8 = Ext.isIE && ieVer >= 8;
        Ext.isIE9 = Ext.isIE && ieVer >= 9;
        if (Ext.isIE9) {
            //Ext.isChrome = true;
            //isIE6 = isIE = false;
            //Ext.isIE8 = false; 
        }
    METADATA.login_content='<img src="images/login.png" />';
    //служебные справочники
// периоды страхования
    METADATA.DOC_STATUSES = {};
    METADATA.DOC_STATUSES.id = 'DOC_STATUSES';
    METADATA.DOC_STATUSES.def_key_value = '0';
    METADATA.DOC_STATUSES.fields = [
        { name: 'ID' },
        { name: 'TEXT' }
    ];
    METADATA.DOC_STATUSES.data = [
        ['', 'Все'],
        ['0', 'действующий'],
        ['1', 'утративший силу'],
        ['2', 'не введенный в действие']
    ];
    METADATA.CLASSIFICATORS = {};
    METADATA.CLASSIFICATORS.id = 'CLASSIFICATORS';
    METADATA.CLASSIFICATORS.def_key_value = '1';
    METADATA.CLASSIFICATORS.fields = [
        { name:"id"},
        { name:"name"},
        { name:"type"}
    ];
    METADATA.CLASSIFICATORS.data = [
        ["1", "Классы", "1"],
        ["2", "Источники", "1"],
        ["3", "Типы", "1"],
        ["9", "Ситуации", "2"],
        ["0", "Закладки", "2"]
    ];
    // регистрация самописных компонент
    afterDomReady();
    // построение пользовательского интерфейса
    continueBuildLightUI();
    //построение поисковых форм
    buildSearchUI(navigateUI);
    //допольнительные кнопки со всплывающими панелями
    $(document).ready(function(){
        $(".right-menu").click(function(){
            var cntName=$(this).attr('menu-content');
            if(!isStrNullOrEmpty(cntName)){
                $(cntName).toggle("fast");
                $(this).toggleClass("active");
            }
            var cntClick=$(this).attr('menu-click');
            if(!isStrNullOrEmpty(cntClick)){
                var cntScope=$(this).attr('menu-click-scope');
                var scope = window[cntScope];
                scope[cntClick](this);
            }
            return false;
        });
        prepareSearchDropDown();
    });
}

//показать главную страницу
function showInfo() {
    $(".right-menu").hide();
    mainTabPanel.setActiveTab(panelInfoParent);
    searchHeaderPanel.setVisible(true);
    searchHeaderPanel.setActiveTab(0);
    $(".ext_search_link").hide();
    $(".simple_search").val( navigateUI.extraData.cSearch.getValue() );
    return true;
}
// получить обработчик закрытия выпадающего поиска
function getDropDownCloseTag(container, tag_div, obj, tag){
    var c = container, d=tag_div, o =  obj, t = tag;
    return function(){
        return dropDownCloseTag(c,d,o,t);
    }
}

// закрытие выпадающего поиска
function dropDownCloseTag(container, tag_div, obj, tag){
    tag_div.find( "."+obj.el).remove();
    // начинаем поиск
    obj.clear(container);
    container.find(".search_btn").click();
}

// получить краткий список полей участвующих в поиске
function fillDropDownTags(container, tag_div, params){
    var dateFormat = function(text, beg, end){
        if(beg===end){
            text+=" равно "+DateToStr(beg);
        }else if(end > beg && end.getYear()==2999){
            text+=" позднее "+DateToStr(beg);
        }else if(end > beg && beg.getYear()==1900){
            text+=" ранее "+DateToStr(end) ;
        }else{
            text += " c "+ DateToStr(beg) + " по "+ DateToStr(end);
        }
        return text;
    };
    var objects = [
        {
            el:'regNumber',
            text:'№ регистрации ',
            clear:function(container){
                container.find(".reg_number").val('');
            }
        },
        {
            el:'regDate',
            text:'',
            empty:function(container, params){
                return params.regDateBeg==null || params.regDateEnd==null;
            },
            format:function(container, params){
                var beg = params.regDateBeg;
                var end = params.regDateEnd;
                return dateFormat('Дата регистрации ', beg, end);
            },
            clear:function(container){
                container.find('.reg_date_beg').val("");
                container.find('.reg_date_end').val("");
            }
        },
        {
            el:'docNumber',
            text:'№ документа ',
            clear:function(container){
                container.find(".doc_number").val('');
            }
        },
        {
            el:'docDate',
            text:'',
            empty:function(container, params){
                return params.publishDateBeg==null || params.publishDateEnd==null;
            },
            format:function(container, params){
                var beg = params.publishDateBeg;
                var end = params.publishDateEnd;
                return dateFormat('Дата документа ', beg, end);
            },
            clear:function(container){
                container.find('.doc_date_beg').val("");
                container.find('.doc_date_end').val("");
            }
        },
        {
            el:'status',
            text:'',
            format:function(container, params){
                return container.find('.doc_type :selected').text();
            },
            clear:function(container){
                container.find('.doc_type').val("");
            }
        },
        {
            el:'exactSearch',
            text:'Точный поиск',
            format:function(container, params){
                return '';
            },
            clear:function(container){
                container.find(".exact_search").attr('checked',false);
            }
        },
        {
            el:'bodySearch',
            text:'Искать в тексте',
            format:function(container, params){
                return '';
            },
            clear:function(container){
                container.find(".search_mode_b").attr('checked',false);
                container.find(".search_mode_t").attr('checked',true);
            }
        },
        {
            el:'bodySearch',
            text:'Искать в заголовке',
            empty:function(container, params){
                return params.bodySearch == true ;
            },
            format:function(container, params){
                return '';
            },
            clear:function(container){
                container.find(".search_mode_b").attr('checked',true);
                container.find(".search_mode_t").attr('checked',false);
            }
        }
    ];
    // формирование чего-то вроде облака тегов
    for (var i = 0; i < objects.length; i++) {
        var obj = objects[i];
        if(!obj.empty && obj.el){
            if(isStrNullOrEmpty(params[obj.el])) continue;
        }else{
            if(obj.empty && obj.empty(container, params)) continue;
        }
        var text = "";
        if(obj.format){
            text = obj.text + obj.format(container, params);
        }else{
            text = obj.text + params[obj.el];
        }
        var tag=$('<span class="tag"/>').text(text).addClass(obj.el);
        var span = $('<span style="float:left;height: 16px;"/>');
        tag_div.append(span);
        span.append(tag);
        var closeTag = $('<span class="tag_close">x</span>').click(getDropDownCloseTag(container, tag_div, obj, tag));
        tag.append(closeTag);
    }
}

//инициирует поиск
function dropDownSearch(container, sender, incSearch, incSearchQuery){
    var tag_div = $($(sender).attr("tag_div"));
    tag_div.empty();
    container.hide();
    var params={
        regNumber:container.find('.reg_number').val(),
        docNumber:container.find('.doc_number').val(),
        status:container.find('.doc_type :selected').val(),
        bodySearch:container.find('.search_mode_b').is(':checked'),
        searchText:$(".simple_search").val(),
        exactSearch:container.find('.exact_search').is(':checked')
    };
    if(!isStrNullOrEmpty(incSearch)){
        params.incSearch = incSearchQuery;
    }
    var getEndDate = function(type,beg,end){
        if(type==3){
            return "31.12.2999";
        }else if(type==2){
            return end;
        }else{
            return beg;
        }
    };
    var getBegDate = function(type,beg){
        if(type==4){
            return "01.01.1900";
        }else{
            return beg;
        }
    };
    params.regDateBeg = DateFromStr(getBegDate(container.find('.reg_date :selected').val(), container.find('.reg_date_beg').val()));
    params.regDateEnd = DateTimeFromStr(getEndDate(container.find('.reg_date :selected').val(), container.find('.reg_date_beg').val(), container.find('.reg_date_end').val()) + " 23:59:59");
    params.publishDateBeg = DateFromStr(getBegDate(container.find('.doc_date :selected').val(), container.find('.doc_date_beg').val()));
    params.publishDateEnd = DateTimeFromStr(getEndDate(container.find('.doc_date :selected').val(), container.find('.doc_date_beg').val(), container.find('.doc_date_end').val()) + " 23:59:59");
    navigateUI.doSearch(params);
    if(!isStrNullOrEmpty(incSearch)){
        navigateUI.incSearchStr = incSearch;
        navigateUI.cResultSearchInc.setValue(incSearch);
    }
    fillDropDownTags(container, tag_div, params);
}

// расширенный поиск в всплывающем окошке
function prepareSearchDropDown(){
    $(".simple_login").click(function(){ userUI.showUI(); });
    $(".simple_search_btn").click(simpleSearchClick);
    $(".ext_search_dropdown").click(extSearchDropDown);
    $(".ext_search_link").click(extSearchClick);
    $(".simple_history").click(historyDropDown);
    $(".simple_monitoring").click(monitoringDropDown);
    $(".simple_search").keypress(simpleSearchKeyDown);

    var obj = $('.ext-search-content');
    obj.find('.reg_date_beg, .reg_date_end, .doc_date_beg, .doc_date_end').each(function(){
        $(this).datepicker(jQuery.extend( {}, DatePickerDefaults ));
    });
    // кнопка закрыть форму расширенного поиска
    obj.find(".ext-search-close").click(function(){
        obj.hide();
    });
    //кнопка поиск
    obj.find(".search_btn").click(function(){
        dropDownSearch(obj, this);
    });
        //очистить все поля
    obj.find(".clear_btn").click(function(){
        obj.find('.search-clear').each(function(){
            $(this).click();
        });
    });
    //комбобоксы тип даты
    obj.find('.reg_date, .doc_date').each(function(){
        var self = this;
        $(this).click(function(){
            var cls = $(self).attr("class");
            var val = $(self).val();
            if(val=="1" || val=="3" || val=="4"){
                $('.'+cls+'_end').datepicker('disable',true).val('');
            }else{
                $('.'+cls+'_end').datepicker('enable',true).val($('.'+cls+'_beg').val());
            }
        })
    });
    // кнопки очистить значение
    obj.find('.reg_number, .doc_number, .reg_date_beg, .reg_date_end, .doc_date_beg, .doc_date_end').each(function(){
        var self = this;
        $(this).after(
            $('<img class="search-clear" src="images/clear.png" alt="Очистить значение" style="vertical-align: bottom; padding-bottom: 2px;">').click(
                function(){
                    $(self).val('');
                }
            ))
    });



}
function historyDropDown(sender){
    var cntName=$(this).attr('menu-content');
    var obj = $(cntName);
    var left = $(this).offset().left - obj.width();
    obj.css('left',left);
    obj.toggle("fast");
}

function extSearchDropDown(){
    var searchcnt = $(this).attr('searchcnt');
    var o = $("."+searchcnt);
    if($("."+searchcnt+":disabled").length > 0){
        return;
    }
    var offset = o.offset();
    var obj = $('.ext-search-content');
    obj.find(".search_btn").attr("tag_div", "." + searchcnt + "_div");
    var left = $(this).offset().left - obj.width();
    obj.css('top',offset.top + o.height() ).css('left',left).show();

}

function extSearchClick(){
    navigateUI.activateNavigateTab();
    searchHeaderPanel.setActiveTab(1);
}
// кнопка простого поиска на главной странице
function simpleSearchClick(sender){
    var searchcnt = $(sender.target).attr('searchcnt');
    var text = $("."+searchcnt).val();
    var obj = $('.ext-search-content');
    //obj.find(".search_btn").click();
    dropDownSearch(obj,obj.find(".search_btn")[0]);
    //navigateUI.makeSearch(text);
}

function simpleSearchKeyDown(e){
 if(e.keyCode==13){
     simpleSearchClick(e);
 }
}

function userRequestCheckPhone(sender){
    var regexObj = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    var phone2 = /^(\+\d)*\s*(\(\d{3}\)\s*)*\d{3}(-{0,1}|\s{0,1})\d{2}(-{0,1}|\s{0,1})\d{2}$/;
    var str = sender.innerText || sender.textContent;
    str = str.replace(/^[\s(&nbsp;)]+/g,'').replace(/[\s(&nbsp;)]+$/g,'');
    return phone2.test(str);
}

function userRequestEmail(sender){
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    var str = sender.innerText || sender.textContent;
    str = str.replace(/^[\s(&nbsp;)]+/g,'').replace(/[\s(&nbsp;)]+$/g,'');
    return emailPattern.test(str);
}


function adjustDocURLs(el,success,response,options){
    var pnlObj = $("#"+el.id);
    var elem =pnlObj.find('a');
    var self = this;
    elem.each(function(){
        var href = $(this).attr('href');
        if(!isStrNullOrEmpty(href)){
            var respText  = href.replace(new RegExp('aps://','g'),'#!doc_');
            respText  = respText.replace(new RegExp('apsfile://','g'),'#!doc_');
            if($(this).text().indexOf('Загрузить инструкцию СПС')>=0){
                /*var simpleHelpClick = function(){

                }
                $("a.simple_help").unbind('click').click(simpleHelpClick);*/
                $("a.simple_help").attr('href',respText);
            }
            $(this).attr('href',respText);
        }
    });
    // ищем служебные ссылки для манипуляции с ними
    var links = pnlObj.find('p b u').each(function(){
        var txt = $(this).text();
        if ('Вход для зарегистрированных пользователей' == txt) {
            $(this).css('cursor','pointer').click(function () {
                userUI.showUI(null);
            })
        }else if('Гостевой вход' == txt){
            $(this).css('cursor','pointer').click(getGuestClickFunction());
        }
    });
    // диалоговое окно
    var pWindow = this && this.pWindow ;
    //кнопка отправить заявку
    var guestWindowObj=pnlObj.find('.btn_user_request_send').click(function(){
        if($(this).attr("disabled")!=null) return;
        var infoObj = pnlObj.find('.user_request_info');
        var hasErrors = false;
        // фукнция обрезация пробелов и пр
        var trim = function (str){
            return str.replace(/^[\s(&nbsp;)]+/g,'').replace(/[\s(&nbsp;)]+$/g,'');
        };
        //проверка на пустые значения
        infoObj.find(".check_empty").each(function(idx,scope){
            if(hasErrors) return;
            var error = isStrNullOrEmpty(trim($(scope).text()));
            hasErrors = hasErrors || error;
            if(error){
                alert($(scope).attr('err'));
            }
        });
        //проверка через функции
        infoObj.find(".check_func").each(function(idx,scope){
            if(hasErrors) return;
            var funcName = $(scope).attr('func');
            var error = !window[funcName](scope);
            hasErrors = hasErrors || error;
            if(error){
                alert($(scope).attr('err'));
            }
        });
        //если есть ошибки, то ничего не делаем
        if(hasErrors) return;
        var info = infoObj.html();
        var sender = this;
        // создаем слушателя - callback функцию
        pWindow.onUserSendRequest = function(response, scope, extraParams, o, requestInfo){
            var pWindow = this ;
            if(pWindow ) pWindow.hide();
            navigateUI.setDisabledNavigate(false);
            alert(response.responseText);
        };
        $(sender).attr('disabled', true).css('opacity','0.4');
        callRequestHandler("doSendUserRequest",{html:info}, CallClassMethodAfterAjaxReturn, CallClassMethodAfterAjaxReturn,
            {thisRef:pWindow,thisMethod:'onUserSendRequest'});

    });
    // кнопка отправить заявку позже
    var guestWindowObj1=pnlObj.find('.btn_user_request_later').click(function(){
        pWindow.hide();
        navigateUI.setDisabledNavigate(false);
    });
    var monDiv = $('#for_monitoring').parent();
    if(monDiv && monDiv.length > 0){
        //var monDiv = $("#monitoring_main");
        //parentDiv.append(monDiv);
        var onMonitoringList = function(response){
            var detailDiv = setMonirotingDropDownPos($(".simple_monitoring"));//$("#monitoring");
            var monGroupedView = $('#monitoring_main');
            monDiv.append(monGroupedView);
            monGroupedView.show();
            prepareMonitoringList(detailDiv, response, false, monDiv.find(".list"));
        };
        callRequestHandler("getMonList",{}, onMonitoringList, onMonitoringList,{});
    }

}

function getGuestClickFunction(){
    return function(){
        var extraData = {};
        var pnl =  new Ext.Panel({ region:'center',autoHeight: false, autoScroll: true, border: false,
            bodyStyle:'overflow:auto;padding-top:10px;padding-left:10px;',extraData:extraData,
            autoLoad:{url: 'UserRequest.html',params:{}, callback:adjustDocURLs, scope:extraData}});
        // окно где показывается панель
        var window = new Ext.Window({
            autoHeight: false
            , height: 450
            , width: 700
            , closable: false
            , closeAction:'hide'
            , maximizable: true
            , modal: true
            , layout: 'fit'
            //, bbar: panelToolbar
            , listeners: {
                /*close: function (scope) {
                 scope.extraData.thisRef.closeWindow('no');
                 },*/
                afterlayout: onAfterPanelLayout
            }
        });
        extraData.pWindow = window;
        window.add(pnl);
        window.show();
        window.doLayout();
    };
}

function initHistoryEvents(navigateUI){
    var documentUI = navigateUI.documentUI;
    $.history.init(function (hash) {
        if(hash == $.history.ignoreHash) {
            $.history.ignoreHash = null;
            return;
        }
        if (hash == "!navigate") {
            navigateUI.onHistoryNavigate();
        }else if (hash == "!main") {
            // показываем главную страницу
            showInfo();
        }else if (hash.indexOf("!doc_") == 0) {
            documentUI.openDocument(hash.substr(5), null, true);
        }
    });
    $(window).bind('beforeunload', function(){
        documentUI.closeDocument(true);
        return null;
    });
    if(window.location.hash.indexOf("#!doc_") < 0){
        // показываем главную страницу
        $.history.load("!main");
    }
}

// построение пользовательского интерфейса
function continueBuildLightUI() {
    userUI = new UserUI('userDiv');
    userUI.buildUI();
    navigateUI = new NavigateUI(userUI);
    bookmarkUI = new UserBookmarkUI(navigateUI);
    //поисковая форма
    panelSearch = navigateUI.buildUI();
    var infoExtraData = {};
    // строит дерево классификатора
    navigateUI.buildNavigateTree(infoExtraData);
    infoExtraData.navTree.region = 'west';
    infoExtraData.navTree.width = 425;
    var pnlInfoHtml = '<div style="height:100%;width:100%;text-align: center; vertical-align: center"> <div style="top: 50%; position: relative; ">\
                                    <input class="simple_search search1" type="text" style="height: 25px; width: 50%;"searchcnt="search1"> \
                                    <input class="simple_search_btn" type="button" value="Поиск" style="width:100px;" searchcnt="search1"> <br/>  \
                                    <a class="simple_login"  href="#" > Вход для зарегистрированных пользователей</a> \
                                    </div></div>';
    //главная страница
    panelInfo = new Ext.Panel({
                border: false,
                autoScroll: true,
                layout: 'border',
                listeners: {
                    afterlayout: onAfterPanelLayout
                },
                title: '',
                defaults: {
                    split: true
                },
                extraData: infoExtraData,
                items: [
                    infoExtraData.navTree,
                    new Ext.Panel({ region:'center',autoHeight: false, autoScroll: true, border: false, bodyStyle:'overflow:auto;padding-top:10px;padding-left:10px;min-height:60px;',
                            autoLoad:{url: 'RequestHandler',params:{proc: 'getDefaultPage', par: '', requestType: 'proc'}, callback:adjustDocURLs}/*,
                                html: pnlInfoHtml */})]
            });
    panelInfoParent = new Ext.Panel({
        layout: 'fit',
        border:false,
        //title: 'Информация',
        items: [panelInfo]
    });
    var mainTabPanelItems = [panelInfoParent];
    mainTabPanelItems.push(panelSearch);
    mainTabPanel = new Ext.Panel({
                layout: 'card',
                activeItem: 0,
                region: 'center',
                border: false,
                collapsible: false,
                items: mainTabPanelItems,
                margins: '1 0 0 0'
            });
    mainTabPanel.getActiveTab = function () {
        return this.layout.activeItem;
    };
    mainTabPanel.setActiveTab = function (tab) {
        this.layout.setActiveItem(tab);
    };
    // логотип
    var logoPnl = new Ext.Panel({ border:false, html: '<div id="header" ><a href="#!main" onclick="showInfo()"  style="float:left;"><img src="images/logo.png" height="80px"></a>    </div>' });
    logoPnl.border = false;
    // панель заголовок с элементами для поиска
    extSearchHeaderPanel=new Ext.Panel({columnWidth: 1, /*region:'center',*/ layout: 'ux.row',border: false,bodyStyle:'white-space: nowrap;',
                listeners: {
                    afterlayout: onAfterPanelLayout
                },height:80
                ,items:navigateUI.buildSearchControls()});

    simpleSearchHeaderPanel = new Ext.Panel({ region:'center',autoHeight: false, autoScroll: true, border: false, bodyStyle:'overflow:auto;padding-top:10px;padding-left:10px;height:100%;min-height:60px;',
        //autoLoad:{url: 'RequestHandler',params:{proc: 'getDefaultPage', par: '', requestType: 'proc'}, callback:adjustDocURLs},
        html: '<div style="width:100%;text-align: left; vertical-align: middle"> <div style="top: 20%; position: relative; ">\
                    <input class="simple_search search2" type="text" style="height: 25px; width: 50%;padding-right: 20px;" searchcnt="search2"> \
                    <img title="Расширенный поиск" src="images/ext_search.png" class="ext_search_dropdown" width="24" height="24" style="position: relative; top: 7px;" searchcnt="search2"> \
                    <input title="Поиск" class="simple_search_btn" type="image" src="images/simple_search_btn.png" searchcnt="search2" style="position:relative;top:7px;">   \
                    <input title="История открытых документов" class="simple_history" menu-content="#open_doc_history" type="image" src="images/simple_history.png" style="position:relative;top:7px;">   \
                    <input title="Мониторинг обновлений" class="simple_monitoring" menu-content="#monitoring" type="image" src="images/doc_mon.png" style="position:relative;top:7px;">   \
                    <a title="Инструкция" class="simple_help" href="#" style="position:relative;top:7px;"><img src="images/simple_help.png"/></a>   \
                    <a title="Главная страница" class="simple_home" href="#!main"  onclick="return showInfo()" style="position:relative;top:7px;"><img src="images/simple_home.png"/></a>   \
                    <a class="ext_search_link"  href="#" > Расширенный поиск </a> \
                    <div id="userDiv" style="float: right; width: 220px; vertical-align: top;position:relative;top:10px;padding-right:10px;text-align:right;"> \
                        <a class="userName" class="right-menu1" href="#" style="vertical-align: top;" title="Информация о пользователе" onclick="return userUI.showUI()" menu-click="showUI" menu-click-scope="userUI">'+METADATA.login_content+'</a>\
                        <a class="logout" href="#" style="display:none;vertical-align: top; " title="Выйти из системы" onclick="return userUI.doLogout()" ><img src="images/logout.png" /></a> \
                        <a class="admin" href="#" style="display:none;vertical-align: top; " title="Управление пользователями" onclick="return userUI.doUserAdmin()" >Пользователи</a> \
                        <a class="activate" href="#" style="display:none;vertical-align: top; " title="Активировать продукт" onclick="return userUI.askActNumber(this)" >Активировать</a> \
                        <a class="get_updates" href="#" style="display:none;vertical-align: top; " title="Скачать и установить обновления" onclick="return userUI.getPatches()" >Обновить</a> \
                    </div> \
                    </div><div class="search2_div"></div> \
        </div>' });
    searchHeaderPanel = new Ext.Panel({
        layout: 'card',
        columnWidth: 1,
        activeItem: 0,
        border: false,
        collapsible: false,
        items: [simpleSearchHeaderPanel/*, extSearchHeaderPanel*/],
        margins: '1 0 0 0'
    });
    searchHeaderPanel.getActiveTab = function () {
        return this.layout.activeItem;
    };
    searchHeaderPanel.setActiveTab = function (tab) {
        this.layout.setActiveItem(tab);
    };
    mainHeaderPanel = new Ext.Panel({
        region: 'north',
        //autoHeight: true,
        layout: 'column',
        visible: true,
        border: false,
        cls:'header-panel',
        height: 80 , //minSize: 100, maxSize: 250,
        ///*autoScroll: true,  */style: 'vertical-align:middle;',
        items: [
            new Ext.Panel({ border: false, width: 150, region: 'west', items: [logoPnl] }),
            searchHeaderPanel]
    });
    var toolbarItems = [];
    initMask.hide();
    toolbarItems.push('-');
    var menuBar = new Ext.Toolbar({ items: toolbarItems });
    var tmpPanel = new Ext.Panel({
                layout: 'border',
                region: 'center',
                border:false,
                deferredRender: false,
                //tbar: menuBar,
                items: [mainTabPanel]
            });
//    mainTabPanel.mytbar = menuBar;
    var mainPanel = new Ext.Panel({
        layout: 'border',
        bodyCls:'roundStyle',
        deferredRender: false,
        items: [tmpPanel, mainHeaderPanel]
    });

    viewPort = new Ext.Viewport({
        layout: 'fit',
        items: [mainPanel]
    });
    // выравниваем пользовательский интерфейс
    mainHeaderPanel.doLayout();
    mainTabPanel.doLayout();
    viewPort.doLayout(true);
    mainTabPanel.setActiveTab(panelSearch);
    viewPort.doLayout();
    // фикс для корректного отображения
    mainPanel.el.dom.style.height = 60;
    //регистрируем горячие клавиши
    navigateUI.initKeyMap(searchHeaderPanel);
    navigateUI.setDisabledNavigate(true);
    initHistoryEvents(navigateUI);
    $(".ext_search_link").hide();
    // проверяем авторизованного пользователя
    checkUserLogged();
    bookmarkUI.buildUI();
}
// периодически опрашивает состояние авторизации
function checkUserLogged(){
    var timerId = null;
    var checkEvent = function(){
        userUI.checkUser();
        if(timerId != null){
            clearInterval(timerId);
        }
        timerId = setInterval(checkEvent,60000);// 1 min
    };
    checkEvent();
}

monthA = 'января,февраля,марта,апреля,мая,июня,июля,августа,сентября,октября,ноября,декабря'.split(',');
monthUpper = 'Январь,Февраль,Март,Апрель,Май,Июнь,Июль,Август,Сентябрь,Октябрь,Ноября,Декабрь'.split(',');
//var monDetails = { current:null };
var monDetailsObj = {};

function getMonDetails(obj){
    monDetailsObj[obj]  = monDetailsObj[obj] ||{ current:null };
    return monDetailsObj[obj]
}

// перейти к документам (измененные, новые, устаревшие и прочее)
function monitorDetailNavigateTo(sender,obj){
    var detailId = $(sender.currentTarget).attr("show_det");
    var first = obj.find("."+detailId);
    if(first && first.length > 0) {
        var el = first.get(0);
        var elObj = obj[0];
        obj.scrollTop(el.offsetTop - elObj.offsetTop);
    }
}
// расстановка документов по категориям
function showMonitoringDetail(sender,obj,id,response){
    var monDetails = getMonDetails(obj.parent().attr("id"));
    if(monDetails.current == id){
        monitorDetailNavigateTo(sender, obj);
        return true;
    }
    var prefix = ".mon_det_";
    for (var j = 1; j <= 4; j++) {
        obj.find(prefix + j).empty();
    }

    if(getResponseErrorText(response)) return false;
    try{
        var data = monDetails[id] || eval('('+getResponseText(response)+")");
        monDetails[id] = data;
        var inc = {
            '1':0,
            '2':0,
            '3':0,
            '4':0
        };
        for (var i = 0; i < data.length; i++) {
            var item = data[i];
            var idx = item[1];
            var val = $("<a style='font-weight: normal;'/>").html( "<b>"+(++inc[idx]) +"</b> "+ item[2]).attr("href","#!doc_"+item[0]);
            obj.find(prefix + idx).append($("<span style='display: block;'/>").append(val));
        }
        monDetails.current = id;
        var cssSelected = "mon_selected";
        for (var k = 0; k < lastList.length; k++) {
            var obj1 = lastList[k];
            var id1 = obj1[0];
            var uiEl = obj.parent().find(".mon_row_"+id1);
            if(id1 == monDetails.current){
                uiEl.addClass(cssSelected);
            }else{
                uiEl.removeClass(cssSelected);
            }
        }
        monitorDetailNavigateTo(sender, obj);
    }catch (e){

    }
    return true;
}
// получение списка документов в обновлении
function getMonitoringDetailAction(obj, id, parentControlID){
    var container = obj;
    var internalID = id;
    var divID = parentControlID;
    return function(sender){
        var divObj = $('#'+divID);
        if(divObj.is(":hidden")){
            divObj.show();
        }
        var monDetails = getMonDetails(divID);
        if(monDetails[internalID]){
            showMonitoringDetail(sender,container, internalID, null);
            return false;
        }else{
            var onMonitoringDetail = function(response){
                showMonitoringDetail(sender,container, internalID, response);
            };
            callRequestHandler("getMonDetail",{id:internalID}, onMonitoringDetail, onMonitoringDetail,{});
            return false;
        }
    }
}

var lastList = null;
// показать список изменений
function prepareMonitoringList(obj, response, isPopup, snapshotDiv){
    if(getResponseErrorText(response)) return;
    try{
        var table = obj.find('table.list');
        var detailDiv = obj.find('.detail');
        var tableHr = obj.find('table.header');
        table.empty();
        tableHr.empty();
//        if(isPopup){
            var tr=$('<tr><td colspan="6" style="text-align: center;font-size: 1.2em;margin-top: 5px;"><strong>Мониторинг обновлений СПС «Правовой консультант»</strong>'+
                '<span style="float: right;"><img class="ext-close" alt="Закрыть" src="images/doc-close.png"></span></td></tr>');
            // кнопка закрыть
            tr.find(".ext-close").click(function(){
                obj.hide();
            });
            tableHr.append(tr);
//        }
        var data = lastList || eval('('+getResponseText(response)+")");
        lastList = data;
        var itemOrder = ["new","edited","old","early"];
        var defaultLink = null;
        var tableMonBuilder = function(i, item, table, clickAction, installDate){
            var tr = $('<tr/>').attr("class","mon_row_"+m_id);
            tr.append($('<td style="width: 17px;text-align: center;">').text( i+1 ));
            var link = $("<a>").text( installDate.getDate()+' '+monthA[installDate.getMonth()]+' '+ installDate.getFullYear()).click(clickAction);
            // only first link
            defaultLink = defaultLink || link;
            tr.append($('<td style="padding: 3px;"/>').append(link));
            var tmp = {
                old:item[2],t_old:'Документы утратившие силу',
                early:item[3],t_early:'Документы, не введенные в действие',
                'new':item[4],t_new:'Новые документы',
                edited:item[5],t_edited:'Измененные документы'
            };
            for (var j = 0; j < itemOrder.length; j++) {
                var obj1 = itemOrder[j];
                var a = $("<a/>").text(tmp[obj1]).attr('title', tmp['t_' + obj1]).attr("show_det","mon_det_"+obj1).attr("href","#").click(clickAction);
                var td = $("<td/>").addClass("mon_" + obj1).append(a);
                tr.append(td);
            }
            table.append(tr);
        };
        var groups = {};
        var groupedTableMonBuilder = snapshotDiv==null || snapshotDiv.length == 0 ? null :
            function(i, item, snapshotDiv, clickAction, installDate){
                var grIdx = ''+installDate.getMonth()+'_'+ installDate.getFullYear();
                var group = groups[grIdx] || { cnt : 0, holder : null };
                groups[grIdx] = group;
                if ( group.holder == null){
                    snapshotDiv.append($('<h4 style="font-size:12px;margin-top:5px;"/>').text((monthUpper[installDate.getMonth()]+' '+installDate.getFullYear())));
                    group.holder = $('<table class="list" cellspacing="0" cellpadding="0" border="1" align="left" style="width: 95%;font-size:11px;border-collapse: collapse;"/>').css("margin-left","5px");
                    snapshotDiv.append(group.holder);
                }
                tableMonBuilder(group.cnt++, item, group.holder, clickAction, installDate);
        };
        var monLimit = 500;
        for( var i = 0; i < data.length; i++){
            var item = data[i];
            var m_id = item[0];
            var clickAction = getMonitoringDetailAction(detailDiv, m_id, obj.attr("id"));
            var installDate = DateTimeFromStr(item[1].replace(".0",""),'Y-m-d H:i:s');

            if(groupedTableMonBuilder == null && i > monLimit) {
                break;
            }
            if(i <= monLimit){
                tableMonBuilder(i, item, table, clickAction, installDate);
            }
            if(groupedTableMonBuilder){
                groupedTableMonBuilder(i, item, snapshotDiv, clickAction, installDate)
            }
        }
        if(groupedTableMonBuilder == null) defaultLink.click();
    }catch(e){

    }
}
function setMonirotingDropDownPos(sender){
    var cntName=$(sender).attr('menu-content');
    var obj = $(cntName);
    var left = $(sender).offset().left - obj.width();
    obj.css('left',left);
    try{
        obj.resizable();
    }catch(e){}

    return obj;
}
// событие при нажатии кнопки показать мониторинг
function monitoringDropDown(sender){
    var obj = setMonirotingDropDownPos(this);
    obj.toggle( null );
    var onMonitoringList = function(response){
        prepareMonitoringList(obj, response, true);
    };
    if (obj.is(':visible')) {
        if(lastList!=null){
            prepareMonitoringList(obj, null, true);
        }else{
            callRequestHandler("getMonList",{}, onMonitoringList, onMonitoringList,{});
        }
    }
}

function onActionLogResponse(response){

}

function sendActionLog(action, msg){
    callRequestHandler("addActionLog", {type:action, msg: msg}, onActionLogResponse, onActionLogResponse,{});
}