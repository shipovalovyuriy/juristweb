function buildSearchUI(navigateUI) {
    (function ($) {

        $(function () {
            var loc = window.location.href;
            if (!loc) loc = 'http://localhost:8080/juristWeb/';
            Manager = new AjaxSolr.Manager({
                solrUrl: loc
            });
            navigateUI.manager = Manager;
            Manager.navigateUI = navigateUI;
            //noinspection JSValidateTypes
            Manager.addWidget(new AjaxSolr.ResultWidget({
                id: 'result',
                target: '#docs'
            }));
            Manager.addWidget(new AjaxSolr.PagerWidget({
                id: 'pager',
                target: '#pager',
                targetNav: '#' + navigateUI.navSpin.id,
                prevLabel: '&lt;',
                nextLabel: '&gt;',
                innerWindow: 1,
                delayedTask: null,
                delayPageSelect: function () {
                    if (this.delayedTask == null) {
                        this.delayedTask = new Ext.util.DelayedTask(this.onPageSelect, this);
                    }
                    this.delayedTask.delay(500);
                },
                onPageSelect: function () {
                    // переход на определенную страницу
                    var val = $(this.targetNav).val();
                    if (val != this.currentPage) {
                        this.customClickHandler(val);
                    }
                },
                windowedLinks: function () {
                    return [];
                },
                renderLinks: function (links) {
                },
                renderHeader: function (perPage, offset, total) {
                    $(this.targetNav).val(this.currentPage);
                    $(this.targetNav).data("spinboxMin", Math.min(1, this.totalPages));
                    $(this.targetNav).data("spinboxMax", this.totalPages);
                    $(this.target + '-header').html($('<span/>').text(' документы c ' + Math.min(total, offset + 1) + ' по  ' + Math.min(total, offset + perPage) + ' из ' + total + ' '));
                    // текущее поле для сортировки
                    var currField = (this.manager.response.responseHeader.params && this.manager.response.responseHeader.params.sort) || '';
                    // картинки направления сортировки
                    var sortDirections = ['<img src="images/sort_up.png" style="position: relative; top: 4px;"/>', '<img src="images/sort_down.png" style="position: relative; top: 4px;"/>'];
                    // поля для сортировки
                    var sortFields = [
                        {name: 'fullName', caption: 'Названию'},
                        {name: 'publishDate', caption: 'Дата издания'},
                        {name: 'score', caption: 'Релевантность'},
                        {name: 'regDate', caption: 'Дата регистрации'},
                        {name: 'priority', caption: 'Сила'}
                    ];
                    var self = this;
                    var sortDir = sortDirections[currField.indexOf(' desc') >= 0 ? 1 : 0];
                    // рисуем иконку сортировки по возрастанию или убыванию
                    var span2 = $('<a href="#"/>').attr('style', 'position: relative; top: -2px;')
                        .html('&nbsp;<img src="images/sort_alphabet.png" style="position: relative; top: 4px;"> ' + sortDir)
                        .click(function () {
                            var fld = currField;
                            if (fld.indexOf(' desc') >= 0) {
                                fld = fld.replace(' desc', ' asc');
                            } else {
                                fld = fld.replace(' asc', ' desc');
                            }
                            self.customClickHandler(1, fld);
                        });
                    // рисуем список полей для сортировки
                    var select = $('<select/>').change(function () {
                        var fld = $(this).val();
                        fld += currField.indexOf(' desc') >= 0 ? ' desc' : ' asc';
                        self.customClickHandler(1, fld);
                    });
                    var mode = this.navigateMode(); //
                    // комбобокс выбора сортировки
                    for (var i = 0, len = sortFields.length; i < len; i++) {
                        var sortField = sortFields[i];
                        if (mode == 'browse' && sortField.name == 'score') continue;
                        var opt = $('<option/>').attr("value", sortField.name).text(sortField.caption);
                        if (currField.indexOf(sortField.name) >= 0) opt.attr('selected', 'selected');
                        select.append(opt);
                    }
                    $(this.target + '-header').append(select).append(span2);
                    //$('#pager-header').append(span1);
                },
                navigateMode: function () {
                    return this.manager.response.mode;
                },
                customClickHandler: function (page, sortMode, incSearch) {
                    // событие отрабатывает при переходе на определенную страницу,
                    // или сортировке по выбранному полю
                    // или при инкрементальном поиске
                    var self1 = this;
                    // смотрим режим поиска или режим классификатора
                    var mode = self1.navigateMode();
                    if (page <= 0) return;
                    // определяем позицию смещения в списке
                    var offset = (page - 1) * self1.customPerPage();
                    // если режим классификатора
                    if (mode == 'browse') {
                        navigateUI.loadList(null, offset, sortMode, incSearch);
                    } else if (mode == null || mode == undefined || mode == '') {
                        // режим поиска по индексу
                        var self = this;
                        self.manager.store.get('start').val(offset);
                        // выбрали поле для сортировки
                        //13082210000197 — ОНЛ_доработка_функционала_1
                        self.manager.store.get('sort').val( sortMode || 'priority asc');
                        // включен инкрементальный поиск
                        if (incSearch) {
                            var fldName = 'sfullName';
                            var obj = $('.ext-search-content');
                            //obj.find(".search_btn").click();
                            // заполним параметры поиска
                            dropDownSearch(obj, obj.find(".search_btn")[0], incSearch, fldName + ':' + incSearch + '*');
                        }else{
                            // посылаем запрос на сервер
                            self.manager.doRequest();
                        }
                    } else {
                        navigateUI.customLoadListHandler(mode, null, offset, sortMode, incSearch);
                    }
                }
            }));
            var pager = Manager.widgets["pager"];
            // переход на определенную страницу
            $(pager.targetNav).spinbox()
                .change(function () {
                    pager.delayPageSelect()
                });
            var fields = [ 'cat_id'/*, 'codes'*/ ];
            for (var i = 0, l = fields.length; i < l; i++) {
                //noinspection JSValidateTypes
                Manager.addWidget(new AjaxSolr.TagcloudWidget({
                    id: fields[i],
                    target: '#' + fields[i],
                    field: fields[i]
                }));
            }

            //noinspection JSValidateTypes
            Manager.addWidget(new AjaxSolr.CurrentSearchWidget({
                id: 'currentsearch',
                target: '#selection'
            }));
            // выпадающие списки с подсказками
            var tsearch = navigateUI.extraData && navigateUI.extraData.cSearch && ('#' + navigateUI.extraData.cSearch.id);
            //noinspection JSValidateTypes
            Manager.addWidget(new AjaxSolr.AutocompleteWidget({
                id: 'text',
                target: tsearch || '#search',
                fields: [ 'cat', 'codes']
            }));

            //tsearch = navigateUI.extraData && navigateUI.extraData.cDocNumber && ('#' + navigateUI.extraData.cDocNumber.id);
            tsearch = '#doc_number';
            //noinspection JSValidateTypes
            Manager.addWidget(new AjaxSolr.AutocompleteWidget({
                id: 'doc_number',
                target: tsearch || '#search',
                fields: [ /*'cat',*/'codes']
            }));

            //tsearch = navigateUI.extraData && navigateUI.extraData.cRegNumber && ('#' + navigateUI.extraData.cRegNumber.id);
            tsearch = '#reg_number';
            //noinspection JSValidateTypes
            Manager.addWidget(new AjaxSolr.AutocompleteWidget({
                id: 'reg_number',
                target: tsearch || '#search',
                fields: [ /*'cat',*/'regNumber']
            }));

            Manager.init();
            Manager.store.addByValue('q', '*:*');
            var params = {
                rows: 20,
                facet: true,
                'facet.field': [ 'cat_id' ],
                'facet.limit': 20,
                'fl': 'id,fullName,shortName,publishDate,priority,lastModified,regDate,score,cat_id,codes',
                'facet.mincount': 0,
                //13082210000197 — ОНЛ_доработка_функционала_1
                'sort': 'priority asc',
                'f.cat_id.facet.multi': true,
                'f.cat_id.facet.limit': 50,
                /*
                 'f.topics.facet.limit': 50,
                 'f.countryCodes.facet.limit': -1,
                 'facet.date': 'date',
                 'facet.date.start': '1987-02-26T00:00:00.000Z/DAY',
                 'facet.date.end': '1987-10-20T00:00:00.000Z/DAY+1DAY',
                 'facet.date.gap': '+1DAY',
                 */
                'json.nl': 'map'
            };
            for (var name1 in params) Manager.store.addByValue(name1, params[name1]);
            // первоначальный поиск
            Manager.doRequest();
        });

        $.fn.showIf = function (condition) {
            if (condition) {
                return this.show();
            } else {
                return this.hide();
            }
        }

    })(jQuery);
}

function NavigateUI(userUI) {
    this.userUI = userUI;
    this.storesToReload = [];
    userUI.on("change", this.afterLogin, this);
    this.listLoadHandlers = {};
}
// обновляем дерево классификатора после авторизации
NavigateUI.prototype.afterLogin = function (userUI) {
    var stores = this.storesToReload;
    for (var i = 0, il = stores.length; i < il; i++) {
        var store = stores[i];
        store.load({});
    }
};
// кпопка постраничного перехода
NavigateUI.prototype.nextPage = function (scope) {
    var pager = Manager.widgets["pager"];
    pager.customClickHandler(pager.currentPage + scope.extraData.step);
};
//очистить поля инкрементного поиска
NavigateUI.prototype.clearSearch = function () {
    this.incSearchStr = '';
    this.cResultSearchInc.setValue('');
};
//очистить поля поиска
NavigateUI.prototype.doSearchClear = function () {
    var extraData = this.extraData;
    extraData.cDocDate_end.setValue(null);
    extraData.cDocDate.setValue(null);
    extraData.cRegDate_end.setValue(null);
    extraData.cRegDate.setValue(null);
    extraData.cRegNumber.setValue(null);
    extraData.cDocNumber.setValue(null);
    extraData.cStatus.setValue('');
    extraData.cSearch.setValue('');
    extraData.cExactSearch_bool.setValue(false);
    extraData.cSearchTitle_bool.setValue(true);
    extraData.cSearchTitle_bool.setValue(true);
};
NavigateUI.prototype.switchSimpleSearch = function (sender) {
    searchHeaderPanel.setActiveTab(0);
    $(".simple_search").val(navigateUI.extraData.cSearch.getValue());
};

//ручное указание поиска
NavigateUI.prototype.makeSearch = function (text) {
    this.extraData.cSearch.setValue(text);
    $(".simple_search").val(text);
    this.activateNavigateTab();
    this.doSearch();
};

// параметры поиска из формы расширенного поиска
NavigateUI.prototype.getSearchParams = function (extraData) {
    return {
        publishDateBeg: extraData.cDocDate.getValue(),
        publishDateEnd: extraData.cDocDate_end.getValue(),
        regDateBeg: extraData.cRegDate.getValue(),
        regDateEnd: extraData.cRegDate_end.getValue(),
        regNumber: extraData.cRegNumber.getValue(),
        docNumber: extraData.cDocNumber.getValue(),
        status: extraData.cStatus.getValue(),
        bodySearch: extraData.cSearchBody_bool.getValue(),
        searchText: extraData.cSearch.getValue(),
        exactSearch: extraData.cExactSearch_bool.getValue()
    };
};

//кнопка поиск
NavigateUI.prototype.doSearch = function (aParams) {
    this.activateNavigateTab();
    this.clearSearch();
    this.mode = '';
    this.manager.store.remove('fq');
    var extraData = this.extraData;
    var fq = [];
    // берем параметры поиска
    var params = aParams || this.getSearchParams(extraData);
    if (params.publishDateBeg && params.publishDateEnd) {
        fq.push('publishDate:[' + DateTimeToSolr(params.publishDateBeg) + " TO " + DateTimeToSolr(params.publishDateEnd) + "]");
    }
    if (params.regDateBeg && params.regDateEnd) {
        fq.push('regDate:[' + DateTimeToSolr(params.regDateBeg) + " TO " + DateTimeToSolr(params.regDateEnd) + "]");
    }
    if (params.regNumber) {
        fq.push('regNumber:' + params.regNumber);
    }
    if (params.docNumber) {
        fq.push('codes:' + params.docNumber);
    }
    // поиск в найденном
    if (params.incSearch) {
        fq.push(params.incSearch);
    }
    if (params.status != '')  fq.push('status:' + params.status || '0');
    var searchField = 'sfullNameFlex';
    if (params.bodySearch) {
        searchField = 'sfullNameBodyFlex';
    }
    var exactSearchBool = params.exactSearch;
    this.manager.store.remove('q');
    var val = (params.searchText || '*');
    val = val.toLowerCase();
    // пишем журнал
    if(!isStrNullOrEmpty(params.searchText)){
        sendActionLog('search', val);
    }
    // если фраза, то ищется вхождение всех слов
    if (exactSearchBool || val.indexOf(' ') > 0) {
        searchField = searchField.substr(0, searchField.length - 4);
        if (val.indexOf(' ') > 0) {
            val = '"' + val + '"~50';
        }
    }
    // если пусто то вывод всех документов
    if (val == '*') {
        //this.manager.store.addByValue('q', fq.length > 0 ? fq[0] : '*:*');
        if (fq.length == 0)fq.push('*:*');
    } else {
        fq.push(searchField + ':' + val);
        //this.manager.store.addByValue('q', searchField+':' + val);
    }
    var manager = this.manager;
    // ищем фильтр по деревьям
    var checked = this.navTree.getChecked("id");
    if (checked && checked.length > 0) {
        var selNodes = "cat_id:(";
        for (var i = 0, len = checked.length; i < len; i++) {
            var row = "";
            if (i > 0) row += " OR ";
            row += checked[i];
            selNodes += row;
        }
        //this.manager.store.remove('rfq');
        fq.push(selNodes + ")");
        //this.manager.store.addByValue('fq', selNodes+")");
    }
    var q = "";
    for (var j = fq.length - 1; j >= 0; j--) {
        var el = fq[j];
        q += ( q != '' ? ' +' : '') + el;
        //manager.store.addByValue('fq', el);
    }
    this.manager.store.addByValue('q', q);
    this.manager.doRequest(0);
    this.collapseDocumentPanel(this.documentUI);
};

// создаем контролы поиска
NavigateUI.prototype.buildSearchControls = function () {
    var items = [];
    var extraData = {};
    var cSearch = new Ext.form.TextField({});//Текст для поиска (Номер документа)
    var cRegNumber = new Ext.form.TextField({});//Номер регистрации
    var cDocNumber = new Ext.form.TextField({});//Номер Документа
    var cStatus = createComboBoxLocal(loadStore(METADATA.DOC_STATUSES), 'TEXT', 'ID', true, '', true);// Актуальность документа
    // чтобы нельзя было вводить буквы
    var maskDate = new RegExp('[0123456789.\x08\s\t\w]');
    var cRegDate = new Ext.form.DateField({ format: 'd.m.Y', altFormats: altDateFormats, maskRe: maskDate });
    var cDocDate = new Ext.form.DateField({ format: 'd.m.Y', altFormats: altDateFormats, maskRe: maskDate });
    var cRegDate_end = new Ext.form.DateField({ format: 'd.m.Y', altFormats: altDateFormats, maskRe: maskDate });
    var cDocDate_end = new Ext.form.DateField({ format: 'd.m.Y', altFormats: altDateFormats, maskRe: maskDate });
    var cExactSearch_bool = new Ext.form.Checkbox({});
    var cSearchTitle_bool = new Ext.form.Radio({name: 'searchType', checked: true});
    var cSearchBody_bool = new Ext.form.Radio({name: 'searchType'});
    var cSearchResult_bool = new Ext.form.Checkbox({});
    cSearch.on('keypress', function () {
        this.taskSearch.delay(500);
    }, this);
    extraData.cSearch = cSearch;
    extraData.cRegNumber = cRegNumber;
    extraData.cDocNumber = cDocNumber;
    extraData.cStatus = cStatus;
    extraData.cRegDate = cRegDate;
    extraData.cDocDate = cDocDate;
    extraData.cRegDate_end = cRegDate_end;
    extraData.cDocDate_end = cDocDate_end;
    extraData.cExactSearch_bool = cExactSearch_bool;
    extraData.cSearchBody_bool = cSearchBody_bool;
    extraData.cSearchTitle_bool = cSearchTitle_bool;
    extraData.cSearchResult_bool = cSearchResult_bool;
    extraData.thisRef = this;
    cDocDate_end.extraData = extraData;
    cRegDate_end.extraData = extraData;
    cDocDate.extraData = extraData;
    cRegDate.extraData = extraData;
    cStatus.extraData = extraData;
    cRegNumber.extraData = extraData;
    cDocNumber.extraData = extraData;
    cSearch.extraData = extraData;
    cExactSearch_bool.extraData = extraData;
    cSearchBody_bool.extraData = extraData;
    cSearchTitle_bool.extraData = extraData;
    cSearchResult_bool.extraData = extraData;
    this.extraData = extraData;
    cRegNumber.columnWidth = 0.15;
    cDocNumber.columnWidth = 0.15;
    cRegDate.columnWidth = 0.10;
    cRegDate_end.columnWidth = 0.10;
    cDocDate.columnWidth = 0.10;
    cDocDate_end.columnWidth = 0.10;
    cSearch.columnWidth = 0.25;
    cExactSearch_bool.columnWidth = 0.02;
    cSearchBody_bool.columnWidth = 0.02;
    cSearchTitle_bool.columnWidth = 0.02;
    cSearchResult_bool.columnWidth = 0.02;
    var cSimpleSearchLnk = createSimpleLink("Простой поиск", CallClassMethodAfterAjaxReturn, { thisRef: this, thisMethod: 'switchSimpleSearch' });
    cSimpleSearchLnk.style = 'font-weight:bold;';
    var cSearchLnk = new Ext.Button({ text: "Поиск", extraData: { thisRef: this, thisMethod: 'doSearch' }, listeners: { click: CallClassMethodAfterAjaxReturn }, cls: "lnk_style_default btn-white" });
    cSearchLnk.addClass('btn-search');
    cSearchLnk.columnWidth = 0.07;
    //var cSearchClearLnk=createSimpleLink("Очистить",CallClassMethodAfterAjaxReturn, );
    var cSearchClearLnk = new Ext.Button({ text: "Очистить", extraData: { thisRef: this, thisMethod: 'doSearchClear' }, listeners: { click: CallClassMethodAfterAjaxReturn }, cls: "lnk_style_default btn-white" });
    cSearchClearLnk.addClass('btn-white');
    cSearchClearLnk.columnWidth = 0.1;
    items.push(createColumnPanel([
        //cSearchResult_bool, createLabel("Искать в найденном", 0.10),createEmptyLabel(0.03),
        cSearch, createEmptyLabel(0.03), cSearchLnk, createEmptyLabel(0.01),
        cExactSearch_bool, createLabel("Точное совпадение", 0.09),
        cSearchTitle_bool, createLabel("Искать в заголовке", 0.10), cSearchBody_bool, createLabel("Искать в тексте", 0.10), cSimpleSearchLnk
    ], [0.3, 0.03, 0.09, 0.01, 0.02, 0.16, 0.02, 0.14, 0.02, 0.11, 0.10]));
    items.push(createColumnPanel([
        createLabel("№ регистрации", 0.10, 'text-align:left;padding-right:3px;'), cRegNumber,
        createLabel("Дата регистрации с", 0.10, 'text-align:right;padding-right:3px;'), cRegDate,
        createLabel(" по ", 0.03, 'text-align:right;padding-right:3px;'), cRegDate_end, createEmptyLabel(0.03),
        cSearchClearLnk, createEmptyLabel(0.02)
    ], [0.15, 0.15, 0.13, 0.12, 0.04, 0.14, 0.04, 0.12, 0.04]));
    cStatus.columnWidth = 0.12;
    items.push(createColumnPanel([
        createLabel("№ документа", 0.10), cDocNumber,
        createLabel("Дата документа с", 0.10, 'text-align:right;padding-right:3px;'), cDocDate,
        createLabel(" по ", 0.03, 'text-align:right;padding-right:3px;'), cDocDate_end, createEmptyLabel(0.03),
        cStatus, createEmptyLabel(0.03) /*, cExactSearch_bool, createLabel("Точное совпадение", 0.10),createEmptyLabel(0.03)*/
    ], [0.15, 0.15, 0.13, 0.12, 0.04, 0.14, 0.04, 0.12, 0.11]));
    cStatus.setValue(0);
    return items;
};

// при получении списка документов
NavigateUI.prototype.onLoadList = function (res, scope, extraParams, o) {
    if (getResponseErrorText(res)) {
        alert(getResponseErrorText(res));
        return;
    }
    var res1 = getResponseText(res);
    var data = eval("(" + res1 + ")");
    this.manager.handleResponse(data);
};
// показать панели навигации
NavigateUI.prototype.activateNavigateTab = function () {
    if (mainTabPanel.getActiveTab() != this.panelSearch) {
        mainTabPanel.setActiveTab(this.panelSearch);
        searchHeaderPanel.setVisible(true);
        $(".ext_search_dropdown").show();
        //$(".ext_search_link").show();
        $(".right-menu").show()
    }
};
NavigateUI.prototype.addLoadListHandler = function (mode, handler, scope) {
    this.listLoadHandlers[mode] = {handler: handler, scope: scope};
};
// загрузить список документов для из обработчика
NavigateUI.prototype.customLoadListHandler = function (mode, tree_id, offset, sortMode, incSearch) {
    var obj = this.listLoadHandlers[mode];
    if (obj && obj.handler) {
        obj.handler.call(obj.scope || this, this, tree_id, offset, sortMode, incSearch);
    }
};
// загрузить список документов для выбранного дерева
NavigateUI.prototype.loadList = function (tree_id, offset, sortMode, incSearch, mode) {
    var count = 20;
    var offset1 = offset || 0;
    this.mode = mode || 'browse';
    this.selTreeId = tree_id || this.selTreeId;
    //13082210000197 — ОНЛ_доработка_функционала_1
    this.sortMode = sortMode || this.sortMode || 'priority asc';
    var incSearchStr = incSearch || this.incSearchStr;
    if (incSearch == '') incSearchStr = '';
    incSearchStr += '%';
    this.manager.beforeRequest();
    callRequestHandler("DocList", {tree_id: this.selTreeId, offset: offset1, count: count, sortMode: this.sortMode, incSearchStr: incSearchStr, mode: this.mode}, CallClassMethodAfterAjaxReturn, CallClassMethodAfterAjaxReturn, {thisRef: this, thisMethod: 'onLoadList'});
    this.activateNavigateTab();
    this.collapseDocumentPanel(this.documentUI);
};
// при выборе ветки дерева классификатора
NavigateUI.prototype.onTreeClick = function (node) {
    if (node.attributes.parameter == 9) {
        //
    } else {
        // node.text
        var selId = node.id;
        this.clearSearch();
        this.loadList(selId, 0);
        this.navTree.expandNodeById(selId);
    }
};
// инкрементальный поиск по дереву классификатора
function searchTree(scope, value, direction) {
    var tree = scope.navTree;
    if (scope.navTreeSearchText != value) {
        var items1 = [];
        var rootNode = tree.getRootNode();
        var regex = new RegExp(value, 'gi');
        tree.findNodeByText(rootNode.attributes, regex, items1);
        scope.navTreeSearchResults = items1;
    }
    var items = scope.navTreeSearchResults;
    if (scope.navTreeSearchIndex == null) scope.navTreeSearchIndex = 0;
    scope.navTreeSearchIndex += direction;
    if (scope.navTreeSearchIndex < 0) scope.navTreeSearchIndex = 0;
    if (scope.navTreeSearchIndex >= items.length) scope.navTreeSearchIndex = items.length - 1;
    if (scope.navTreeSearchIndex >= 0) {
        tree.expandNodeById(items[scope.navTreeSearchIndex]);
    }
    if (direction == 0) scope.cSearchInc.focus(false);
}

//  строит дерево классификатора
NavigateUI.prototype.buildNavigateTree = function (holder, checkboxes) {
    var store = createRemoteStore('trees', 'name', 'id', 'id', 'parent_id');
    store.extraData.filtered = false;
    store.extraData.thisRef = this;
    store.extraData.holder = holder;
    var checkboxEnabled = checkboxes;
    holder.navStore = store;
    holder.cClasses = this.buildControlClasses(holder);
    var sortField = 'sort_index';
    store.on('datachanged', function (scope) {
        // событие возникает при подгрузке данных или их фильтрации
        // @see NavigateUI.prototype.onClassChange
        var extraData = this.extraData;
        if (extraData.isSorting) return;
        // проверим фильтровали данные или нет
        if (!extraData.filtered) {
            extraData.filtered = true;
            // если не фильтровали, то выберем первый тип классификатора и отфильтруем его
            //extraData.holder.cClasses.setValue(1);
            extraData.thisRef.onClassChange(extraData.holder.cClasses);
            //extraData.thisRef.onClassClick(extraData.holder.cClasses.extraData.items[0]);
            return;
        }
        // выбираем поля для сортировки
        var f = this.fields.get(sortField);
        if (f != null) f.sortType = Ext.data.SortTypes.asFloat;
        extraData.isSorting = true;
        //сортируем
        this.sort(sortField, 'ASC');
        extraData.isSorting = false;
        // выделим ветки на которые есть права отдельним стилем grant
        var highlightStr = this.reader.jsonData.highlight || "";
        var highlight = highlightStr.split(",");
        // получим дерево из входных данных
        var node = buildTreeNodesFromStore(this, extraData.tableName, extraData.displayField, extraData.valueField, extraData.nodeField, extraData.parentField, checkboxEnabled, highlight);
        // покажем дерево
        if (extraData.tree != null) {
            extraData.tree.setRootNode(node);
            extraData.tree.show();
            extraData.filtered = false;
        }
    });
    // поле для инкрементального поиска
    holder.cSearchInc = new Ext.form.TextField({extraData: {searchIndex: 0}, enableKeyEvents: true, emptyText: 'Поиск по классификатору'});
    // запускаем поиск
    holder.cSearchInc.on('keypress', function (scope, e) {
        searchTree(this, holder.cSearchInc.getValue(), 0);
    }, holder);
    // ищем следующий найденный узел
    holder.searchTreeNext = function () {
        searchTree(this, holder.cSearchInc.getValue(), +1);
    };
    // ищем предыдущий найденный узел
    holder.searchTreePrev = function () {
        searchTree(this, holder.cSearchInc.getValue(), -1);
    };
    // ищем предыдущий найденный узел
    holder.treeCollapse = function () {
        var rootNode = this.navTree.getRootNode();
        rootNode.collapseChildNodes(true, false);
    };
    // ищем предыдущий найденный узел
    holder.treeExpand = function () {
        var rootNode = this.navTree.getRootNode();
        rootNode.expandChildNodes(true, false);
    };
    var treeConfig = {
        border: false,
        //title:'Классификатор',
        region: 'center',
        rootVisible: false,
        autoScroll: true,
        //autoHeight: true,
        root: new Ext.tree.AsyncTreeNode({
            text: 'treeRoot',
            id: '0'
        }), listeners: {
            // если нажем чекбокс на узле, выделим также все дочерние узлы
            'checkchange': function (n, checked) {
                // запомнить состояние узла (свернут или развернут)
                var isExpanded = n.isExpanded();
                //развернем все дочерние узлы
                n.expand(false, false);
                // если узел был свернут, свернем его
                if (!isExpanded) n.collapse();
                // жмем все чекбоксы
                n.eachChild(function (child) {
                    child.ui.toggleCheck(checked);
                    child.fireEvent('checkchange', child, checked);
                });
            }
        }, tbar: [
            // выбора типа классификатора
            holder.cClasses,
            // кнопка перейти к предудущему найденному узлу
            {iconCls: 'tree-node-expand', text: '', handler: holder.treeExpand, scope: holder, tooltipType: 'title', tooltip: 'развернуть классификатор'},
            // кнопка перейти к следующему найденному узлу
            {iconCls: 'tree-node-collapse', text: '', handler: holder.treeCollapse, scope: holder, tooltipType: 'title', tooltip: 'свернуть классификатор'},
            // разделитель
            '->',
            // поле ввода для быстрого поиска
            holder.cSearchInc,
            // кнопка перейти к предудущему найденному узлу
            {iconCls: 'search-prev', text: '', handler: holder.searchTreePrev, scope: holder, tooltipType: 'title', tooltip: 'предыдущий'},
            // кнопка перейти к следующему найденному узлу
            {iconCls: 'search-next', text: '', handler: holder.searchTreeNext, scope: holder, tooltipType: 'title', tooltip: 'следующий'}
        ], expandNodeById: function (id) {
            // развернуть узел
            var rootNode = this.getRootNode();
            var path = [];
            var node = this.getNodeById(id);
            if (node == null) {
                var tnode = this.findNodeById(rootNode.attributes, id, path);
                if (tnode != null) {
                    var cNode = rootNode;
                    for (var i = path.length - 2; i >= 0; i--) {
                        cNode = this.getNodeById(path[i]);
                        cNode.expand(true, false);
                    }
                    node = this.getNodeById(id);
                }
            }
            if (node != null) {
                node.ensureVisible();
                node.expand();
                node.select();
            }

        }, findNodeByText: function (nodeAttr, text, path) {
            // ищем ветки название которых соответствует поисковой строке
            // nodeAttr узел в котороМ начинается поиск
            // path массив куда положим все найденные узлы
            // text строка для поиска
            var children = nodeAttr.children;
            for (var i = 0, len = children.length; i < len; i++) {
                // если строка найдена
                if (children[i].text.search(text) >= 0) {
                    // запомним путь
                    path.push(children[i].id);
                }
                // ищем рекурсивно
                var child = this.findNodeByText(children[i], text, path);
            }
        }, findNodeById: function (nodeAttr, id, path) {
            // nodeAttr узел в котором начинается поиск
            // id идентификатор узла, который нужно найти
            // path массив кула складывается пути до найденного узла
            if (nodeAttr.id == id) return nodeAttr;
            for (var i = 0, len = nodeAttr.children.length; i < len; i++) {
                if (nodeAttr.children[i].id == id) {
                    path.push(nodeAttr.id);
                    return nodeAttr.children[i];
                } else {
                    var child = this.findNodeById(nodeAttr.children[i], id, path);
                    if (child != null) {
                        path.push(nodeAttr.id);
                        return child;
                    }
                }
            }
            return null;
        }
    };
    // строим дерево
    var tree = new Ext.tree.TreePanel(treeConfig);
    tree.nodeAction = 0;
    tree.on('click', this.onTreeClick, this);
    //var treeSorter = new Ext.tree.TreeSorter(tree, { dir: "asc" });
    store.extraData.tree = tree;

    holder.navTree = tree;
//    holder.navTreeSorter = treeSorter;
    store.baseParams['some_date'] = DateToStr(new Date());
    store.baseParams['query'] = 'search';
    store.load({});
    this.storesToReload.push(store);
};

// выбор типа классификатора
NavigateUI.prototype.onClassChange = function (sender) {
    var value = sender.getValue();
    var extraData = sender.extraData || this.extraData;
    if(value==0 && extraData.classes == null){
        navigateUI.activateNavigateTab();
        navigateUI.cClasses.setValue(value);
        extraData = navigateUI.cClasses.extraData;
    }
    if (extraData.thisRef.navigatePnl) extraData.thisRef.navigatePnl.setActiveTab(value == 0 ? 1 : 0);
    if (extraData.classes) extraData.classes.setValue(value);
    if (value != 0 && extraData.treeStore) {
        extraData.treeStore.filter('class_id', value, true, true);
    }
};
// @deprecated
NavigateUI.prototype.onClassClick = function (sender) {
    var extraData = sender.extraData || this.extraData;
    var items = extraData.items;
    for (var i = 0, l = items.length; i < l; i++) {
        if (items[i].textEl) items[i].setDisabled(items[i] == sender);
    }
    extraData.treeStore.filter('class_id', extraData.id, true, true);
};
// строим элемент для выбора типа классификатора
NavigateUI.prototype.buildControlClasses = function (holder) {
    var cClasses = createComboBoxLocal(loadStore(METADATA.CLASSIFICATORS), 'name', 'id', true, '', true);// Классы документа
    cClasses.setValue(METADATA.CLASSIFICATORS.data[0][0]);
    cClasses.on('select', this.onClassChange, cClasses);
    cClasses.on('change', this.onClassChange, cClasses);
    cClasses.extraData.treeStore = holder.navStore;
    cClasses.extraData.thisRef = holder;
    cClasses.defaultTriggerWidth = 120;
    cClasses.cls = 'width_120px';
    cClasses.setWidth(200);
    /*
     var store=loadStore(METADATA.CLASSIFICATORS);
     var items=[];
     for(var i=0,l=store.data.items.length;i<l;i++){
     var data=store.data.items[i].data;
     var tmpLnk = createSimpleLink(data.name, CallClassMethodAfterAjaxReturn, { holder:holder, thisRef: this, thisMethod: 'onClassClick', id: data.id});
     tmpLnk.columnWidth = 1/l;
     items.push(tmpLnk);
     tmpLnk.extraData.items = items;
     tmpLnk.extraData.treeStore = holder.navStore;
     }
     var classes=createColumnPanel(items);
     classes.region='north';
     classes.height=25;
     classes.extraData.items = items;
     */
    return cClasses;
};

NavigateUI.prototype.openDocument = function (docId, divDocId) {
    this.documentUI.openDocument(docId);
};

NavigateUI.prototype.onHistoryNavigate = function () {
    this.activateNavigateTab();
    this.documentPanel.setVisible(false);
    this.documentPanel.collapse();
    this.navigatePnl.expand(false);
    //if(this.bodyPnl.resultPanel) this.bodyPnl.resultPanel.expand();
    if (this.panelSearch.resultPanel) {
        this.panelSearch.resultPanel.expand(false);
        this.panelSearch.resultPanel.setWidth('98%');
    }
    this.panelSearch.doLayout(true);
    this.bodyPnl.doLayout(true);
};

NavigateUI.prototype.collapseDocumentPanel = function (docUI, noNavigate) {
    if(noNavigate === true) return;
    $.history.load("!navigate");
};

NavigateUI.prototype.onDocOpenEvent = function (docUI) {
    this.activateNavigateTab();
    this.navigatePnl.collapse();
    this.documentPanel.expand(false);
    this.documentPanel.setVisible(true);
    if (!this.panelSearch.resultPanel.collapsed) {
        this.documentPanel.setWidth('60%');
        this.panelSearch.resultPanel.setWidth('40%');
    }
    this.panelSearch.doLayout(true);
    this.bodyPnl.doLayout(true);
};

NavigateUI.prototype.getOnDocCloseEvent = function () {
    var self = this;
    return function (docUI, noNavigate) {
        self.collapseDocumentPanel(docUI, noNavigate);
    }
};

//2013082210000268 — ОНЛ_доработка_функционала_8
NavigateUI.prototype.getOnNextDocEvent = function () {
    var self = this;
    return function (docUI, docId) {
        return self.lookForNextDoc(docUI, docId);
    }
};

//2013082210000268 — ОНЛ_доработка_функционала_8
NavigateUI.prototype.lookForNextDoc = function (docUI, docId) {
    var docs = this.manager && this.manager.response && this.manager.response.response && this.manager.response.response.docs || [];
    for (var i = 0, len = docs.length; i < len; i++) {
        var doc = docs[i];
        // нашли наш документ, возвращаем следующий
        if(docId == doc.id){
            // если документ последний в списке, то возвращаем пусто
            if(i == len -1 ) return null;
            // следующий документ
            return docs[i + 1];
        }
    }
    // документа нету в списке
    return null;
};

NavigateUI.prototype.getOnDocOpenEvent = function () {
    var self = this;
    return function (docUI) {
        self.onDocOpenEvent(docUI);
    }
};

NavigateUI.prototype.searchKeyMapEvent = function (key, event) {
    var extraData = this.extraData;
    for (var i in extraData) {
        if (!extraData.hasOwnProperty(i)) continue;
        if (extraData[i].el && extraData[i].el.dom && extraData[i].el.dom == event.target) {
            this.doSearch();
        }
    }
};

NavigateUI.prototype.initKeyMap = function (searchHeaderPanel) {
    if (this.searchKeyMap == null && searchHeaderPanel.el != null) {
        this.searchKeyMap = new Ext.KeyMap(searchHeaderPanel.el.dom, {
            key: 13, // or Ext.EventObject.ENTER
            fn: this.searchKeyMapEvent,
            scope: this
        });
    }
};

/* Инкрементальный поиск */
NavigateUI.prototype.onResultSearchKeyPress = function () {
    this.resultSearch(this.cResultSearchInc.getValue());
};

NavigateUI.prototype.resultSearch = function (search) {
//    if(search==null || search=='' || search.length<3) return;
    var cSearchInc = this.cResultSearchInc;
    if (cSearchInc.extraData.thisRef.incSearchStr != search) {
        this.manager.widgets['pager'].customClickHandler(1, null, search);
        cSearchInc.extraData.thisRef.incSearchStr = search;
    }
};

NavigateUI.prototype.collapseBodyPnl = function () {
    if (this.panelSearch.resultPanel) this.panelSearch.resultPanel.collapse(false);
};

//событие когда панель классификатора сворачивается
NavigateUI.prototype.onNavigatePnlCollapse = function (sender) {
    var id = sender.container.id;
    var html = '<img id="' + id + 'img" title="Развернуть классификатор" src="images/media_step_forward.png" style="display: block; left: 0px; position: absolute; visibility: visible; width: 15px; z-index: 20; top: 48%; height: 32px;">';
    var found = false;
    var container = $('#' + id);
    var imgID = '#' + id + 'img';
    container.find(imgID).each(function () {
        found = true;
    });
    if (!found) {
        var pnl = sender;
        container.append($(html).click(function () {
            pnl.expand();
        }));
    }
    if (sender.collapsed) {
        $('#' + id + 'img').show();
    } else {
        $('#' + id + 'img').hide();
    }
};
// выключаем или включаем контролы поиска для гостей и авторизованных пользователей
NavigateUI.prototype.setDisabledNavigate = function (disabled) {
    if (panelInfo && panelInfo.extraData && panelInfo.extraData.navTree) {
        panelInfo.extraData.navTree.setDisabled(disabled);
    }
    this.navTree.setDisabled(disabled);
    if (simpleSearchHeaderPanel) {
        if (disabled) {
            $('#' + simpleSearchHeaderPanel.id + ' input , #' + simpleSearchHeaderPanel.id + ' a , #' + simpleSearchHeaderPanel.id + ' img').attr('disabled', true);
        } else {
            $('#' + simpleSearchHeaderPanel.id + ' input , #' + simpleSearchHeaderPanel.id + ' a , #' + simpleSearchHeaderPanel.id + ' img').removeAttr('disabled');
        }
    }
};

NavigateUI.prototype.buildUI = function () {
    this.documentUI = new DocumentUI();
    this.documentPanel = this.documentUI.buildUI();
    this.documentUI.onDocumentOpen = this.getOnDocOpenEvent();
    this.documentUI.onDocumentClose = this.getOnDocCloseEvent();
    //2013082210000268 — ОНЛ_доработка_функционала_8
    this.documentUI.onDocumentNext  = this.getOnNextDocEvent();
    this.taskSearch = new Ext.util.DelayedTask(this.onResultSearchKeyPress, this);
    this.cResultSearchInc = new Ext.form.TextField({extraData: {thisRef: this}, enableKeyEvents: true, emptyText: 'Поиск в списке'});
    this.cResultSearchInc.on('keypress', function (scope, e) {
        this.taskSearch.delay(500);
    }, this);
    this.cResultSearchInc.cls = 'doc-search-btn';
    this.navSpin = new Ext.form.TextField({style: 'width:50px;', cls: 'spinbox', extraData: {}, enableKeyEvents: true});
    this.bodyPnl = new Ext.Panel({
        border: false,
        autoScroll: true,
        region: 'center',
        layout: 'border',
        collapsible: false,
        hideCollapseTool: true,
        listeners: {
            afterlayout: onAfterPanelLayout
        },
        defaults: {
            collapsible: true,
            split: true,
            bodyStyle: 'padding:0px'
        },
        title: '',
        extraData: {},
        items: [ new Ext.Panel({ ref: '../resultPanel', collapsible: true, /*collapseMode:'mini',hideCollapseTool:true,*/autoHeight: false,
            cls: 'noCollapseTool', minSize: 400, minWidth: 400,
            autoScroll: false, border: false, region: 'west', width: '95%',
            tbar: [
                this.cResultSearchInc, '-',
                '->',
                {iconCls: 'arrow-left', tooltipType: 'title', tooltip: 'Свернуть', handler: this.collapseBodyPnl, scope: this}
            ],
            bbar: [
                createLabel(' стр.'),
                this.navSpin, '-',
                new Ext.form.Label({html: '<div id="navigation"><ul id="pager"></ul><div id="pager-header"></div></div>'})
            ],
            html: '    <div class="right" style="overflow-y:auto;height: 100%;">' + '<div id="result" style="height: 100%;"><div id="docs"></div></div></div> '})
            , this.documentPanel]
    });
    this.buildNavigateTree(this, false);
    this.navigatePnl = new Ext.Panel({
        border: false,
        //title:"Навигация",
        autoScroll: true,
        region: 'west',
        width: 350,
        layout: 'card',
        activeItem: 0,
        collapsible: true,
        hideCollapseTool: true,
        listeners: {
            afterlayout: onAfterPanelLayout
        },
        extraData: {},
        items: [ /*this.cClasses,*/this.navTree], collapseMode: 'mini'
    });
    this.navigatePnl.getActiveTab = function () {
        return this.layout.activeItem;
    };
    this.navigatePnl.setActiveTab = function (tab) {
        this.layout.setActiveItem(tab);
    };
    this.navigatePnl.on('collapse', this.onNavigatePnlCollapse, this);
    this.navigatePnl.on('expand', this.onNavigatePnlCollapse, this);
    this.panelSearch = new Ext.Panel({
        //layout: 'fit',
        layout: 'border',
        border: false,
        defaults: {
            collapsible: true,
            split: true,
            bodyStyle: 'padding:0px'
            //,collapseMode:'mini'
        },
        //title: 'Поиск',
        items: [this.bodyPnl, this.navigatePnl]
    });
    return this.panelSearch;
};
