(function ($) {

// For an AutocompleteWidget that uses the q parameter, see:
// https://github.com/evolvingweb/ajax-solr/blob/gh-pages/examples/reuters/widgets/AutocompleteWidget.q.js
AjaxSolr.AutocompleteWidget = AjaxSolr.AbstractFacetWidget.extend({
  init: function () {
    var self = this;
      /*
    $(this.target).find('input').bind('keydown', function(e) {
      if (self.requestSent === false && e.which == 13) {
        var value = $(this).val();
        if (value && self.add(value)) {
          self.manager.doRequest(0);
        }
      }
    });*/
  },

  afterRequest: function () {
   var self = this;
   $(self.target).each(function(){
       var inp = $(this);
       if(!inp.is('input'))  inp = inp.find('input');
//    inp.val('');
//    $(this.target).find('input').val('');
       var callback = function (response) {
           var list = [];
           for (var i = 0; i < self.fields.length; i++) {
               var field = self.fields[i];
               for (var facet in response.facet_counts.facet_fields[field]) {
                   list.push({
                       field: field,
                       value: facet,
                       text: facet //+ ' (' + response.facet_counts.facet_fields[field][facet] + ') - ' + field
                   });
               }
           }

           self.requestSent = false;
           inp.autocomplete(list, {
               formatItem: function(facet) {
                   return facet.text;
               }
           }).result(function(e, facet) {
                   inp.val(facet.value.toLowerCase());
                   self.requestSent = true;
                   /*if (self.manager.store.addByValue('fq', facet.field + ':' + AjaxSolr.Parameter.escapeValue(facet.value))) {
                    self.manager.doRequest(0);
                    }*/
               });
       }; // end callback

       var params = [ 'q=*:*&rows=0&facet=true&facet.limit=-1&facet.mincount=1&json.nl=map' ];
       for (var i = 0; i < self.fields.length; i++) {
           params.push('facet.field=' + self.fields[i]);
       }
//    jQuery.getJSON(this.manager.solrUrl + 'select?' + params.join('&') + '&wt=json&json.wrf=?', {}, callback);
       jQuery.getJSON('select?' + params.join('&') + '&wt=json&json.wrf=?', {}, callback);
   });
  }
});

})(jQuery);
