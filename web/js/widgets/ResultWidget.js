(function ($) {
// выводит список результатов
AjaxSolr.ResultWidget = AjaxSolr.AbstractWidget.extend({
  facetLinks: function (facet_field, facet_values) {
    var links = [];
    if (facet_values) {
      for (var i = 0, l = facet_values.length; i < l; i++) {
        links.push(AjaxSolr.theme('facet_link', facet_values[i], this.facetHandler(facet_field, facet_values[i])));
      }
    }
    return links;
  },

  facetHandler: function (facet_field, facet_value) {
    var self = this;
    return function () {
      self.manager.store.remove('fq');
      self.manager.store.addByValue('fq', facet_field + ':' + AjaxSolr.Parameter.escapeValue(facet_value));
      self.manager.doRequest(0);
      return false;
    };
  },

  beforeRequest: function () {
    $(this.target).html($('<img/>').attr('src', 'images/bigLoader.gif'));
  },

  afterRequest: function () {
    $(this.target).empty();
    for (var i = 0, l = this.manager.response.response.docs.length; i < l; i++) {
      var doc = this.manager.response.response.docs[i];
      $(this.target).append(AjaxSolr.theme('result', doc, AjaxSolr.theme('snippet', doc)));
/*
      var items = [];
      items = items.concat(this.facetLinks('topics', doc.topics));
      // теги категорий
      items = items.concat(this.facetLinks('cat', doc.cat));
      items = items.concat(this.facetLinks('exchanges', doc.exchanges));
      AjaxSolr.theme('list_items', '#links_' + doc.id, items);
*/
    }
  },

  init: function () {
      var self = this;
    $('a.more').livequery(function () {
      $(this).toggle(function () {
        $(this).parent().find('span').show();
        $(this).text('less');
        return false;
      }, function () {
        $(this).parent().find('span').hide();
        $(this).text('подробнее');
        return false;
      });
    });
      $('a.view_doc').livequery(function () {
          $(this).click(function () {
              var href=$(this).attr('href');
              if(!isStrNullOrEmpty(href) && href.indexOf("#!doc_")==0){
                  return true;
              }
              var ldoc_id=$(this).attr('id');
              var docId = ldoc_id.substr(5);
              var docDiv= $(this).parent().parent().find('div');
              var divDocId =docDiv.attr('id');
              self.manager.navigateUI.openDocument(docId,divDocId);
            return false;
          });
      });
  }
});

})(jQuery);