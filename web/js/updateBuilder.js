var UpdateUIList = {};

function UpdateAdminUI(user) {
    this.user = user;
    this.id = Ext.id();
    this.listeners = {};
    UpdateUIList[this.id] = this;
}

UpdateAdminUI.prototype.showUI = function () {
    this.buildUI();
    this.pWindow.show();
    this.pWindow.doLayout();
};

UpdateAdminUI.prototype.doNothing = function () {

}

UpdateAdminUI.prototype.showUser = function () {
    this.panel.ownerCt.setActiveTab(1);
};

function getUpdateAdminUI(id) {
    return UpdateUIList[id];
}
//добавить событие в список
UpdateAdminUI.prototype.on = function (event, fn, scope) {
    var clb = {fn: fn, scope: scope};
    this.listeners[event] = this.listeners[event] || [];
    this.listeners[event].push(clb);
};

//оповестить о событии
UpdateAdminUI.prototype.fire = function (event) {
    var listeners = this.listeners[event];
    if (listeners && listeners.length > 0) {
        for (var i = 0, il = listeners.length; i < il; i++) {
            var clb = listeners[i];
            clb.fn.call(clb.scope, this);
        }
    }
};

function getStoreRenderer(self, locStore) {
    var selfID = self.id;
    var store1 = locStore;
    return function (value, metaData, record, rowIndex, colIndex, store) {
        var data = record.json || record.data;
        for (var i = 0, il = store1.data.items.length; i < il; i++) {
            var rec = store1.data.items[i].data;
            if (rec.id == value) {
                return rec.value;
            }
        }
        return value;
    }
}

function passwordRenderer(value, metaData, record, rowIndex, colIndex, store) {
    return '***';
}

UpdateAdminUI.prototype.createXmlStore = function (name, fields, groupable) {
    // create the Data Store
    var storeCons = groupable === true ? Ext.data.GroupingStore : Ext.data.Store;
    var store = new storeCons({
        // destroy the store if the grid is destroyed
        autoDestroy: true,
        proxy: new Ext.data.HttpProxy({ url: 'UpdateBuilderHandler', timeout: 300000 }),
        baseParams: {
            par: '',
            requestType: name
        },
        reader: new Ext.ux.XmlReader({
            totalProperty: "total",
            record: 'row',
            fields: fields || [
                {name: 'id', type: 'string'},
                {name: 'value', type: 'string'}
            ]
        })
    });
    return store;
};

UpdateAdminUI.prototype.creatJsonStore = function (name, fields, groupable) {
    // create the Data Store
    var storeCons = groupable === true ? Ext.data.GroupingStore : Ext.data.Store;
    var store = new storeCons({
        // destroy the store if the grid is destroyed
        autoDestroy: true,
        proxy: new Ext.data.HttpProxy({ url: 'UpdateBuilderHandler', timeout: 300000 }),
        baseParams: {
            par: '',
            requestType: name
        },
        reader: new Ext.ux.JsonReader({
            totalProperty: "total",
            record: 'row',
            fields: fields || [
                {name: 'id', type: 'string'},
                {name: 'value', type: 'string'}
            ]
        })
    });
    return store;
};

UpdateAdminUI.prototype.refreshUI = function () {
    this.roleGrid.store.reload({});
    this.roleTreeGrid.store.reload({});
    this.userGrid.store.reload({});
};

UpdateAdminUI.prototype.addRecord = function (scope) {
    var extraData = scope.extraData;
    var store = extraData.store;
    var grid = store.extraData.grid;
    var ObjType = store.recordType;
    var obj = new ObjType({id: 0, value: 'Нажмите чтоб выбрать', login: 'Нажмите чтоб выбрать'});
    grid.stopEditing();
    store.insert(0, obj);
    grid.fireEvent('rowmousedown', grid, 0, {button: 0});
    if (grid.colModel.isCellEditable(0, 0)) {
        grid.startEditing(0, 0);
        grid.fireEvent('cellclick', grid, 0, 0)
    } else {
        grid.startEditing(0, 1);
        grid.fireEvent('cellclick', grid, 0, 1)
    }
};

UpdateAdminUI.prototype.onRecordSave = function (res, scope) {
    var store = scope.store;
    var lastRole = store.lastOptions && store.lastOptions.params && store.lastOptions.params.par;
    store.load({ params: { par: lastRole} });
};

UpdateAdminUI.prototype.buildUpdateFile = function (scope, event, record) {
    var extraData = scope.extraData;
    var store = extraData.store;
    var grid = store.extraData.grid;
    var methodSave = store.extraData.methodSave;
    var extra = extraDatas[extraData.id];
    var ids=[];
    for(var n in extra.IDS){
        if(!extra.IDS.hasOwnProperty(n)) continue;
        var val = extra.IDS[n];
        if(isStrNullOrEmpty(val)) continue;
        ids.push(val);
    }
    var v_ids=[];
    for(var n in extra.V_IDS){
        if(!extra.V_IDS.hasOwnProperty(n)) continue;
        var val = extra.V_IDS[n];
        if(isStrNullOrEmpty(val)) continue;
        v_ids.push(val);
    }
    var dt = extra.cDateFrom.getValue().format(extra.dbDateFormat);//2011-12-22
    var dt2 = extra.cDateTill.getValue().format(extra.dbDateFormat);//2011-12-22
    Ext.ux.HiddenForm('/UpdateBuilderHandler',[
        ['requestType',methodSave],
        ['date_from',dt],
        ['date_till',dt2],
        ['ids',ids.join(",")],
        ['v_ids',v_ids.join(",")]
    ]);
    /*
     var report = new Ext.ux.Report({ renderTo: Ext.getBody() });
     report.load({
        url: '/UpdateBuilderHandler',
        params: {
            requestType: methodSave,
            date_from:dt,
            date_till:dt2,
            ids: ids.join(","),
            v_ids: ids.join(",")
        }
    });*/

}

UpdateAdminUI.prototype.saveRecord = function (scope, event, record) {
    var extraData = scope.extraData;
    var store = extraData.store;
    var grid = store.extraData.grid;
    var methodSave = store.extraData.methodSave;
    var saveSelectedOnly = store.extraData.saveSelectedOnly;
    var lastRole = store.lastOptions && store.lastOptions.params && store.lastOptions.params.par || '';
    var recs = [];
    if (record != null) {
        recs.push(record);
    } else if (!saveSelectedOnly) {
        for (var i = 0, il = store.data.items.length; i < il; i++) {
            var rec = store.data.items[i].data;
            recs.push(rec);
        }
    } else {
        for (var i = 0; i < grid.selModel.selections.items.length; i++) {
            var selectRec = grid.selModel.selections.items[i].data;
            recs.push(selectRec);
        }
    }
    var obj = {obj: {count: recs.length, parent: lastRole, row: recs}};
    var xml = json2xml(obj);
    callRequestHandler(methodSave, {par: xml}, CallClassMethodAfterAjaxReturn, CallClassMethodAfterAjaxReturn, {thisRef: this, thisMethod: 'onRecordSave', store: store});
};

UpdateAdminUI.prototype.getGridButtons = function (store, canAdd, canDel, canSave, customForm, extraDataID) {
    var res = [];
    res.push({icon: '', handler: this.doNothing, scope: this, tooltip: ' ', tooltipType: 'title', extraData: {store: store, id:extraDataID}});
    if (canAdd) {
        res.push({icon: 'images/plus.png', handler: canAdd === true ? this.addRecord : this[canAdd], scope: this, tooltip: 'Выбрать все', tooltipType: 'title', extraData: {store: store, id:extraDataID}});
    }
    if (canDel) {
        res.push({icon: 'images/minus.png', handler: canDel === true ? this.delRecord : this[canDel], scope: this, tooltip: 'Выбрать все', tooltipType: 'title', extraData: {store: store, id:extraDataID}});
    }
    if (canSave) {
        res.push({icon: 'images/save_small.png', handler: canSave === true ? this.saveRecord : this[canSave], scope: this, tooltip: 'Сохранить', tooltipType: 'title', extraData: {store: store, id:extraDataID}});
    }
    if (customForm) {
        var self = this;
        var obj = {extraData: {store: store}};
        customForm.on('save', function (scope, record) {
            var data = record.data;
            for (var val in data) {
                if (!data.hasOwnProperty(val)) continue;
                if (data[val] instanceof Date) {
                    data[val] = data[val].format('Y-m-d');//2011-12-22
                }
            }
            self.saveRecord(obj, null, data);
        });
    }
    return res;
};

UpdateAdminUI.prototype.validateUsersEdit = function (e) {
    if (e.value instanceof Date) {
        e.value = e.value.format('Y-m-d');//2011-12-22
    }
};

function updateConfirmCheckBox(sender, id) {
    var change_id = $(sender).attr('change_id');
    var extraData = extraDatas[id];
    if (!extraData) return true;
    extraData.iAmount += sender.checked == true ? 1 : -1;
    extraData.cAmount.setText(' ' + extraData.iAmount);
    extraData.IDS[change_id] = sender.checked == true ? change_id : null;
    return true;
}

function updateVisibleCheckBox(sender, id) {
    var change_id = $(sender).attr('visible_id');
    var extraData = extraDatas[id];
    if (!extraData) return true;
    extraData.iVAmount += sender.checked == true ? 1 : -1;
    extraData.cVAmount.setText(' ' + extraData.iVAmount);
    extraData.V_IDS[change_id] = sender.checked == true ? change_id : null;
    return true;
}

function updateConfirmRenderer(value, metaData, record, rowIndex, colIndex, store) {
    var data = record.json;
    var thisRef = store.extraData;
    var tmp = '';
    if (!isStrNullOrEmpty(value)) {
        var checked = isStrNullOrEmpty(thisRef.IDS[data.id])? ' ' : ' checked ';
        tmp = '<input title="Добавить в выгрузку" role="checkbox" type="checkbox" '+checked+'change_id="' + data.id + "\" onclick='return updateConfirmCheckBox(this,\"" + thisRef.id + "\");'></input>";
        if('documents' == data.doc_type && data.oper_type.indexOf("del") < 0){
            var v_checked = isStrNullOrEmpty(thisRef.V_IDS[data.id1])? ' ' : ' checked ';
            tmp += '&nbsp;<input title="Показывать в статистике обновления" role="checkbox" type="checkbox" '+v_checked+'visible_id="' + data.id1 + "\" onclick='return updateVisibleCheckBox(this,\"" + thisRef.id + "\");'></input>";
        }
    } else {
        tmp = data.confirmed;
    }
    return tmp;
}


UpdateAdminUI.prototype.onCheckAll = function (scope) {
    var extraData = scope.extraData;
    extraData = extraDatas[extraData.id];
    if (extraData.customPnl && extraData.customPnl.body) {
        var items = $(extraData.customPnl.body.dom).find('[change_id]').each(function () {
            if (this.checked != scope.checked) {
                this.checked = scope.checked;
                updateConfirmCheckBox(this, extraData.id);
            }
        });
    }
};

UpdateAdminUI.prototype.onVCheckAll = function (scope) {
    var extraData = scope.extraData;
    extraData = extraDatas[extraData.id];
    if (extraData.customPnl && extraData.customPnl.body) {
        var items = $(extraData.customPnl.body.dom).find('[visible_id]').each(function () {
            if (this.checked != scope.checked) {
                this.checked = scope.checked;
                updateVisibleCheckBox(this, extraData.id);
            }
        });
    }
};


// установить текущего пользователя
UpdateAdminUI.prototype.setUserInfo = function (user) {
    var userChange = this.user != user;
    if (user) {
        this.user = user;
        $('#' + this.lnkId + ' .username').text(user.name);
        $('#' + this.lnkId + ' .logout').css('display', '');
        this.panel.setActiveTab(1);
        if (this.panel.lUserName)
            this.panel.lUserName.setText(user.name);
        var roles = user.roles;
        var rolesTxt = '';
        var hasAdminRole = false;
        for (var i = 0; i < roles.length; i++) {
            if (roles[i].role_id == 1) hasAdminRole = true;
            if (i > 0) rolesTxt += ",";
            rolesTxt += roles[i].role;
        }
        if (this.panel.lUserRoles)
            this.panel.lUserRoles.setText(rolesTxt);
//        this.pWindow.hide();
    } else {
        $('#' + this.lnkId + ' .logout').css('display', 'none');
        $('#' + this.lnkId + ' .admin').css('display', 'none');
        this.user = null;
        this.panel.setActiveTab(0);
    }
    if (userChange) this.fire('change');
};
// событие при получении информации об авторизованном пользователе
UpdateAdminUI.prototype.onGetUserInfo = function (resp) {
    var data = null;
    if (!getResponseErrorText(resp)) {
        var res = getResponseText(resp);
        if (!isStrNullOrEmpty(res)) {
            var data = eval("(" + res + ")");
            if (data.error) {
                this.setUserInfo(null);
                alert(data.error);
                return;
            }
        }
    }
    this.setUserInfo(data);
};
// получить информацию о текущем авторизованном пользователе
UpdateAdminUI.prototype.checkUser = function () {
    callRequestHandler("getUserInfo", {}, CallClassMethodAfterAjaxReturn, CallClassMethodAfterAjaxReturn, {thisRef: this, thisMethod: 'onGetUserInfo'}, 'UpdateBuilderHandler');
};

// событие на кнопку выйти
UpdateAdminUI.prototype.onLogout = function (res, scope, extraParams, o) {
    this.setUserInfo(null);
};

/*
 * событие при входе в систему
 * */
UpdateAdminUI.prototype.onLogin = function (res, scope, extraParams, o) {
    if (getResponseErrorText(res)) {
        alert(getResponseErrorText(res));
        return;
    }
    var res = getResponseText(res);
    if (isStrNullOrEmpty(res)) {
        alert('Неверное имя пользователя или пароль');
        return;
    }
    var data = eval("(" + res + ")");
    if (data.error) {
        alert(data.error);
        return;
    }
    //this.setUserInfo(data);
    this.loginPnl.fldPass.setValue('');
    this.checkUser();
};
// нажатие на кнопку выйти
UpdateAdminUI.prototype.doLogout = function () {
    callRequestHandler("doLogout", {},
        CallClassMethodAfterAjaxReturn, CallClassMethodAfterAjaxReturn, {thisRef: this, thisMethod: 'onLogout'}, 'UpdateBuilderHandler');

};
//нажатие на кнопку Войти
UpdateAdminUI.prototype.doLogin = function () {
    callRequestHandler("doLogin", {login: this.loginPnl.fldLogin.getValue(), pwd: this.loginPnl.fldPass.getValue()},
        CallClassMethodAfterAjaxReturn, CallClassMethodAfterAjaxReturn, {thisRef: this, thisMethod: 'onLogin'}, 'UpdateBuilderHandler');

};

UpdateAdminUI.prototype.buildUpdateUI = function () {
    var store = this.creatJsonStore('getChanges', [
        {name: 'confirm',   mapping: 'confirm'},
        {name: 'doc_type',  mapping: 'doc_type'},
        {name: 'title',     mapping: 'title'},
        {name: 'id',        mapping: 'id'},
        {name: 'oper_type', mapping: 'oper_type'},
        {name: 'changed_by', mapping: 'changed_by'},
        {name: 'oper_date', mapping: 'oper_date'}
    ], true);
    store.remoteSort = false;
    var columns = [];
    var dateFormat = 'd.m.Y';
    var dbDateFormat = 'Y-m-d';
    columns.push({ header: 'выбрать', dataIndex: 'confirm', name: 'confirm', sortable: true, hidden: false, renderer: updateConfirmRenderer, width: 70 });
    columns.push({ header: 'тип документа', width: 140, dataIndex: 'doc_type', sortable: true, hidden: false});
    columns.push({ header: 'наименование', width: 140, dataIndex: 'title', sortable: true, hidden: false});
    columns.push({ header: 'Тип операции', width: 140, dataIndex: 'oper_type', sortable: true, hidden: false});
    columns.push({ header: 'Дата операции', width: 140, dataIndex: 'oper_date', sortable: true, hidden: false});
    columns.push({ header: 'Изменено', width: 140, dataIndex: 'changed_by', sortable: true, hidden: false});
    var rows_per_page = 50;
    /*
     var filters = new Ext.ux.grid.GridFilters({local: false, menuFilterText: 'Фильтры',
     filters: [
     {type: 'string', dataIndex: 'doc_type'},
     {type: 'string', dataIndex: 'oper_type'},
     {type: 'date', dateFormat: dbDateFormat, dataIndex: 'oper_date'}
     ],
     buildQuery: function (filters, store, options) {
     var p = {};
     var limit = options.params && options.params.limit || rows_per_page ;
     var start = options.params && options.params.start || 0;
     var sort = options.params && options.params.sort || 'login';
     var dir = options.params && options.params.dir || 'asc';
     var cnt = filters.length;
     var tmp = '';
     for (var i = 0, len = filters.length; i < len; i++) {
     var f = filters[i];
     var fldName = f.field;
     var fldValue = f.data.value;
     var fldCmp = f.data.comparison || 'like';
     var fldType = f.data.type;
     if (typeof fldValue == 'string') {
     fldValue = fldValue.replace(new RegExp("&", "g"), "&amp;").replace(new RegExp("'", "g"), "");
     if (fldCmp == 'like') fldValue = fldValue + '%';
     }
     tmp += "<p type='" + fldType + "' cmp='" + fldCmp + "' name='" + fldName + "'>" + fldValue + "</p>";
     var root = [this.paramPrefix, '[', i, ']'].join('');
     p[root + '[field]'] = f.field;
     var dataPrefix = root + '[data]';
     for (var key in f.data)
     p[[dataPrefix, '[', key, ']'].join('')] = f.data[key];
     }
     p.par = "<filter><cnt>" + cnt + "</cnt><start>" + start + "</start><limit>" + limit + "</limit><sort>" + sort + "</sort><dir>" + dir + "</dir>" + tmp + "</filter>";
     return p;
     }});

     */
    var extraData = { id: Ext.id(), thisRef: this};
    extraData.IDS = {};
    extraData.V_IDS = {};
    extraDatas[extraData.id] = extraData;
    extraData.iVAmount = 0;
    extraData.iAmount = 0;
    extraData.cVAmount = createLabel("0", null, 'font-weight:bold;',{width:35});
    extraData.cAmount = createLabel("0", null, 'font-weight:bold;',{width:35});
    extraData.cDateFrom = new Ext.form.DateField({ format: dateFormat, altFormats: altDateFormats, width:90 });
    extraData.cDateTill = new Ext.form.DateField({ format: dateFormat, altFormats: altDateFormats, width:90  });
    extraData.cDateFrom.setValue(new Date());
    extraData.cDateTill.setValue(new Date());
    var cCheckBox = new Ext.form.Checkbox({ extraData: extraData });
    cCheckBox.on("check", onCheckBoxClick);
    cCheckBox.on("check", this.onCheckAll, this);
    cCheckBox.width = 15;
    var cVCheckBox = new Ext.form.Checkbox({ extraData: extraData });
    cVCheckBox.on("check", onCheckBoxClick);
    cVCheckBox.on("check", this.onVCheckAll, this);
    cVCheckBox.width = 15;
    extraData.cCheckBox = cCheckBox;
    extraData.cVCheckBox = cVCheckBox;
    extraData.dbDateFormat = dbDateFormat;

    store.extraData=extraData;
    extraData.cDateFrom.columnWidth = 0.45;
    extraData.cDateTill.columnWidth = 0.45;
    var btns = this.getGridButtons(store, false, false, 'buildUpdateFile',null, extraData.id);
    btns.push(
        createColumnPanel(
            [createLabel("Кол. ", null, null, {width: 24}),
        extraData.cAmount,
        extraData.cCheckBox,
        extraData.cVAmount,
        extraData.cVCheckBox,
        createEmptyLabel(0.03),
        createLabel("Дата с ", null, null,{width: 40}),
        extraData.cDateFrom,
        createLabel(" по ", null,null,{width: 20}),
        extraData.cDateTill],null,null,420)
    )
    store.on('load', function (sender, opt) {
        if(extraData.customPnl && extraData.customPnl.bottomToolbar){
            extraData.customPnl.bottomToolbar.pageSize = this.getTotalCount();
        }
    });
    store.on('beforeload', function (sender, opt) {
        var dt = extraData.cDateFrom.getValue().format(dbDateFormat);//2011-12-22
        var dt2 = extraData.cDateTill.getValue().format(dbDateFormat);//2011-12-22
        opt.params.date_from = dt;
        opt.params.date_till = dt2;
        extraData.IDS = {};
        extraData.V_IDS = {};
        extraData.iAmount = 0;
        extraData.iVAmount = 0;
    });
    this.userGrid = createGridEditablePanel(columns, store, btns, [/*filters*/], rows_per_page);
    extraData.customPnl = this.userGrid;
    this.userGrid.clicksToEdit = 2;
    this.userGrid.on('validateedit', this.validateUsersEdit, this);

    this.userGrid.extraData.thisRef = this;
    store.extraData = store.extraData || {};
    store.extraData.grid = this.userGrid;
    store.extraData.methodSave = "buildUpdate";
    store.extraData.saveSelectedOnly = true;

};

UpdateAdminUI.prototype.buildUI = function () {
    if (!this.panel) {
        this.buildUpdateUI();
        var self = this;
        this.loginPnl = new Ext.Panel({
            border: false,
            autoScroll: true,
            autoHeight: true,
            region: 'center',
            collapsible: false,
            hideCollapseTool: true,
            //layout: 'border',
            listeners: {
                afterlayout: onAfterPanelLayout
            },
            closable: true,
            extraData: {id: null},
            minSize: 400,
            minWidth: 400,
            items: [createColumnPanel([createLabel(''), createLabel('Авторизация', 0.5, 'font-size:16px;font-weight:bold;')], [0.35, 0.65]),
                createColumnPanel([createLabel(''), createLabel('Имя пользователя', null, 'text-align:right;padding-right:5px'), new Ext.form.TextField({ref: '../fldLogin'}), createLabel('')], [0.05, 0.30, 0.5, 0.15], 25),
                createColumnPanel([createLabel(''), createLabel('Пароль', null, 'text-align:right;padding-right:5px'), new Ext.form.TextField({ref: '../fldPass', inputType: 'password', enableKeyEvents: true, listeners: {keypress: function (sender, e) {
                    if (e.getKey() == e.ENTER) {
                        self.doLogin();
                    }
                }}}), createLabel('')], [0.05, 0.30, 0.5, 0.15], 25),
                createColumnPanel([createLabel(''), new Ext.Button({border: false, text: 'Войти', ref: '../btnLogin', handler: this.doLogin, scope: this}), createLabel('')], [0.35, 0.5, 0.25])
            ],
            defaults: {
                collapsible: false,
                split: false,
                bodyStyle: 'padding:0px'
            }
        });

        this.updatePanel = new Ext.Panel({
            border: false,
            autoScroll: false,
            //autoHeight: true,
            layout: 'fit',
            region: 'center',
            collapsible: false,
            hideCollapseTool: true,
            //layout: 'border',
            listeners: {
                afterlayout: onAfterPanelLayout
            },
            extraData: {id: null},
            items: [
                createPanelSimple(
                    createPanelSimple(
                        [
                            createPanelSimple(this.userGrid, {region: 'center', layout: 'fit', height: 200})
                        ],
                        {
                            border: false, autoHeight: false, autoScroll: false,
                            //height: 500,
                            layout: 'border',
                            region: 'center',
                            listeners: {afterlayout: onAfterPanelLayout},
                            defaults: {collapsible: false, split: true}
                        }
                    ),
                    {
                        listeners: {afterlayout: onAfterPanelLayout},
                        title: 'Управление выгрузками',
                        layout: 'fit'
                    })
            ],
            defaults: {
                collapsible: false,
                split: false,
                bodyStyle: 'padding-left:0px'
            }
        });
        // все панел воедино
        this.panel = new Ext.Panel({ autoHeight: false, region: 'center', height: 100, layout: 'card', activeItem: 0,
            hideCollapseTool: true, collapsible: false, collapseMode: 'mini',
            autoScroll: true, border: false, items: [this.loginPnl, this.updatePanel] });
        this.panel.getActiveTab = function () {
            return this.layout.activeItem;
        };
        this.panel.setActiveTab = function (tab) {
            this.layout.setActiveItem(tab);
        };

        // окно где показывается панель
        var window = new Ext.Window({
            autoHeight: false, height: 400, width: 700, closable: true, closeAction: 'hide', maximizable: true, modal: true, layout: 'fit'
            //, bbar: panelToolbar
            , extraData: { thisRef: this }, listeners: {
                /*close: function (scope) {
                 scope.extraData.thisRef.closeWindow('no');
                 },*/
                afterlayout: onAfterPanelLayout
            }

        });
        this.pWindow = window;
        window.add(this.panel);
        window.show();
        window.doLayout();
        window.hide();
    }
    return this.panel;
};

function domReady() {
    extraDatas = {};
    // регистрация самописных компонент
    afterDomReady();
    builderUI = new UpdateAdminUI(null);
    builderUI.buildUI();
    builderUI.showUI();

}