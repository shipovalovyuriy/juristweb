(function ($) {

    AjaxSolr.theme.prototype.result = function (doc, snippet) {
        var output = '<div><h3><a href="#!doc_' + doc.id + '" id="ldoc_' + doc.id + '" class="view_doc">' + (doc.fullName > 300 ? (doc.fullName.substring(0, 300) + '...') : doc.fullName) + '</a></h3>';
//    output += '<p id="links_' + doc.id + '" class="links"></p>';
        output += '<p>' + snippet + '</p><hr style="height:2px;margin-bottom:10px;">';
        /*+'<div style="display:none;" id="doc_'+doc.id+'">'+doc.body+'</div></div>'*/
        output += '</div>';
        return output;
    };

    AjaxSolr.theme.prototype.snippet = function (doc) {
        var output = '';
        var pdate = doc.publishDate;
        if (pdate) {
            pdate = DateToStr(DateTimeFromSolr(pdate));
        }
        var pubDate = (pdate ? ('<b>' + pdate + '</b>') : '');
        if (doc.shortName.length > 3000) {
            output += pubDate + ' ' + doc.fullName.substring(0, 300);
            output += '<span style="display:none;">' + doc.fullName.substring(300);
            output += '</span> <a href="#" class="more">Попробнее</a>';
        }
        else {
//    output += pubDate + ' ' + doc.shortName;
        }
//    output+='<a href="#" id="ldoc_' + doc.id+'" class="view_doc">Попробнее</a>';
        return output;
    };
    /*показать ссылки на теги с счетчиком*/
    AjaxSolr.theme.prototype.tagcount = function (value, weight, handler) {
        return $('<div/>').append($('<a href="#" class="tagcloud_item"/>').text(value + " (" + weight + ")").addClass('tagcloud_size_0').click(handler));
    };
    /*показать ссылки на теги разного размера в зависимости от счетчика*/
    AjaxSolr.theme.prototype.tag = function (value, weight, handler) {
        return $('<a href="#" class="tagcloud_item"/>').text(value).addClass('tagcloud_size_' + weight).click(handler);
    };
    /* списко тегов которые встречаются в документе */
    AjaxSolr.theme.prototype.facet_link = function (value, handler) {
        return $('<a href="#"/>').text(value).click(handler);
    };

    AjaxSolr.theme.prototype.no_items_found = function () {
        return 'нет элементов для выбора';
    };

})(jQuery);
