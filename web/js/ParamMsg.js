﻿/* $Id: ParamMsg.js 2583 2011-05-18 12:09:12Z nurbol $ */
/*
  Title заголовок
  params - это контролы что нужно показать
  ref - это объект что запросил диалог
  callback - это метод объекта что нужно вызвать
  extraData - на всякий случай
*/
function ParamMsgBox(title, params, ref, callback, extraData, customPnl) {
    this.title = title;
    this.paramList = params;
    this.ref = ref;
    this.callback = callback;
    this.extraData = extraData;
    this.closeHandled = false;
    this.panels = this.paramList;
    this.customPnl = customPnl;
}

function BtnOKClickEvent(scope) {
   scope.extraData.thisRef.closeWindow('yes');
}

function BtnCancelClickEvent(scope) {
    scope.extraData.thisRef.closeWindow('no');
}

ParamMsgBox.prototype.requestValues = function () {
    this.closeHandled = false;
    var panel = this.customPnl || new Ext.Panel({
        title: this.title,
        autoScroll: true,
        frame: true,
        layout: 'ux.row',
        //		layout: 'fit',
        items: this.panels
    });
    panel.setTitle(this.title);
    var pitems = [];
    pitems.push({ xtype: 'tbfill' });
    pitems.push({
        text: 'ОК',
        handler: BtnOKClickEvent,
        typeId: 'okBtn'
        , extraData: { thisRef: this }
    });
    pitems.push({
        text: 'Отмена'
        , handler: BtnCancelClickEvent
        , typeId: 'cancelBtn'
        , extraData: { thisRef: this }
    });
    var panelToolbar = new Ext.Toolbar({
        items: pitems
    });
    var window = new Ext.Window({
        autoHeight: false
            , height: 500
            , width: 700
            , closable: true
            , maximizable: true
            , modal: true
            , title: this.title
            , layout: 'fit'
            , bbar: panelToolbar
            , extraData: { thisRef: this }
            , listeners: {
                close: function (scope) {
                    scope.extraData.thisRef.closeWindow('no');
                },
                afterlayout: onAfterPanelLayout
            }
    });
    this.pWindow = window;
    window.add(panel);
    window.show();
    window.doLayout();
}


ParamMsgBox.prototype.closeWindow = function (answer) {
    if (this.closeHandled) return;
    this.closeHandled = true;
    try {
        this.ref[this.callback](this, answer);
        if (this.pWindow.isVisible()) {
            this.pWindow.close();
        }
    } catch (e) {
        this.closeHandled = false;

    }
}


