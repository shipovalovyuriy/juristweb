Ext.namespace("Ext.ux.grid.filter");
Ext.ux.grid.filter.Filter = function(config){
	Ext.apply(this, config);

	this.events = {
		/**
		 * @event activate
		 * Fires when a inactive filter becomes active
		 * @param {Ext.ux.grid.filter.Filter} this
		 */
		'activate': true,
		/**
		 * @event deactivate
		 * Fires when a active filter becomes inactive
		 * @param {Ext.ux.grid.filter.Filter} this
		 */
		'deactivate': true,
		/**
		 * @event update
		 * Fires when a filter configuration has changed
		 * @param {Ext.ux.grid.filter.Filter} this
		 */
		'update': true,
		/**
		 * @event serialize
		 * Fires after the serialization process. Use this to apply additional parameters to the serialized data.
		 * @param {Array/Object} data A map or collection of maps representing the current filter configuration.
		 * @param {Ext.ux.grid.filter.Filter} filter The filter being serialized.
		 **/
		'serialize': true
	};
	Ext.ux.grid.filter.Filter.superclass.constructor.call(this);

	this.menu = new Ext.menu.Menu();
	this.init(config);

	if(config && config.value){
		this.setValue(config.value);
		this.setActive(config.active !== false, true);
		delete config.value;
	}
};
Ext.extend(Ext.ux.grid.filter.Filter, Ext.util.Observable, {
	/**
	 * @cfg {Boolean} active
	 * Indicates the default status of the filter (defaults to false).
	 */
    /**
     * True if this filter is active. Read-only.
     * @type Boolean
     * @property
     */
	active: false,
	/**
	 * @cfg {String} dataIndex
	 * The {@link Ext.data.Store} data index of the field this filter represents. The dataIndex does not actually
	 * have to exist in the store.
	 */
	dataIndex: null,
	/**
	 * The filter configuration menu that will be installed into the filter submenu of a column menu.
	 * @type Ext.menu.Menu
	 * @property
	 */
	menu: null,

	/**
	 * @cfg {Number} updateBuffer
	 * Some filters may wait a short time after user interaction to fire an update.
	 */
	updateBuffer: 500,

	/**
	 * Initialize the filter and install required menu items.
	 */
	init: Ext.emptyFn,

	fireUpdate: function(){
		if(this.active)
			this.fireEvent("update", this);

		this.setActive(this.isActivatable());
	},

	/**
	 * Returns true if the filter has enough configuration information to be activated.
	 *
	 * @return {Boolean}
	 */
	isActivatable: function(){
		return true;
	},

	/**
	 * Sets the status of the filter and fires that appropriate events.
	 *
	 * @param {Boolean} active        The new filter state.
	 * @param {Boolean} suppressEvent True to prevent events from being fired.
	 */
	setActive: function(active, suppressEvent){
		if(this.active != active){
			this.active = active;
			if(suppressEvent !== true)
				this.fireEvent(active ? 'activate' : 'deactivate', this);
		}
	},

	/**
	 * Get the value of the filter
	 *
	 * @return {Object} The 'serialized' form of this filter
	 */
	getValue: Ext.emptyFn,

	/**
	 * Set the value of the filter.
	 *
	 * @param {Object} data The value of the filter
	 */
	setValue: Ext.emptyFn,

	/**
	 * Serialize the filter data for transmission to the server.
	 *
	 * @return {Object/Array} An object or collection of objects containing key value pairs representing
	 * 	the current configuration of the filter.
	 */
	serialize: Ext.emptyFn,

	/**
	 * Validates the provided Ext.data.Record against the filters configuration.
	 *
	 * @param {Ext.data.Record} record The record to validate
	 *
	 * @return {Boolean} True if the record is valid with in the bounds of the filter, false otherwise.
	 */
	 validateRecord: function(){return true;}
});



Ext.ux.grid.filter.DateFilter = Ext.extend(Ext.ux.grid.filter.Filter, {
	dateFormat: 'm/d/Y',
	pickerOpts: {},

    beforeText: 'До',
    afterText:  'После',
    onText:     'На дату',

	init: function(){
		var opts = Ext.apply(this.pickerOpts, {
			minDate: this.minDate,
			maxDate: this.maxDate,
			format:  this.dateFormat
		});
		var dates = this.dates = {
			'before': new Ext.menu.CheckItem({text: this.beforeText, menu: new Ext.menu.DateMenu(opts)}),
			'after':  new Ext.menu.CheckItem({text: this.afterText, menu: new Ext.menu.DateMenu(opts)}),
			'on':     new Ext.menu.CheckItem({text: this.onText, menu: new Ext.menu.DateMenu(opts)})};

		this.menu.add(dates.before, dates.after, "-", dates.on);

		for(var key in dates){
			var date = dates[key];
			date.menu.on('select', function(date, menuItem, value, picker){
				date.setChecked(true);

				if(date == dates.on){
					dates.before.setChecked(false, true);
					dates.after.setChecked(false, true);
				} else {
					dates.on.setChecked(false, true);

					if(date == dates.after && dates.before.menu.picker.value < value)
            dates.before.setChecked(false, true);
          else if (date == dates.before && dates.after.menu.picker.value > value)
            dates.after.setChecked(false, true);
				}

				this.fireEvent("update", this);
			}.createDelegate(this, [date], 0));

			date.on('checkchange', function(){
				this.setActive(this.isActivatable());
			}, this);
		};
	},

	getFieldValue: function(field){
		return this.dates[field].menu.picker.getValue();
	},

	getPicker: function(field){
		return this.dates[field].menu.picker;
	},

	isActivatable: function(){
		return this.dates.on.checked || this.dates.after.checked || this.dates.before.checked;
	},

	setValue: function(value){
		for(var key in this.dates)
			if(value[key]){
				this.dates[key].menu.picker.setValue(value[key]);
				this.dates[key].setChecked(true);
			} else {
				this.dates[key].setChecked(false);
			}
	},

	getValue: function(){
		var result = {};
		for(var key in this.dates)
			if(this.dates[key].checked)
				result[key] = this.dates[key].menu.picker.getValue();

		return result;
	},

	serialize: function(){
		var args = [];
		if(this.dates.before.checked)
			args = [{type: 'date', comparison: 'lt', value: this.getFieldValue('before').format(this.dateFormat)}];
		if(this.dates.after.checked)
			args.push({type: 'date', comparison: 'gt', value: this.getFieldValue('after').format(this.dateFormat)});
		if(this.dates.on.checked)
			args = {type: 'date', comparison: 'eq', value: this.getFieldValue('on').format(this.dateFormat)};

    this.fireEvent('serialize', args, this);
		return args;
	},

	validateRecord: function(record){
		var val = record.get(this.dataIndex).clearTime(true).getTime();

		if(this.dates.on.checked && val != this.getFieldValue('on').clearTime(true).getTime())
			return false;

		if(this.dates.before.checked && val >= this.getFieldValue('before').clearTime(true).getTime())
			return false;

		if(this.dates.after.checked && val <= this.getFieldValue('after').clearTime(true).getTime())
			return false;

		return true;
	}
});




Ext.ux.grid.filter.StringFilter = Ext.extend(Ext.ux.grid.filter.Filter, {
	updateBuffer: 500,
	icon: 'ux-gridfilter-text-icon',

	init: function(config){
		var value = this.value = new Ext.ux.menu.EditableItem({editor:config.editor, iconCls: this.icon});
		value.on('keyup', this.onKeyUp, this);
		this.menu.add(value);

		this.updateTask = new Ext.util.DelayedTask(this.fireUpdate, this);
	},

	onKeyUp: function(event){
		if(event.getKey() == event.ENTER){
			this.menu.hide(true);
			return;
		}

		this.updateTask.delay(this.updateBuffer);
	},

	isActivatable: function(){
        var value=this.value.getValue();
		return value.length > 0;
	},

	fireUpdate: function(){
		if(this.active)
			this.fireEvent("update", this);

		this.setActive(this.isActivatable());
	},

	setValue: function(value){
		this.value.setValue(value);
		this.fireEvent("update", this);
	},

	getValue: function(){
		return this.value.getValue();
	},

	serialize: function(){
		var args = {type: 'string', value: this.getValue()};
		this.fireEvent('serialize', args, this);
		return args;
	},

	validateRecord: function(record){
		var val = record.get(this.dataIndex);

		if(typeof val != "string")
			return this.getValue().length == 0;

		return val.toLowerCase().indexOf(this.getValue().toLowerCase()) > -1;
	}
});

Ext.ux.grid.filter.ComboFilter = Ext.extend(Ext.ux.grid.filter.StringFilter, {
  init: function(config){
		var value = this.value = new Ext.ux.menu.EditableItem({editor:config.editor, iconCls: this.icon});
		value.on('keyup', this.onKeyUp, this);
        value.editor.on('select', this.onSelect, this);
		this.menu.add(value);

		this.updateTask = new Ext.util.DelayedTask(this.fireUpdate, this);
	},
    onSelect: function(event){
		this.updateTask.delay(this.updateBuffer);
	},
    isActivatable: function(){
        var value=this.getValue();
		return value!=null && value > 0;
	},
    serialize: function(){
		var args = {type: 'string', comparison:'eq', value: this.getValue()};
		this.fireEvent('serialize', args, this);
		return args;
	}
});


Ext.ux.grid.filter.NumericFilter = Ext.extend(Ext.ux.grid.filter.Filter, {
	init: function(){
		this.menu = new Ext.ux.menu.RangeMenu({updateBuffer: this.updateBuffer});

		this.menu.on("update", this.fireUpdate, this);
	},

	fireUpdate: function(){
		this.setActive(this.isActivatable());
		this.fireEvent("update", this);
	},

	isActivatable: function(){
		var value = this.menu.getValue();
		return value.eq !== undefined || value.gt !== undefined || value.lt !== undefined;
	},

	setValue: function(value){
		this.menu.setValue(value);
	},

	getValue: function(){
		return this.menu.getValue();
	},

	serialize: function(){
		var args = [];
		var values = this.menu.getValue();
		for(var key in values)
			args.push({type: 'numeric', comparison: key, value: values[key]});

		this.fireEvent('serialize', args, this);
		return args;
	},

	validateRecord: function(record){
		var val    = record.get(this.dataIndex),
			values = this.menu.getValue();

		if(values.eq != undefined && val != values.eq)
			return false;

		if(values.lt != undefined && val >= values.lt)
			return false;

		if(values.gt != undefined && val <= values.gt)
			return false;

		return true;
	}
});



Ext.ux.grid.filter.BooleanFilter = Ext.extend(Ext.ux.grid.filter.Filter, {
	defaultValue: false,
	yesText: "Да",
	noText:  "Нет",

	init: function(){
    var gId = Ext.id();
		this.options = [
			new Ext.menu.CheckItem({text: this.yesText, group: gId, checked: this.defaultValue === true}),
			new Ext.menu.CheckItem({text: this.noText, group: gId, checked: this.defaultValue === false})];

		this.menu.add(this.options[0], this.options[1]);

		for(var i=0; i<this.options.length; i++){
			this.options[i].on('click', this.fireUpdate, this);
			this.options[i].on('checkchange', this.fireUpdate, this);
		}
	},

	isActivatable: function(){
		return true;
	},

	fireUpdate: function(){
		this.fireEvent("update", this);
		this.setActive(true);
	},

	setValue: function(value){
		this.options[value ? 0 : 1].setChecked(true);
	},

	getValue: function(){
		return this.options[0].checked;
	},

	serialize: function(){
		var args = {type: 'boolean', value: this.getValue()};
		this.fireEvent('serialize', args, this);
		return args;
	},

	validateRecord: function(record){
		return record.get(this.dataIndex) == this.getValue();
	}
});



Ext.ux.grid.filter.ListFilter = Ext.extend(Ext.ux.grid.filter.Filter, {
	phpMode:     false,

	init: function(config){
		this.dt = new Ext.util.DelayedTask(this.fireUpdate, this);

		this.menu = new Ext.ux.menu.ListMenu(config);
		this.menu.on('checkchange', this.onCheckChange, this);
	},

	onCheckChange: function(){
		this.dt.delay(this.updateBuffer);
	},

	isActivatable: function(){
		return this.menu.getSelected().length > 0;
	},

	setValue: function(value){
		this.menu.setSelected(value);

		this.fireEvent("update", this);
	},

	getValue: function(){
		return this.menu.getSelected();
	},

	serialize: function(){
	    var args = {type: 'list', value: this.phpMode ? this.getValue().join(',') : this.getValue()};
	    this.fireEvent('serialize', args, this);

		return args;
	},

	validateRecord: function(record){
		return this.getValue().indexOf(record.get(this.dataIndex)) > -1;
	}
});