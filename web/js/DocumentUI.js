function DocumentUI() {
    this.loadingImgHtml = '<img src="images/ajax-loader.gif">';
    this.loadingBigImgHtml = '<center><img src="images/bigLoader.gif"></center>';
    this.onDocumentOpen = null;
    this.onDocumentClose = null;
    //Handler to return next document for load
    this.onDocumentNext = null;
    this.isVisible = false;
    this.isScrollEventActivated = false;
    this.isDocLoading = false;
    this.autoDocLoading = true;
    this.listeners = {};
    //2013082210000268 — ОНЛ_доработка_функционала_8
    this.resetSearch = false;
}
// корректируются ссылки на сам документ в теле документа
// покызвается процент загрузки документа
DocumentUI.prototype.preprocessDocumentBody = function (text) {
    var respText = text.replace(new RegExp('http://' + this.docId + '/', 'g'), '');
    respText = respText.replace(new RegExp('aps://' + this.docId + '#', 'g'), '#');
    respText = respText.replace(new RegExp('aps://' + this.docId + '/#', 'g'), '#');
    respText = respText.replace(new RegExp('apsfile://' + this.docId + '#', 'g'), '#');
    respText = respText.replace(new RegExp('apsfile://' + this.docId + '/#', 'g'), '#');
    if (respText.indexOf('<!--') != 0) {
    } else {
        var tmp = respText.substring(4, respText.indexOf('-->'));
        var sOffset = tmp.substring(0, tmp.indexOf('|'));
        var sSize = tmp.substring(tmp.indexOf('|') + 1);
        var offset = Number(sOffset);
        var size = Number(sSize);
        this.docInfo.offset = offset;
        this.docInfo.size = size;
        var perc = Number(100 * offset / size);
        var jProgress = $("#" + this.pnlDocLoadProgessId);
        jProgress.progressbar();
        $("#p" + this.pnlDocLoadProgessId).text(perc.toFixed(0) + "%");
        jProgress.progressbar("option", "value", perc);
        var isVisible = this.pnlDocLoadProgess.isVisible();
        var showProgressBar = this.autoDocLoading && (perc < 100);
        this.pnlDocLoadProgess.setVisible(showProgressBar);
        /*if(!showProgressBar){
         this.addToHistory()
         }*/
        if (isVisible != showProgressBar) {
            this.panel.ownerCt.doLayout(true);
            Logger.message(new Date(), Logger.logDebug);
        }
    }
    return respText;
};
// Обработка тела документа
// корректируются ссылки на другие документы и размер шрифтов
DocumentUI.prototype.afterDocumentBodyLoad = function (pnlId, innerPnlId, innerObj) {
    var pnlObj = innerObj || $("#" + innerPnlId);
    var elem = pnlObj.find('a');
    var self = this;
    elem.each(function () {
        $(this).click(function (event) {
            var href = $(this).attr('href');
            var res = false;
            if (href) {
                res = self.openDocument(null, href);
            }
            if (res) event.preventDefault(); // отменяем действие по умолчанию, но не трогаем bubbling - чтобы не мешать другим обработчикам
            return !res;
        });
    });
    pnlObj.css("fontSize", "12pt");
    var list = pnlObj.find('font[size]');
    list.each(function (i, val) {
        var psize = getActualStyle(val.parentNode, "font-size");
        var size = getActualStyle(val, "font-size");
        if (psize.indexOf("em") > 0) {
            //psize = val.parentNode.oldFontSize;
            var tmp = document.emSize(val.parentNode);
            psize = tmp[1] + "px";
        }
        if (size == psize) {
            $(val).removeAttr("size").css("fontSize", "");
        } else {
            size = getSizePT(size);//size.substr(0,size.indexOf("px"));
            psize = getSizePT(psize);//psize.substr(0,psize.indexOf("px"));
            val.oldFontSize = size;
            var tsize = (size / psize);
            if (tsize == 1) tsize = "";
            else tsize += "em";
            $(val).removeAttr("size").css("fontSize", "" + tsize);
        }
    });
    var first = true;
    list = pnlObj.find('[style*=FONT-SIZE],[style*=font-size]');
    for (var i = list.length - 1; i >= 0; i--) {
        var val = list[i];
        //if(first) { first=false; continue;}
        var size = $(val).css("fontSize");
        if (size.indexOf("+") > 0 || size.indexOf("-") > 0 || size.indexOf("em") > 0) continue;
        var psize = getActualStyle(val.parentNode, "font-size");
        size = getActualStyle(val, "font-size");
        size = getSizePT(size);
        psize = getSizePT(psize);
        var tsize = size / psize;
        if (tsize == 1) tsize = "";
        else tsize += "em";
        $(val).css("fontSize", tsize);
    }
};

// открываем нужную часть документа
DocumentUI.prototype.tryShowDocAnchor = function () {
    if (!isStrNullOrEmpty(this.docOffset)) {
        if (this.gotoAnchor(null, this.docOffset)) {
            this.docOffset = null;
        }
    } else if (!isStrNullOrEmpty(this.docAnchor)) {
        if (this.gotoAnchor(this.docAnchor)) {
            this.docAnchor = null;
        }
    }
};

// Обработка подкачанного тела документа перед просмотром
DocumentUI.prototype.continueOpenDocumentLoad = function (result, scope, extraParams, o, response) {
    if (getResponseErrorText(result)) {
        alert(getResponseErrorText(result));
        return;
    }
    var respText = getResponseText(result);
    if (!respText) {
        respText = 'Документ не найден';
    }
    respText = this.preprocessDocumentBody(respText);
    var innerPnl = scope.innerPnl;
    var innerPnlId = scope.innerPnlId;
    if (innerPnl && innerPnl.body && innerPnl.body.dom) {
        var newId = Ext.id();
        var newDiv = $('<div style="font-size: 12pt;"/>').attr('id', newId).html(respText);
        this.afterDocumentBodyLoad(innerPnlId, newId, newDiv);
        newDiv.attr("style", "");
        replaceHtml(innerPnlId, "");
        $("#" + innerPnlId).css("fontSize", "12pt").append(newDiv);
        //replaceHtml(innerPnlId,respText);
        newDiv.after("<div style='position: relative; bottom: 70px; right: 0pt; text-align: center;' id='" + this.pnlDocLoadingId + "'></div>");
        this.tryShowDocAnchor();
        //this.afterDocumentBodyLoad(innerPnl.id,innerPnlId);
    }
    if (response && response.params && response.params.id == this.docId) {
        this.docInfo.html = respText;
        if (!this.isScrollEventActivated) {
            this.isScrollEventActivated = true;
            $('#' + innerPnl.body.id).scroll(this.getDocumentScrollHandler());
        }
        this.isDocLoading = false;
        var perc = Number(100 * this.docInfo.offset / this.docInfo.size);
        var showProgressBar = this.autoDocLoading && (perc < 100);
        if (showProgressBar) {
            this.loadNextPart();
        }else{
            //начать поиск с начала этого документа
            if(this.resetSearch == false){
                var search = this.cSearchInc.extraData.searchStr;
                this.cSearchInc.extraData.searchStr = "";
                this.cSearchInc.extraData.searchIndex = 0;
                this.search(search);
            }
        }
    }
};

// добавляем документ в историю открытых документов
DocumentUI.prototype.addToHistory = function (id, title, html) {
    if (!this.fromHistory) {
        $.history.load("!doc_" + id);
    }
    this.fire('history', id, title, html);
};

// запомним позицию в старом документе
// нужно для возвращения назад
DocumentUI.prototype.saveAnchorToHistory = function (fromHistory) {
    if (fromHistory) return;
    var pos = $('#' + this.innerPnlId).parent().scrollTop();
    var str = "!doc_" + this.docId + "!off_" + pos;
    $.history.ignoreHash = str;
    $.history.load(str);
};

// обработка информации о документе и начинаем  подкачку документа для просмотра
DocumentUI.prototype.continueOpenDocument = function (result, scope, extraParams, o) {
    if (getResponseErrorText(result)) {
        alert(getResponseErrorText(result));
        return;
    }
    Logger.message(new Date(), Logger.logDebug);
    var resp = eval('(' + getResponseText(result).replace(new RegExp('\r', 'g'), ' ').replace(new RegExp('\n', 'g'), ' ') + ')');
    if (resp && resp.length > 0 && isStrNullOrEmpty(resp[0].error)) {
        if (resp[0].ext) {
            var report = new Ext.ux.Report({ renderTo: Ext.getBody() });
            report.load({url: 'RequestHandler', params: {requestType: 'loadAttachment', id: this.docId}});
            window.location = '#';
            //mainTabPanel.remove(this.panel);
        } else {
            this.isVisible = false;
            this.docInfo = resp[0];
            this.panel.lnkTitle.setText(this.docInfo.fullName/*.substr(0, 50) + '...'*/);
            $('#' + this.panel.lnkTitle.id).attr('title', this.docInfo.fullName);
            //this.panel.lnkTitle.setText(this.docInfo.shortName.substr(0, 100) + '...');
            this.isDocLoading = true;
            this.autoDocLoading = true;
            this.addToHistory(this.docId, this.docInfo.fullName);
            replaceHtml(this.innerPnlId, this.loadingBigImgHtml);
            var cls = this.docInfo.status == 1 ? "document-expired" : this.docInfo.status == 2 ? "document-early" : "";
            $("#" + this.innerPnlId).parent().removeClass("document-early").removeClass("document-expired").addClass(cls);
            if (this.panel.btnComments) {
                this.isVisible = true;
                this.panel.btnComments.toggle(true, true);
                $('#' + this.innerPnl.id).removeClass("sps_comment_show").addClass("sps_comment_show");
                //2013082210000268 — ОНЛ_доработка_функционала_8
                if (this.resetSearch) {
                    this.cSearchInc.extraData.searchStr = '';
                    this.cSearchInc.setValue('');
                }
            }
            if (this.panel.btnCommentDoc) this.panel.btnCommentDoc.toggle(false, true);
            if (this.panel.btnDocCorr) this.panel.btnDocCorr.toggle(false, true);
            if (this.panel.btnDocResp) this.panel.btnDocResp.toggle(false, true);
            this.innerPnlBottom.collapse();
            callRequestHandler("DocLoadPartial", {id: this.docId}, CallClassMethodAfterAjaxReturn, CallClassMethodAfterAjaxReturn, {thisRef: this, thisMethod: 'continueOpenDocumentLoad', innerPnl: this.innerPnl, innerPnlId: this.innerPnlId});
            callRequestHandler("DocListRespCorr", {id: this.docId}, CallClassMethodAfterAjaxReturn, CallClassMethodAfterAjaxReturn, {thisRef: this, thisMethod: 'onRespCorrLoad'});
            if (!(this.docInfo.comment == null || this.docInfo.comment == '' || this.docInfo.comment == 'null')) {
                replaceHtml(this.innerPnlBottomId, this.loadingBigImgHtml);
                callRequestHandler("DocLoad", {id: this.docInfo.comment}, CallClassMethodAfterAjaxReturn, CallClassMethodAfterAjaxReturn, {thisRef: this, thisMethod: 'continueOpenDocumentLoad', innerPnl: this.innerPnlBottom, innerPnlId: this.innerPnlBottomId});
            } else {
                replaceHtml(this.innerPnlBottomId, '');
            }
            if (this.onDocumentOpen) {
                this.onDocumentOpen(this);
            }
        }
    } else {
        var respText = resp && resp.length > 0 && resp[0].error || 'Документ не найден';
        replaceHtml(this.innerPnlId, respText);
        //this.innerPnl.body.dom.innerHTML = respText;
    }
};
// показать комментарии в тексте
function onCommentsToggle(scope) {
    var visible = this.isVisible;
    var innerPnl = scope.innerPnl || (scope.extraData && scope.extraData.innerPnl) || this.innerPnl || (this.extraData && this.extraData.innerPnl);
    if (innerPnl) {
        visible = !visible;
        this.isVisible = visible;
        if (visible) {
            $('#' + innerPnl.id).addClass("sps_comment_show");
        } else {
            $('#' + innerPnl.id).removeClass("sps_comment_show");
        }
    }

}

/*
 function onCommentDocToggle(scope){
 var innerPnl=scope.innerPnl ||(scope.extraData && scope.extraData.innerPnl) || this.innerPnl||(this.extraData && this.extraData.innerPnl);
 if(innerPnl){
 if(scope.pressed) {
 innerPnl.expand();
 }else{
 innerPnl.collapse();
 }
 }
 }
 */
//спрятать или показать панельку снизу документа
function onDocInfoToggle(scope) {
    var innerPnl = scope.innerPnl || (scope.extraData && scope.extraData.innerPnl) || this.innerPnl || (this.extraData && this.extraData.innerPnl);
    var activatePanel = scope.activatePanel || (scope.extraData && scope.extraData.activatePanel) || this.activatePanel || (this.extraData && this.extraData.activatePanel);
    if (innerPnl) {
        if (scope.pressed) {
            innerPnl.expand();
        } else {
            innerPnl.collapse();
        }
        if (activatePanel) {
            innerPnl.setActiveTab(activatePanel);
        }
    }
}

// перейти в нужную часть документа
DocumentUI.prototype.gotoAnchor = function (anchor, offset) {
    if (offset !== null) {
        var top = null;
        try {
            top = Number(offset);
        } catch (e) {
        }
        var div = $('#' + this.innerPnlId);
        if (top >= 0 && div.height() > top) {
            div.parent().scrollTop(top);
            return true;
        }
    }
    if (anchor == null) return false;
    var first = null;
    if (typeof anchor == 'string') {
        anchor = $('#' + this.innerPnlId).find('#' + anchor);
    }
    if (anchor instanceof jQuery && anchor.length > 0) {
        first = anchor[0];
    }
    if (anchor instanceof  HTMLElement) {
        first = anchor;
    }
    if (first) {
        $('#' + this.innerPnlId).parent().scrollTop(first.offsetTop);
        return true;
    }
    return false;
};

// найти следующее совпадение по тексту
DocumentUI.prototype.searchNext = function () {
    this.search(this.cSearchInc.getValue());

};

//найти предыдущее совпадение по тексту
DocumentUI.prototype.searchPrev = function () {
    this.cSearchInc.extraData.searchIndex -= 2;
    this.search(this.cSearchInc.getValue());
};

//поиск по тексту документа
DocumentUI.prototype.onSearchKeyPress = function () {
    this.search(this.cSearchInc.getValue());
};

//инкрементальный поиск по тексту документа
DocumentUI.prototype.search = function (search) {
    if (search == null || search == '' || search.length < 3) return;
    // новая строка поиска
    if (this.cSearchInc.extraData.searchStr != search) {
        if (this.docInfo && this.docInfo.html) {
            // текст документа
            var html = this.docInfo.html;
            var insensitive = true;
            var klass = 'highlight';
            var regex = new RegExp('(<[^>]*>)|(' + search.replace(/([-.*+?^${}()|[\]\/\\])/g, "\\$1") + ')', insensitive ? 'ig' : 'g');
            // документ с подсветкой найденных слов
            var html2 = html.replace(regex, function (a, b, c) {
                return (a.charAt(0) == '<') ? a : '<strong class="' + klass + '">' + c + '</strong>';
            });
            // если оба текста равны, значит не нашли строку поиска
            if (html2 == html) {
                // подсветим красным
                this.cSearchInc.addClass('not_found');
            } else {
                // уберем подсветку красным
                this.cSearchInc.removeClass('not_found');
                replaceHtml(this.innerPnlId, html2);
            }
            var elements = $('#' + this.innerPnlId + '  strong.' + klass);
            var self = this;
            this.cSearchInc.extraData.searchIndex = 0;
            this.cSearchInc.extraData.elems = elements;
            this.cSearchInc.extraData.searchStr = search;
        }
    }
    // мы не можем начать с отрицательного индекса
    if (this.cSearchInc.extraData.searchIndex < 0) this.cSearchInc.extraData.searchIndex = 0;
    // переходим к следующем блоку
    var index = this.cSearchInc.extraData.searchIndex++;
    var items = this.cSearchInc.extraData.elems;
    var first = items.get(index);
    var found = this.gotoAnchor(first);
    if (!found) {
        var nextDoc = this.onDocumentNext && typeof this.onDocumentNext == 'function' && this.onDocumentNext(this, this.docId);
        var docTitle = nextDoc == null ? "" : ( nextDoc.fullName > 300 ? (nextDoc.fullName.substring(0, 300) + '...') : nextDoc.fullName);
        // ничего не нашли в документе
        if(items.length == 0){
            if(docTitle == ""){
                alert("Не удалось найти искомые данные");
                return;
            }
            if(confirm("Не удалось найти искомые данные\nНачать поиск в следующем документе \n" + docTitle)){
                this.resetSearch = false;
                this.internalOppenDocument(nextDoc.id, null, 0, false);
                return;
            }
        }else{
            // нашли в документе, но похоже дошли до конца документа
            if(confirm("Поиск дошел до конца документа\nНачать поиск с начала этого документа ?")){
                this.cSearchInc.extraData.searchStr = "";
                this.cSearchInc.extraData.searchIndex = 0;
                this.search(search);
            }else if(confirm("Начать поиск в следующем документе \n" + docTitle)){
                this.resetSearch = false;
                this.internalOppenDocument(nextDoc.id, null, 0, false);
            }
        }
    }
};


DocumentUI.prototype.buildUI = function () {
    this.innerPnlId = Ext.id();
    this.innerPnlBottomId = Ext.id();
    this.taskSearch = new Ext.util.DelayedTask(this.onSearchKeyPress, this);
    this.cSearchInc = new Ext.form.TextField({extraData: {searchIndex: 0}, enableKeyEvents: true, emptyText: 'Поиск по документу'});
    this.cSearchInc.on('keypress', function (scope, e) {
        this.taskSearch.delay(500);
    }, this);
    this.innerPnl = new Ext.Panel({ autoHeight: false, region: 'center', hideCollapseTool: true, collapsible: false, autoScroll: true, border: false,
        html: '<div class="document" style="color:black;padding: 5px;" id=' + this.innerPnlId + '></div>' });
    this.pnlDocLoadProgessId = Ext.id();
    this.pnlDocLoadProgess = new Ext.Panel({ autoHeight: false, split: false, height: 22, minHeight: 22, region: 'north', hideCollapseTool: true, collapsible: false, autoScroll: true, border: false,
        items: [new Ext.Button({border: false, enableToggle: false, iconCls: 'doc-close', style: 'float:left;', tooltipType: 'title', tooltip: 'Отменить докачку документа', handler: this.stopDownloading, scope: this, extraData: { }}),
            new Ext.Panel({ height: 20, html: '<div style="color:black;float:left;height: 20px;width: 100%;" id=' + this.pnlDocLoadProgessId + "></div><div id='p" + this.pnlDocLoadProgessId + "' style='left: 50%; top: 0px; width: 10%; font-weight: bold; position: absolute;'></div>"})] });
    this.pnlDocComment = new Ext.Panel({ autoHeight: false, region: 'south', height: 100,
        hideCollapseTool: true, title: 'Документ-комментарий', collapsible: true, collapseMode: 'mini', bodyStyle: 'background-color: Azure;padding: 10px;',
        autoScroll: true, border: false, html: '<div class="document" id=' + this.innerPnlBottomId + '></div>' });
    this.pnlDocCorrId = Ext.id();
    this.pnlDocCorr = new Ext.Panel({ autoHeight: false, region: 'south', height: 100,
        hideCollapseTool: true, title: 'Кореспонденты', collapsible: true, collapseMode: 'mini', bodyStyle: 'background-color: Azure;padding: 10px;',
        autoScroll: true, border: false, html: '<div class="document" id=' + this.pnlDocCorrId + '></div>' });
    this.pnlDocRespId = Ext.id();
    this.pnlDocResp = new Ext.Panel({ autoHeight: false, region: 'south', height: 100,
        hideCollapseTool: true, title: 'Респонденты', collapsible: true, collapseMode: 'mini', bodyStyle: 'background-color: Azure;padding: 10px;',
        autoScroll: true, border: false, html: '<div class="document" id=' + this.pnlDocRespId + '></div>' });

    this.innerPnlBottom = new Ext.Panel({ autoHeight: false, region: 'south', height: 100, layout: 'card', activeItem: 0,
        hideCollapseTool: true, collapsible: true, collapseMode: 'mini',
        autoScroll: true, border: false, items: [this.pnlDocComment, this.pnlDocCorr, this.pnlDocResp] });
    this.innerPnlBottom.getActiveTab = function () {
        return this.layout.activeItem;
    };
    this.innerPnlBottom.setActiveTab = function (tab) {
        this.layout.setActiveItem(tab);
    };
    this.pnlDocLoadingId = Ext.id();
    this.rightPnlToolbar = new Ext.Panel({ autoHeight: false, region: 'east', hideCollapseTool: true, collapsible: false, split: false, border: false,
        frame: true, bodyStyle: 'padding: 0px;width:30px;', width: 35, minWidth: 35,
        cls: 'x-toolbar x-toolbar-vert', //html: '<div style="padding: 0px;" ></div>',
        items: [new Ext.Button({border: false, enableToggle: true, icon: 'images/commentsInText.png',
            ref: '../btnComments', tooltipType: 'title', tooltip: 'Показать / спрятать комментарии',
            handler: onCommentsToggle, scope: this, extraData: { innerPnl: this.innerPnl }}),
            new Ext.Button({border: false, enableToggle: true, toggleGroup: 'info', icon: 'images/commentDoc.png',
                ref: '../btnCommentDoc', tooltipType: 'title', tooltip: 'Показать / спрятать справку к документу',
                handler: onDocInfoToggle, scope: this, extraData: { innerPnl: this.innerPnlBottom, activatePanel: this.pnlDocComment}}),
            new Ext.Button({border: false, enableToggle: true, toggleGroup: 'info', icon: 'images/doc_corr.png',
                ref: '../btnDocCorr', tooltipType: 'title', tooltip: 'Показать / спрятать кореспондентов',
                handler: onDocInfoToggle, scope: this, extraData: { innerPnl: this.innerPnlBottom, activatePanel: this.pnlDocCorr}}),
            new Ext.Button({border: false, enableToggle: true, toggleGroup: 'info', icon: 'images/doc_resp.png',
                ref: '../btnDocResp', tooltipType: 'title', tooltip: 'Показать / спрятать респондентов',
                handler: onDocInfoToggle, scope: this, extraData: { innerPnl: this.innerPnlBottom, activatePanel: this.pnlDocResp}}),
            new Ext.Button({border: false, enableToggle: false, icon: 'images/doc_word.png',
                ref: '../btnDocWord', tooltipType: 'title', tooltip: 'Сохранить в Word',
                handler: this.downloadDocument, scope: this, extraData: { mode: 1 }}),
            new Ext.Button({border: false, enableToggle: false, icon: 'images/doc_print.png',
                ref: '../btnDocPrint', tooltipType: 'title', tooltip: 'Версия для печати',
                handler: this.downloadDocument, scope: this, extraData: { mode: 0 }}),
            new Ext.Button({border: false, enableToggle: false, icon: 'images/doc_size_inc.png',
                ref: '../btnDocSizeInc', tooltipType: 'title', tooltip: 'Увеличить шрифт',
                handler: this.documentSizeInc, scope: this, extraData: { step: 1 }}),
            new Ext.Button({border: false, enableToggle: false, icon: 'images/doc_size_dec.png',
                ref: '../btnDocSizeDec', tooltipType: 'title', tooltip: 'Уменьшить шрифт',
                handler: this.documentSizeInc, scope: this, extraData: { step: -1 }}),
            new Ext.Button({border: false, enableToggle: false, icon: 'images/doc_bookmark.png',
                ref: '../btnDocBookmark', tooltipType: 'title', tooltip: 'Добавить в закладку',
                handler: this.documentBookmark, scope: this, extraData: { step: -1 }})
        ]
    });
    var tmpLabel = createLabel('', 0, 'padding-left:5px;font-weight:bold;overflow: hidden; text-overflow: ellipsis; display: block; visibility: visible; height: 15px; white-space: normal;');
    tmpLabel.boxMinWidth = 10;
    tmpLabel.minWidth = 10;
    this.cSearchInc.cls = 'doc-search-btn';
    this.panel = new Ext.Panel({
        border: false,
        autoScroll: false,
        autoHeight: false,
        region: 'center',
        collapsible: true,
        hideCollapseTool: true,
        layout: 'border',
        listeners: {
            afterlayout: onAfterPanelLayout
        },
        closable: true,
        //title: 'Документ ',
        extraData: {id: null},
        minSize: 300,
        minWidth: 300,
        items: [this.innerPnl, this.innerPnlBottom, this.rightPnlToolbar, this.pnlDocLoadProgess],
        defaults: {
            collapsible: true,
            split: true,
            bodyStyle: 'padding:0px'
        },
        tbar: [tmpLabel,
            /*{
             typeId: 'openBtn',
             enableToggle:true,
             iconCls: 'icon-pencil',
             tooltipType:'title',
             tooltip: 'Показать / спрятать комментарии' ,
             handler: onCommentsToggle,
             scope:this,
             //ref:'../btnComments',
             extraData: {
             innerPnl: this.innerPnl
             }
             },*/'->', this.cSearchInc,
            {text: 'предыдущий', iconCls: 'doc-search-up', cls: 'popup_btn', handler: this.searchPrev, scope: this, tooltipType: 'title', tooltip: 'предыдущий'},
            {text: 'следующий', iconCls: 'doc-search-down', cls: 'popup_btn', handler: this.searchNext, scope: this, tooltipType: 'title', tooltip: 'следующий'},
            ' ',
            {iconCls: 'doc-close', tooltipType: 'title', tooltip: 'Закрыть', handler: this.closeDocument, scope: this}
        ]

    });
    this.panel.lnkTitle = tmpLabel;
    return this.panel;
};
// открыть документ
DocumentUI.prototype.internalOppenDocument = function (docId, anchor, offset, fromHistory) {
    // если из одного документа переходим в другой
    if (this.docId) {
        this.saveAnchorToHistory(fromHistory === true);
        sendActionLog('doc_close', this.docId);
    }
    this.docId = docId;
    this.docAnchor = anchor;
    this.docOffset = offset;
    this.fromHistory = fromHistory === true;
    sendActionLog('doc_open', this.docId);
    callRequestHandler("DocInfo", {id: docId}, CallClassMethodAfterAjaxReturn, CallClassMethodAfterAjaxReturn, {thisRef: this, thisMethod: 'continueOpenDocument'});
    return true;
};

DocumentUI.prototype.openDocument = function (docId, href, fromHistory) {
    var tmp2 = null;
    if (!docId && href) {
        if (href && href.charAt(0) == "#") return false;
        tmp2 = href;
        if (href.indexOf("//") >= 0) {
            tmp2 = href.substr(href.indexOf("//") + 2);
        }
    } else if (docId) {
        tmp2 = docId;
        docId = null;
    }
    if (isStrNullOrEmpty(tmp2)) return false;
    var anchor = null;
    var offset = null;
    // ссылки могут быть след видов
    // 5990578/#5749999
    // 5990578#5749999
    // 5990578/
    // 5990578
    // jl:
    if (tmp2.indexOf("jl:") >= 0) {
        var msg = "Документ, который вас интересует,\nперемещен в новый классификатор,\nдля его открытия вы можете\nвоспользоваться поиском.\nВ случае, если поиск не дал нужных результатов,\nпросим обратиться в службу поддержки\n8 (727) 271-01-83";
        alert(msg);
        return true;
    }
    if (tmp2.indexOf("/") > 0) {
        var tmp = tmp2.substr(tmp2.indexOf("/"));
        if (tmp.lastIndexOf("#") > 0) {
            anchor = tmp.substr(tmp.lastIndexOf("#") + 1);
        }
        tmp2 = tmp2.substr(0, tmp2.indexOf("/"));
    }
    if (tmp2.indexOf("#") > 0) {
        anchor = tmp2.substr(tmp2.indexOf("#") + 1);
        tmp2 = tmp2.substr(0, tmp2.indexOf("#"));
    }
    if (tmp2.indexOf("!") > 0) {
        var opts = tmp2.substr(tmp2.indexOf("!")).split("!");
        for (var k = 0; k < opts.length; k++) {
            var opt = opts[k];
            if (opt.indexOf("off_") == 0) {
                offset = opt.substr(4);
            }
        }
        tmp2 = tmp2.substr(0, tmp2.indexOf("!"));
    }
    if (Number(tmp2) > 0) {
        docId = tmp2;
    }
    if (!docId) return false;
    if (docId == this.docId) {
        this.gotoAnchor(anchor);
        if (this.onDocumentOpen) {
            this.onDocumentOpen(this);
        }
        return true;
    }
    this.resetSearch = true;
    return this.internalOppenDocument(docId, anchor, offset, fromHistory);
};

/*
 * возвращает функцию для прокрутки тела документа
 * */
DocumentUI.prototype.getDocumentScrollHandler = function () {
    var self = this;
    return function () {
        self.onDocumentScroll();
    }
};

/*
 * возвращает функцию для открытия документа с указанным ид
 */
DocumentUI.prototype.getDocumentOpenHandler = function (id, fromHistory) {
    var self = this;
    return function () {
        self.openDocument(id, null, fromHistory);
        return false;
    };
};

// загрузка списка респондентов корреспондентов
DocumentUI.prototype.onRespCorrLoad = function (result, scope, extraParams, o, response) {
    if (getResponseErrorText(result)) {
        alert(getResponseErrorText(result));
        return;
    }
    var resp = eval('(' + getResponseText(result).replace(new RegExp('\r', 'g'), ' ').replace(new RegExp('\n', 'g'), ' ') + ')');
    if (resp && resp.docs && resp.docs.length > 0) {
        var target = '#' + this.pnlDocCorrId;
        var docs = resp.docs;
        var respSwitched = false;
        for (var i = 0, len = docs.length; i < len; i++) {
            var doc = docs[i];
            if (doc.resp_id == this.docId && !respSwitched) {
                target = '#' + this.pnlDocRespId;
                respSwitched = true;
            }
            var a = $('<div/>').append($('<a href="#"/>').text(doc.fullName).click(this.getDocumentOpenHandler(doc.id)));
            if (i % 2 == 1) a.addClass('PowderBlue');
            $(target).append(a);
        }
    }
};


/*
 * будем проверять нужна ли подкачка документа
 * */
DocumentUI.prototype.downloadDocument = function (scope) {
    var mode = 0;
    if (scope && scope.extraData && scope.extraData.mode) {
        mode = scope.extraData.mode;
    }
    var params = {
        requestType: 'DocLoad',
        id: this.docId,
        mode: mode,
        //показывать или нет коментарии
        comment: this.isVisible ? 1 : 0
    };
    var handlerUrl = 'RequestHandler';
    if (mode) {
        var report = new Ext.ux.Report({ renderTo: Ext.getBody() });
        report.load({
            url: handlerUrl,
            params: params
        });
    } else {
        var cDate = new Date();
        var tmp = cDate.format("dmYHis");
        //tmp = tmp;
        var wnd = window.open(handlerUrl + (params ? '?' + Ext.urlEncode(params) : ''), tmp, "width=820,height=680, menubar=yes,scrollbars=1,resizable=1, location=no, status=1");
    }

};

// подкачка следующего фрагмента документа
DocumentUI.prototype.loadNextPart = function () {
    if (this.docInfo) {
        if (this.docInfo.offset >= this.docInfo.size) {
            return;
        }
        this.isDocLoading = true;
        replaceHtml(this.pnlDocLoadingId, this.loadingBigImgHtml);
        callRequestHandler("DocLoadPartial", {id: this.docId, offset: this.docInfo.offset}, CallClassMethodAfterAjaxReturn, CallClassMethodAfterAjaxReturn, {thisRef: this, thisMethod: 'onDocumentLoadPartial', innerPnl: this.innerPnl, innerPnlId: this.innerPnlId});
    }
};

//если при прокрутки документа, есть неподкачання часть и мы в конце - запускаем подкачку
DocumentUI.prototype.onDocumentScroll = function () {
    var jq = $("#" + this.innerPnl.body.id);
    if (jq.scrollTop() >= ($("#" + this.innerPnlId).height() - jq.height()) && this.isDocLoading == false) {
        this.loadNextPart();
    }
};

// обработчик получего фрагмента документа
DocumentUI.prototype.onDocumentLoadPartial = function (result, scope, extraParams, o, response) {
    if (getResponseErrorText(result)) {
        alert(getResponseErrorText(result));
        return;
    }
    var respText = getResponseText(result);
    if (!respText) respText = 'Документ не найден';
    respText = this.preprocessDocumentBody(respText);
    var innerPnl = scope.innerPnl;
    var innerPnlId = scope.innerPnlId;
    if (innerPnl && innerPnl.body && innerPnl.body.dom) {
        var jLoading = $('#' + this.pnlDocLoadingId);
        jLoading.empty();
        var newId = Ext.id();
        var newDiv = $('<div style="font-size: 12pt;"/>').attr('id', newId).html(respText);
        this.afterDocumentBodyLoad(innerPnlId, newId, newDiv);
        newDiv.attr("style", "");
        jLoading.before(newDiv);
        //$('#'+this.pnlDocLoadingId).before(respText);
        this.tryShowDocAnchor();
    }
    if (response && response.params && response.params.id == this.docId) {
        this.docInfo.html += respText;
        this.isDocLoading = false;
        var percent = Number(100 * this.docInfo.offset / this.docInfo.size);
        var showProgressBar = this.autoDocLoading && (percent < 100);
        if (showProgressBar) {
            this.loadNextPart();
        }else{
            //начать поиск с начала этого документа
            if(this.resetSearch == false){
                var search = this.cSearchInc.extraData.searchStr;
                this.cSearchInc.extraData.searchStr = "";
                this.cSearchInc.extraData.searchIndex = 0;
                this.search(search);
            }
        }
    }
};

// закрыть панель с документом
DocumentUI.prototype.closeDocument = function (noNavigate) {
    if (!isStrNullOrEmpty(this.docId)) {
        sendActionLog('doc_close', this.docId);
    }
    this.docId = null;
    this.docAnchor = null;
    this.docOffset = null;
    if (this.onDocumentClose) {
        this.onDocumentClose(this, noNavigate);
    }
};

// изменить размер шрифта
DocumentUI.prototype.documentSizeInc = function (scope) {
    var step = 0;
    if (scope && scope.extraData && scope.extraData.step) {
        step = scope.extraData.step;
    }
    var innerPnlId = this.innerPnlId;
    var obj = $("#" + innerPnlId);
    var size = getActualStyle(obj[0], "font-size");
    size = getSizePT(size);
    size = size + step;
    if (size < 5) size = 5;
    if (size > 30) size = 30;
    obj.css("fontSize", size + "pt");
};

// выключает автоподкачку документа
DocumentUI.prototype.stopDownloading = function (scope) {
    this.autoDocLoading = false;
};

//добавить событие в список
DocumentUI.prototype.on = function (event, fn, scope) {
    var clb = {fn: fn, scope: scope};
    this.listeners[event] = this.listeners[event] || [];
    this.listeners[event].push(clb);
};

//оповестить о событии
DocumentUI.prototype.fire = function (event, arg1, arg2, arg3) {
    var listeners = this.listeners[event];
    if (listeners && listeners.length > 0) {
        for (var i = 0, il = listeners.length; i < il; i++) {
            var clb = listeners[i];
            clb.fn.call(clb.scope, this, arg1, arg2, arg3);
        }
    }
};

// Добавить документ в закладки
DocumentUI.prototype.documentBookmark = function (scope) {
    if (userUI == null || userUI.user == null || (userUI.user.locMode && !userUI.user.locMode1)) {
        return;
    }
    this.fire('bookmark', this.docId);
};
