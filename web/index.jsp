﻿<!DOCTYPE html>
<%@ page import="org.apache.solr.core.SolrCore" %>
<%@ page import="java.util.Collection" %>
<%@ page import="kz.novelty.jurist.Connector" %>
<%
    boolean  debug = true;
    boolean updateBuilderMode = Connector.Instance().isUpdateBuilderMode();
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Правовой консультант Online</title>
  <link rel="stylesheet" type="text/css" href="css/navigate.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="css/ext-all-notheme.css" />
	<!-- Theme includes -->
	<link rel="stylesheet" type="text/css" title="gray" href="css/xtheme-gray.css" />
    <link rel="stylesheet" type="text/css" href="js/ext/jquery.autocomplete.css" media="screen" />
    <link rel="stylesheet" type="text/css" media="screen" href="css/jquery/jquery-ui-1.8.11.custom.css" />
    <link rel="stylesheet" type="text/css" href="js/ext/smoothness/jquery-ui.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/jquery/jquery.spinbox.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/grid-filter/style.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/jquery/ui.notify.css" media="screen" />
    <link rel="icon" href="css/favicon.ico" type="image/ico"/>
    <link rel="shortcut icon" href="css/favicon.ico" type="image/ico"/>
<% if(debug){ %>
    <script type="text/javascript" src="js/extjs/ext-base-debug.js"></script>
    <script type="text/javascript" src="js/extjs/ext-all-debug.js"></script>
    <script type="text/javascript" src="js/ext/jquery-1.5.1.full.js"></script>
    <script type="text/javascript" src="js/ext/jquery-ui-1.8.11.custom.js"></script>
<% }else{ %>
    <script type="text/javascript" src="js/extjs/ext-base.js"></script>
    <script type="text/javascript" src="js/extjs/ext-all.js"></script>
    <script type="text/javascript" src="js/ext/jquery-1.5.1.min.js"></script>
    <script type="text/javascript" src="js/ext/jquery-ui-1.8.11.custom.min.js"></script>
<% } %>
    <script type="text/javascript" src="js/Utils.js"></script>
    <script type="text/javascript" src="js/WUIUtils.js"></script>
    <script type="text/javascript" src="js/Logger.js"></script>
    <script type="text/javascript" src="js/extjs/GridFilterMenu.js"></script>
    <script type="text/javascript" src="js/extjs/GridFilterTypes.js"></script>
    <script type="text/javascript" src="js/extjs/GridFilters.js"></script>
    <script type="text/javascript" src="js/userAdminUI.js"></script>
    <script type="text/javascript" src="js/ParamMsg.js"></script>
    <script type="text/javascript" src="js/extjs/ext-lang-ru.js"></script>
    <script type="text/javascript" src="js/ext/jquery.history.js"></script>
    <script type="text/javascript" src="js/ext/jquery.spinbox.js"></script>
    <% if(updateBuilderMode){ %>
    <script type="text/javascript" src="js/updateBuilder.js"></script>
    <% }else{ %>
  <script type="text/javascript" src="js/navigateUI.js"></script>
  <script type="text/javascript" src="js/lightUI.js"></script>
  <script type="text/javascript" src="js/core/Core.js"></script>
  <script type="text/javascript" src="js/core/AbstractManager.js"></script>
  <script type="text/javascript" src="js/managers/Manager.jquery.js"></script>
  <script type="text/javascript" src="js/core/Parameter.js"></script>
  <script type="text/javascript" src="js/core/ParameterStore.js"></script>
  <script type="text/javascript" src="js/core/AbstractWidget.js"></script>
  <script type="text/javascript" src="js/widgets/ResultWidget.js"></script>
  <script type="text/javascript" src="js/helpers/jquery/ajaxsolr.theme.js"></script>
  <script type="text/javascript" src="js/navigate.theme.js"></script>
  <script type="text/javascript" src="js/jquery.livequery.js"></script>
  <script type="text/javascript" src="js/widgets/jquery/PagerWidget.js"></script>
  <script type="text/javascript" src="js/core/AbstractFacetWidget.js"></script>
  <script type="text/javascript" src="js/widgets/TagcloudWidget.js"></script>
  <script type="text/javascript" src="js/widgets/CurrentSearchWidget.9.js"></script>
  <script type="text/javascript" src="js/ext/jquery.autocomplete.js"></script>
  <script type="text/javascript" src="js/widgets/AutocompleteWidget.js"></script>
  <script type="text/javascript" src="js/helpers/ajaxsolr.support.js"></script>
  <script type="text/javascript" src="js/helpers/ajaxsolr.theme.js"></script>
  <script type="text/javascript" src="js/DocumentUI.js"></script>
  <script type="text/javascript" src="js/userUI.js"></script>
  <script type="text/javascript" src="js/userBookmarks.js"></script>
  <script type="text/javascript" src="js/ext/jquery.notify.js"></script>
    <% } %>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-7137249-4']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
</head>

<body>
<form id="form1" style="width:100%; height:100;">
    <div id="mainContainer"></div>
</form>
<div class="ext-search-content" style="left: 150px; top: 55px; width: 250px;" id='ext-search-content'>
    <input type="checkbox" class="exact_search"/><span>&nbsp; Точное совпадение </span><span style="float: right;"><img class="ext-search-close" src="images/doc-close.png" alt="Закрыть" /></span><br/>
    <input type="radio" class="search_mode_t" name="ext-srch" checked/> Искать в заголовке<br/>
    <input type="radio" class="search_mode_b" name="ext-srch" /> Искать в тексте<br/>
    <div align="center">№ Регистрации</div>
    <input type="text" class="reg_number" id="reg_number" value=""/><br/>
    <div>№ Документа</div>
    <input type="text" class="doc_number" id="doc_number" value=""/><br/>
    <div align="center">Дата регистрации</div>
    <select type="text" class="reg_date">
        <option value="1">Совпадение</option>
        <option value="2" selected>Период</option>
        <option value="3">Больше или равно</option>
        <option value="4">Меньше или равно</option>
    </select><br/>
    <div style="padding-top: 5px;">с
    <input type="text" class="reg_date_beg" style="width:36%;" value=""/>
    <span>по</span>
    <input type="text" class="reg_date_end" style="width:36%;" value=""/><br/>
    </div>
    <div align="center">Дата документа</div>
    <select type="text" class="doc_date" >
        <option value="1">Совпадение</option>
        <option value="2" selected>Период</option>
        <option value="3">Больше или равно</option>
        <option value="4">Меньше или равно</option>
    </select><br/>
    <div style="padding-top: 5px;">с
    <input type="text" class="doc_date_beg" style="width:36%;" value=""/>
    <span>по</span>
    <input type="text" class="doc_date_end" style="width:36%;" value=""/>
    </div>
    <div align="center">Фильтр документов</div>
    <select type="text" class="doc_type" >
        <option value="">Все</option>
        <option value="0">Действуюшие</option>
        <option value="2">Не введенные в действие</option>
        <option value="1">Утратившие силу</option>
    </select>
    <div style="padding-top: 20px;">
        <input class="search_btn" type="button" style="width:80px;" value="Поиск">
        <input class="clear_btn" type="button" style="width:140px;" value="Очистить все поля">
    </div>
</div>
<div class="right-menu-content ui-widget-content" style="display: none; position: absolute; top: 15px; width: 390px;height: 400px;opacity: 1;max-height: 600px;overflow:hidden;padding-bottom: 70px;" id='monitoring'>
    <table class="header" cellspacing="0" cellpadding="0" border="1" align="left" style="width: 100%;font-size:11px;">
    </table>
    <div style="overflow-y: scroll; width: 100%; max-height: 150px;">
        <table class="list" cellspacing="0" cellpadding="0" border="1" align="left" style="width: 100%;font-size:11px;">
        </table>
    </div>
    <div class="detail" style="width: 100%;overflow-y: auto; max-height: 100%;font-size: 11px;">
        <label class = "accr mon_det_old">Утратившие силу</label>
        <div   class = "odd_event mon_det_1" style="/*overflow-y: auto; max-height: 250px;*/">&nbsp;</div>
        <label class = "accr mon_det_early">Документы, не введенные в действие</label>
        <div   class = "odd_event mon_det_2" style="">&nbsp;</div>
        <label class = "accr mon_det_new">Новые документы</label>
        <div   class = "odd_event mon_det_3" style="">&nbsp;</div>
        <label class = "accr mon_det_edited">Измененные документы</label>
        <div   class = "odd_event mon_det_4" style="">&nbsp;</div>
    </div>
</div>
<div style="display: none;opacity: 1;" id='monitoring_main'>
    <div>
        <h2 style="text-align: center;">Мониторинг обновлений</h2>
        <table cellspacing="5" style="width: 100%;padding-left: 10px;padding-right: 10px; font-size: 12px;">
            <tr>
                <td style="width: 25%;"><span style="font-size: 10px;" class="mon_new">&nbsp;</span> новые </td>
                <td style="width: 25%;"><span style="font-size: 10px;" class="mon_edited">&nbsp;</span> измененные </td>
            </tr>
            <tr>
                <td style="width: 25%;"><span style="font-size: 10px;" class="mon_old">&nbsp;</span> утратившие силу</td>
                <td style="width: 25%;"><span style="font-size: 10px;" class="mon_early">&nbsp;</span> не введенные в действие </td>
            </tr>
        </table>
    </div>
    <hr/>
    <div class="list" style="height:300px; overflow-y: auto">

    </div>
</div>
<div id='user_notification'>
    <div id="default">
        <h1>#{title}</h1>
        <p>#{text}</p>
    </div>
</div>
<div class="right-menu-content" style="display: none;top: 45px; width: 330px;" id='user_license_notification'> </div>
<div class="right-menu-content" style="display: none;top: 45px; width: 330px;" id='open_doc_history'> </div>
    <a class="right-menu1" href="#" style="display: none;width:60px;right: 1px;top: 30px;" title='История открытых документов' menu-content='#open_doc_history'>История</a>
    <!--
    <a id='userName' class="right-menu" href="#" style="display: none;width:100px;right: 1px;top: 5px;" title='Информация о пользователе' menu-click='showUI' menu-click-scope='userUI'>Войти</a>
     -->
  <div id="wrap" style="display: none;">
    <div id="header">
      <h1>Правовой консультант Online</h1>
      <h2>Поиск правовой информации</h2>
    </div>
  </div>

  <script type="text/javascript">
      METADATA = { vars: {} };
      Ext.onReady(function() {
        initMask = new Ext.LoadMask(Ext.getBody(), { msg: "Загрузка..." });;
        domReady();
    });
  </script>
</body>
</html>