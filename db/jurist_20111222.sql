-- MySQL dump 10.13  Distrib 5.5.9, for Win32 (x86)
--
-- Host: dust    Database: jurist
-- ------------------------------------------------------
-- Server version	5.1.49-1ubuntu8.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `jurist`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `jurist` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `jurist`;

--
-- Table structure for table `role_trees`
--

DROP TABLE IF EXISTS `role_trees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_trees` (
  `tree_id` bigint(20) NOT NULL DEFAULT '0',
  `role_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tree_id`,`role_id`),
  UNIQUE KEY `idx_role_tree_id` (`tree_id`,`role_id`),
  KEY `fk_tree_role_id` (`role_id`),
  CONSTRAINT `fk_tree_role_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=FIXED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_trees`
--

LOCK TABLES `role_trees` WRITE;
/*!40000 ALTER TABLE `role_trees` DISABLE KEYS */;
INSERT INTO `role_trees` VALUES (567,2),(1284,2),(2407,2),(2415,2);
/*!40000 ALTER TABLE `role_trees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'имя роли',
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `role_UNIQUE` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Роли пользователей';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (5,'user1'),(1,'Админ'),(2,'Пользователь');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `role_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `fk_ur_user_id` (`user_id`),
  KEY `fk_ur_role_id` (`role_id`),
  CONSTRAINT `fk_ur_role_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ur_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Роли присвоенные пользователям';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES (1,1),(3,5),(4,2);
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_types`
--

DROP TABLE IF EXISTS `user_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_types` (
  `user_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`user_type_id`),
  UNIQUE KEY `user_type_UNIQUE` (`user_type`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_types`
--

LOCK TABLES `user_types` WRITE;
/*!40000 ALTER TABLE `user_types` DISABLE KEYS */;
INSERT INTO `user_types` VALUES (2,'nalogi.kz'),(1,'свои пользователи');
/*!40000 ALTER TABLE `user_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `pwd` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_type_id` int(11) NOT NULL COMMENT 'Тип пользователя',
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'email',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'Наименование',
  `valid_from` date DEFAULT NULL,
  `valid_till` date DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `login_UNIQUE` (`login`),
  KEY `fk_u_type_id` (`user_type_id`),
  CONSTRAINT `fk_u_type_id` FOREIGN KEY (`user_type_id`) REFERENCES `user_types` (`user_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='пользователи системы юрист';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','*23AE809DDACAF96AF0FD78ED04B6A265E05AA257',1,'','','2011-12-22','2019-12-31'),(3,'user1','*832EB84CB764129D05D498ED9CA7E5CE9B8F83EB',1,'tmp@mail.ru','tmp','2011-12-22','2012-12-01'),(4,'user2','*FD571203974BA9AFE270FE62151AE967ECA5E0AA',1,'','','2011-12-01','2012-12-31'),(7,'user3','*832EB84CB764129D05D498ED9CA7E5CE9B8F83EB',1,'','','2011-12-08','2011-12-22');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`test`@`192.168.1.89`*/ /*!50003 TRIGGER `trigger1` BEFORE INSERT ON `users`
FOR EACH ROW BEGIN
  if(new.valid_from is NULL or new.valid_from ='')THEN
     set new.valid_from = now();
  end if;
  if(new.valid_till is NULL)THEN
     set new.valid_till = curdate();
  end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Dumping routines for database 'jurist'
--
/*!50003 DROP FUNCTION IF EXISTS `changePassword` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`192.168.1.89`*/ /*!50003 FUNCTION `changePassword`(`userId` int,`par` text) RETURNS text CHARSET utf8
BEGIN
	declare cnt int;
	declare id int;
	declare login1 text;
	declare pwd1 text;
	declare newpwd text;
	set login1= ExtractValue(par,'chg/login');
	set pwd1= ExtractValue(par,'chg/pwd');
	set newpwd= ExtractValue(par,'chg/newpwd');
	select count(*),max(u.user_id) into cnt,id from users u where u.user_type_id=1 and u.login=login1 and u.pwd=PASSWORD(pwd1);
	if(cnt=0)THEN
		return '<res><success>0</success></res>';
	end if;
  update users set pwd=PASSWORD(newpwd) where u.user_type_id=1 and login=login1;
	return '<res><success>1</success></res>';

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getDeniedTreeList` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`192.168.1.89`*/ /*!50003 FUNCTION `getDeniedTreeList`(userId int) RETURNS text CHARSET utf8
BEGIN
	declare res text DEFAULT '0';
	declare cnt int;
	select count(*) into cnt from user_roles ur where ur.role_id=1 and ur.user_id=userId;
	if(cnt=0)then
	  select DISTINCT GROUP_CONCAT(cast(tree_id as char) ) into res   from role_trees rt where  
		  rt.tree_id not in (select tree_id from role_trees rt1, user_roles ur where ur.role_id=rt1.role_id and ur.user_id = 1);
    if(res is null) then
      set res = '0';
    end if;
	end if;
	RETURN res;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getDocumentScheme` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`192.168.1.89`*/ /*!50003 FUNCTION `getDocumentScheme`() RETURNS text CHARSET utf8
BEGIN
	
	RETURN "sps";
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getRoles` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`192.168.1.89`*/ /*!50003 FUNCTION `getRoles`(`user_id` int) RETURNS text CHARSET utf8
BEGIN  
	declare cnt int default 0;  
	declare id int default 0;
	declare val1 VARCHAR(100) CHARSET utf8 default "" ;
	declare res text;
	set res="";
	SELECT count(*) as cnt1,GROUP_CONCAT("<row><id>",CAST(role_id as char),"</id><value>",role,"</value></row>" SEPARATOR '') as res1  into cnt, res from roles;
	if(cnt=-1)THEN
		BEGIN
			DECLARE done INT DEFAULT 0; 
			DECLARE curRoles CURSOR FOR SELECT role_id,role FROM roles;
			DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
			OPEN curRoles;
			REPEAT
					FETCH curRoles INTO id, val1;
					IF done=0 THEN
					 set res = CONCAT(res ,"<row><id>",id,"</id><value>",val1,"</value></row>");
					END IF;
			UNTIL done=0 END REPEAT;
			CLOSE curRoles;
		end;
	end if;
	set res = CONCAT("<dataset><total>",cnt,"</total>", res ,"</dataset>");
	return res;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getRolesTree` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`192.168.1.89`*/ /*!50003 FUNCTION `getRolesTree`(`user_id` int, par text) RETURNS text CHARSET utf8
BEGIN  
	declare cnt int default 0;  
	declare id int default 0;
	declare val1 VARCHAR(100) CHARSET utf8 default "" ;
	declare res text;
	set res="";
	SELECT count(*) as cnt1,GROUP_CONCAT("<row><id>",CAST(role_id as char),"</id><value>",tree_id,"</value></row>" SEPARATOR '') as res1  
				into cnt, res from role_trees where role_id=par;
	if(res is null) then set res =''; end if;
	set res = CONCAT("<dataset><total>",cnt,"</total>", res ,"</dataset>");
	return res;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getUserRoles` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`192.168.1.89`*/ /*!50003 FUNCTION `getUserRoles`(userid int, par text) RETURNS text CHARSET utf8
BEGIN 
	declare cnt int default 0;  
	declare id int default 0;
	declare val1 VARCHAR(100) CHARSET utf8 default "" ;
  DECLARE done INT DEFAULT 0; 
	declare res text;
	set res=" ";
	SELECT count(*) as cnt1,GROUP_CONCAT("<row><id>",CAST(u.role_id as char),"</id><value>",CAST(u.role_id as char),"</value></row>" SEPARATOR '') as res1  
			into cnt, res from user_roles u,roles r 
			where u.role_id=r.role_id and u.user_id=par;
	if(cnt=0) THEN
		set res = "";
	end if;
	set res = CONCAT("<dataset><total>",cnt,"</total>", res ,"</dataset>");
	return res;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getUserTypes` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`192.168.1.89`*/ /*!50003 FUNCTION `getUserTypes`(`user_id` int) RETURNS text CHARSET utf8
BEGIN 
	declare cnt int default 0;  
	declare id int default 0;
	declare val1 VARCHAR(100) CHARSET utf8 default "" ;
  DECLARE done INT DEFAULT 0; 
	declare res text;
	set res="";
	SELECT count(*) as cnt1,GROUP_CONCAT("<row><id>",CAST(user_type_id as char),"</id><value>",user_type,"</value></row>" SEPARATOR '') as res1  
		into cnt, res from user_types;
	set res = CONCAT("<dataset><total>",cnt,"</total>", res ,"</dataset>");
	return res;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `setRole` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`192.168.1.89`*/ /*!50003 FUNCTION `setRole`(userId int,`par` text) RETURNS text CHARSET utf8
    DETERMINISTIC
BEGIN   
	DECLARE xml2 VARCHAR(25) DEFAULT '<obj><count>2</count><parent>2</parent><row><id>0</id><value>2197</value></row><row><id>0</id><value>258</value></row></obj>';
  declare roleId int;
	DECLARE tmp text;
	DECLARE res text;
  set tmp = ExtractValue(par,'obj/row/id');
	set roleId = cast(tmp as UNSIGNED );
	SELECT ExtractValue(par,'obj/row/value') into tmp;
	if ((roleId is null )or (roleId=0) or (tmp = '' )) THEN
		insert into roles (role) VALUES(tmp);
		select role_Id into res from roles where role=tmp;
	ELSE
		update roles set role=tmp where role_id=roleId;
		select roleId into res;
	end if;
	return res;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `setRolesTree` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`192.168.1.89`*/ /*!50003 FUNCTION `setRolesTree`(userId int,`par` text) RETURNS text CHARSET utf8
    DETERMINISTIC
BEGIN   
	DECLARE xml2 VARCHAR(25) DEFAULT '<obj><count>2</count><role>2</role><row><id>0</id><value>2197</value></row><row><id>0</id><value>258</value></row></obj>';
  declare cnt int;
  declare roleId int;
  declare i int DEFAULT 0;
	DECLARE tmp text;
	DECLARE res text;
  set tmp = ExtractValue(par,'obj/count');
	set cnt = cast(tmp as UNSIGNED );
  set tmp = ExtractValue(par,'obj/parent');
	set roleId = cast(tmp as UNSIGNED );
	if(roleId is null or roleId=0) THEN
		return 'nothing saved';
  end if;
	delete from role_trees where role_id=roleId;
	WHILE i < cnt DO
		set i=i+1;
		SELECT ExtractValue(par,'obj/row[$i]/value') into tmp;
		insert into role_trees VALUES(tmp,roleId);
 END WHILE;
	select i into res;
	return res;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `setUser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`192.168.1.89`*/ /*!50003 FUNCTION `setUser`(userId int,`par` text) RETURNS text CHARSET utf8
    DETERMINISTIC
BEGIN   
	DECLARE xml2 VARCHAR(25) DEFAULT '<obj><count>2</count><parent>2</parent><row><id>0</id><value>2197</value></row><row><id>0</id><value>258</value></row></obj>';
  declare userId int;
	DECLARE tmp text;
	DECLARE tmp1 text;
	DECLARE tmp2 text;
	DECLARE tmpValidFrom  text;
	DECLARE tmpValidTill  text;
	DECLARE tmpEmail  text;
	DECLARE tmpName  text;
	DECLARE res text;
  set tmp = ExtractValue(par,'obj/row/id');
	set userId = cast(tmp as UNSIGNED );
	SELECT ExtractValue(par,'obj/row/value') into tmp;
	SELECT ExtractValue(par,'obj/row/pass') into tmp1;
	SELECT ExtractValue(par,'obj/row/type_id') into tmp2;
	SELECT ExtractValue(par,'obj/row/valid_from') into tmpValidFrom;
	SELECT ExtractValue(par,'obj/row/valid_till') into tmpValidTill;
	SELECT ExtractValue(par,'obj/row/email') into tmpEmail;
	SELECT ExtractValue(par,'obj/row/name') into tmpName;
	if(tmp2 is null  or tmp2='') THEN
		set tmp2 = '1';
	end if;
	if(tmpValidFrom='' or tmpValidFrom='0000-00-00') THEN
		set tmpValidFrom = CURDATE();
	end if;
	if(tmpValidTill='' or tmpValidTill='0000-00-00') THEN
		set tmpValidTill = CURDATE();
	end if;

	if ((userId is null )or (userId=0)) THEN
		insert into users (login,pwd,user_type_id, valid_from, valid_till,email,name) VALUES(tmp,tmp1,tmp2,tmpValidFrom, tmpValidTill,tmpEmail,tmpName);
		select user_Id into userId from users where login=tmp and user_type_id=tmp2;
	ELSE
		update users set login=tmp, valid_from=tmpValidFrom, valid_till=tmpValidTill, email=tmpEmail, name=tmpName where user_id=userId;
	end if;
	if(tmp1 is not null and tmp1<>'')then
		update users set pwd=PASSWORD(tmp1) where user_id=userId;
	end if;
	select userId into res;
	return res;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `setUserRoles` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`192.168.1.89`*/ /*!50003 FUNCTION `setUserRoles`(userId int,`par` text) RETURNS text CHARSET utf8
    DETERMINISTIC
BEGIN   
	DECLARE xml2 VARCHAR(25) DEFAULT '<obj><count>2</count><role>2</role><row><id>0</id><value>2197</value></row><row><id>0</id><value>258</value></row></obj>';
  declare cnt int;
  declare userId int;
  declare i int DEFAULT 0;
	DECLARE tmp text;
	DECLARE res text;
  set tmp = ExtractValue(par,'obj/count');
	set cnt = cast(tmp as UNSIGNED );
  set tmp = ExtractValue(par,'obj/parent');
	set userId = cast(tmp as UNSIGNED );
	if(userId is null or userId=0) THEN
		return 'nothing saved';
  end if;
	delete from user_roles where user_id=userId;
	WHILE i < cnt DO
		set i=i+1;
		SELECT ExtractValue(par,'obj/row[$i]/value') into tmp;
		insert into user_roles(user_id,role_id) VALUES(userId,tmp);
 END WHILE;
	select i into res;
	return res;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `testXml` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`192.168.1.89`*/ /*!50003 FUNCTION `testXml`(`xml1` varchar(100)) RETURNS varchar(255) CHARSET utf8
    DETERMINISTIC
BEGIN
	DECLARE xml2 VARCHAR(25) DEFAULT '<a>    <b c="1"><d>X</d></b>    <b c="2"><d>X</d></b>    </a>';
	DECLARE res text;
	set res = ExtractValue(xml1,'a/b/d[../@c="1"]');
	SET res = '1';
	set res = getRoles(0);
/*
 WHILE i < 4 DO
    SELECT xml, i, ExtractValue(xml, '//a[$i]');
    SET i = i+1;
 END WHILE;
*/
	return res;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `doLogin` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`192.168.1.89`*/ /*!50003 PROCEDURE `doLogin`(IN `login` text,IN `pwd` text,INOUT `userId` int)
BEGIN 
	declare cnt int;
	declare id int;
	select count(*),max(u.user_id) into cnt,id from users u where u.user_type_id=1 and u.login=login and u.pwd=PASSWORD(pwd);
	if(cnt>0)THEN
		set userId = id;
	end if;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `execute_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`192.168.1.89`*/ /*!50003 PROCEDURE `execute_proc`(IN user_id int, IN reqType text, IN par text, INOUT res text)
BEGIN
		if (reqType="getRoles") THEN
			set res = getRoles(user_id);
		elseif (reqType = "getUserTypes") then
			set res = getUserTypes(user_id);
		elseif (reqType = "getUserRoles") then
			set res = getUserRoles(user_id,par);
		elseif (reqType = "getUsers") then
			call getUsers(user_id,par, res);
		elseif (reqType = "changePassword") then
			set res = changePassword(user_id,par);
		elseif (reqType = "getRolesTree") then
			set res = getRolesTree(user_id,par);
		elseif (reqType = "setRolesTree") then
			set res = setRolesTree(user_id,par);
		elseif (reqType = "setRole") then
			set res = setRole(user_id,par);
		elseif (reqType = "setUserRoles") then
			set res = setUserRoles(user_id,par);
		elseif (reqType = "setUser") then
			set res = setUser(user_id,par);
		elseif (reqType = "isTreeDenied") then
			call isTreeDenied(user_id, par, res);
		elseif (reqType = "isDocumentDenied") then
			call isDocumentDenied(user_id, par, res);
		end if;
		if(res is null)then
			set res='';
		end if;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getUsers` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`192.168.1.89`*/ /*!50003 PROCEDURE `getUsers`(`userId` int, par text, out res text)
BEGIN
	declare cnt int default 0;  
	declare id int default 0;
	declare val1 VARCHAR(100) CHARSET utf8 default "" ;
	declare where1 text CHARSET utf8 default "" ;
	SELECT ExtractValue(par,'filter/start') into @start1;
	SELECT ExtractValue(par,'filter/limit') into @limit1;
	set res="";
	set @sql1 = concat('SELECT GROUP_CONCAT("<row><id>",CAST(user_id as char),
							"</id><value>",login,"</value><type_id>",
							user_type_id,"</type_id><valid_from>",
							IFNULL(valid_from,CURDATE()),"</valid_from><valid_till>",
							IFNULL(valid_till,CURDATE()),"</valid_till><email>",
							IFNULL(email,""),"</email><name>",
							IFNULL(name, ""),"</name></row>" SEPARATOR "") as res1  ,
				(select count(*) from users ',where1,') as cnt1
				into @res,@cnt
		 from (select t.* from users t ',where1,'limit ?,? ) k');
	PREPARE stmt FROM @sql1;
	EXECUTE stmt USING @start1,@limit1;
	DEALLOCATE PREPARE stmt;
	set res = @res;
	set cnt = @cnt;
	set res = CONCAT("<dataset><total>",cnt,"</total>", res ,"</dataset>");
	/*return res;*/
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `isDocumentDenied` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`192.168.1.89`*/ /*!50003 PROCEDURE `isDocumentDenied`(IN userId int,IN par text,OUT res text)
BEGIN
	declare sch text;
	declare deniedTrees text;
	declare cnt BIGINT;
	declare tid BIGINT;
  DECLARE i INT DEFAULT 0;
	select getDocumentScheme() into sch;
	select getDeniedTreeList(userId) into deniedTrees;
	set res = '0';
	set @sql1 = concat('select count(*) into @cnt from ',sch,'.tree_document where doc_id=?');
  set @par  = par;
	PREPARE stmt FROM @sql1;
	EXECUTE stmt USING @par;
	DEALLOCATE PREPARE stmt;
  set cnt = @cnt;
	if(cnt is null or cnt=0)THEN
		set res = '0';
	end if;
	if(cnt>0)then
		set res = '888';
    WHILE(i<cnt and res<>'0')do
  		BEGIN
  	    set @sql2 = concat('select tree_id into @cid2 from ',sch,'.tree_document where doc_id=? ORDER BY tree_id ASC LIMIT ?, 1');
        set @cid2=null;
        SET @iter = i;
  			PREPARE stmt1 FROM @sql2;
  			EXECUTE stmt1 USING @par,@iter;
  			DEALLOCATE PREPARE stmt1;
        set tid=@cid2;
        call isTreeDenied(userId,tid,@pid);
  			if(@pid ='0')THEN
  				set res = '0';
      	end if;
        set i=i+1;
  		END;
  	END WHILE;
	END if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `isTreeDenied` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`192.168.1.89`*/ /*!50003 PROCEDURE `isTreeDenied`(IN userId int,IN par text,OUT res text)
BEGIN DECLARE cr_stack_depth INTEGER DEFAULT cr_debug.ENTER_MODULE('isTreeDenied', 'jurist', 7);
	declare sch text;
	declare deniedTrees text;
	declare cid BIGINT;
	declare pid BIGINT;
	CALL cr_debug.UPDATE_WATCH2('userId', userId, cr_stack_depth);CALL cr_debug.UPDATE_WATCH2('par', par, cr_stack_depth);CALL cr_debug.UPDATE_WATCH2('res', res, cr_stack_depth);CALL cr_debug.UPDATE_WATCH2('sch', sch, cr_stack_depth);CALL cr_debug.UPDATE_WATCH2('deniedTrees', deniedTrees, cr_stack_depth);CALL cr_debug.UPDATE_WATCH2('cid', cid, cr_stack_depth);CALL cr_debug.UPDATE_WATCH2('pid', pid, cr_stack_depth); CALL cr_debug.TRACE(2, 2, 0, 5, cr_stack_depth); CALL cr_debug.TRACE(7, 7, 1, 37, cr_stack_depth);select getDocumentScheme() into sch;CALL cr_debug.UPDATE_SYSTEM_CALLS(101);CALL cr_debug.UPDATE_WATCH2('sch', sch, cr_stack_depth);
	 CALL cr_debug.TRACE(8, 8, 1, 51, cr_stack_depth);select getDeniedTreeList(userId) into deniedTrees;CALL cr_debug.UPDATE_SYSTEM_CALLS(101);CALL cr_debug.UPDATE_WATCH2('deniedTrees', deniedTrees, cr_stack_depth);
	 CALL cr_debug.TRACE(9, 9, 1, 15, cr_stack_depth);set res = '0';CALL cr_debug.UPDATE_WATCH2('res', res, cr_stack_depth);
	 CALL cr_debug.TRACE(10, 10, 1, 122, cr_stack_depth);set @sql1 = concat('select id ,parent_id into @cid, @pid  from ',sch,'.trees where id=? and id not in(',deniedTrees,')');CALL cr_debug.UPDATE_WATCH2('@sql1', @sql1, cr_stack_depth);
   CALL cr_debug.TRACE(11, 11, 2, 18, cr_stack_depth);set @par  = par;CALL cr_debug.UPDATE_WATCH2('@par', @par, cr_stack_depth);
	 CALL cr_debug.TRACE(12, 12, 1, 25, cr_stack_depth);PREPARE stmt FROM @sql1;
	 CALL cr_debug.TRACE(13, 13, 1, 25, cr_stack_depth);EXECUTE stmt USING @par;
	 CALL cr_debug.TRACE(14, 14, 1, 25, cr_stack_depth);DEALLOCATE PREPARE stmt;
   CALL cr_debug.TRACE(15, 15, 2, 17, cr_stack_depth);set cid = @cid;CALL cr_debug.UPDATE_WATCH2('cid', cid, cr_stack_depth);CALL cr_debug.UPDATE_WATCH2('@cid', @cid, cr_stack_depth);
   CALL cr_debug.TRACE(16, 16, 2, 17, cr_stack_depth);set pid = @pid;CALL cr_debug.UPDATE_WATCH2('pid', pid, cr_stack_depth);CALL cr_debug.UPDATE_WATCH2('@pid', @pid, cr_stack_depth);
	 CALL cr_debug.TRACE(17, 19, 1, 8, cr_stack_depth);if(cid is null)THEN
		 CALL cr_debug.TRACE(18, 18, 2, 16, cr_stack_depth);set res = '1';CALL cr_debug.UPDATE_WATCH2('res', res, cr_stack_depth);
	end if;
	 CALL cr_debug.TRACE(20, 35, 1, 11, cr_stack_depth);WHILE pid is not null and pid!=0 do
		 CALL cr_debug.TRACE(21, 34, 2, 6, cr_stack_depth);BEGIN
	     CALL cr_debug.TRACE(22, 22, 5, 128, cr_stack_depth);set @sql2 = concat('select id ,parent_id into @cid2, @pid2  from ',sch,'.trees where id=? and id not in(',deniedTrees,')');CALL cr_debug.UPDATE_WATCH2('@sql2', @sql2, cr_stack_depth);
       CALL cr_debug.TRACE(23, 23, 6, 20, cr_stack_depth);set @pid1=pid;CALL cr_debug.UPDATE_WATCH2('@pid1', @pid1, cr_stack_depth);
       CALL cr_debug.TRACE(24, 24, 6, 21, cr_stack_depth);set @cid2=null;CALL cr_debug.UPDATE_WATCH2('@cid2', @cid2, cr_stack_depth);
       CALL cr_debug.TRACE(25, 25, 6, 21, cr_stack_depth);set @pid2=null;CALL cr_debug.UPDATE_WATCH2('@pid2', @pid2, cr_stack_depth);
			 CALL cr_debug.TRACE(26, 26, 3, 28, cr_stack_depth);PREPARE stmt1 FROM @sql2;
			 CALL cr_debug.TRACE(27, 27, 3, 29, cr_stack_depth);EXECUTE stmt1 USING @pid1;
			 CALL cr_debug.TRACE(28, 28, 3, 28, cr_stack_depth);DEALLOCATE PREPARE stmt1;
       CALL cr_debug.TRACE(29, 29, 6, 22, cr_stack_depth);set cid = @cid2;CALL cr_debug.UPDATE_WATCH2('cid', cid, cr_stack_depth);CALL cr_debug.UPDATE_WATCH2('@cid2', @cid2, cr_stack_depth);
       CALL cr_debug.TRACE(30, 30, 6, 22, cr_stack_depth);set pid = @pid2;CALL cr_debug.UPDATE_WATCH2('pid', pid, cr_stack_depth);CALL cr_debug.UPDATE_WATCH2('@pid2', @pid2, cr_stack_depth);
			 CALL cr_debug.TRACE(31, 33, 3, 10, cr_stack_depth);if(cid is null)THEN
				 CALL cr_debug.TRACE(32, 32, 4, 18, cr_stack_depth);set res = '1';CALL cr_debug.UPDATE_WATCH2('res', res, cr_stack_depth);
			end if;
		 CALL cr_debug.TRACE(34, 34, 2, 5, cr_stack_depth);END;
	END WHILE;
 CALL cr_debug.TRACE(36, 36, 0, 3, cr_stack_depth);END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-12-22 20:35:25
