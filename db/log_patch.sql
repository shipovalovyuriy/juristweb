CREATE  TABLE log_type (
  log_type_id INT NOT NULL AUTO_INCREMENT ,
  log_type VARCHAR(200) NOT NULL ,
  PRIMARY KEY (log_type_id) );

INSERT INTO log_type (log_type_id, log_type) VALUES (1, 'Начало сеанса');
INSERT INTO log_type (log_type_id, log_type) VALUES (2, 'Завершение сеанса');
INSERT INTO log_type (log_type_id, log_type) VALUES (3, 'Просмотр документа');
INSERT INTO log_type (log_type_id, log_type) VALUES (4, 'Закрытие документа');
INSERT INTO log_type (log_type_id, log_type) VALUES (5, 'Поисковый запрос');



CREATE  TABLE action_log (
  action_log_id INT NOT NULL AUTO_INCREMENT ,
  user_id INT NULL ,
  action_type INT NULL ,
  action_date DATETIME NULL ,
  doc_id BIGINT(20) NULL ,
  descr VARCHAR(255) NULL ,
  PRIMARY KEY (action_log_id) ,
  INDEX idx_action_user (user_id ASC) ,
  INDEX idx_action_date (action_date ASC) ,
  INDEX idx_action_type (action_type ASC) ,
  INDEX idx_action_doc (doc_id ASC) );


ALTER TABLE action_log ADD COLUMN `session` VARCHAR(255) NULL  AFTER `descr` ;




USE `jurist`;

DROP procedure IF EXISTS `addActionLog`;



DELIMITER $$

CREATE DEFINER=`test`@`localhost` PROCEDURE `addActionLog`(action text, `user_id` int, descr text, sess text)
BEGIN
	declare action_id int;
	declare doc_id bigint;
  IF(action='login')THEN
    set action_id = 1;
  elseif (action ='logout') then              
    set action_id = 2;
  elseif (action ='search') then              
    set action_id = 5;
  elseif (action ='doc_open') then              
    set action_id = 3;
    set doc_id = cast(descr as unsigned integer);
  elseif (action ='doc_close') then              
    set action_id = 4;
    set doc_id = cast(descr as unsigned integer);
  END IF;                
    
  INSERT INTO action_log(user_id, action_type, action_date, doc_id, descr, session)VALUES(user_id, action_id, now(), doc_id, descr, sess);
END
$$

