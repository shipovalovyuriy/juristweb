CREATE TABLE `updates` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `fileName` varchar(50) NOT NULL,
  `data` longblob,
  `length` int(11) NOT NULL,
  `changed_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `hidden` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE `clients_details` (
  `clients_detail_id` bigint(20) NOT NULL AUTO_INCREMENT ,
  `uuid` varchar(45) DEFAULT NULL,
  `last_update_id` int(11) DEFAULT NULL,
  `client_id` bigint(20) NOT NULL,
  PRIMARY KEY (`clients_detail_id`),
  KEY `fk_client_detail_cl` (`client_id`),
  CONSTRAINT `fk_client_detail_cl` FOREIGN KEY (`client_id`) REFERENCES `clients` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE  TABLE `clients_sys_info` (
  `clients_sys_info_id` BIGINT NOT NULL AUTO_INCREMENT ,
  `computer_name` VARCHAR(255) NOT NULL ,
  `prop_name` VARCHAR(255) NOT NULL ,
  `prop_value` VARCHAR(255) NOT NULL ,
  `allowed` INT NOT NULL DEFAULT 0 ,
  `clients_detail_id` BIGINT NULL ,
  PRIMARY KEY (`clients_sys_info_id`) ,
  INDEX `fk_client_sys_detail` (`clients_detail_id` ASC) ,
  CONSTRAINT `fk_client_sys_detail`
    FOREIGN KEY (`clients_detail_id` )
    REFERENCES `billing`.`clients_details` (`clients_detail_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION);

ALTER TABLE `billing`.`clients_details` ADD COLUMN `user_count` INT NULL DEFAULT 1  AFTER `license_period` ;

    