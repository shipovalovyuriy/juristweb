CREATE DATABASE  IF NOT EXISTS `jurist` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `jurist`;
-- MySQL dump 10.13  Distrib 5.6.11, for Win32 (x86)
--
-- Host: localhost    Database: jurist
-- ------------------------------------------------------
-- Server version	5.6.12-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'jurist'
--
/*!50003 DROP FUNCTION IF EXISTS `changePassword` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`interconsult`@`192.168.1.89` FUNCTION `changePassword`(`userId` int,`par` text) RETURNS text CHARSET utf8
BEGIN
	declare cnt int;
	declare id int;
	declare login1 text;
	declare pwd1 text;
	declare newpwd text;
	set login1= ExtractValue(par,'chg/login');
	set pwd1= ExtractValue(par,'chg/pwd');
	set newpwd= ExtractValue(par,'chg/newpwd');
	select count(*),max(u.user_id) into cnt,id from users u where u.user_type_id=1 and u.login=login1 and u.pwd=PASSWORD(pwd1);
	if(cnt=0)THEN
		return '<res><success>0</success></res>';
	end if;
  update users set pwd=PASSWORD(newpwd) where user_type_id=1 and login=login1;
	return '<res><success>1</success></res>';

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `delUser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`interconsult`@`localhost` FUNCTION `delUser`(userId int,`par` text) RETURNS text CHARSET utf8
    DETERMINISTIC
BEGIN   
	DECLARE xml2 VARCHAR(250) DEFAULT '<obj><count>2</count><parent>2</parent><row><id>0</id><value>2197</value></row><row><id>0</id><value>258</value></row></obj>';
  declare userId int;
	DECLARE tmp text;
	DECLARE tmp1 text;
	DECLARE tmp2 text;
	DECLARE tmp3 text;
	DECLARE tmpValidFrom  text;
	DECLARE tmpValidTill  text;
	DECLARE tmpEmail  text;
	DECLARE tmpName  text;
	DECLARE tmpRnn  text;
	DECLARE tmpCompany  text;    
	DECLARE tmpSerial   text;
	DECLARE res text;
  set tmp = ExtractValue(par,'obj/row/user_id');
  set tmp3= ExtractValue(par,'obj/row/id');
  if(tmp is null or tmp='')then
    set userId = cast(tmp3 as UNSIGNED );
  else 
	  set userId = cast(tmp as UNSIGNED );
  end if;  
	delete from users where user_id = userId;
	select userId into res;
	return res;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getBookmarks` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`interconsult`@`localhost` FUNCTION `getBookmarks`(`user_id` int,`xml1` text) RETURNS text CHARSET utf8
BEGIN
	declare cnt int default 0;  
	declare id int default 0;
	declare val1 VARCHAR(100) CHARSET utf8 default "" ;
	declare res text;
	set res="";
	SELECT count(*) as cnt1,GROUP_CONCAT("{id:'",CAST(b.id as char),"',text:'",name,"'}" SEPARATOR ',') as res1  into cnt, res 
		from bookmark_tree b where b.user_id=user_id and ( b.parent_id=xml1 or (xml1='0' and b.parent_id is null));
  if(cnt=0)THEN
    set res = "";
	end if;
	set res = CONCAT("[", res ,"]");
	RETURN res;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getDeniedTreeList` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`interconsult`@`localhost` FUNCTION `getDeniedTreeList`(userId int) RETURNS text CHARSET utf8
BEGIN
	declare res text DEFAULT '0';
	declare cnt int;
	select count(*) into cnt from user_roles ur where ur.role_id=1 and ur.user_id=userId;
	if(cnt=0)then
/*	  select DISTINCT GROUP_CONCAT(cast(tree_id as char) ) into res   from role_trees rt where  
		  rt.tree_id not in (select tree_id from role_trees rt1, user_roles ur where ur.role_id=rt1.role_id and ur.user_id = userId);
*/
    select DISTINCT GROUP_CONCAT(cast(tree_id as char) ) into res  from role_trees rt where  
		  rt.tree_id not in (select tree_id from role_trees rt1 
          where rt1.role_id in (
            select ur.role_id from user_roles ur where (ur.user_id = userId) 
            union select r.role_id from roles r where r.role_id=5 
            ) 
      );
          

    if(res is null) then
      set res = '0';
    end if;
	end if;
	RETURN res;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getDocumentScheme` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = cp866 */ ;
/*!50003 SET character_set_results = cp866 */ ;
/*!50003 SET collation_connection  = cp866_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`interconsult`@`localhost` FUNCTION `getDocumentScheme`() RETURNS text CHARSET utf8
BEGIN
	
	RETURN "sps_20131120";
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getGrantedTreeList` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`interconsult`@`localhost` FUNCTION `getGrantedTreeList`(userId int) RETURNS text CHARSET utf8
BEGIN
	declare res text DEFAULT '0,1338';
	declare cnt int;
	select count(*) into cnt from user_roles ur where ur.role_id=1 and ur.user_id=userId;
	if(cnt=0)then
	  select DISTINCT GROUP_CONCAT(cast(tree_id as char) ) into res  from role_trees rt
      where rt.role_id in (
            select ur.role_id from user_roles ur where (ur.user_id = userId) 
            union select r.role_id from roles r where r.role_id=5 
            ) ;
     
    if(res is null) then
      set res = '0';
    end if;
	end if;
	RETURN res; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getRoles` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`interconsult`@`localhost` FUNCTION `getRoles`(`user_id` int) RETURNS text CHARSET utf8
BEGIN  
	declare cnt int default 0;  
	declare id int default 0;
	declare val1 VARCHAR(100) CHARSET utf8 default "" ;
	declare res text;
	set res="";
	SELECT count(*) as cnt1,GROUP_CONCAT("<row><id>",CAST(role_id as char),"</id><value>",role,"</value></row>" SEPARATOR '') as res1  into cnt, res from roles;
	if(cnt=-1)THEN
		BEGIN
			DECLARE done INT DEFAULT 0; 
			DECLARE curRoles CURSOR FOR SELECT role_id,role FROM roles;
			DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
			OPEN curRoles;
			REPEAT
					FETCH curRoles INTO id, val1;
					IF done=0 THEN
					 set res = CONCAT(res ,"<row><id>",id,"</id><value>",val1,"</value></row>");
					END IF;
			UNTIL done=0 END REPEAT;
			CLOSE curRoles;
		end;
	end if;
	set res = CONCAT("<dataset><total>",cnt,"</total>", res ,"</dataset>");
	return res;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getRolesTree` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`interconsult`@`localhost` FUNCTION `getRolesTree`(`user_id` int, par text) RETURNS text CHARSET utf8
BEGIN  
	declare cnt int default 0;  
	declare id int default 0;
	declare val1 VARCHAR(100) CHARSET utf8 default "" ;
	declare res text;
	set res="";
	SELECT count(*) as cnt1,GROUP_CONCAT("<row><id>",CAST(role_id as char),"</id><value>",tree_id,"</value></row>" SEPARATOR '') as res1  
				into cnt, res from role_trees where role_id=par;
	if(res is null) then set res =''; end if;
	set res = CONCAT("<dataset><total>",cnt,"</total>", res ,"</dataset>");
	return res;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getUserRoles` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`interconsult`@`localhost` FUNCTION `getUserRoles`(userid int, par text) RETURNS text CHARSET utf8
BEGIN 
	declare cnt int default 0;  
	declare id int default 0;
	declare val1 VARCHAR(100) CHARSET utf8 default "" ;
  DECLARE done INT DEFAULT 0; 
	declare res text;
	set res=" ";
	SELECT count(*) as cnt1,GROUP_CONCAT("<row><id>",CAST(u.role_id as char),"</id><value>",CAST(u.role_id as char),"</value></row>" SEPARATOR '') as res1  
			into cnt, res from user_roles u,roles r 
			where u.role_id=r.role_id and u.user_id=par;
	if(cnt=0) THEN
		set res = "";
	end if;
	set res = CONCAT("<dataset><total>",cnt,"</total>", res ,"</dataset>");
	return res;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getUserTypes` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`interconsult`@`localhost` FUNCTION `getUserTypes`(`user_id` int) RETURNS text CHARSET utf8
BEGIN 
	declare cnt int default 0;  
	declare id int default 0;
	declare val1 VARCHAR(100) CHARSET utf8 default "" ;
  DECLARE done INT DEFAULT 0; 
	declare res text;
	set res="";
	SELECT count(*) as cnt1,GROUP_CONCAT("<row><id>",CAST(user_type_id as char),"</id><value>",user_type,"</value></row>" SEPARATOR '') as res1  
		into cnt, res from user_types;
	set res = CONCAT("<dataset><total>",cnt,"</total>", res ,"</dataset>");
	return res;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `isGrantedTree` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`interconsult`@`localhost` FUNCTION `isGrantedTree`(userId int,treeId int) RETURNS int(11)
BEGIN
	declare res int DEFAULT 1;
  declare txt text;  
	declare cnt int;
	select count(*) into cnt from user_roles ur where ur.role_id=1 and ur.user_id=userId;
	if(cnt=0)then
	  select DISTINCT GROUP_CONCAT(cast(tree_id as char) ) into txt  from role_trees rt
      where rt.tree_id=treeId and rt.role_id in (
            select ur.role_id from user_roles ur where (ur.user_id = userId) 
            union select r.role_id from roles r where r.role_id=5 
            ) ;
     
    if(txt is null) then
      set res = 0;
    end if;
	end if;
	RETURN res; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `setRole` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`interconsult`@`localhost` FUNCTION `setRole`(userId int,`par` text) RETURNS text CHARSET utf8
    DETERMINISTIC
BEGIN   
	DECLARE xml2 VARCHAR(25) DEFAULT '<obj><count>2</count><parent>2</parent><row><id>0</id><value>2197</value></row><row><id>0</id><value>258</value></row></obj>';
  declare roleId int;
	DECLARE tmp text;
	DECLARE res text;
  set tmp = ExtractValue(par,'obj/row/id');
	set roleId = cast(tmp as UNSIGNED );
	SELECT ExtractValue(par,'obj/row/value') into tmp;
	if ((roleId is null )or (roleId=0) or (tmp = '' )) THEN
		insert into roles (role) VALUES(tmp);
		select role_Id into res from roles where role=tmp;
	ELSE
		update roles set role=tmp where role_id=roleId;
		select roleId into res;
	end if;
	return res;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `setRolesTree` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`interconsult`@`localhost` FUNCTION `setRolesTree`(userId int,`par` text) RETURNS text CHARSET utf8
    DETERMINISTIC
BEGIN   
	DECLARE xml2 VARCHAR(25) DEFAULT '<obj><count>2</count><role>2</role><row><id>0</id><value>2197</value></row><row><id>0</id><value>258</value></row></obj>';
  declare cnt int;
  declare roleId int;
  declare i int DEFAULT 0;
	DECLARE tmp text;
	DECLARE res text;
  set tmp = ExtractValue(par,'obj/count');
	set cnt = cast(tmp as UNSIGNED );
  set tmp = ExtractValue(par,'obj/parent');
	set roleId = cast(tmp as UNSIGNED );
	if(roleId is null or roleId=0) THEN
		return 'nothing saved';
  end if;
	delete from role_trees where role_id=roleId;
	WHILE i < cnt DO
		set i=i+1;
		SELECT ExtractValue(par,'obj/row[$i]/value') into tmp;
		insert into role_trees VALUES(tmp,roleId);
 END WHILE;
	select i into res;
	return res;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `setUser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`interconsult`@`localhost` FUNCTION `setUser`(userId int,`par` text) RETURNS text CHARSET utf8
    DETERMINISTIC
BEGIN   
	DECLARE xml2 VARCHAR(250) DEFAULT '<obj><count>2</count><parent>2</parent><row><id>0</id><value>2197</value></row><row><id>0</id><value>258</value></row></obj>';
  declare userId int;
	DECLARE tmp text;
	DECLARE tmp1 text;
	DECLARE tmp2 text;
	DECLARE tmp3 text;
	DECLARE tmpValidFrom  text;
	DECLARE tmpValidTill  text;
	DECLARE tmpEmail  text;
	DECLARE tmpName  text;
	DECLARE tmpRnn  text;
	DECLARE tmpCompany  text;    
	DECLARE tmpSerial   text;
	DECLARE res text;
  set tmp = ExtractValue(par,'obj/row/user_id');
  set tmp3= ExtractValue(par,'obj/row/id');
  if(tmp is null or tmp='')then
    set userId = cast(tmp3 as UNSIGNED );
  else 
	  set userId = cast(tmp as UNSIGNED );
  end if;  
	SELECT ExtractValue(par,'obj/row/login') into tmp;
	SELECT ExtractValue(par,'obj/row/pass') into tmp1;
	SELECT ExtractValue(par,'obj/row/user_type_id') into tmp2;
	SELECT ExtractValue(par,'obj/row/valid_from') into tmpValidFrom;
	SELECT ExtractValue(par,'obj/row/valid_till') into tmpValidTill;
	SELECT ExtractValue(par,'obj/row/email') into tmpEmail;
	SELECT ExtractValue(par,'obj/row/name') into tmpName;
	SELECT ExtractValue(par,'obj/row/rnn') into tmpRnn;
	SELECT ExtractValue(par,'obj/row/company') into tmpCompany;
	SELECT ExtractValue(par,'obj/row/serial') into tmpSerial;
    
	if(tmp2 is null  or tmp2='') THEN
		set tmp2 = '1';
	end if;
	if(tmpValidFrom='' or tmpValidFrom='0000-00-00') THEN
		set tmpValidFrom = CURDATE();
	end if;
	if(tmpValidTill='' or tmpValidTill='0000-00-00') THEN
		set tmpValidTill = CURDATE();
	end if;

	if ((userId is null )or (userId=0)) THEN
		insert into users (login,pwd,user_type_id, valid_from, valid_till,email,name, rnn, company, serial) 
        VALUES(tmp,tmp1,tmp2,tmpValidFrom, tmpValidTill,tmpEmail,tmpName,tmpRnn,tmpCompany,tmpSerial);
		select user_Id into userId from users where login=tmp and user_type_id=tmp2;
    -- "All docs" role 
    insert into user_roles( user_id, role_id) values(userId, 7);
	ELSE
		update users set login=tmp, valid_from=tmpValidFrom, 
           valid_till=tmpValidTill, email=tmpEmail, serial=tmpSerial,
           name=tmpName, rnn = tmpRnn, company=tmpCompany
    where user_id=userId;
	end if;
	if(tmp1 is null or tmp1='')then
     set tmp1 ='';
  else   
		update users set pwd=PASSWORD(tmp1) where user_id=userId;
	end if;
	select userId into res;
	return res;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `setUserRoles` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`interconsult`@`localhost` FUNCTION `setUserRoles`(userId int,`par` text) RETURNS text CHARSET utf8
    DETERMINISTIC
BEGIN   
	DECLARE xml2 VARCHAR(25) DEFAULT '<obj><count>2</count><role>2</role><row><id>0</id><value>2197</value></row><row><id>0</id><value>258</value></row></obj>';
  declare cnt int;
  declare userId int;
  declare i int DEFAULT 0;
	DECLARE tmp text;
	DECLARE res text;
  set tmp = ExtractValue(par,'obj/count');
	set cnt = cast(tmp as UNSIGNED );
  set tmp = ExtractValue(par,'obj/parent');
	set userId = cast(tmp as UNSIGNED );
	if(userId is null or userId=0) THEN
		return 'nothing saved';
  end if;
	delete from user_roles where user_id=userId;
	WHILE i < cnt DO
		set i=i+1;
		SELECT ExtractValue(par,'obj/row[$i]/value') into tmp;
		insert into user_roles(user_id,role_id) VALUES(userId,tmp);
 END WHILE;
	select i into res;
	return res;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `testXml` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`interconsult`@`localhost` FUNCTION `testXml`(`xml1` varchar(100)) RETURNS varchar(255) CHARSET utf8
    DETERMINISTIC
BEGIN
	DECLARE xml2 VARCHAR(25) DEFAULT '<a>    <b c="1"><d>X</d></b>    <b c="2"><d>X</d></b>    </a>';
	DECLARE res text;
	set res = ExtractValue(xml1,'a/b/d[../@c="1"]');
	SET res = '1';
	set res = getRoles(0);
/*
 WHILE i < 4 DO
    SELECT xml, i, ExtractValue(xml, '//a[$i]');
    SET i = i+1;
 END WHILE;
*/
	return res;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `addActionLog` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`interconsult`@`localhost` PROCEDURE `addActionLog`(action text, `user_id` int, descr text, sess text)
BEGIN
	declare action_id int;
	declare doc_id bigint;
  IF(action='login')THEN
    set action_id = 1;
  elseif (action ='logout') then              
    set action_id = 2;
  elseif (action ='search') then              
    set action_id = 5;
  elseif (action ='doc_open') then              
    set action_id = 3;
    set doc_id = cast(descr as unsigned integer);
  elseif (action ='doc_close') then              
    set action_id = 4;
    set doc_id = cast(descr as unsigned integer);
  END IF;                
    
  INSERT INTO action_log(user_id, action_type, action_date, doc_id, descr, session)VALUES(user_id, action_id, now(), doc_id, descr, sess);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `addBookmark` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`interconsult`@`localhost` PROCEDURE `addBookmark`(`user_id` int,`xml1` text,INOUT `res` text)
BEGIN
  if xml1 ='0' THEN
   set xml1 = null;
  end if;
	insert into bookmark_tree(name,parent_id, user_id) values ('Новая папка',xml1,user_id);
	set res  = "1";
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `bookmarkDocument` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`interconsult`@`localhost` PROCEDURE `bookmarkDocument`(`user_id` int,`par` text,INOUT `res` text)
BEGIN
	DECLARE tmp text;
	DECLARE tmp1 text;
	DECLARE tmp2 text;
	DECLARE tmp3 text;
  set tmp1 = ExtractValue(par,'obj/id');
  set tmp3 = ExtractValue(par,'obj/docid');
  if(tmp3 = '0')THEN
   set tmp3 = null;
  end if;
  if(tmp1 = '0')THEN
   set tmp1 = null;
  end if;
 if(tmp1 is not null and tmp3 is not null) then begin
	 INSERT into bookmark_docs (tree_id, doc_id) VALUES(tmp1, tmp3);
	 set res = '1';
  end;
 else 
   set res = 'Возможно вы не авторизованы или не выбрали папку';
 end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delBookmark` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`interconsult`@`localhost` PROCEDURE `delBookmark`(`user_id` int,`par` text,INOUT `res` text)
BEGIN
	DECLARE tmp text;
	DECLARE tmp1 text;
	DECLARE tmp2 text;
	DECLARE tmp3 text;
  set tmp1 = par;
  delete from bookmark_tree where id = par;
  set res = '1';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `doLogin` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`interconsult`@`localhost` PROCEDURE `doLogin`(IN `login` text,IN `pwd` text,INOUT `userId` int)
BEGIN 
	declare cnt int;
	declare id int;
	select count(*),max(u.user_id) into cnt,id from users u where u.user_type_id=1 and u.login=login and u.pwd=PASSWORD(pwd) and now() between u.valid_from and u.valid_till;
	if(cnt>0)THEN
		set userId = id;
	end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `editBookmark` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`interconsult`@`localhost` PROCEDURE `editBookmark`(`user_id` int,`par` text,INOUT `res` text)
BEGIN
	DECLARE tmp text;
	DECLARE tmp1 text;
	DECLARE tmp2 text;
	DECLARE tmp3 text;
  set tmp1 = ExtractValue(par,'obj/id');
  set tmp2 = ExtractValue(par,'obj/name');
  set tmp3 = ExtractValue(par,'obj/parent');
  if(tmp3 = '0')THEN
   set tmp3 = null;
  end if;
  update bookmark_tree b set b.parent_id=tmp3 , 	b.name=tmp2 where b.id=tmp1;
  set res = '1';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `execute_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`interconsult`@`localhost` PROCEDURE `execute_proc`(IN user_id int, IN reqType text, IN par text, INOUT res text)
BEGIN
		if (reqType="getRoles") THEN
			set res = getRoles(user_id);
		elseif (reqType = "getUserTypes") then
			set res = getUserTypes(user_id);
		elseif (reqType = "getUserRoles") then
			set res = getUserRoles(user_id,par);
		elseif (reqType = "getUsers") then
			call getUsers(user_id,par, res);
		elseif (reqType = "changePassword") then
			set res = changePassword(user_id,par);
		elseif (reqType = "getRolesTree") then
			set res = getRolesTree(user_id,par);
		elseif (reqType = "setRolesTree") then
			set res = setRolesTree(user_id,par);
		elseif (reqType = "setRole") then
			set res = setRole(user_id,par);
		elseif (reqType = "setUserRoles") then
			set res = setUserRoles(user_id,par);
		elseif (reqType = "setUser") then
			set res = setUser(user_id,par);
		elseif (reqType = "delUser") then
			set res = delUser(user_id,par);
		elseif (reqType = "isTreeDenied") then
			call isTreeDenied(user_id, par, res);
		elseif (reqType = "getGrantedTreeList") then
			set res = getGrantedTreeList(user_id);
		elseif (reqType = "isDocumentDenied") then
			call isDocumentDenied(user_id, par, res);
		elseif (reqType = "addBookmark") then
			call addBookmark(user_id, par, res);
		elseif (reqType = "getBookmarks") then
			set res = getBookmarks(user_id, par);
		elseif (reqType = "editBookmark") then
			call editBookmark(user_id, par,res );
		elseif (reqType = "delBookmark") then
			call delBookmark(user_id, par,res );
		elseif (reqType = "bookmarkDocument") then
			call bookmarkDocument(user_id, par, res );
		elseif (reqType = "getDefaultPage") then
			call getDefaultPage(user_id, res );
    else  
        set res = concat('unknown request ',reqType);
		end if;
		if(res is null)then
			set res='';
		end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getDefaultPage` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`interconsult`@`localhost` PROCEDURE `getDefaultPage`(IN userId int, OUT `aBody` text)
BEGIN
  declare sch text;
	select getDocumentScheme() into sch;
	/*840065639*/
	set @sql1 = concat('select d.body into @aBody from  ',sch,'.document_body d where d.docid=840065639');
  /*select d.body into aBody from sps.document_body d where d.docid=840065639;*/
	PREPARE stmt FROM @sql1;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
  set aBody = @aBody;
  -- returning just ID
  set aBody = '840065639';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getUsers` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`interconsult`@`localhost` PROCEDURE `getUsers`(`userId` int, par text, out res longtext)
BEGIN
	declare cnt int default 0;  
	declare fcnt int default 0;  
	declare id int default 0;
	declare i int default 0;
	declare val1 VARCHAR(100) CHARSET utf8 default "" ;
	declare pname VARCHAR(100) CHARSET utf8 default "" ;
	declare pcmp VARCHAR(100) CHARSET utf8 default "" ;
	declare cmp VARCHAR(100) CHARSET utf8 default "" ;
	declare ptype VARCHAR(100) CHARSET utf8 default "" ;
	declare pval VARCHAR(100) CHARSET utf8 default "" ;
	declare sort1 VARCHAR(100) CHARSET utf8 default " login " ;
	declare dir1 VARCHAR(100) CHARSET utf8 default " asc " ;
	declare orderBy VARCHAR(100) CHARSET utf8 default " " ;
	declare where1 text CHARSET utf8 default " where user_type_id is not null " ;
	SELECT ExtractValue(par,'filter/start') into @start1;
	SELECT ExtractValue(par,'filter/limit') into @limit1;
	SELECT ExtractValue(par,'filter/cnt') into fcnt;
	SELECT ExtractValue(par,'filter/sort') into sort1;
	SELECT ExtractValue(par,'filter/dir') into dir1;
  IF(sort1='pass')THEN
				set sort1 = 'pwd';
  elseif (sort1 is null) then              
				set sort1 = 'login';
  END IF;                
	set orderBy = CONCAT(" ORDER BY ",sort1," ",dir1);
  SET SESSION group_concat_max_len = 20000;
	while i<fcnt DO
		BEGIN
			set i=i+1;
			SELECT ExtractValue(par,'filter/p[$i]/@name') into pname;
			SELECT ExtractValue(par,'filter/p[$i]/@cmp') into pcmp;
			SELECT ExtractValue(par,'filter/p[$i]') into pval;
			SELECT ExtractValue(par,'filter/p[$i]/@type') into ptype;
			set  cmp = ' = ';
			IF(pcmp='lt')THEN
				set cmp = ' < ';
			ELSEIF (pcmp='gt')THEN
				set cmp = ' > ';
			ELSEIF (pcmp='like')THEN
				set cmp = ' like ';
			END IF;
			if(ptype='string' or ptype='date')THEN
				set pval = CONCAT('''',REPLACE(pval,'''',''),'''');
			end if;
			set where1 = CONCAT(where1,' and ',pname, cmp, pval);
		END;
	END WHILE;
	set res="";
	set @sql1 = concat('SELECT GROUP_CONCAT("<row><u>",/*CAST(*/user_id /*as char)*/,
							"</u><l>",login,"</l><ut>",
							user_type_id,"</ut><vf>",
							IFNULL(valid_from,""),"</vf><vt>",
							IFNULL(valid_till,""),"</vt><e>",
							IFNULL(email,""),"</e><n>",
							IFNULL(name, ""),"</n><r>",
							IFNULL(rnn, ""),"</r><c>",
							IFNULL(company, ""),"</c><s>",
							IFNULL(serial, ""),"</s></row>" SEPARATOR "") as res1  ,
				(select count(*) from users',where1,' )as cnt1 into @res,@cnt from(select t.* from users t',where1,orderBy,' limit ?,? ) k');
	PREPARE stmt FROM @sql1;
	EXECUTE stmt USING @start1,@limit1;
	DEALLOCATE PREPARE stmt;
	set res = @res;
	set cnt = @cnt;
	set res = CONCAT("<dataset><total>",cnt,"</total>", res ,"</dataset>");
	/*return res;*/
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `isDocumentDenied` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`interconsult`@`localhost` PROCEDURE `isDocumentDenied`(IN userId int,IN par text,OUT res text)
BEGIN
	declare sch text;
	declare deniedTrees text;
	declare cnt BIGINT;
	declare grantedCnt BIGINT;
	declare tid BIGINT default 0;
  declare treeRes text;
  DECLARE i INT DEFAULT 0;
	select getDocumentScheme() into sch;
	select getDeniedTreeList(userId) into deniedTrees;
	set res = '0';
	set @sql1 = concat('select count(*) into @cnt from ',sch,'.tree_document where doc_id=?');
  set @par  = par;
	PREPARE stmt FROM @sql1;
	EXECUTE stmt USING @par;
  set cnt = @cnt;
	DEALLOCATE PREPARE stmt;
	set @sql1 = concat('select count(*) into @cnt from ',sch,'.tree_document where doc_id=?  and 
     tree_id in (select tree_id from role_trees rt1 
          where rt1.role_id in (
            select ur.role_id from user_roles ur where (ur.user_id = ?) 
            union select r.role_id from roles r where r.role_id=5 
            ) 
      )');
  set @par  = par;
  set @userId  = userId;
	PREPARE stmt FROM @sql1;
	EXECUTE stmt USING @par,@userId;
  set grantedCnt= @cnt;
	DEALLOCATE PREPARE stmt;
	if(cnt is null or cnt=0 or grantedCnt > 0)THEN
		set res = '0';
  else
    set res = '1';
	end if;
	if(cnt>0 and res='1')then
		set res = '840070852';
    set i=0;
    set tid=0;
    WHILE(i<cnt and res='840070852')do
  		BEGIN
  	    set @sql2 = concat(
            'select tree_id into @cid2  from ',
            sch,
            '.tree_document where doc_id=? and tree_id > ? order by tree_id  LIMIT 0, 1');
        set @cid2=null;
        SET @iter = tid;
  			PREPARE stmt1 FROM @sql2;
  			EXECUTE stmt1 USING @par,@iter;
        set tid=@cid2;
  			DEALLOCATE PREPARE stmt1;
        set treeRes = '';
        call isTreeDenied(userId,tid,treeRes);
  			if(treeRes ='0' )THEN
  				set res = '0';
      	end if;
        set i=i+1;
  		END;
  	END WHILE;
	END if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `isTreeDenied` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`interconsult`@`localhost` PROCEDURE `isTreeDenied`(IN userId int,IN par text,OUT res text)
BEGIN
	declare sch text;
	declare deniedTrees text;
	declare grantedTree int;
	declare cid BIGINT;
	declare pid BIGINT;
	select getDocumentScheme() into sch;
	select getDeniedTreeList(userId) into deniedTrees;
	select isGrantedTree(userId,par) into grantedTree;
	set res = '0';
	set @sql1 = concat('select id ,parent_id into @cid, @pid  from ',sch,'.trees where id=? and id not in(',deniedTrees,')');
  set @par  = par;
	PREPARE stmt FROM @sql1;
	EXECUTE stmt USING @par;
	DEALLOCATE PREPARE stmt;
  set cid = @cid;
  set pid = @pid;
	if(cid is null)THEN
		set res = '1';
	end if;
	WHILE pid is not null and pid!=0 do
		BEGIN
	    set @sql2 = concat('select id ,parent_id into @cid2, @pid2  from ',sch,'.trees where id=? and id not in(',deniedTrees,')');
      set @pid1=pid;
      set @cid2=null;
      set @pid2=null;
			PREPARE stmt1 FROM @sql2;
			EXECUTE stmt1 USING @pid1;
			DEALLOCATE PREPARE stmt1;
      set cid = @cid2;
      set pid = @pid2;
			if(cid is null)THEN
				set res = '1';
			end if;
		END;
	END WHILE;
  if(grantedTree=1) then
   set res='0';
  end if;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-11-20  9:56:23
