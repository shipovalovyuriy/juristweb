CREATE DATABASE  IF NOT EXISTS `jurist` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `jurist`;
-- MySQL dump 10.13  Distrib 5.6.11, for Win32 (x86)
--
-- Host: localhost    Database: jurist
-- ------------------------------------------------------
-- Server version	5.6.12-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bookmark_tree`
--

DROP TABLE IF EXISTS `bookmark_tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookmark_tree` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `user_id` int(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `fk_bk_users_id` (`user_id`),
  KEY `fk_bk_parent_id` (`parent_id`),
  CONSTRAINT `fk_bk_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `bookmark_tree` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_bk_users_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COMMENT='дерево закладок ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bookmark_tree`
--

LOCK TABLES `bookmark_tree` WRITE;
/*!40000 ALTER TABLE `bookmark_tree` DISABLE KEYS */;
INSERT INTO `bookmark_tree` VALUES (1,'Постановления',NULL,1),(2,'Прочие 1',NULL,1),(3,'',NULL,80),(4,'фыв',NULL,80),(6,'моя закладка',NULL,81),(8,'12',NULL,80),(9,'Просто Новая папка',NULL,1),(10,'Тест',NULL,61),(11,'1',10,61),(14,'Новая папка',10,61),(15,'Новая папка',NULL,688),(16,'11',NULL,1408),(17,'232',16,1408),(18,'Новая папка',NULL,1408),(19,'Новая папка',NULL,1408),(20,'Новая папка',NULL,1067);
/*!40000 ALTER TABLE `bookmark_tree` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-11-20  9:56:22
