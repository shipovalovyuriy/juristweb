package kz.novelty.jurist;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import sun.misc.BASE64Decoder;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.security.PublicKey;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UpdateReBuilder extends BaseUpdateBuilder {
    DesUtil des = new DesUtil();
    CertUtil certUtil = new CertUtil();
    // Current user UUID
    String currentUUID = "";
    long currentUpdateID = 0;
    long previousUpdateID = 0;

    @Override
    public BaseDesUtil getDes() {
        return des;
    }

    @Override
    protected BaseCertUtil getCertUtil() {
        return certUtil;
    }

    @Override
    protected void putMagic(DataOutputStream dsEncoded, int magicMode) throws IOException {
        if (0 == magicMode) {
            super.putMagic(dsEncoded, magicMode);
        }
    }

    @Override
    protected byte[] getLicense() {
        byte[] license = null;
        try {
            JSONObject res = (JSONObject) ConnectorUpdate.Instance().executeQuery("SELECT clients_detail_id, valid_from, valid_till, user_count FROM clients_details where uuid=?", new Object[]{currentUUID}, new ConnectorUpdate.QueryHandler() {
                public Object onQuery(ResultSet rs, Object... params) throws SQLException {
                    if (rs.next()) {
                        JSONObject obj = new JSONObject();
                        obj.put("id", rs.getLong(1));
                        obj.put("valid_from", rs.getString(2));
                        obj.put("valid_till", rs.getString(3));
                        obj.put("user_count", rs.getLong(4));
                        obj.put("update_id", currentUpdateID);
                        obj.put("previous_id", previousUpdateID);
                        return obj;
                    }
                    return null;
                }
            });
            final JSONArray allowed = new JSONArray();
            ConnectorUpdate.Instance().executeQuery("SELECT prop_name, prop_value FROM clients_sys_info where allowed = 1 and  clients_detail_id = ?", new Object[]{res.getLong("id")}, new DBConnector.QueryHandler() {
                public Object onQuery(ResultSet rs, Object... params) throws SQLException {
                    while (rs.next()) {
                        JSONObject obj = new JSONObject();
                        obj.put("name", rs.getString(1));
                        obj.put("value", rs.getString(2));
                        allowed.add(obj);
                    }
                    return null;
                }
            });
            res.put("allowed", allowed);
            byte[] licenseData = res.toString().getBytes("utf-8");
            byte[] zipped = LZMAHelper.compress(new ByteArrayInputStream(licenseData));
            // add sign and license
            byte[] signed = sign(zipped, null);
            // crypt whole packet
            byte[] crypted = crypt(signed, 1);
            license = crypted;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return license;
    }

    public String putUpdate(byte[] data, int size, final String name) {
        final StringBuilder log = new StringBuilder();
        try {
            ByteArrayInputStream is = new ByteArrayInputStream(data, 0, size);
            DataInputStream ds = new DataInputStream(is);
            // check headers
            if (ds.readInt() != 1229) {
                log.append("Неверный тип обновления\n");
            }
            if (ds.readInt() != 4) {
                log.append("Неверный тип обновления\n");
            }
            byte[] dec = des.decrypt(data, 8, size - 8);
            int[] sizeInfo = checkSign(dec);
            final int unsignedLength = sizeInfo[0];
            final ByteArrayInputStream uis = new ByteArrayInputStream(dec, 0, unsignedLength);
            ConnectorUpdate.Instance().executeCall("INSERT INTO updates(fileName,data,length)VALUES(?,?,?)", new DBConnector.CallHandler() {
                public void onCall(CallableStatement st, Object... params) throws SQLException {
                    st.setString(1, name);
                    st.setBinaryStream(2, uis, unsignedLength);
                    st.setInt(3, unsignedLength);
                    int rs = st.executeUpdate();
                    log.append(name).append(" update saved ").append(rs);
                }
            });
        } catch (Exception e) {
            log.append("\nerror ").append(e.getMessage());
            e.printStackTrace();
        }
        return log.toString();
    }


    public String processUserLicense(String codedInfo, String remoteHost) {
        try {
            byte[] coded = getCertUtil().byteFromBase64(codedInfo);
            byte[] zipped = des.decrypt(coded, 0, coded.length);
            String json = new String(LZMAHelper.decompress(zipped), "UTF-8");
            JSONObject obj = (JSONObject) JSONSerializer.toJSON(json);
            saveUserLicense(obj, remoteHost);
            return obj.getString("uuid");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public long getUserLicenseID(String uuid) {
        try {
            Long id = (Long) ConnectorUpdate.Instance().executeQuery("select clients_detail_id from clients_details where uuid = ?", new Object[]{uuid}, new DBConnector.QueryHandler() {
                public Object onQuery(ResultSet rs, Object... params) throws SQLException {
                    if (rs.next()) {
                        return rs.getLong(1);
                    }
                    return Long.valueOf(0);
                }
            });
            return id;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void saveUserLicense(JSONObject obj, final String remoteHost) {
        final JSONArray arr = obj.getJSONArray("info");
        final JSONArray logArr = obj.optJSONArray("log");
        currentUUID = obj.getString("uuid");
        final long lastInstalledPatch = obj.getLong("last_id");
        final long id = getUserLicenseID(currentUUID);
        final String computerNamePrefix = "win.CSName=";
        ConnectorUpdate.Instance().executeBatchIgnoreException(new DBConnector.BatchCallHandler() {
            @Override
            public void OnBatch() throws SQLException {
                String computerName = "";
                int computerNameIndex = -1;
                // найдем имя компьютера
                for (int i = 0; i < arr.size(); i++) {
                    String el = arr.getString(i);
                    if (el.startsWith(computerNamePrefix)) {
                        computerName = el.substring(computerNamePrefix.length());
                        computerNameIndex = i;
                        break;
                    }
                }
                final String finalComputerName = computerName;
                for (int i = 0, len=arr.size(); i < len; i++) {
                    // имя компьютера сохраняется во всех свойствах
                    if (i == computerNameIndex) continue;
                    String el = arr.getString(i);
                    final String[] prop = el.split("=");
                        executeCallIgnoreException("INSERT IGNORE INTO clients_sys_info " +
                                "(clients_detail_id, computer_name, prop_name, prop_value,remote_host) VALUES " +
                                "(?, ?, ?, ?, ?)", new DBConnector.CallHandler() {
                            public void onCall(CallableStatement st, Object... params) throws SQLException {
                                st.setLong(1, id);
                                st.setString(2, finalComputerName);
                                st.setString(3, prop[0]);
                                st.setString(4, prop[1]);
                                st.setString(5, remoteHost);
                                st.execute();
                            }
                        });
                }
                // активируем сроки лицензию если еще не инициализированы
                executeCallIgnoreException("update clients_details \n" +
                        "SET  valid_from = now(), \n" +
                        "     valid_till = DATE_ADD(now(), INTERVAL license_period MONTH) \n" +
                        "WHERE valid_from is null \n" +
                        "  and valid_till is null \n" +
                        "  and clients_detail_id = ?\n", new DBConnector.CallHandler() {
                    public void onCall(CallableStatement st, Object... params) throws SQLException {
                        st.setLong(1, id);
                        st.execute();
                    }
                });
                executeCallIgnoreException("update clients_details \n" +
                        "SET  last_update_id = ? \n" +
                        "WHERE clients_detail_id = ?\n", new DBConnector.CallHandler() {
                    public void onCall(CallableStatement st, Object... params) throws SQLException {
                        st.setLong(1, lastInstalledPatch);
                        st.setLong(2, id);
                        st.execute();
                    }
                });
                Integer allowed = (Integer) executeQuery("select max(allowed)as allowed from clients_sys_info where clients_detail_id = ?", new Object[]{id}, new DBConnector.QueryHandler() {
                    public Object onQuery(ResultSet rs, Object... params) throws SQLException {
                        if (rs.next()) {
                            return rs.getInt(1);
                        }
                        return null;
                    }
                });
                // если пришли данные для новой лицензии
                if(allowed == 0) {
                    executeCall("update clients_sys_info set allowed=1 where clients_detail_id = ? and ( prop_name = 'win.InstallDate' or prop_name = 'mb.SerialNumber')", new DBConnector.CallHandler() {
                        public void onCall(CallableStatement st, Object... params) throws SQLException {
                            st.setLong(1, id);
                            st.execute();
                        }
                    });
                }
                // пишем пользовательские события
                if(logArr != null && logArr.size() > 0){
                    for (int i = 0, len = logArr.size(); i < len; i++) {
                        final JSONObject obj = logArr.getJSONObject(i);
                        executeCallIgnoreException("INSERT IGNORE INTO action_log " +
                                "(user_id, action_type, action_date, doc_id, descr, session) VALUES " +
                                "(?, ?, ?, ?, ?, ?)", new DBConnector.CallHandler() {
                            public void onCall(CallableStatement st, Object... params) throws SQLException {
                                st.setLong(1, id);
                                st.setString(2, obj.optString("type"));
                                st.setString(3, obj.optString("dt"));
                                st.setString(4, obj.optString("doc_id"));
                                st.setString(5, obj.optString("descr"));
                                st.setString(6, obj.optString("s"));
                                st.execute();
                            }
                        });
                    }
                }
            }
        });
    }

    public byte[] prepareUpdate(String uuid, Long update_id, byte[] data, Long prev_id) throws Exception {
        currentUUID = uuid;
        currentUpdateID = update_id;
        previousUpdateID = prev_id;
        byte[] zipped = LZMAHelper.compress(new ByteArrayInputStream(data));
        // add sign and license
        byte[] signed = sign(zipped);
        // crypt whole packet
        byte[] crypted = crypt(signed);

        return crypted;
    }
}
