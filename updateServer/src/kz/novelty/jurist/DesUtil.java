package kz.novelty.jurist;

import java.security.MessageDigest;

/**
 * Created with IntelliJ IDEA.
 * User: Malik.N
 * Date: 20.03.13
 * Time: 14:45
 * To change this template use File | Settings | File Templates.
 */
public class DesUtil extends BaseDesUtil {
    @Override
    protected byte[] getSalt() {
        // 8-byte Salt
        byte[] salt = {
                (byte) 0xA9, (byte) 0x9B, (byte) 0xC8, (byte) 0x32,
                (byte) 0x56, (byte) 0x35, (byte) 0xE3, (byte) 0x03
        };
        return salt;
    }

    @Override
    protected byte[] CreateKeyBytes() {
        byte[] key = new byte[7];
        try {
            MessageDigest md;
            md = MessageDigest.getInstance("SHA-1");
            byte[] sha1hash = new byte[40];
            String str = ("" + Math.E).substring(0, 12);
            md.update(str.getBytes(), 0, str.length());
            sha1hash = md.digest();
            key[0] = sha1hash[1];
            key[1] = sha1hash[5];
            key[2] = sha1hash[3];
            key[3] = sha1hash[18];
            key[4] = sha1hash[13];
            key[5] = sha1hash[11];
            key[6] = sha1hash[2];
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return key;
    }
}
