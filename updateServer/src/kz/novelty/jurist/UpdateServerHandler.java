package kz.novelty.jurist;


import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;
import java.net.URLEncoder;
import java.sql.ResultSet;
import java.sql.SQLException;
@WebServlet(name = "UpdateServerHandler", urlPatterns = {"/UpdateServerHandler"})
public class UpdateServerHandler extends HttpServlerHandler<UpdUser> {
    public UpdateServerHandler() {
        handlers.put("activate", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
                process_activate(req, resp, out);
            }
        });
        handlers.put("getList", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
                process_getList(req, resp, out);
            }
        });
        handlers.put("download", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
                try {
                    process_download(req, resp, out);
                } catch (Exception e) {
                    throw new ServletException(e);
                }
            }
        });
        handlers.put("upload", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
                process_upload(req, resp, out);
            }
        });
        handlers.put("login", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
                process_login(req, resp, out);
            }
        });
        handlers.put("logout", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
                setUserInfo(req, resp, null);
            }
        });
        handlers.put("getUser", new handler() {
            public void process(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws ServletException, IOException, SQLException {
                UpdUser user = getUserInfo(req, resp);
                if(user != null && ! "".equals(user.login)) {
                    print(out, user.login);
                }
            }
        });

    }

    private void process_login(final HttpServletRequest req, final HttpServletResponse resp, OutputStream out) throws IOException, ServletException, SQLException {
        final String pass = getParameter(req, "pass");
        final String username = getParameter(req, "username");
        Long id = (Long) ConnectorUpdate.Instance().executeQuery("select ID,password from users where login=?",new Object[]{username}, new DBConnector.QueryHandler() {
            public Object onQuery(ResultSet rs, Object... params) throws SQLException {
                if(rs.next()){
                    String password = rs.getString(2);
                    if (!BCrypt.checkpw(pass, password)) {
                        UpdUser user = new UpdUser();
                        user.login = username;
                        setUserInfo(req, resp, user);
                        return rs.getLong(1);
                    }
                }
                return Long.valueOf(0);
            }
        });
        resp.setContentType("text/plain");
        print(out, "" + id);
    }

    private String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(
                        content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }
    private void process_upload(HttpServletRequest req, HttpServletResponse resp, OutputStream out) throws IOException, ServletException {
        UpdUser user = getUserInfo(req, resp);
        if(user==null || user.login==null || "".equals(user.login)){
            print(out, "access denied");
            return;
        }
        // Create path components to save the file
        final Part filePart = req.getPart("file");
        final String fileName = getFileName(filePart);
        ByteArrayOutputStream stream = new TinyByteArrayOutStream();
        InputStream filecontent = filePart.getInputStream();
        CertUtil.copyStream(filecontent, stream);
        if (stream != null) {
            stream.close();
        }
        UpdateReBuilder builder = new UpdateReBuilder();
        String log = builder.putUpdate(stream.toByteArray(), stream.size(), fileName);
        print(out, log);
        if (filecontent != null) {
            filecontent.close();
        }
        //req.setAttribute("servlet_result",log.toString());
        //RequestDispatcher dispatcher = req.getRequestDispatcher("index.jsp");
        //dispatcher.forward( req, resp);
    }

    private void process_download(HttpServletRequest req, HttpServletResponse resp, final OutputStream out) throws Exception {
        final String userAgent = req.getHeader("USER-AGENT").toLowerCase();
        String uuid = getParameter(req, "uuid");
        final String id = getParameter(req, "id");
        UpdUser user = getUserInfo(req, resp);
        if(user!=null && !"".equals(user.uuid)){
            uuid = user.uuid;
        }
        final String denied = "access denied";
        if(null == uuid || "".equals(uuid)){
            print(out, denied);
            return;
        }
        Object[] res = (Object[]) ConnectorUpdate.Instance().executeQuery("SELECT id,fileName,data,(select max(id) from updates u1 where hidden = 0 and u1.id<u.id ) as prev_id\n" +
                "FROM updates u\n" +
                "where id     = ?\n" +
                "  and hidden <= ifnull( (select view_all_updates from clients_details c where c.uuid = ?) ,0)",new Object[]{id, uuid}, new DBConnector.QueryHandler() {
            public Object onQuery(ResultSet rs, Object... params) throws SQLException {
                if(!rs.next()){
                    return null;
                }
                Long id = rs.getLong(1);
                String fileName = rs.getString(2);
                byte[] data = rs.getBytes(3);
                Long prev_id = rs.getLong(4);
                return new Object[]{id,fileName, data, prev_id};
            }
        });
        if(null == res){
            print(out, denied);
            return;
        }
        Long update_id = (Long) res[0];
        String name = (String) res[1];
        byte[] data = (byte[]) res[2];
        Long prev_id = (Long) res[3];
        UpdateReBuilder builder = new UpdateReBuilder();
        byte[] buff = builder.prepareUpdate(uuid, update_id, data, prev_id);
        resp.setContentType("application/octet-stream");
        String fileName = URLEncoder.encode(name, "UTF-8");
        if (userAgent != null && (userAgent.contains("chrome") || userAgent.contains("msie"))) {
            resp.setHeader("Content-Disposition", "attachment; filename=" + fileName);
        } else {
            resp.setHeader("Content-Disposition", "attachment; filename*=utf-8''" + fileName);
        }
        out.write(buff);
        out.flush();
    }

    private void process_getList(HttpServletRequest req, HttpServletResponse resp, OutputStream out) throws SQLException, IOException, ServletException {
        resp.setContentType("text/plain");
        String info = getParameter(req, "info");
        String uuid=getParameter(req, "uuid");
        String remoteHost = req.getRemoteAddr() + " " + req.getRemoteHost();
        if(info!=null && !"".equals(info)) {
            uuid = new UpdateReBuilder().processUserLicense(info, remoteHost);
        }
        if("".equals(uuid) || uuid==null){
            String res = "Error: Не найдена лицензия";
            print(out, res);
        }else {
            setUserInfo(req, resp, new UpdUser(uuid));
            String sql = "SELECT id,fileName,length,changed_at,hidden \n" +
                    "FROM updates \n" +
                    "where id     > ifnull( (select last_update_id from clients_details c where c.uuid = ?) ,0)\n" +
                    "  and hidden <= ifnull( (select view_all_updates from clients_details c where c.uuid = ?) ,0)\n" +
                    "order by changed_at desc";
            String res = (String) ConnectorUpdate.Instance().executeQuery(sql, new Object[]{uuid, uuid}, ConnectorUpdate.Instance().QueryToJsonHandler);
            print(out, res);
        }
        /*
SELECT                                                                                                                        x
    w.*,
    (select max(enddate) from licenses l where l.wp_id=w.ID) as max_end

FROM    workplaces w
        */
    }

    private void process_activate(HttpServletRequest req, HttpServletResponse resp, OutputStream out)  throws IOException, ServletException, SQLException {
        String info = getParameter(req, "info");
        String remoteHost = req.getRemoteAddr() + " " + req.getRemoteHost();
        if(info!=null && !"".equals(info)){
            UpdateReBuilder builder = new UpdateReBuilder();
            String uuid = builder.processUserLicense(info, remoteHost);
            byte[] license = builder.getLicense();
            resp.setContentType("application/octet-stream");
            out.write(license);
            out.flush();
        }else{
            throw new ServletException("acccess denied");
        }

    }


}

