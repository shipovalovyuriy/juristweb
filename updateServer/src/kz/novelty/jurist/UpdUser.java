package kz.novelty.jurist;

public class UpdUser {
    public String login;
    public String uuid;

    public UpdUser() {
    }

    public UpdUser(String uuid) {
        this.uuid = uuid;
    }

    public UpdUser(String login, String uuid) {

        this.login = login;
        this.uuid = uuid;
    }
}
