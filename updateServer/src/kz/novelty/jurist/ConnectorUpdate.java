package kz.novelty.jurist;

/**
 * Created with IntelliJ IDEA.
 * User: Malik.N
 * Date: 15.03.13
 * Time: 11:10
 * To change this template use File | Settings | File Templates.
 */
public class ConnectorUpdate extends DBConnector {

    private static ConnectorUpdate instance = new ConnectorUpdate("jdbc/UpdateDBJurist");

    public ConnectorUpdate(String jndiName) {
        super(jndiName);
    }


    public static ConnectorUpdate Instance() {
        return instance;
    }


}
