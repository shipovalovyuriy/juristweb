<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    boolean debug = true;
%>
<html>
<head>
    <title>сервер обновлений</title>
    <% if (debug) { %>
    <script type="text/javascript" src="js/extjs/ext-base-debug.js"></script>
    <script type="text/javascript" src="js/extjs/ext-all-debug.js"></script>
    <script type="text/javascript" src="js/ext/jquery-1.5.1.full.js"></script>
    <script type="text/javascript" src="js/ext/jquery-ui-1.8.11.custom.js"></script>
    <% } else { %>
    <script type="text/javascript" src="js/extjs/ext-base.js"></script>
    <script type="text/javascript" src="js/extjs/ext-all.js"></script>
    <script type="text/javascript" src="js/ext/jquery-1.5.1.min.js"></script>
    <script type="text/javascript" src="js/ext/jquery-ui-1.8.11.custom.min.js"></script>
    <% } %>
    <script type="text/javascript" src="js/ext/jquery.form.js"></script>
    <script type="text/javascript" src="js/Utils.js"></script>
    <script type="text/javascript" src="js/WUIUtils.js"></script>
    <script type="text/javascript" src="js/update.js"></script>
</head>
<body>
<div id="user" style="display: none;">
    <form id="upload_form" method="POST" action="UpdateServerHandler" enctype="multipart/form-data">
        File:
        <input type="file" name="file" id="file"/> <br/>
        <input type="hidden" value="upload" name="requestType"/>
        <input type="submit" value="Upload" name="upload"/>
    </form>
    <pre id="user_res"></pre>
</div>
<div style="text-align: center;">
    <a class="show_login" href="#">Вход по логину</a>
    <a class="show_download" href="#">Ввести активационный код для скачивания обновлений</a>
</div>
<div id="login" style="display: none;">
    <form id="login_form" method="POST" action="UpdateServerHandler">
        User Name: <input type="text" name="username"/> <br/>
        Password: <input type="password" name="pass"/> <br/>
        <input type="hidden" value="login" name="requestType"/>
        <input type="submit" value="Login"/>
    </form>
</div>
<div id="download" style="display: none;">
    <form id="download_form" method="POST" action="UpdateServerHandler">
        Активационный КОД: <input type="text" name="uuid"/> <br/>
        <input type="hidden" value="getList" name="requestType"/>
        <input type="submit" value="Список"/>
    </form>
    <div id="files">

    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        onUpdateDomReady();
    });
</script>
</body>
</html>