﻿/* $Id: Utils.js 3083 2011-07-28 04:54:38Z nurbol $ */
var MILISECONDS_PER_DAY = 86400000;
var altDateFormats = "d.m.y|d/m/Y|d-m-y|d-m-Y|d/m|d-m|dm|dmy|dmY|d|Y-m-d|j.m.y|d.n.y|j.n.y|j.n.Y|d.m.Y|j.m.Y";
var DatePickerDefaults={
    firstDay: 1,
//changeMonth: true,
    changeYear: true,
    dayNamesMin: [ "Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб" ],
    monthNames: [ "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август",
        "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" ],
    dateFormat: "dd.mm.yy"
};

function createNewArray(arr) {
    var tmp = arr;
    if (tmp == null || tmp == undefined) {
        tmp = [];
    }
    tmp.last = function (index) { var ind = 0; if (!(index == null || index == '')) { ind = index; } return this[this.length - ind - 1]; };
    return tmp;
}

var oldErrorHandler = window.onerror;
function globalErrorHandler(msg, url, lno) {
    alert('Произошла ошибка. ' + msg + '\n' + url + ' ' + lno);
    if (oldErrorHandler) {
        return oldErrorHandler;
    }
    return false;
}
window.onerror = globalErrorHandler;

function DateToStr(val) {
    var tmpDate = val;
    if (tmpDate == null || tmpDate == "") return ""; 
    tmpDate = tmpDate.format('d.m.Y');
    return tmpDate;
}

function DateFromStr(val) {
    var tmpDate = val;
    if (tmpDate == null || tmpDate == "") return null;
    tmpDate = tmpDate.trim();
    tmpDate = tmpDate.replace(/'/g, "");
    tmpDate = Date.parseDate(tmpDate, 'd.m.Y');
    return tmpDate;
}

function DateTimeToStr(val) {
    var tmpDate = val;
    if (tmpDate == null || tmpDate == "") return "";
    tmpDate = tmpDate.format('d.m.Y H:i:s');
    return tmpDate;
}

function DateTimeFromStr(val) {
    var tmpDate = val;
    if (tmpDate == null || tmpDate == "") return null;
    tmpDate = tmpDate.trim();
    tmpDate = tmpDate.replace(/'/g, "");
    tmpDate = Date.parseDate(tmpDate, 'd.m.Y H:i:s');
    return tmpDate;
}
function DateTimeToSolr(val) {
    var tmpDate = val;
    if (tmpDate == null || tmpDate == "") return "*";
    tmpDate = tmpDate.format('c');
    tmpDate = tmpDate.substr(0,19)+'Z';
//    tmpDate = tmpDate.replace(' ','T');
//    tmpDate +="Z";
    return tmpDate;
}

function DateTimeFromSolr(val) {
    var tmpDate = val;
    if (tmpDate == null || tmpDate == "") return null;
    tmpDate = tmpDate.trim();
    tmpDate = tmpDate.replace(/'/g, "");
    tmpDate = Date.parseDate(tmpDate, 'c');
    return tmpDate;
}

function TimeToStr(val) {
    var tmpDate = val;
    if (tmpDate == null || tmpDate == "") return "";
    tmpDate = tmpDate.format('H:i:s');
    return tmpDate;
}

function TimeFromStr(val) {
    var tmpDate = val;
    if (tmpDate == null || tmpDate == "") return null; 
    tmpDate = tmpDate.replace(/'/g, "");
    tmpDate = Date.parseDate(tmpDate, 'H:i:s');
    return tmpDate;
}

function isStrNullOrEmpty(val) {
    return val == undefined || val == null || val == "";
}

function IsArray(a) {
    if (Array.isArray) return Array.isArray(a);
    return Object.prototype.toString(a) == "[object Array]";
}

document.emSize = function(pa) {
    pa = pa || document.body;
    var who = document.createElement('div');
    var atts = { fontSize: '1em', padding: '0', position: 'absolute', lineHeight: '1', visibility: 'hidden' };
    for (var p in atts) {
        who.style[p] = atts[p];
    }
    who.appendChild(document.createTextNode('M'));
    pa.appendChild(who);
    var fs = [who.offsetWidth, who.offsetHeight];
    try {
        pa.removeChild(who);
    } catch(e) { }
    return fs;
};

var PIXEL_PER_PT = 16 / 12;
function getSizePT(val) {
    if(val.indexOf("pt")>0) return Number(val.substr(0,val.indexOf("pt")));
    if(val.indexOf("px")>0){
        var tmp1 = Number(val.substr(0,val.indexOf("px")));
        return Math.round(tmp1/PIXEL_PER_PT);
    }
    var tmp = Number(val);
    var mult =1;
    switch (tmp) {
            case 1: mult = 0.6;break;
            case 2: mult = 0.89;break;
            case 3: mult = 1;break;
            case 4: mult = 1.2;break;
            case 5: mult = 1.5;break;
            case 6: mult = 2;break;
            case 7: mult = 3; break;
    }
    tmp = 12 * mult;
    return tmp;
}

function getActualStyle(x,styleProp){
    var y = null;
    if (x.currentStyle)
        y = x.currentStyle[styleProp];
    if(y==null && window.getComputedStyle)
        y = document.defaultView.getComputedStyle(x,null).getPropertyValue(styleProp,null);
    return y;
}

function onCheckBoxClick(scope) {
    if (scope.el != null && scope.el.dom != null) {
        scope.el.dom.style.position = "static";
        //scope.el.dom.style.position = "inherit";
    }
}



// rgb( 128, 128, 128) to array ["128", "128", "128"]
function rgbConvert(str) {
    if (str.indexOf('rgb') >= 0) {
        str = str.replace(/rgb\(|\)/g, "").split(",");
    } else if (str.trim()[0] == "#") {
        str = str.trim();
        var val = parseInt(str.substring(1, str.length), 16);
        str = [];
        str.push("" + ((val >> 16) & 255));
        str.push("" + ((val >> 8) & 255));
        str.push("" + ((val) & 255));
    }
    return str;

}

/* Returns the class name of the argument or undefined if 
it's not a valid JavaScript object. 
*/
function getObjectClass(obj) {
    if (obj && obj.constructor && obj.constructor.toString) {
        var arr = obj.constructor.toString().match(
            /function\s*(\w+)/);

        if (arr && arr.length == 2) {
            return arr[1];
        }
    }

    return undefined;
}
/*
*	Generated new ID unique (almost)
*/
function GetNextID() {
    var st = new Date();
    var tmp = '' + st.getTime();
    return -Number(tmp.substring(4, tmp.length));
}

/*
*	Used in calls makeRequest
*/
function CallClassMethodAfterAjaxReturn(res, response, extraParams,o) {
    if (this.extraData && this.extraData.thisRef) {
        this.extraData.thisRef[this.extraData.thisMethod](res, this, extraParams, o, response);
    } else if (this.thisRef){
        this.thisRef[this.thisMethod](res, this, extraParams, o, response);
    }
}

function CallClassMethodAfterAjaxFails(res) {
    this.thisRef[this.thisFailMethod](res, this);
}

 /*
initiates loading new javascript
*/
function LoadJavaScript(path) {
    var fileref = document.createElement('script');
    fileref.setAttribute("type", "text/javascript");
    fileref.setAttribute("src", path);
    if (typeof fileref != "undefined") {
        var head = document.getElementsByTagName('head')[0];
        head.appendChild(fileref);
    }
    fileref = undefined;

}
function onAfterPanelLayout(container, layout) {
    for (var i = 0, len = container.items.items.length; i < len; i++) {
        var item = container.items.items[i];
        if ((item.doLayout != null) && (item.body != null)) {
            item.doLayout();
        }
    }
}
function ChangeLoadingMaskText(mask, text) {
    if (mask != null && mask.el != null
      && mask.el._maskMsg != null
      && mask.el._maskMsg.dom != null
      && mask.el._maskMsg.dom.firstChild != null
      ) {
        mask.el._maskMsg.dom.firstChild.innerHTML=text;
      } else {
        mask.msg = text;
        mask.onBeforeLoad();
      }
}

function encodeBorlandTime(a) {
    if (a == null) return '';
    var HoursPerDay = 24;
    var MinsPerHour = 60;
    var SecsPerMin = 60;
    var MSecsPerSec = 1000;
    var MinsPerDay = HoursPerDay * MinsPerHour;
    var SecsPerDay = MinsPerDay * SecsPerMin;
    var SecsPerHour = SecsPerMin * MinsPerHour;
    var MSecsPerDay = SecsPerDay * MSecsPerSec;

    return (a.getHours() * (MinsPerHour * SecsPerMin * MSecsPerSec) +
             a.getMinutes() * (SecsPerMin * MSecsPerSec) +
             a.getSeconds() * MSecsPerSec /*+ MSec*/) / MSecsPerDay;

}

function getDays(a, b) {
    if (a == null || a == '') return '';
    if (b == null || b == '') return '';
    return Math.floor((a.getTime() - b.getTime()) / (1000 * 60 * 60 * 24));
}

function encodeBorlandDate(a) {
    if (a == null || a == '') return '';
    return getDays(a, new Date(1899, 11, 30, 0, 0));
}

function dateToBorlandTDateTime(dt) {
    if (dt == null || dt == '') return null;
    var days = encodeBorlandDate(dt);
    return days > 0 ? (days + encodeBorlandTime(dt)) : days - encodeBorlandTime(dt);
}

if (!this.JSON) {
    this.JSON = {};
}

(function() {

    function f(n) {
        // Format integers to have at least two digits.
        return n < 10 ? '0' + n : n;
    }

    if (typeof Date.prototype.toJSON !== 'function') {

        Date.prototype.toJSON = function(key) {

            return isFinite(this.valueOf()) ?
                   this.getUTCFullYear() + '-' +
                 f(this.getUTCMonth() + 1) + '-' +
                 f(this.getUTCDate()) + 'T' +
                 f(this.getUTCHours()) + ':' +
                 f(this.getUTCMinutes()) + ':' +
                 f(this.getUTCSeconds()) + 'Z' : null;
        };

        String.prototype.toJSON =
        Number.prototype.toJSON =
        Boolean.prototype.toJSON = function(key) {
            return this.valueOf();
        };
    }

    var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        gap,
        indent,
        meta = {    // table of character substitutions
            '\b': '\\b',
            '\t': '\\t',
            '\n': '\\n',
            '\f': '\\f',
            '\r': '\\r',
            '"': '\\"',
            '\\': '\\\\'
        },
        rep;


    function quote(string) {

        // If the string contains no control characters, no quote characters, and no
        // backslash characters, then we can safely slap some quotes around it.
        // Otherwise we must also replace the offending characters with safe escape
        // sequences.

        //escapable.lastIndex = 0;
        return escapable.test(string) ?
            '"' + string.replace(escapable, function(a) {
                var c = meta[a];
                return typeof c === 'string' ? c :
                    '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
            }) + '"' :
            '"' + string + '"';
    }


    function str(key, holder) {

        // Produce a string from holder[key].

        var i,          // The loop counter.
            k,          // The member key.
            v,          // The member value.
            length,
            mind = gap,
            partial,
            value = holder[key];

        // If the value has a toJSON method, call it to obtain a replacement value.

        if (value && typeof value === 'object' &&
                typeof value.toJSON === 'function') {
            value = value.toJSON(key);
        }

        // If we were called with a replacer function, then call the replacer to
        // obtain a replacement value.

        if (typeof rep === 'function') {
            value = rep.call(holder, key, value);
        }

        // What happens next depends on the value's type.

        switch (typeof value) {
            case 'string':
                return quote(value);

            case 'number':

                // JSON numbers must be finite. Encode non-finite numbers as null.

                return isFinite(value) ? String(value) : 'null';

            case 'boolean':
            case 'null':

                // If the value is a boolean or null, convert it to a string. Note:
                // typeof null does not produce 'null'. The case is included here in
                // the remote chance that this gets fixed someday.

                return String(value);

                // If the type is 'object', we might be dealing with an object or an array or
                // null.

            case 'object':

                // Due to a specification blunder in ECMAScript, typeof null is 'object',
                // so watch out for that case.

                if (!value) {
                    return 'null';
                }

                // Make an array to hold the partial results of stringifying this object value.

                gap += indent;
                partial = [];

                // Is the value an array?

                if (Object.prototype.toString.apply(value) === '[object Array]') {

                    // The value is an array. Stringify every element. Use null as a placeholder
                    // for non-JSON values.

                    length = value.length;
                    for (i = 0; i < length; i += 1) {
                        partial[i] = str(i, value) || 'null';
                    }

                    // Join all of the elements together, separated with commas, and wrap them in
                    // brackets.

                    v = partial.length === 0 ? '[]' :
                    gap ? '[\n' + gap +
                            partial.join(',\n' + gap) + '\n' +
                                mind + ']' :
                          '[' + partial.join(',') + ']';
                    gap = mind;
                    return v;
                }

                // If the replacer is an array, use it to select the members to be stringified.

                if (rep && typeof rep === 'object') {
                    length = rep.length;
                    for (i = 0; i < length; i += 1) {
                        k = rep[i];
                        if (typeof k === 'string') {
                            v = str(k, value);
                            if (v) {
                                partial.push(quote(k) + (gap ? ': ' : ':') + v);
                            }
                        }
                    }
                } else {

                    // Otherwise, iterate through all of the keys in the object.

                    for (k in value) {
                        if (Object.hasOwnProperty.call(value, k)) {
                            v = str(k, value);
                            if (v) {
                                partial.push(quote(k) + (gap ? ': ' : ':') + v);
                            }
                        }
                    }
                }

                // Join all of the member texts together, separated with commas,
                // and wrap them in braces.

                v = partial.length === 0 ? '{}' :
                gap ? '{\n' + gap + partial.join(',\n' + gap) + '\n' +
                        mind + '}' : '{' + partial.join(',') + '}';
                gap = mind;
                return v;
        }
    }

    // If the JSON object does not yet have a stringify method, give it one.

    if (typeof JSON.stringify !== 'function') {
        JSON.stringify = function(value, replacer, space) {

            // The stringify method takes a value and an optional replacer, and an optional
            // space parameter, and returns a JSON text. The replacer can be a function
            // that can replace values, or an array of strings that will select the keys.
            // A default replacer method can be provided. Use of the space parameter can
            // produce text that is more easily readable.

            var i;
            gap = '';
            indent = '';

            // If the space parameter is a number, make an indent string containing that
            // many spaces.

            if (typeof space === 'number') {
                for (i = 0; i < space; i += 1) {
                    indent += ' ';
                }

                // If the space parameter is a string, it will be used as the indent string.

            } else if (typeof space === 'string') {
                indent = space;
            }

            // If there is a replacer, it must be a function or an array.
            // Otherwise, throw an error.

            rep = replacer;
            if (replacer && typeof replacer !== 'function' &&
                    (typeof replacer !== 'object' ||
                     typeof replacer.length !== 'number')) {
                throw new Error('JSON.stringify');
            }

            // Make a fake root object containing our value under the key of ''.
            // Return the result of stringifying the value.

            return str('', { '': value });
        };
    }


    // If the JSON object does not yet have a parse method, give it one.

    if (typeof JSON.toString !== 'function') {
        JSON.toString = function() { return '[object JSON]'; };
    }
    if (typeof JSON.parse !== 'function') {
        JSON.parse = function(text, reviver) {

            // The parse method takes a text and an optional reviver function, and returns
            // a JavaScript value if the text is a valid JSON text.

            var j;

            function walk(holder, key) {

                // The walk method is used to recursively walk the resulting structure so
                // that modifications can be made.

                var k, v, value = holder[key];
                if (value && typeof value === 'object') {
                    for (k in value) {
                        if (Object.hasOwnProperty.call(value, k)) {
                            v = walk(value, k);
                            if (v !== undefined) {
                                value[k] = v;
                            } else {
                                delete value[k];
                            }
                        }
                    }
                }
                return reviver.call(holder, key, value);
            }


            // Parsing happens in four stages. In the first stage, we replace certain
            // Unicode characters with escape sequences. JavaScript handles many characters
            // incorrectly, either silently deleting them, or treating them as line endings.

            //cx.lastIndex = 0;
            if (cx.test(text)) {
                text = text.replace(cx, function(a) {
                    return '\\u' +
                        ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
                });
            }

            // In the second stage, we run the text against regular expressions that look
            // for non-JSON patterns. We are especially concerned with '()' and 'new'
            // because they can cause invocation, and '=' because it can cause mutation.
            // But just to be safe, we want to reject all unexpected forms.

            // We split the second stage into 4 regexp operations in order to work around
            // crippling inefficiencies in IE's and Safari's regexp engines. First we
            // replace the JSON backslash pairs with '@' (a non-JSON character). Second, we
            // replace all simple value tokens with ']' characters. Third, we delete all
            // open brackets that follow a colon or comma or that begin the text. Finally,
            // we look to see that the remaining characters are only whitespace or ']' or
            // ',' or ':' or '{' or '}'. If that is so, then the text is safe for eval.

            if (/^[\],:{}\s]*$/.
test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@').
replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {

                // In the third stage we use the eval function to compile the text into a
                // JavaScript structure. The '{' operator is subject to a syntactic ambiguity
                // in JavaScript: it can begin a block or an object literal. We wrap the text
                // in parens to eliminate the ambiguity.

                j = eval('(' + text + ')');

                // In the optional fourth stage, we recursively walk the new structure, passing
                // each name/value pair to a reviver function for possible transformation.

                return typeof reviver === 'function' ?
                    walk({ '': j }, '') : j;
            }

            // If the text is not JSON parseable, then a SyntaxError is thrown.

            throw new SyntaxError('JSON.parse');
        };
    }
} ());

var __json = null;
if ( typeof JSON !== "undefined" ) {
    __json = JSON.parse;
}

JSON.parse = function (text) {
    if (Ext.isGecko) {
        return new Function("return " + text)();
    }
    /*        if ( __json !== null ) {
    return (__json)(text);
    } */
    return eval(text);
};
 


function jsonDecode(str) {
    var res=null;
    Logger.timerStart("jsonDecode");
//    res=eval(str);
    res=JSON.parse(str);
    Logger.timerStop("jsonDecode");
    return res;
}

function jsonEncode(object) {
    var res ='';
    try{
    if(JSON.stringify){
       res = JSON.stringify(object);
    }else{
       res=Ext.util.JSON.encode(object);
    }
    }catch(e){
        Logger.message(e,Logger.logDebug);
        Logger.message(object,Logger.logDebug);
    }
    return res;
}

function callRequestHandler(methodName, params, success, failure, extraData, url) {
    var reqParams = {};
    reqParams.timeout = 300000;
    reqParams.url = url || 'RequestHandler';
    if (extraData) {
        reqParams.extraData = extraData;
        reqParams.scope = extraData;
    }
    reqParams.success = success;
    reqParams.failure = failure;
    reqParams.params = params;
    if (reqParams.params === null) {
        reqParams.params = {};
    }
    reqParams.params.requestType = methodName;
    Ext.Ajax.request(reqParams);
}

function getResponseErrorText(response) {
    if (response.status != 200) {
        return response.responseText || response.statusText || response.ErrorMessage;
    }
    return null;
}

function getResponseText(response) {
        return response.responseText || response.Result;
}


function makeRequest(methodName, params, success, failure, extraData) {
    params.timeout = 300000;
    if (extraData) {
        params.extraData = extraData;
    }
    if (!params.success) params.success = success;
    if (!params.failure) params.failure = failure;
	Ext.net.DirectMethod.request(methodName,params);
}

function afterDomReady() {
    regUIComponents();
}

var projectDep = {};

function getdProjectDep(project, visibleCondition) {
    var r = projectDep[project];
    if (r == null || r == undefined) {
        r = { visibles: createNewArray(), visibleCondition: visibleCondition };
        projectDep[project] = r;
    }
    if (r.visibleCondition == null) r.visibleCondition = visibleCondition;
    return r;
}

function isProjectDefined(proj) {
    if (METADATA.projects != null) {
        for (var i = 0, len = METADATA.projects.length; i < len; i++) {
            if (METADATA.projects[i] == proj) return true;
      }
    }
    return METADATA.project == proj;
}

function isDef(proj) {
    return isProjectDefined(proj)
}

function loadStore(metaObject) {
    var store = new Ext.data.Store({
        id: metaObject.id,
        reader: new Ext.data.ArrayReader({ idIndex: 0 },
                Ext.data.Record.create(metaObject.fields)
            )
    });
    store.loadData(metaObject.data);
    return store;
}

function getMetaObjectKeyByValue(metaObject, value, fieldName) {
    var keyIndex = 0;
    var fieldIndex = 0;
    for (var i = 0, len = metaObject.fields.length; i < len; i++) {
        if (fieldName == metaObject.fields[i].name) {
            fieldIndex = i;
            break;
        }
    }
    for (var i = 0, len = metaObject.data.length; i < len; i++) {
        if (metaObject.data[i][fieldIndex] == value) {
            return metaObject.data[i][keyIndex];
        }
    }
    return null;
}

function getMetaObjectValue(metaObject, keyValue, fieldName) {
    var keyIndex = 0;
    var fieldIndex = 0;
    for (var i = 0, len = metaObject.fields.length; i < len; i++) {
        if (fieldName == metaObject.fields[i].name) {
            fieldIndex = i;
            break;
        }
    }
    for (var i = 0, len = metaObject.data.length; i < len; i++) {
        if (metaObject.data[i][keyIndex] == keyValue) {
            return metaObject.data[i][fieldIndex];
        }
    }
    return "";
}

function applyProjectDependency(proj, someObject) {
    var project = projectDep[proj];
    var visibles = project.visibles;
    for (var i = 0, len = visibles.length; i < len; i++) {
        var isVisible = true;
        if (project.visibleCondition == null) {
            isVisible = isProjectDefined(proj);
        } else {
            isVisible = project.visibleCondition(someObject);
        }
        visibles[i].setVisible(isVisible);
    }
}

function applyProjectDependencies(someObject) {
    for (var proj in projectDep) {
        if (!projectDep.hasOwnProperty(proj)) continue;
        applyProjectDependency(proj, someObject);
    }
}



var numb_res_ru = [['', '', ''],
		[['один', 'одна'], ['десять', 'одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать'], 'сто'],
		[['два', 'две'], 'двадцать', 'двести'],
		['три', 'тридцать', 'триста'],
		['четыре', 'сорок', 'четыреста'],
		['пять', 'пятьдесят', 'пятьсот'],
		['шесть', 'шестьдесят', 'шестьсот'],
		['семь', 'семьдесят', 'семьсот'],
		['восемь', 'восемьдесят', 'восемьсот'],
		['девять', 'девяносто', 'девятьсот']];
var numb_tg_ru = [['тиын', 'тиын', 'тиын'],
	['тенге', 'тенге', 'тенге'], //['доллар','доллара','долларов'] или ['килограмм','килограмма','килограммов']
	['тысяча', 'тысячи', 'тысяч'],
	['миллион', 'миллиона', 'миллионов'],
	['миллиард', 'миллиарда', 'миллиардов']//,['трилион',...],[]
	];

var numb_res_kz = [['', '', ''],
		[['бір', 'бір'], ['он', 'он бір', 'он екі', 'он үш', 'он төрт', 'он бес', 'он алты', 'он жеті', 'он сегіз', 'он тоғыз'], 'жүз'],
		[['екі', 'екі'], 'жиырма', 'екі жүз'],
		['үш', 'отыз', 'үш жүз'],
		['төрт', 'қырык', 'төрт жүз'],
		['бес', 'елү', 'бес жүз'],
		['алты', 'алпыс', 'алты жүз'],
		['жеті', 'жетпіс', 'жеті жүз'],
		['сегіз', 'сексен', 'сегіз жүз'],
		['тоғыз', 'тоқсан', 'тоғыз жүз']];

var numb_tg_kz = [['тиын', 'тиын', 'тиын'],
	['теңге', 'тенге', 'тенге'], 
	['мың', 'мың', 'мың'],
	['миллион', 'миллион', 'миллион'],
	['миллиард', 'миллиард', 'миллиард']
	];

function money2string(n,s1,b) {
function plural(n, forms) {
	return forms[n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2];
}
    function m999(n, b, f) {
        var s = '';
        var t = s1[Math.floor(n / 100) % 10][2];
        if (t) s += t + ' ';

        var d = Math.floor(n / 10) % 10;
        t = s1[d][1];
        if (t instanceof Array) {
            t = t[n % 10];
            if (t) s += t + ' ';
        } else {
            if (t) s += t + ' ';
            t = s1[n % 10][0];
            if (t instanceof Array) t = t[f == 0 || f == 2 ? 1 : 0];
            if (t) s += t;
        }

        return s + ' ' + plural(n, b[f]) + (f > 1 ? ' ' : '');
    }

    var i = Math.floor(n + 0.005),
		f = Math.floor(((n - i) * 100) + 0.5),
		s = '';
    for (var j = 1; i > 0.9999; i /= 1000) {
        s = m999(Math.floor(i % 1000), b, j) + s;
        j++;
    }
    if (f > 0) s = s + ' ' + m999(f, b, 0);
    return s;
}

function money2string_ru(n, deleteWordTenge) {
    var res = money2string(n, numb_res_ru, numb_tg_ru);
    if (deleteWordTenge === true) {
        res = res.replace(' тенге', '');
    }
    return res;
}

function money2string_kz(n) {
    return money2string(n, numb_res_kz, numb_tg_kz);
}

function checktprn(tprn) {
    if (tprn == null ||
       tprn.length != 12) {
        return -1;
    }
    var allSame = true;
    for (var i = 1, len = tprn.length; i < len; i++) {
        if (tprn[i] != tprn[0]) {
            allSame = false; break;
        }
    }
    if (allSame == true) return -1; //забито одной и той же цифрой    
    var Weight = Number(tprn);
    var TPRN11 = tprn.substr(0, 11);
    var Chk = Number(tprn[11]);
    for (var i = 1; i < 10; i++) {
        var SumP = 0;
        Weight = i - 1;
        for (var j = 0; j < 11; j++) {
            Weight = Weight + 1;
            if (Weight == 11) {
                Weight = 1;
            }
            SumP += Weight * Number(TPRN11[j]);
        }
        if ((SumP - Math.floor(SumP / 11) * 11) < 10) {
            if (SumP - Math.floor(SumP / 11) * 11 == Chk) {
                return 1;
            } else {
                return 0;
            }
        }
    }
    return 1;
}

/*
    функция для связывания печати при использовании в  функции setTimeout
*/
function callLaterPrint(printObj) {
    return (function () {
        printObj.onPrintTimeOut.call(printObj);
    });
}



function mod(X, Y) {return X - Math.floor(X / Y) * Y; }

function div(X, Y) { return Math.floor(X / Y) /* full range */; }

function getPrintableModelValue(mark, model) {
    var vModel = model.trim();
    var vMark = mark.trim();
    return vModel.toUpperCase().indexOf(vMark.toUpperCase()) == 0 ? model : (mark + ' ' + model);
}

function classExists(c) { 
    return (typeof(c) == "function" && typeof(c.prototype) == "object") ? true : false; 
}


function parseXml(xmlString) {
    var xmlDoc = null;
    if (window.DOMParser) {
        var parser = new DOMParser();
        xmlDoc = parser.parseFromString(xmlString, "text/xml");
        parser = null;
    }else {// Internet Explorer    
        xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
        xmlDoc.async = "false";
        xmlDoc.loadXML(xmlString);
    }
    return xmlDoc;
}

function selectNode(node,name) {
    return node.querySelector ?  node.querySelector(name) : node.selectSingleNode(name) ;
}

function getXMLNodeText(node) {
    return node.text || node.textContent;
}


function replaceHtml(el, html) {
	var oldEl = typeof el === "string" ? document.getElementById(el) : el;
	/*@cc_on // Pure innerHTML is slightly faster in IE
		oldEl.innerHTML = html;
		return oldEl;
	@*/
	var newEl = oldEl.cloneNode(false);
	newEl.innerHTML = html;
	oldEl.parentNode.replaceChild(newEl, oldEl);
	/* Since we just removed the old element from the DOM, return a reference
	to the new element, which can be used to restore variable references. */
	return newEl;
}

/*	This work is licensed under Creative Commons GNU LGPL License.

	License: http://creativecommons.org/licenses/LGPL/2.1/
   Version: 0.9
	Author:  Stefan Goessner/2006
	Web:     http://goessner.net/
*/
function json2xml(o, tab) {
   var toXml = function(v, name, ind) {
      var xml = "";
      if (v instanceof Array) {
         for (var i=0, n=v.length; i<n; i++)
            xml += ind + toXml(v[i], name, ind+"\t") + "\n";
      }
      else if (typeof(v) == "object") {
         var hasChild = false;
         xml += ind + "<" + name;
         for (var m in v) {
            if (m.charAt(0) == "@")
               xml += " " + m.substr(1) + "=\"" + v[m].toString() + "\"";
            else
               hasChild = true;
         }
         xml += hasChild ? ">" : "/>";
         if (hasChild) {
            for (var m in v) {
               if (m == "#text")
                  xml += v[m];
               else if (m == "#cdata")
                  xml += "<![CDATA[" + v[m] + "]]>";
               else if (m.charAt(0) != "@")
                  xml += toXml(v[m], m, ind+"\t");
            }
            xml += (xml.charAt(xml.length-1)=="\n"?ind:"") + "</" + name + ">";
         }
      }
      else {
         xml += ind + "<" + name + ">" + v.toString() +  "</" + name + ">";
      }
      return xml;
   }, xml="";
   for (var m in o)
      xml += toXml(o[m], m, "");
   return tab ? xml.replace(/\t/g, tab) : xml.replace(/\t|\n/g, "");
}


/*	This work is licensed under Creative Commons GNU LGPL License.

	License: http://creativecommons.org/licenses/LGPL/2.1/
   Version: 0.9
	Author:  Stefan Goessner/2006
	Web:     http://goessner.net/
*/
function xml2json(xml, tab) {
   var X = {
      toObj: function(xml) {
         var o = {};
         if (xml.nodeType==1) {   // element node ..
            if (xml.attributes.length)   // element with attributes  ..
               for (var i=0; i<xml.attributes.length; i++)
                  o["@"+xml.attributes[i].nodeName] = (xml.attributes[i].nodeValue||"").toString();
            if (xml.firstChild) { // element has child nodes ..
               var textChild=0, cdataChild=0, hasElementChild=false;
               for (var n=xml.firstChild; n; n=n.nextSibling) {
                  if (n.nodeType==1) hasElementChild = true;
                  else if (n.nodeType==3 && n.nodeValue.match(/[^ \f\n\r\t\v]/)) textChild++; // non-whitespace text
                  else if (n.nodeType==4) cdataChild++; // cdata section node
               }
               if (hasElementChild) {
                  if (textChild < 2 && cdataChild < 2) { // structured element with evtl. a single text or/and cdata node ..
                     X.removeWhite(xml);
                     for (var n=xml.firstChild; n; n=n.nextSibling) {
                        if (n.nodeType == 3)  // text node
                           o["#text"] = X.escape(n.nodeValue);
                        else if (n.nodeType == 4)  // cdata node
                           o["#cdata"] = X.escape(n.nodeValue);
                        else if (o[n.nodeName]) {  // multiple occurence of element ..
                           if (o[n.nodeName] instanceof Array)
                              o[n.nodeName][o[n.nodeName].length] = X.toObj(n);
                           else
                              o[n.nodeName] = [o[n.nodeName], X.toObj(n)];
                        }
                        else  // first occurence of element..
                           o[n.nodeName] = X.toObj(n);
                     }
                  }
                  else { // mixed content
                     if (!xml.attributes.length)
                        o = X.escape(X.innerXml(xml));
                     else
                        o["#text"] = X.escape(X.innerXml(xml));
                  }
               }
               else if (textChild) { // pure text
                  if (!xml.attributes.length)
                     o = X.escape(X.innerXml(xml));
                  else
                     o["#text"] = X.escape(X.innerXml(xml));
               }
               else if (cdataChild) { // cdata
                  if (cdataChild > 1)
                     o = X.escape(X.innerXml(xml));
                  else
                     for (var n=xml.firstChild; n; n=n.nextSibling)
                        o["#cdata"] = X.escape(n.nodeValue);
               }
            }
            if (!xml.attributes.length && !xml.firstChild) o = null;
         }
         else if (xml.nodeType==9) { // document.node
            o = X.toObj(xml.documentElement);
         }
         else
            alert("unhandled node type: " + xml.nodeType);
         return o;
      },
      toJson: function(o, name, ind) {
         var json = name ? ("\""+name+"\"") : "";
         if (o instanceof Array) {
            for (var i=0,n=o.length; i<n; i++)
               o[i] = X.toJson(o[i], "", ind+"\t");
            json += (name?":[":"[") + (o.length > 1 ? ("\n"+ind+"\t"+o.join(",\n"+ind+"\t")+"\n"+ind) : o.join("")) + "]";
         }
         else if (o == null)
            json += (name&&":") + "null";
         else if (typeof(o) == "object") {
            var arr = [];
            for (var m in o)
               arr[arr.length] = X.toJson(o[m], m, ind+"\t");
            json += (name?":{":"{") + (arr.length > 1 ? ("\n"+ind+"\t"+arr.join(",\n"+ind+"\t")+"\n"+ind) : arr.join("")) + "}";
         }
         else if (typeof(o) == "string")
            json += (name&&":") + "\"" + o.toString() + "\"";
         else
            json += (name&&":") + o.toString();
         return json;
      },
      innerXml: function(node) {
         var s = ""
         if ("innerHTML" in node)
            s = node.innerHTML;
         else {
            var asXml = function(n) {
               var s = "";
               if (n.nodeType == 1) {
                  s += "<" + n.nodeName;
                  for (var i=0; i<n.attributes.length;i++)
                     s += " " + n.attributes[i].nodeName + "=\"" + (n.attributes[i].nodeValue||"").toString() + "\"";
                  if (n.firstChild) {
                     s += ">";
                     for (var c=n.firstChild; c; c=c.nextSibling)
                        s += asXml(c);
                     s += "</"+n.nodeName+">";
                  }
                  else
                     s += "/>";
               }
               else if (n.nodeType == 3)
                  s += n.nodeValue;
               else if (n.nodeType == 4)
                  s += "<![CDATA[" + n.nodeValue + "]]>";
               return s;
            };
            for (var c=node.firstChild; c; c=c.nextSibling)
               s += asXml(c);
         }
         return s;
      },
      escape: function(txt) {
         return txt.replace(/[\\]/g, "\\\\")
                   .replace(/[\"]/g, '\\"')
                   .replace(/[\n]/g, '\\n')
                   .replace(/[\r]/g, '\\r');
      },
      removeWhite: function(e) {
         e.normalize();
         for (var n = e.firstChild; n; ) {
            if (n.nodeType == 3) {  // text node
               if (!n.nodeValue.match(/[^ \f\n\r\t\v]/)) { // pure whitespace text node
                  var nxt = n.nextSibling;
                  e.removeChild(n);
                  n = nxt;
               }
               else
                  n = n.nextSibling;
            }
            else if (n.nodeType == 1) {  // element node
               X.removeWhite(n);
               n = n.nextSibling;
            }
            else                      // any other node
               n = n.nextSibling;
         }
         return e;
      }
   };
   if (xml.nodeType == 9) // document node
      xml = xml.documentElement;
   var json = X.toJson(X.toObj(X.removeWhite(xml)), xml.nodeName, "\t");
   return "{\n" + tab + (tab ? json.replace(/\t/g, tab) : json.replace(/\t|\n/g, "")) + "\n}";
}