function onGetList(responseText, statusText)  {
    $("#files").html(responseText);
}

function onLogin(responseText, statusText)  {
    $("#user").hide();
    if(!isStrNullOrEmpty(responseText)){
        $("#user").show();

    }
}

function showResponse(responseText, statusText)  {
    $("#user_res").html(responseText);
}

function onUpdateDomReady(){
    $('.show_login').click(function(){
        $("#login").show();
        $("#download").hide();
    });
    $('.show_download').click(function(){
        $("#download").show();
        $("#login").hide();
        $("#user").hide();
    });
    $('#upload_form').ajaxForm({success:showResponse});
    $('#login_form').ajaxForm({success:onLogin});
    $('#download_form').ajaxForm({success:onGetList});

}
