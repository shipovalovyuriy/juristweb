﻿/* $Id: WUIUtils.js 3118 2011-08-04 09:35:26Z nurbol $ */
function createEmptyLabel(colWidth) {
    return new Ext.form.Label({ html: "&nbsp;", columnWidth: colWidth });
}

function recheckVisibility(control, visible) {
    if (visible == null) visible = control.isVisible();
    var htmlEl = document.getElementById(control.id);
    if (htmlEl == null) return;
    var parentNode = htmlEl.parentNode;
    if (parentNode == null) return;
    if (parentNode.style.width == 0 ||
            parentNode.style.width == "0px" ||
            parentNode.style.width == "0pt") {
        parentNode.style.width = "auto";
    }
}
function createLabel(text, colWidth, addStyle, config) {
    var tmpStyle = "";
    if (!(addStyle == null || addStyle == "")) {
        tmpStyle = addStyle;
    }
    var lconf = config ||{};
    lconf.text = text;
    lconf.style= 'font:11px tahoma,arial,helvetica;padding-top:2px;' + tmpStyle;
    lconf.columnWidth = colWidth;
    return new Ext.form.Label(lconf);
}

function createSimpleImageButton(imgUrl, callback, info, tooltip, style) {
    var imgObj = new Ext.net.ImageButton({ imageUrl: imgUrl, cls: 'image_align_default', style: style,
        listeners: { click: callback },
        tooltip: tooltip,
        extraData: info
    });
    return imgObj;
    
}

function createSimpleLink(capion, callback, info) {
    var lnk = new Ext.net.LinkButton({
        text: capion,
        extraData: info,
        listeners: { click: callback },
        cls: "lnk_style_default"
    });
    return lnk;
}

function createColumnPanel(items, itemsSizes, height, width) {
    if (itemsSizes && itemsSizes.length > 0) {
        for (var i = 0, len = itemsSizes.length; i < len; i++) {
            var sz = itemsSizes[i];
            if (items[i]) {
                items[i][sz > 1 ? 'width':'columnWidth'] = sz;
            }
        }
    }
    var pnlCfg ={
        border: false, bodyStyle: 'padding:2px',frame:false,
        layout: 'column',
        width: '98%',
        extraData: {},
        items: items
    };
    if(height){
        pnlCfg.height = height;
    }
    if(width){
        pnlCfg.width = width;
    }
    return new Ext.Panel(pnlCfg);
}

function createPanelSimple(item, cnf) {
    var items = Ext.isArray(item) ? item : [item];
    var config = cnf || { border: false, header: false, width: '98%', style: 'font:11px tahoma,arial,helvetica;' };
    config.items = items;
    var pnl = new Ext.Panel(config);
    return pnl;
}

function isInstanceOfCombobox(control) {
    return control instanceof Ext.form.ComboBox || control instanceof Ext.ux.TreeComboBox;
}

function isInstanceOfLinkButton(control) {
    return control instanceof Ext.net.LinkButton;
}

function onLoadExteption(scope, o, response) {
    alert(response.responseText || response.statusText);
}


function renderCheckbox(val) {
    return '<input type="CHECKBOX" disabled ' + (val == true ? 'checked' : '') + '/>';
}

function renderDate(val) {
    if (val == "" || val == null) return "";
    return val.substring(0, 10);
/*    var tmpVar = val.indexOf(" 00:00:00");
    if (tmpVar >= 0) return val.substring(0, tmpVar);
    return Date.parseDate(val, 'j.m.Y G:i:s').format('j.m.Y');
*/
}

function bitRenderer(value) {
    if (value == true) return 'Да';
    if (value == false) return 'Нет';
    return 'неверное значение';
}

function floatSortType(s) {
    if (s == null || s == "") return 0;
    return; 
}
function buildTreeNodesFromStore(store, tableName, displayField, valueField, nodeField, parentField,checked,highlight) {
    var nodes = [];
    var nodesIndex = {};
    var highlightIndex = {};
    if(highlight){
        for (var k = 0, len = highlight.length; k < len; k++) {
            highlightIndex[highlight[k]] = highlight[k];
        }
    }

    var nodesNoParentsFound = [];
    for (var i = 0, len = store.data.items.length; i < len; i++) {
        var data = store.data.items[i].data;
        var node= { text: data[displayField], id: data[valueField], children: [],leaf:true, checked:checked };
        if(highlightIndex[node.id]){
            //node.iconCls='';
            node.cls='grant';
        }
        nodesIndex[data[nodeField]] = node;
        if (!(data[parentField] == null || data[parentField] == ''|| data[parentField] == '0')) {
            /*if (nodesIndex[data[parentField]]) {
                nodesIndex[data[parentField]].children.push(node);
                nodesIndex[data[parentField]].leaf = false;
            } else */{
                nodesNoParentsFound.push(data);
            }
        } else {
            nodes.push(node);
        }
    }
    for (var i = 0, len = nodesNoParentsFound.length; i < len; i++) {
        var data = nodesNoParentsFound[i];
        var node = nodesIndex[data[nodeField]];
        var parentNode = nodesIndex[data[parentField]];
        if (parentNode) {
            parentNode.children.push(node);
            parentNode.leaf = false;
        }
    }
    var root = new Ext.tree.AsyncTreeNode({
            text: 'treeRoot',
            id: '0',
            loaded:false,
            extraData:{nodesIndex:nodesIndex},
            "children": nodes.length==1 ? nodes[0].children : nodes
        });
    return root;
}

/*
используется рисованной частью веб модуля
Создать выпадающий список на удаленный справочник, который будет подкачан
*/
function createComboBoxRemote(tableName, displayField, valueField,cfg) {
    var locStore = createRemoteStore(tableName,displayField, valueField);
    var commonConfig = Ext.applyClean(
                cfg||{}, {
        typeId: 'field'
        , typeName: valueField
        , hideLabel: false
        , disabledClass: 'x-item-disabled1'
        , fieldLabel: ''
        , allowBlank: true
    });
    var triggersConfig = [
    /*{ iconCls: "x-form-clear-trigger" } */
    /*,{ iconCls: "x-form-search-trigger" }*/
    	];
    var tpl1 = displayField.indexOf('{') >= 0 ? displayField : '{' + displayField + '} ';
    control = new Ext.form.ComboBox(
            Ext.applyClean(
                commonConfig, {
                    tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">', tpl1, '</div></tpl>'),
                    itemSelector: 'div.search-item',
                    store: locStore,
                    editable: true,
                    minChars: 5,
                    queryDelay: 1000,
                    lazyInit: false,
                    lazyRender: false,
                    triggerAction: 'all',
                    mode: 'remote',
                    valueField: valueField,
                    displayField: displayField,
                    extraData: { modal: false },
                    triggersConfig: triggersConfig,
                    onCustomTriggerClick: comboFieldClickEvent
                }
            )
        );
    locStore.extraData.controlRef = control;
    locStore.on('load', function (scope) {
        this.sort(this.extraData.displayField, 'ASC');
        /*
        if (this.lastOptions.controlRef != null) {
        this.lastOptions.controlRef.setValue(this.lastOptions.controlRef.getValue());
        }
        */
    });

    return control;
}



function createComboBoxLocal(locStore, displayField, valueField, sortByDisplayField, tpl, canBeBlank) {
    if (sortByDisplayField != false) {
        locStore.sort(displayField, 'ASC');
    }
    var allowblank = false;
    if (canBeBlank == true) allowblank = canBeBlank;
    var commonConfig = {
        typeId: 'field'
        , typeName: valueField
        , hideLabel: false
        , disabledClass: 'x-item-disabled1'
        , fieldLabel: ''
        , allowBlank: allowblank
    };
    var tpl1 = displayField.indexOf('{') >= 0 ? displayField : '{' + displayField + '} ';
    if (!(tpl == null || tpl == '') && tpl.indexOf('{') >= 0) {
        tpl1 = tpl;
    }
    var triggersConfig = [];
    if (allowblank) {
        triggersConfig.push({ iconCls: "x-form-clear-trigger",qtip:'Очистить' });
    }
    var control = new Ext.form.ComboBox(
            Ext.applyClean(
                commonConfig, {
                    tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">', tpl1, '</div></tpl>'),
                    itemSelector: 'div.search-item',
                    store: locStore,
                    editable: false,
                    minChars: 3,
                    lazyInit: false,
                    lazyRender: false,
                    triggerAction: 'all',
                    mode: 'local',
                    valueField: valueField,
                    displayField: displayField,
                    triggersConfig: triggersConfig,
                    onCustomTriggerClick: comboFieldClickEvent,
                    extraData:{}
                }
            )
        );
    control.addListener('render', function (combo) {
        combo.el.addListener('focus', function (event, htmlElement, options) {
            try {
                htmlElement.blur();
            } catch (ex) {
            }
        });
    });
    return control;
}

function createRemoteStore(tableName, displayField, valueField, nodeField, parentField) {
    var locStore = new Ext.data.Store({
            proxy: new Ext.data.HttpProxy({ url: 'RequestHandler', timeout: 300000 }),
            reader: new Ext.ux.JsonReader(),
            baseParams: {
                TableName: tableName,
                DisplayField: displayField,
                ValueField: valueField,
                ParentField: parentField,
                requestType: 'loadDictionary'
            }, extraData: { tableName: tableName, displayField: displayField, valueField: valueField, nodeField:nodeField, parentField: parentField
            }
        });
        locStore.on("loadexception", onLoadExteption);
    return locStore;
}
function createComboBoxRemoteTree(tableName, displayField, valueField, nodeField, parentField,sortField) {
    var locStore = createRemoteStore(tableName, displayField, valueField, nodeField, parentField,sortField);
    var commonConfig = {
        typeId: 'field'
        , typeName: valueField
        , hideLabel: false
        , disabledClass: 'x-item-disabled1'
        , fieldLabel: ''
        , allowBlank: true
    };
    var triggersConfig = [
    /*{ iconCls: "x-form-clear-trigger" } */
    /*,{ iconCls: "x-form-search-trigger" }*/
    	];
    var tpl1 = displayField.indexOf('{') >= 0 ? displayField : '{' + displayField + '} ';
    control = new Ext.ux.TreeComboBox(
            Ext.applyClean(
                commonConfig, {
                    store: locStore,
                    extStore: locStore,
                    editable: false,
                    //minChars: 5,
                    queryDelay: 1000,
                    lazyInit: false,
                    lazyRender: false,
                    triggerAction: 'all',
                    mode: 'remote',
                    valueField: valueField,
                    displayField: displayField,
                    extraData: { modal: false },
                    triggersConfig: triggersConfig,
                    onCustomTriggerClick: comboFieldClickEvent
                }
            )
        );
        control.addListener('render', function (combo) {
            combo.el.addListener('focus', function (event, htmlElement, options) {
                try {
                    htmlElement.blur();
                } catch (ex) {
                }
            });
        });
    locStore.extraData.controlRef = control;
    locStore.extraData.sortField = sortField;
    locStore.on('load', function (scope) {
        var extraData = this.extraData;
        var f = this.fields.get(extraData.sortField);
        if (f != null) f.sortType = Ext.data.SortTypes.asFloat;
        this.sort(extraData.sortField, 'ASC');
        var node = buildTreeNodesFromStore(this, extraData.tableName, extraData.displayField, extraData.valueField, extraData.nodeField, extraData.parentField);
        if (extraData.controlRef != null ) {
            extraData.controlRef.tree.setRootNode(node);
            if(extraData.controlRef.rendered){
                extraData.controlRef.tree.render(this.extraData.controlRef.treeId);
                extraData.controlRef.collapse();
                extraData.controlRef.expand();
            }
            //this.lastOptions.controlRef.setValue(this.lastOptions.controlRef.getValue());
        }
    });

    return control;
}


function createGridPanel(columns, store, pagingItems) {
    var ROW_PAGING_SIZE=null;
    var gridColumns = columns;
    var width = 250;

    var grid = new Ext.grid.GridPanel({
        stripeRows: false,
        closable: true,
        extraData: {},
        store: store,
        colModel: new Ext.grid.ColumnModel({
            defaultWidth: width,
            defaults: {
                width: width,
                sortable: true
            },
            columns: gridColumns
        }),
        sm: new Ext.grid.RowSelectionModel({ singleSelect: false }),
        header: false
            , bbar: new Ext.PagingToolbar({
                pageSize: ROW_PAGING_SIZE || 30,
                displayInfo: true,
                store: store,
                displayMsg: 'Записи с {0} по {1} из {2}',
                emptyMsg: "Нет данных для отображения",
                items: pagingItems||[]
            })
    });
//    grid.addListener('celldblclick', dblClickEvent || function() { });
    return grid;
}

function createGridEditFromPanel(columns) {
    var form = new Ext.form.FormPanel({
        frame: true,
        labelAlign: 'right',
        labelWidth: 120,
        frame: true,
        width: 500,
        //height: 530,
        defaultType: 'textfield',
        defaults: {
            anchor: '100%'
        },

        // private A pointer to the currently loaded record
        record : null,
        events:{
            create:true,
            save:true
        },
        items:columns,
        buttons: [
            {
                text: 'Сохранить',
                iconCls: 'icon-save',
                handler: function(btn,evt){
                    var self = this.ownerCt.ownerCt;
                    if (self.record == null) {
                        return;
                    }
                    if (!self.getForm().isValid()) {
                        alert( "Не правильно заполнено одно из полей");
                        return false;
                    }
                    self.getForm().updateRecord(self.record);
                    self.fireEvent('save', self, self.record);
                }//,scope: this
            },
            {
                text: 'Отменить',
                handler: function(btn, ev){
                    var self = this.ownerCt.ownerCt;
                    self.getForm().reset();
                }//,scope: this
            }
        ],

        /**
         * loadRecord
         * @param {Record} rec
         */
        loadFormRecord : function(rec) {
            this.record = rec;
            this.getForm().reset();
            this.getForm().loadRecord(rec);
        },

        /**
         * onUpdate
         */
        onUpdate : function(btn, ev) {
            if (this.record == null) {
                return;
            }
            if (!this.getForm().isValid()) {
                alert( "Не правильно заполнено одно из полей");
                return false;
            }
            this.fireEvent('save', this, this.record);
            this.getForm().updateRecord(this.record);
        },

        /**
         * onCreate
         */
        onCreate : function(btn, ev) {
            if (!this.getForm().isValid()) {
                alert( "Не правильно заполнено одно из полей");
                return false;
            }
            this.fireEvent('create', this, this.getForm().getValues());
            this.getForm().reset();
        },

        /**
         * onReset
         */
        onReset : function(btn, ev) {
            this.fireEvent('update', this, this.getForm().getValues());
            this.getForm().reset();
        }
    });
    return form;
}

function createGridEditablePanel(columns, store, pagingItems, plugins,ROW_PAGING_SIZE) {
    var gridColumns = columns;
    var width = 250;

    var grid = new Ext.grid.EditorGridPanel({
        stripeRows: false,
        closable: true,
        extraData: {},
        clicksToEdit: 1,
        store: store,
        colModel: new Ext.grid.ColumnModel({
            defaultWidth: width,
            defaults: {
                width: width,
                sortable: true
            },
            columns: gridColumns
        }),
        sm: new Ext.grid.RowSelectionModel({ singleSelect: false }),
        plugins:plugins ||[],
        header: false
            , bbar: new Ext.PagingToolbar({
                pageSize: ROW_PAGING_SIZE || 300,
                displayInfo: true,
                ref:'pageBar',
                store: store,
                plugins:plugins ||[],
                displayMsg: 'Записи с {0} по {1} из {2}',
                emptyMsg: "Нет данных для отображения",
                items: pagingItems||[]
            })
    });
    if(store instanceof Ext.data.GroupingStore){
        grid.view =  new Ext.grid.GroupingView({});
    }
//    grid.addListener('celldblclick', dblClickEvent || function() { });
    return grid;
}

function resizeJQueryGrid(pane, $Pane, paneState) {
    if (grid = $('.ui-jqgrid-btable:visible')) {
        grid.each(function (index) {
            var gridId = $(this).attr('id');
            $('#' + gridId).setGridWidth(paneState.innerWidth - 2);
        });
    }
}

function onJqgResize(el, width, height, rawwidth, rawheight) {
    if (this.jqueryGridId) {
        var obj = $(this.jqueryGridId);
        obj.setGridWidth(width - 5);
        obj.setGridHeight(height - 58);
    }
}

function createJGridPanel(tableId, dblClickEvent, jqgridPanel) {

    var gridColumns = [];

    var table = tablesConfigHash[tableId];
    var tableFields = fieldsConfigHash[tableId].fields;
    var tree = {};

    if (table.TREE_BOOL == true) {
        tree.enabled = true;

        for (var i = 0, len = tableFields.length; i < len; i++) { //ищем primaryKey
            if (tableFields[i].PRIMARY_KEY_BOOL == true) {
                tree.keyFieldName = tableFields[i].FIELD_NAME;  //нашли
                tree.keyFieldIndex = i;
                for (var e = 0; e < len; e++) { //ищем parentKey
                    if (tableFields[e].FIELD_NAME == ('PARENT_' + tree.keyFieldName)) {
                        tree.parentFieldName = tableFields[e].FIELD_NAME;
                        tree.parentFieldIndex = e;
                    }
                }
            }
        }
    }
    var visibleField = 0;

    for (var i = 0, len = tableFields.length; i < len; i++) {
        var field = fieldsConfigHash[tableId].fields[i];
        var hidden = /*field.VIRTUAL_BOOL == true || */field.VISIBLE_BOOL == false || field.PRIMARY_KEY_BOOL == true;
        var fieldName = field.FIELD_NAME;
        if (fieldName.substr(fieldName.length - 3, fieldName.length) == '_ID') continue;
        var tmpFieldName = fieldName.substr(0, fieldName.length - 3);
        if (fieldsConfigHash[tableId].namesHash[tmpFieldName]) {
            fieldName = tmpFieldName;
        }

        if (tree.enabled) hidden |= tree.parentFieldIndex == i;
        //{ label: 'ID', name: 'id', index: 'id', width: 20, resizable: true },
        var col1 = { label: field.FIELD_CAPTION, name: fieldName, index: fieldName, resizable: true, width: (field.FIELD_CAPTION.length * 8), hidden: hidden };
        if (field.DATA_TYPE_ID == 3) {
            col1.datefmt = 'dd.mm.yyyy';
            col1.sorttype = 'date';
            col1.unformat = renderDate;
        }
        if (field.DATA_TYPE_ID == 7) {
            col1.formatter = 'checkbox';
            //col1.renderer = renderCheckbox;
        }
        gridColumns.push(col1);
        if (!hidden) visibleField++;
    }

    var width;

    if ((visibleField * 160) > viewPort.getWidth())
        width = 160;
    else
        width = viewPort.getWidth() / visibleField - 5;

    var mytable = document.createElement("TABLE");
    mytable.style.width = '100%';
    var extTable = new Ext.Element(mytable);
    var extTablePager = new Ext.Element(document.createElement("DIV"));
    jqgridPanel.body.dom.appendChild(mytable);
    jqgridPanel.body.dom.appendChild(extTablePager.dom);
//    panel.extTable = extTable;
//    panel.extTablePager = extTablePager;
    var grid = $('#' + extTable.id).jqGrid({
        datatype: 'local',
        //autowidth: true,
        shrinkToFit: false, 
        viewrecords:true,
        //scroll: true,
        rowNum: ROW_PAGING_SIZE, rowList: [10, 20, 30, 100],
        sortable: true,
        //height: '100%',
        pager: $('#' + extTablePager.id),
        colModel: gridColumns,
        ondblClickRow: dblClickEvent
    });
    $('#' + extTable.id).jqGrid('setGridParam', { ondblClickRow: dblClickEvent });
    jqgridPanel.on('resize', onJqgResize);
    jqgridPanel.body.addClass('ui-layout-center');
    jqgridPanel.jqueryGridId = '#' + extTable.id;
    return grid;
}


function comboFieldClickEvent(event, element, options) {   /* браузер таблицы */
    switch (options.index) {
        case 0: // кнопка очистить
            this.setValue("");
            this.fireEvent('change', this);
            break;
        case 1: // кнопка поиск
            var ed = this.extraData;
            generateSearchForm(ed.contextValue, ed.table.TABLE_ID, this, ed.linkObjectClass, ed.modal);
            break;
            if (this.getValue() != null && this.getValue()!="") {
                var report = new Ext.ux.Report({ renderTo: Ext.getBody() });
                report.load({
                    url: '/RequestHandler.ashx',
                    params: {
                        RequestType: 'download',
                        AttachmentId: this.getValue(),
                        AttachmentName: this.lastSelectionText.split(';')[1]
                    }
                });
            }
            break;
    }
}


function checkDataChangeControl(rules, origData) {
    var res = false;
    if (origData == null || origData == undefined) return res;
    for (var i = 0, len = rules.length; i < len; i++) {
        var rule = rules[i];
        var oldValue = origData[rule.name];
        var newValue = null;
        if (rule.valueChanged != null) {
            res = res || rule.valueChanged(origData, rule.extraData);
        } else {
            if (rule.control instanceof Ext.form.DateField) {
                newValue = DateToStr(rule.control.getValue());
            } else if (rule.control instanceof Ext.form.Checkbox) {
                oldValue = oldValue == true || oldValue == "1";
                if (rule.name == "person_bool") {
                    newValue = !rule.control.getValue();
                } else { newValue = rule.control.getValue(); }
            } else if (rule.control instanceof Ext.form.ComboBox || rule.control instanceof Ext.ux.TreeComboBox) {
                oldValue = origData[rule.name + '_id'];
                newValue = rule.control.getValue();
                //newValue = rule.control.lastSelectionText;
            } else if (rule.control instanceof Ext.ux.form.SpinnerField) {
                newValue = rule.control.getValue();
                newValue = "" + newValue;
                oldValue = "" + oldValue;
            } else if (rule.control instanceof Ext.form.TextField) {
                newValue = rule.control.getValue();
            } else if (rule.control instanceof Ext.net.LinkButton) {
                continue;
            } else if (rule.control instanceof Ext.form.Label) {
                continue;
            } else {
                newValue = rule.control[rule.value];
            }
            if ((newValue == null || newValue == "") != (oldValue == null || oldValue == "") || newValue != oldValue) {
                res = true;
            }
        }
        if (res) break;
    }
    return res;
}

function convertDataFromControl(rules, origData,changeOrigData) {
    if (origData == null || origData == undefined) return;
    var wasChanged = origData.changed;
    var data = changeOrigData ? origData : {};
    if (data != origData) {
        for (var i in origData) {
            if (!origData.hasOwnProperty(i)) continue;
            data[i] = origData[i];
        }
    }
    for (var i = 0, len = rules.length; i < len; i++) {
        var rule = rules[i];
        var oldValue = data[rule.name];
        if (rule.valueFrom != null) {
            rule.valueFrom(data, rule.extraData);
        }else if (rule.control instanceof Ext.form.DateField) {
            data[rule.name] = DateToStr(rule.control.getValue());
        } else if (rule.control instanceof Ext.form.Checkbox) {
            if (rule.name == "person_bool") {
                data[rule.name] = !rule.control.getValue();
            } else { data[rule.name] = rule.control.getValue(); }
        } else if (rule.control instanceof Ext.form.ComboBox || rule.control instanceof Ext.ux.TreeComboBox) {
            data[rule.name + '_id'] = rule.control.getValue();
            data[rule.name] = rule.control.lastSelectionText;
        } else if (rule.control instanceof Ext.ux.form.SpinnerField) {
            data[rule.name] = rule.control.getValue();
        } else if (rule.control instanceof Ext.form.TextField) {
            data[rule.name] = rule.control.getValue();
        } else if (rule.control instanceof Ext.net.LinkButton) {
        } else if (rule.control instanceof Ext.form.Label) {
        } else {
            data[rule.name] = rule.control[rule.value];

        }
        if (data[rule.name] != oldValue) wasChanged = true;
    }
    data.changed = wasChanged;
    return data;
}

function convertDataToControl(rules, data) {
    if (data == null || data == undefined) return;
    for (var i = 0, len = rules.length; i < len; i++) {
        var rule = rules[i];
        if (rule.valueTo != null) {
            rule.valueTo(data, rule.extraData);
        }else if (rule.control instanceof Ext.form.DateField) {
            rule.control.setValue(DateFromStr(data[rule.name]));
        } else if (rule.control instanceof Ext.form.Checkbox) {
            if (rule.name == "person_bool") {
                rule.control.setValue(!data[rule.name]);
            } else {
                rule.control.setValue(data[rule.name]);
            }
        } else if (rule.control instanceof Ext.form.ComboBox || rule.control instanceof Ext.ux.TreeComboBox) {
            rule.control.setValue(data[rule.name + '_id']);
            if (!(data[rule.name] == "" || data[rule.name] == null)) {
                rule.control.lastSelectionText = data[rule.name];
                if (rule.control.el != null && rule.control.el.dom != null) {
                    rule.control.el.dom.value = rule.control.lastSelectionText;
                }
            }
            rule.control.fireEvent('change', rule.control);
        } else if (rule.control instanceof Ext.ux.form.SpinnerField) {
            rule.control.setValue(data[rule.name]);
        } else if (rule.control instanceof Ext.form.TextField) {
            rule.control.setValue(data[rule.name]);
        } else if (rule.control instanceof Ext.net.LinkButton) {
            if (data[rule.name] == null || data[rule.name] == "") {
                rule.control.setText(rule.caption + rule.emptyText);
            } else {
                rule.control.setText(rule.caption + data[rule.name]);
            }
        } else if (rule.control instanceof Ext.form.Label) {
            if (data[rule.name] == null || data[rule.name] == "") {
                rule.control.setText(rule.caption + rule.emptyText);
            } else {
                rule.control.setText(rule.caption + data[rule.name]);
            }
        } else {
            rule.control[rule.value] = data[rule.name];
        }
    }
}

function createComboBoxControl(foreignTable, LINK_OBJECT_CLASS_ID, commonConfig, modal, contextValue) { 
    if (LINK_OBJECT_CLASS_ID == "" || LINK_OBJECT_CLASS_ID == 0 || LINK_OBJECT_CLASS_ID == '0') {
        LINK_OBJECT_CLASS_ID = foreignTable.LINK_OBJECT_CLASS_ID;
    }
    var fieldsArray = fieldsConfigHash[foreignTable.TABLE_ID].fields;
    var fieldsNamesHash = fieldsConfigHash[foreignTable.TABLE_ID].namesHash;
    var recordPreview = recordPreviewFieldsHash[foreignTable.TABLE_ID];

    var PK, PKName, PKNameWID;

    PK = fieldsNamesHash[foreignTable.PRIMARY_KEY_FIELD];
    PKName = PK.FIELD_NAME;
    PKNameWID = PKName.substr(0, PKName.length - 3);

    var valueField = PKName;
    var displayField;
    var tmpField;
    var tpl;
    var control = null;
    if (recordPreview) {    //По умолчанию Record Preview Fields
        tpl = '';
        var tplCount = 0;
        for (var e = 0, len = recordPreview.length; e < len; e++) {    //Ищем первый элемент списка, fix: добавить грид
            var rec = recordPreview[e];
            var fName = fieldsConfigHash[rec.TABLE_ID].fieldsHash[rec.TABLE_FIELD_ID].FIELD_NAME;
            var tmpFName = fName.substr(0, fName.length - 3);
            if (!fieldsConfigHash[foreignTable.TABLE_ID].namesHash[tmpFName]) tmpFName = fName;
            if (e == 0) displayField = tmpFName;
            var recPrefix = rec.PREFIX || '';
            tplCount++;
            tpl += recPrefix + '{' + tmpFName + '} ';
        }

    } else {    //Если RecordPreviewFields отсутствует

        tmpField = fieldsNamesHash[PKNameWID];
        if (tmpField) {
            displayField = tmpField.FIELD_NAME;
        } else {
            tmpField = fieldsNamesHash[PKNameWID + '_TEXT'];
            if (tmpField) {
                displayField = tmpField.FIELD_NAME;
            } else {
                tmpField = fieldsNamesHash[PKNameWID + '_NAME'];
                if (tmpField) {
                    displayField = tmpField.FIELD_NAME;
                } else {
                    displayField = valueField;
                }
            }
        }
    }

    if (LINK_OBJECT_CLASS_ID == 1 /*Выпадающий список*/
        || LINK_OBJECT_CLASS_ID == 4 /*Выпадающий список из временной таблицы*/
        || LINK_OBJECT_CLASS_ID == 5 /*Выпадающий список с фильтрацией*/) {
        var locStore;
        var mode = 'local';

        var tableId = foreignTable.TABLE_ID;
        var store = globalStores['DIRECTORY_' + tableId];
        if (store) {//Локальная загрузка
            locStore = store;
        } else {    //Если локальная загрузка недоступна

        locStore = new Ext.data.Store({
            proxy: new Ext.data.HttpProxy({ url: '/RequestHandler.ashx', timeout: 300000 }),
            reader: new Ext.ux.JsonReader(),
            baseParams: {
                TableName: foreignTable.TABLE_NAME,
                TableID: tableId,
                queryKeyName:foreignTable.PRIMARY_KEY_FIELD,
                RequestType: 'getData'}
              , extraData: { displayField: displayField}
            });
            locStore.on("loadexception", onLoadExteption);
            locStore.on('load', function (scope, r, options) {
                if (this.extraData.controlRef != null && this.extraData.controlRef.store != null) {
                    this.sort(this.extraData.displayField, 'ASC');
                    if (options.params.queryKey == null) {
                        this.extraData.controlRef.mode = 'local';
                    }

                }
            });

            mode = 'remote';
        }

        if (mode == 'local') {
            locStore.sort(displayField, 'ASC');
        }
        control = new Ext.form.ComboBox(
            Ext.applyClean(
                commonConfig, {
                    tpl: tpl ? new Ext.XTemplate('<tpl for="."><div class="search-item">', tpl, '</div></tpl>') : undefined,
                    itemSelector: 'div.search-item',
                    store: locStore,
                    editable: true,
                    minChars: 3,
                    lazyInit: false,
                    lazyRender: false,
                    triggerAction: 'all',
                    mode: mode,
                    valueField: valueField,
                    displayField: displayField,
                    triggersConfig: [                		{
                		    iconCls: "x-form-clear-trigger"
                		}],
                    onCustomTriggerClick: comboFieldClickEvent
                }
            )
        );
        if (mode == 'remote') {
            locStore.extraData.controlRef = control;
        }
        if (tplCount > 1) {
            control.setValue = function(v) {
                var text = v;
                if (this.valueField) {
                    var r = this.findRecord(this.valueField, v);
                    if (r) {
                        text = getRecordPreviewText(tableId, r.data);
                    } else if (Ext.isDefined(this.valueNotFoundText)) {
                        text = this.valueNotFoundText;
                    }
                }
                this.lastSelectionText = text;
                if (this.hiddenField) {
                    this.hiddenField.value = Ext.value(v, '');
                }
                Ext.form.ComboBox.superclass.setValue.call(this, text);
                this.value = v;
                return this;
            };
            /*control.on('select', function(scope, record, index) {
                this.lastSelectionText = getRecordPreviewText(tableId, record.data);
                Ext.form.ComboBox.superclass.setValue.call(this, this.lastSelectionText);
            });*/
        }
        return control;
    }

    var triggersConfig = [
    		{ iconCls: "x-form-clear-trigger" },
    		{ iconCls: "x-form-search-trigger" }
    	];
   		if (foreignTable.TABLE_NAME == 'ATTACHMENTS') triggersConfig.push({ iconCls: "x-form-page-trigger" });
   		if (foreignTable.TABLE_NAME == 'CLIENTS' ||foreignTable.TABLE_NAME == 'VEHICLES' ) triggersConfig.push({ iconCls: "x-form-trigger-slate" });

    var locStore = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({ url: '/RequestHandler.ashx', timeout: 300000 }),
        reader: new Ext.ux.JsonReader(),
        baseParams: {
            TableName: foreignTable.TABLE_NAME,
            TableID: foreignTable.TABLE_ID,
            cls: commonConfig.cls,
            RequestType: 'quickSearch'
        }
        ,extraData: {displayField: displayField}
    });
    locStore.on("loadexception", onLoadExteption);
    locStore.on('load', function (scope) {
        if (this.extraData.controlRef != null) {
            this.sort(this.extraData.displayField, 'ASC');
            //this.extraData.controlRef.mode = 'local';
        }
        if (this && this.data && this.data.items && this.data.items.length == 0) {
            if (confirm("По вашему запросу ничего не найдено.\nЖелаете воспользоваться расширенным поиском?")) {
                var ed = this.extraData;
                var control = ed.controlRef;
                ed = control.extraData;
                generateSearchForm(ed.contextValue, ed.table.TABLE_ID, control, ed.linkObjectClass, ed.modal, this.lastOptions.params.query);
            }
        }
    });
    tmpField = fieldsNamesHash[PKNameWID];
    if (tmpField) {
        displayField = tmpField.FIELD_NAME;
    } else {
        tmpField = fieldsNamesHash[PKNameWID + '_TEXT'];
        if (tmpField) {
            displayField = tmpField.FIELD_NAME;
        } else {
            tmpField = fieldsNamesHash[PKNameWID + '_NAME'];
            if (tmpField) {
                displayField = tmpField.FIELD_NAME;
            } else {
                displayField = valueField;
            }
        }
    }
    tpl = "{"+displayField+"}";
    control = new Ext.form.ComboBox(
            Ext.applyClean(
                commonConfig, {
                    tpl: tpl ? new Ext.XTemplate('<tpl for="."><div class="search-item">', tpl, '</div></tpl>') : undefined,
                    itemSelector: 'div.search-item',
                    store: locStore,
                    editable: true,
                    minChars: 3,
                    queryDelay: 1000,
                    lazyInit: false,
                    lazyRender: false,
                    triggerAction: 'all',
                    mode: 'remote',
                    valueField: valueField,
                    displayField: displayField,
                    extraData: { table: foreignTable, linkObjectClass: LINK_OBJECT_CLASS_ID, modal: modal, contextValue: contextValue },
                    triggersConfig: triggersConfig,
                    onCustomTriggerClick: comboFieldClickEvent
                }
            )
        );
    locStore.extraData.controlRef = control;
    return control;      
}
//
//  Аргументы:
//      descr - элемент-описатель поля таблицы из контейнера fieldsConfigHash[]
//      width - ширина контрола на форме
//
//
function createControl(descr, width, labelShow, showRequired, reference, contextValue) {

    var typeId = 'field';
    var typeName = descr.FIELD_NAME;

    var control;

    var foreignTable = null;
    if (reference) foreignTable = tablesConfigHash[reference.FOREIGN_TABLE_ID];
    
    var maxLength = descr.FIELD_SIZE;
    if ((descr.DATA_TYPE_ID == 4)) {
        maxLength = 20;
    }
    if ((descr.DATA_TYPE_ID == 3) ) {
    	maxLength = 10;
    }
    if (foreignTable != null) {
    	maxLength = Number.MAX_VALUE;
    }
    if ((maxLength <= 0) && (descr.DATA_TYPE_ID == 6)) {
    	maxLength = Number.MAX_VALUE;
    }

    var required = ((descr.FIELD_NULLABLE_TYPE_ID == 1) || (descr.FIELD_NULLABLE_TYPE_ID == 3)) && showRequired;
    
    var commonConfig = {
        typeId: typeId
        , typeName: typeName
        , fieldDescriptor: descr
        , hideLabel: !labelShow
        , disabledClass:'x-item-disabled1'
        , width: width
        , fieldLabel: descr.FIELD_CAPTION
        , maxLength: maxLength
        , allowBlank: !required
        , cls: ''
    };
    if (descr.FIELD_FORMAT && descr.FIELD_FORMAT.indexOf("U") >= 0) {
        commonConfig.cls = "x-inp-upcase";
    }

    switch (descr.DATA_TYPE_ID) {
        case 1:
            control = new Ext.form.TextField(commonConfig);
            break;        /*Строка*/
        case 2:
            control = new Ext.form.TextArea( Ext.applyClean(commonConfig, {height: 40}));
            break;         /*Мемо*/
        case 3:
            control = new Ext.form.DateField(Ext.applyClean(commonConfig, { format: 'd.m.Y', altFormats: altDateFormats }));
            break;        /*Дата*/
        case 4:
            //control = new Ext.form.DateTimeField(Ext.applyClean(commonConfig, { format: 'd.m.Y H:i:s' }));
            control = new Ext.ux.form.DateTimeField(Ext.applyClean(commonConfig, { format: 'd.m.Y H:i:s', dateFormat: 'd.m.Y', timeFormat: 'H:i:s', altFormats: altDateFormats }));
            break;    /*Дата и время*/
        case 5:
            control = new Ext.form.NumberField(commonConfig);
            break;      /*Маленькое целое число*/
        case 6:
            if (foreignTable) {
                var LINK_OBJECT_CLASS_ID = descr.LINK_OBJECT_CLASS_ID;
                control = createComboBoxControl(foreignTable, LINK_OBJECT_CLASS_ID, commonConfig, contextValue);
                break;
            }
            commonConfig.decimalPrecision = descr.FIELD_PRECISION > 0 ? descr.FIELD_PRECISION : 6;
            control = new Ext.form.NumberField(commonConfig);
            break;
        case 7:
           	control = new Ext.form.Checkbox(Ext.applyClean(commonConfig, {}));
           	break;   /*Логическое*/
        case 8:
            //control = new Ext.form.TextField({ typeId: typeId, typeName: typeName, fieldDescriptor:descr, width: width, fieldLabel: descr.FIELD_CAPTION});
            //icon - pencil
            //control = new Ext.ux.form.FileUploadField( Ext.applyClean(commonConfig, { emptyText: 'Выберите файл', buttonText: 'Найти' }) );
            //control = new Ext.form.TriggerField(Ext.applyClean(commonConfig, {inputType:'file'}));
            control = new Ext.form.FileUploadField(
                Ext.applyClean(commonConfig, {
                    emptyText: 'Выберите файл...',
                    buttonText: '',
                    buttonCfg: { iconCls: 'icon-page_add' },
                    listeners: {
                        fileselected: function(fb, v) {
                            //Logger.message(fb);
                            //Logger.message(v);
                        }
                    }

                })
            );

            break;      /*Файл*/
        case 9:
            control = new Ext.form.TextField(commonConfig);
            break;    /*Рассчитываемое*/

    }
    return control;
}

function getReportResponse(sender) {
    var s = '';
    s = (this.contentWindow.document || this.contentDocument).body.innerHTML;
    if(!(s==null || s=='' || s==' ')) Ext.MessageBox.alert("Ошибка!", s);
}
// регистрация самописных компонент
function regUIComponents() {
    //Ext.USE_NATIVE_JSON = true;
    Ext.Element.addMethods({
                /*getWidth : function(contentWidth){
                    var me = this,
                            dom = me.dom,
                            hidden = Ext.isIE && me.isStyle('display', 'none'),
                            w = Math.min(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
                    w = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
                    return w < 0 ? 0 : w;
                },
                getHeight : function(contentHeight){
                    var me = this,
                            dom = me.dom,
                            hidden = Ext.isIE && me.isStyle('display', 'none'),
                            h = Math.min(dom.offsetHeight, hidden ? 0 : dom.clientHeight) || 0;
                    h = !contentHeight ? h : h - me.getBorderWidth("tb") - me.getPadding("tb");
                    return h < 0 ? 0 : h;
                },*/
                hasClass: function (className) {
                    return className && this.dom && (' ' + this.dom.className + ' ').indexOf(' ' + className + ' ') != -1;
                },
        addClass: function (className) {
            var me = this,
                   i,
                   len,
                   v,
                   cls = [];
            if (!Ext.isArray(className)) {
                if (typeof className == 'string' && me.dom && !this.hasClass(className)) {
                    me.dom.className += " " + className;
                }
            }
            else {
                for (i = 0, len = className.length; i < len; i++) {
                    v = className[i];
                    if (typeof v == 'string' && me.dom && (' ' + me.dom.className + ' ').indexOf(' ' + v + ' ') == -1) {
                        cls.push(v);
                    }
                }
                if (cls.length && me.dom) {
                    me.dom.className += " " + cls.join(" ");
                }
            }
            return me;
        }
    });
    Ext.ux.Report = Ext.extend(Ext.Component, {
        autoEl: { tag: 'iframe', cls: 'x-hidden', src: Ext.SSL_SECURE_URL },
        load: function (config) {
            this.getEl().dom.onload = getReportResponse;
            this.getEl().dom.src = config.url + (config.params ? '?' + Ext.urlEncode(config.params) : '');
        }
    });
    Ext.reg('ux.report', Ext.ux.Report);

    Ext.ux.HiddenForm = function(url,fields){
        if (!Ext.isArray(fields))
            return;
        var body = Ext.getBody(),
            frame = body.createChild({
                tag:'iframe',
                cls:'x-hidden',
                id:'hiddenform-iframe',
                name:'iframe'
            }),
            form = body.createChild({
                tag:'form',
                cls:'x-hidden',
                id:'hiddenform-form',
                action: url,
                target:'iframe'
            });

        Ext.each(fields, function(el,i){
            if (!Ext.isArray(el))
                return false;
            form.createChild({
                tag:'input',
                type:'text',
                cls:'x-hidden',
                id: 'hiddenform-' + el[0],
                name: el[0],
                value: el[1]
            });
        });

        form.dom.submit();

        return frame;
    }
    Ext.applyClean = function(o, c) {
        var n = {};
        if (o && c && typeof c == 'object') {
            for (var p in o) { n[p] = o[p]; };
            for (var p in c) { n[p] = c[p]; };
        }
        return n;
    };
    
    Ext.layout.MultiToolbarLayout = Ext.extend(Ext.layout.ContainerLayout, {

        defaultMargins: { left: 0, top: 0, right: 0, bottom: 0 },
        padding: '0',
        pack: 'start',
        monitorResize: true,
        scrollOffset: 0,
        extraCls: 'x-box-item',
        ctCls: 'x-box-layout-ct',
        innerCls: 'x-box-inner',
        isValidParent: function(c, target) {
            return c.getEl().dom.parentNode == this.innerCt.dom;
        },

        renderItem: function(c) {
            if (typeof c.margins == 'string') {
                c.margins = this.parseMargins(c.margins);
            } else if (!c.margins) {
                c.margins = this.defaultMargins;
            }
            Ext.layout.MultiToolbarLayout.superclass.renderItem.apply(this, arguments);
        },

        getTargetSize: function(target) {
            return (Ext.isIE6 && Ext.isStrict && target.dom == document.body) ? target.getStyleSize() : target.getViewSize();
        },

        getItems: function(ct) {
            var items = [];
            ct.items.each(function(c) {
                if (c.isVisible()) {
                    items.push(c);
                }
            });
            return items;
        },

        onLayout: function(ct, target) {

            var cs = ct.items.items, len = cs.length, c, i, last = len - 1, cm;

            if (!this.innerCt) {
                target.addClass(this.ctCls);
                this.innerCt = target.createChild({ cls: this.innerCls });
                this.padding = this.parseMargins(this.padding);
            }
            this.renderAll(ct, this.innerCt);

            var size = this.getTargetSize(target),
            w = size.width - target.getPadding('lr') - this.scrollOffset,
            h = size.height - target.getPadding('tb'),
            l = this.padding.left, t = this.padding.top,
            initLeft = l;

            var maxHeight = 0;
            Ext.each(cs, function(c) {
                var el = c.getEl();
                var cw = el.getWidth();
                var ch = el.getHeight();


                if (ch > maxHeight) {
                    maxHeight = ch;
                }

                if (l + cw >= w) {
                    l = initLeft;
                    t += maxHeight;
                    maxHeight = ch;
                }

                el.setLeftTop(l, t);

                l += cw;
            });

            this.innerCt.setSize(w, t + maxHeight);
        }
    });

    Ext.Container.LAYOUTS.mtbar = Ext.layout.MultiToolbarLayout;

    Ext.MultiToolbar = Ext.extend(Ext.Panel, {
        autoHeight: true,
        baseCls: 'x-toolbar',
        layout: 'mtbar'
    });

    Ext.reg('multitoolbar', Ext.MultiToolbar);
/**
* @class Ext.ux.Spinner
* @extends Ext.util.Observable
* Creates a Spinner control utilized by Ext.ux.form.SpinnerField
*/
//#region spinner
Ext.ux.Spinner = Ext.extend(Ext.util.Observable, {
    incrementValue: 1,
    alternateIncrementValue: 5,
    triggerClass: 'x-form-spinner-trigger',
    splitterClass: 'x-form-spinner-splitter',
    alternateKey: Ext.EventObject.shiftKey,
    defaultValue: 0,
    accelerate: false,

    constructor: function(config) {
        Ext.ux.Spinner.superclass.constructor.call(this, config);
        Ext.apply(this, config);
        this.mimicing = false;
    },

    init: function(field) {
        this.field = field;

        field.afterMethod('onRender', this.doRender, this);
        field.afterMethod('onEnable', this.doEnable, this);
        field.afterMethod('onDisable', this.doDisable, this);
        field.afterMethod('afterRender', this.doAfterRender, this);
        field.afterMethod('onResize', this.doResize, this);
        field.afterMethod('onFocus', this.doFocus, this);
        field.beforeMethod('onDestroy', this.doDestroy, this);
    },

    doRender: function(ct, position) {
        var el = this.el = this.field.getEl();
        var f = this.field;

        if (!f.wrap) {
            f.wrap = this.wrap = el.wrap({
                cls: "x-form-field-wrap"
            });
        }
        else {
            this.wrap = f.wrap.addClass('x-form-field-wrap');
        }

        this.trigger = this.wrap.createChild({
            tag: "img",
            src: Ext.BLANK_IMAGE_URL,
            cls: "x-form-trigger " + this.triggerClass
        });

        if (!f.width) {
            this.wrap.setWidth(el.getWidth() + this.trigger.getWidth());
        }

        this.splitter = this.wrap.createChild({
            tag: 'div',
            cls: this.splitterClass,
            style: 'width:13px; height:0px;'
        });
        this.splitter.setRight((Ext.isIE) ? 1 : 2).setTop(10).show();

        this.proxy = this.trigger.createProxy('', this.splitter, true);
        this.proxy.addClass("x-form-spinner-proxy");
        this.proxy.setStyle('left', '0px');
        this.proxy.setSize(14, 1);
        this.proxy.hide();
        this.dd = new Ext.dd.DDProxy(this.splitter.dom.id, "SpinnerDrag", {
            dragElId: this.proxy.id
        });

        this.initTrigger();
        this.initSpinner();
    },

    doAfterRender: function() {
        var y;
        if (Ext.isIE && this.el.getY() != (y = this.trigger.getY())) {
            this.el.position();
            this.el.setY(y);
        }
    },

    doEnable: function() {
        if (this.wrap) {
            this.wrap.removeClass(this.field.disabledClass);
        }
    },

    doDisable: function() {
        if (this.wrap) {
            this.wrap.addClass(this.field.disabledClass);
            this.el.removeClass(this.field.disabledClass);
        }
    },

    doResize: function(w, h) {
        if (typeof w == 'number') {
            this.el.setWidth(w - this.trigger.getWidth());
        }
        this.wrap.setWidth(this.el.getWidth() + this.trigger.getWidth());
    },

    doFocus: function() {
        if (!this.mimicing) {
            this.wrap.addClass('x-trigger-wrap-focus');
            this.mimicing = true;
            Ext.get(Ext.isIE ? document.body : document).on("mousedown", this.mimicBlur, this, {
                delay: 10
            });
            this.el.on('keydown', this.checkTab, this);
        }
    },

    // private
    checkTab: function(e) {
        if (e.getKey() == e.TAB) {
            this.triggerBlur();
        }
    },

    // private
    mimicBlur: function(e) {
        if (!this.wrap.contains(e.target) && this.field.validateBlur(e)) {
            this.triggerBlur();
        }
    },

    // private
    triggerBlur: function() {
        this.mimicing = false;
        Ext.get(Ext.isIE ? document.body : document).un("mousedown", this.mimicBlur, this);
        this.el.un("keydown", this.checkTab, this);
        this.field.beforeBlur();
        this.wrap.removeClass('x-trigger-wrap-focus');
        this.field.onBlur.call(this.field);
        this.field.fireEvent("blur", this);
    },

    initTrigger: function() {
        this.trigger.addClassOnOver('x-form-trigger-over');
        this.trigger.addClassOnClick('x-form-trigger-click');
    },

    initSpinner: function() {
        this.field.addEvents({
            'spin': true,
            'spinup': true,
            'spindown': true
        });

        this.keyNav = new Ext.KeyNav(this.el, {
            "up": function(e) {
                e.preventDefault();
                this.onSpinUp();
            },

            "down": function(e) {
                e.preventDefault();
                this.onSpinDown();
            },

            "pageUp": function(e) {
                e.preventDefault();
                this.onSpinUpAlternate();
            },

            "pageDown": function(e) {
                e.preventDefault();
                this.onSpinDownAlternate();
            },

            scope: this
        });

        this.repeater = new Ext.util.ClickRepeater(this.trigger, {
            accelerate: this.accelerate
        });
        this.field.mon(this.repeater, "click", this.onTriggerClick, this, {
            preventDefault: true
        });

        this.field.mon(this.trigger, {
            mouseover: this.onMouseOver,
            mouseout: this.onMouseOut,
            mousemove: this.onMouseMove,
            mousedown: this.onMouseDown,
            mouseup: this.onMouseUp,
            scope: this,
            preventDefault: true
        });

        this.field.mon(this.wrap, "mousewheel", this.handleMouseWheel, this);

            this.dd.setXConstraint(0, 0, 10);
            this.dd.setYConstraint(1500, 1500, 10);
            this.dd.endDrag = this.endDrag.createDelegate(this);
            this.dd.startDrag = this.startDrag.createDelegate(this);
            this.dd.onDrag = this.onDrag.createDelegate(this);
        },

    onMouseOver: function() {
        if (this.disabled) {
            return;
        }
        var middle = this.getMiddle();
        this.tmpHoverClass = (Ext.EventObject.getPageY() < middle) ? 'x-form-spinner-overup' : 'x-form-spinner-overdown';
        this.trigger.addClass(this.tmpHoverClass);
    },

    //private
    onMouseOut: function() {
        this.trigger.removeClass(this.tmpHoverClass);
    },

    //private
    onMouseMove: function() {
        if (this.disabled) {
            return;
        }
        var middle = this.getMiddle();
        if (((Ext.EventObject.getPageY() > middle) && this.tmpHoverClass == "x-form-spinner-overup") ||
        ((Ext.EventObject.getPageY() < middle) && this.tmpHoverClass == "x-form-spinner-overdown")) {
        }
    },

    //private
    onMouseDown: function() {
        if (this.disabled) {
            return;
        }
        var middle = this.getMiddle();
        this.tmpClickClass = (Ext.EventObject.getPageY() < middle) ? 'x-form-spinner-clickup' : 'x-form-spinner-clickdown';
        this.trigger.addClass(this.tmpClickClass);
    },

    //private
    onMouseUp: function() {
        this.trigger.removeClass(this.tmpClickClass);
    },

    //private
    onTriggerClick: function() {
        if (this.disabled || this.el.dom.readOnly) {
            return;
        }
        var middle = this.getMiddle();
        var ud = (Ext.EventObject.getPageY() < middle) ? 'Up' : 'Down';
        this['onSpin' + ud]();
    },

    //private
    getMiddle: function() {
        var t = this.trigger.getTop();
        var h = this.trigger.getHeight();
        var middle = t + (h / 2);
        return middle;
    },

    //private
    //checks if control is allowed to spin
    isSpinnable: function() {
        if (this.disabled || this.el.dom.readOnly || this.field.disabled) {
            Ext.EventObject.preventDefault(); //prevent scrolling when disabled/readonly
            return false;
        }
        return true;
    },

    handleMouseWheel: function(e) {
        //disable scrolling when not focused
        if (this.wrap.hasClass('x-trigger-wrap-focus') == false) {
            return;
        }

        var delta = e.getWheelDelta();
        if (delta > 0) {
            this.onSpinUp();
            e.stopEvent();
        }
        else
            if (delta < 0) {
            this.onSpinDown();
            e.stopEvent();
        }
    },

    //private
    startDrag: function() {
        this.proxy.show();
        this._previousY = Ext.fly(this.dd.getDragEl()).getTop();
    },

    //private
    endDrag: function() {
        this.proxy.hide();
    },

    //private
    onDrag: function() {
        if (this.disabled) {
            return;
        }
        var y = Ext.fly(this.dd.getDragEl()).getTop();
        var ud = '';

        if (this._previousY > y) {
            ud = 'Up';
        } //up
        if (this._previousY < y) {
            ud = 'Down';
        } //down
        if (ud != '') {
            this['onSpin' + ud]();
        }

        this._previousY = y;
    },

    //private
    onSpinUp: function() {
        if (this.isSpinnable() == false) {
            return;
        }
        if (Ext.EventObject.shiftKey == true) {
            this.onSpinUpAlternate();
            return;
        }
        else {
            this.spin(false, false);
        }
        this.field.fireEvent("spin", this);
        this.field.fireEvent("spinup", this);
    },

    //private
    onSpinDown: function() {
        if (this.isSpinnable() == false) {
            return;
        }
        if (Ext.EventObject.shiftKey == true) {
            this.onSpinDownAlternate();
            return;
        }
        else {
            this.spin(true, false);
        }
        this.field.fireEvent("spin", this);
        this.field.fireEvent("spindown", this);
    },

    //private
    onSpinUpAlternate: function() {
        if (this.isSpinnable() == false) {
            return;
        }
        this.spin(false, true);
        this.field.fireEvent("spin", this);
        this.field.fireEvent("spinup", this);
    },

    //private
    onSpinDownAlternate: function() {
        if (this.isSpinnable() == false) {
            return;
        }
        this.spin(true, true);
        this.field.fireEvent("spin", this);
        this.field.fireEvent("spindown", this);
    },

    spin: function(down, alternate) {
        var v = parseFloat(this.field.getValue());
        var incr = (alternate == true) ? this.alternateIncrementValue : this.incrementValue;
        (down == true) ? v -= incr : v += incr;

        v = (isNaN(v)) ? this.defaultValue : v;
        v = this.fixBoundries(v);
        this.field.setRawValue(v);
    },

    fixBoundries: function(value) {
        var v = value;

        if (this.field.minValue != undefined && v < this.field.minValue) {
            v = this.field.minValue;
        }
        if (this.field.maxValue != undefined && v > this.field.maxValue) {
            v = this.field.maxValue;
        }

        return this.fixPrecision(v);
    },

    // private
    fixPrecision: function(value) {
        var nan = isNaN(value);
        if (!this.field.allowDecimals || this.field.decimalPrecision == -1 || nan || !value) {
            return nan ? '' : value;
        }
        return parseFloat(parseFloat(value).toFixed(this.field.decimalPrecision));
    },

    doDestroy: function() {
        if (this.trigger) {
            this.trigger.remove();
        }
        if (this.wrap) {
            this.wrap.remove();
            delete this.field.wrap;
        }

        if (this.splitter) {
            this.splitter.remove();
        }

        if (this.dd) {
            this.dd.unreg();
            this.dd = null;
        }

        if (this.proxy) {
            this.proxy.remove();
        }

        if (this.repeater) {
            this.repeater.purgeListeners();
        }
        if (this.mimicing) {
            Ext.get(Ext.isIE ? document.body : document).un("mousedown", this.mimicBlur, this);
        }
    }
});
//#endregion

//backwards compat
Ext.form.Spinner = Ext.ux.Spinner;


/*!
* Ext JS Library 3.3.0
* Copyright(c) 2006-2010 Ext JS, Inc.
* licensing@extjs.com
* http://www.extjs.com/license
*/
Ext.ns('Ext.ux.form');

/**
* @class Ext.ux.form.SpinnerField
* @extends Ext.form.NumberField
* Creates a field utilizing Ext.ux.Spinner
* @xtype spinnerfield
*/
    Ext.copyTo = function(c, d, e) {
        if (typeof e == "string") { e = e.split( /[,;\s]/ ); } Ext.each(e, function(f) { if (d.hasOwnProperty(f)) { c[f] = d[f]; } }, this); return c;
    };
Ext.ux.form.SpinnerField = Ext.extend(Ext.form.NumberField, {
    actionMode: 'wrap',
    deferHeight: true,
    autoSize: Ext.emptyFn,
    onBlur: Ext.emptyFn,
    adjustSize: Ext.BoxComponent.prototype.adjustSize,

    constructor: function(config) {
        var spinnerConfig = Ext.copyTo({}, config, 'incrementValue,alternateIncrementValue,accelerate,defaultValue,triggerClass,splitterClass');

        var spl = this.spinner = new Ext.ux.Spinner(spinnerConfig);

        var plugins = config.plugins
			? (Ext.isArray(config.plugins)
				? config.plugins.push(spl)
				: [config.plugins, spl])
			: spl;

        Ext.ux.form.SpinnerField.superclass.constructor.call(this, Ext.apply(config, { plugins: plugins }));
    },

    // private
    getResizeEl: function() {
        return this.wrap;
    },

    // private
    getPositionEl: function() {
        return this.wrap;
    },

    // private
    alignErrorIcon: function() {
        if (this.wrap) {
            this.errorIcon.alignTo(this.wrap, 'tl-tr', [2, 0]);
        }
    },

    validateBlur: function() {
        return true;
    }
});

Ext.reg('spinnerfield', Ext.ux.form.SpinnerField);

//backwards compat
Ext.form.SpinnerField = Ext.ux.form.SpinnerField;


    Ext.ux.RowLayout = Ext.extend(Ext.layout.ContainerLayout, {
        // private
        monitorResize: true,

        type: 'row',

        // private
        allowContainerRemove: false,

        // private
        isValidParent: function (c, target) {
            return this.innerCt && c.getPositionEl().dom.parentNode == this.innerCt.dom;
        },

        getLayoutTargetSize: function () {
            var target = this.container.getLayoutTarget(), ret;
            if (target) {
                ret = target.getViewSize();

                // IE in strict mode will return a height of 0 on the 1st pass of getViewSize.
                // Use getStyleSize to verify the 0 height, the adjustment pass will then work properly
                // with getViewSize
                if (Ext.isIE && Ext.isStrict && ret.height == 0) {
                    ret = target.getStyleSize();
                }

                ret.width -= target.getPadding('lr');
                ret.height -= target.getPadding('tb');
            }
            return ret;
        },

        renderAll: function (ct, target) {
            if (!this.innerCt) {
                // the innerCt prevents wrapping and shuffling while
                // the container is resizing
                this.innerCt = target.createChild({ cls: 'x-column-inner1' });
                this.innerCt.createChild({ cls: 'x-clear' });
            }
            Ext.layout.ColumnLayout.superclass.renderAll.call(this, ct, this.innerCt);
        },

        // private
        onLayout: function (ct, target) {
            var rs = ct.items.items,
            len = rs.length,
            r,
            m,
            i,
            margins = [];

            this.renderAll(ct, target);

            var size = this.getLayoutTargetSize();

            if (size.width < 1 && size.height < 1) { // display none?
                return;
            }

            var h = size.height,
            ph = h;

            this.innerCt.setSize({ height: h });

            // some rows can be percentages while others are fixed
            // so we need to make 2 passes

            for (i = 0; i < len; i++) {
                r = rs[i];
                m = r.getPositionEl().getMargins('tb');
                margins[i] = m;
                if (!r.rowHeight) {
                    ph -= (r.getHeight() + m);
                }
            }

            ph = ph < 0 ? 0 : ph;

            for (i = 0; i < len; i++) {
                r = rs[i];
                m = margins[i];
                if (r.rowHeight) {
                    r.setSize({ height: Math.floor(r.rowHeight * ph) - m });
                }
            }

            // Browsers differ as to when they account for scrollbars.  We need to re-measure to see if the scrollbar
            // spaces were accounted for properly.  If not, re-layout.
            if (Ext.isIE) {
                if (i = target.getStyle('overflow') && i != 'hidden' && !this.adjustmentPass) {
                    var ts = this.getLayoutTargetSize();
                    if (ts.width != size.width) {
                        this.adjustmentPass = true;
                        this.onLayout(ct, target);
                    }
                }
            }
            delete this.adjustmentPass;
        }

        /**
        * @property activeItem
        * @hide
        */
    });

    Ext.Container.LAYOUTS['ux.row'] = Ext.ux.RowLayout;

LoadJavaScript("js/PagingMemoryProxy.js");

// Declare a custom VType
Ext.apply(Ext.form.VTypes, {
    // This function validates input text
    AlphaNum: function (v) {
        return /^[a-z0-9]$/i.test(v);
    },
    // This is the tooltip message displayed when invalid input occurs
    AlphaNumText: 'Must be an alphanumeric word',
    // This mask filter invalid keystrokes
    AlphaNumMask: /[a-z0-9]/i
});
// Declare a custom VType
Ext.apply(Ext.form.VTypes, {
    // This function validates input text
    NumOnly: function (v) {
        return /[0-9]$/i.test(v);
    },
    // This is the tooltip message displayed when invalid input occurs
    NumOnlyText: 'Допускаются только цифры',
    // This mask filter invalid keystrokes
    NumOnlyMask: /[0-9]/i
});


Ext.ux.MyTabPanel = Ext.extend(Ext.TabPanel, {
    autoScrollTabs: function () {
        this.pos = this.tabPosition == 'bottom' ? this.footer : this.header;
        if (this.mytbar) {
            this.pos = this[this.stripTarget];
        }
        var count = this.items.length,
    ow = this.pos.dom.offsetWidth,
    tw = this.pos.dom.clientWidth,
    wrap = this.stripWrap,
    wd = wrap.dom,
    cw = wd.offsetWidth,
    pos = this.getScrollPos(),
    l = this.edge.getOffsetsTo(this.stripWrap)[0] + pos;
        if (!this.enableTabScroll || cw < 20) {
            return;
        }
        if (count == 0 || l <= tw) {
            wd.scrollLeft = 0;
            wrap.setWidth(tw);
            if (this.scrolling) {
                this.scrolling = false;
                this.pos.removeClass('x-tab-scrolling');
                this.scrollLeft.hide();
                this.scrollRight.hide();
                if (Ext.isAir || Ext.isWebKit) {
                    wd.style.marginLeft = '';
                    wd.style.marginRight = '';
                }
            }
        } else {
            if (!this.scrolling) {
                this.pos.addClass('x-tab-scrolling');
                if (Ext.isAir || Ext.isWebKit) {
                    wd.style.marginLeft = '18px';
                    wd.style.marginRight = '18px';
                }
            }
            tw -= wrap.getMargins('lr');
            wrap.setWidth(tw > 20 ? tw : 20);
            if (!this.scrolling) {
                if (!this.scrollLeft) {
                    this.createScrollers();
                } else {
                    this.scrollLeft.show();
                    this.scrollRight.show();
                }
            }
            this.scrolling = true;
            if (pos > (l - tw)) {
                wd.scrollLeft = l - tw;
            } else {
                this.scrollToTab(this.activeTab, false);
            }
            this.updateScrollButtons();
        }
    },
    adjustBodyWidth: function (w) {
        var ww = w;
        if (this.mytbar) {
            this[this.stripTarget].setWidth(ww - this.headerOffsetWidth);
        }
        if (this.header) {
            //if (this.headerOffsetWidth) ww = ww - this.headerOffsetWidth;
            this.header.setWidth(ww);
        }
        if (this.footer) {
            this.footer.setWidth(w);
        }
        return w;
    },
    onRender: function (ct, position) {
        Ext.TabPanel.superclass.onRender.call(this, ct, position);
        if (this.plain) {
            var pos = this.tabPosition == 'top' ? 'header' : 'footer';
            this[pos].addClass('x-tab-panel-' + pos + '-plain');
        }
        if (this.mytbar != null) {
            var pos = this.tabPosition == 'top' ? 'header' : 'footer';
            var tr = this.mytbar.el.dom.childNodes[0].rows[0];
            var td = tr.lastChild;

            var tdFirst = tr.firstChild;
            var clientWidth = tr.clientWidth;
            var offsetWidth = 0;
            for (var i = 0, il = tdFirst.children.length; i < il; i++) {
                offsetWidth += tdFirst.children[i].clientWidth;
            }
            tdFirst.style.witdh = offsetWidth + 'px';
            td.style.width = '100%';
            this.headerOffsetWidth = offsetWidth;
            /*
            this[pos].dom.parentNode.removeChild(this[pos].dom);
            td.appendChild(this[pos].dom);
            this[pos].dom.style.width = (clientWidth-this.headerOffsetWidth) + 'px';
            */
            var divPropName = 'mytbardiv';
            this.elements += "," + divPropName;
            this[divPropName + 'Cls'] = "mytbardivcls";
            var v = this.createElement(divPropName, td);
            this[divPropName].dom.style.width = (clientWidth - offsetWidth) + 'px';
            //this[divPropName].dom.style.width = '-10px';
            this[this.stripTarget].setVisible(false);
            this.header.setVisible(false);
            if (this.header.dom) this.header.dom.style.display = 'none';
            this.stripTarget = divPropName;
        }
        var st = this[this.stripTarget];

        this.stripWrap = st.createChild({ cls: 'x-tab-strip-wrap', cn: {
            tag: 'ul', cls: 'x-tab-strip x-tab-strip-' + this.tabPosition
        }
        });

        var beforeEl = (this.tabPosition == 'bottom' ? this.stripWrap : null);
        st.createChild({ cls: 'x-tab-strip-spacer' }, beforeEl);
        this.strip = new Ext.Element(this.stripWrap.dom.firstChild);

        this.edge = this.strip.createChild({ tag: 'li', cls: 'x-tab-edge', cn: [{ tag: 'span', cls: 'x-tab-strip-text', cn: '&#160;'}] });
        this.strip.createChild({ cls: 'x-clear' });

        this.body.addClass('x-tab-panel-body-' + this.tabPosition);

        if (!this.itemTpl) {
            var tt = new Ext.Template(
                '<li class="{cls}" id="{id}"><a class="x-tab-strip-close"></a>',
                '<a class="x-tab-right" href="#"><em class="x-tab-left">',
                '<span class="x-tab-strip-inner"><span class="x-tab-strip-text {iconCls}">{text}</span></span>',
                '</em></a></li>'
                );
            tt.disableFormats = true;
            tt.compile();
            Ext.TabPanel.prototype.itemTpl = tt;
        }
        this.items.each(this.initTab, this);
    }
});
Ext.reg('tabtoolbar', Ext.ux.MyTabPanel);
Ext.ux.TreeComboBox = Ext.extend(Ext.form.ComboBox, {
    extStore: null,
    tree: null,
    treeId: 0,
    initComponent: function () {
        this.treeId = Ext.id();
        this.focusLinkId = Ext.id();
        Ext.apply(this, {
            store: this.store || new Ext.data.SimpleStore({
                fields: [],
                data: [[]]
            }),
            editable: false,
            shadow: false,
            mode: this.store ? 'remote' : 'local',
            triggerAction: 'all',
            maxHeight: 200,
            tpl: '<div style="height:200px"><div id="' + this.treeId + '"></div></div><tpl for="."></tpl>',
            selectedClass: '',
            onSelect: Ext.emptyFn
        });
        var treeConfig = {
            border: false,
            rootVisible: false
        };
        Ext.apply(treeConfig, this.treeConfig);
        if (!treeConfig.root) {
            treeConfig.root = new Ext.tree.AsyncTreeNode({
                text: 'treeRoot',
                id: '0'
            });
        }
        this.tree = new Ext.tree.TreePanel(treeConfig);
        this.on('expand', this.onExpand);
        this.on('collapse', this.onCollapse);
        this.tree.nodeAction = 0;
        this.tree.on('click', this.onClick, this);
        this.tree.on('beforeexpandnode', this.onExpandNode);
        this.tree.on('beforecollapsenode', this.onExpandNode);
        this.treeSorter = new Ext.tree.TreeSorter(this.tree, { dir: "asc" });
        Ext.ux.TreeComboBox.superclass.initComponent.apply(this, arguments);
    },
    onExpandNode: function (node, deep, anim) {
        this.nodeAction = 1;
    },
    onClick: function (node) {
        if (node.attributes.parameter == 9) {
            // 
        } else {
            // node.text
            this.setValue(node.id);
            if (this.hiddenField) {
                this.hiddenField.value = node.id;
            }
            this.tree.nodeAction = 0;
            this.collapse();
        }
    }
    , findNodeByIdInIndex: function (id) {
          var rootNode = this.tree.getRootNode();
          if(rootNode && rootNode.attributes && rootNode.attributes.extraData && rootNode.attributes.extraData.nodesIndex){
              return rootNode.attributes.extraData.nodesIndex[id];
          }
          return null;
      }
    , findNodeById: function (nodeAttr, id, path) {
        if (nodeAttr.id == id) return nodeAttr;
        if(nodeAttr.children==null) return null;
        for (var i = 0, len = nodeAttr.children.length; i < len; i++) {
            if (nodeAttr.children[i].id == id) {
                path.push(nodeAttr.id);
                return nodeAttr.children[i];
            } else {
                var child = this.findNodeById(nodeAttr.children[i], id, path);
                if (child != null) {
                    path.push(nodeAttr.id);
                    return child;
                }
            }
        }
        return null;
    }, onCollapse: function () {
        Logger.message('collapsing');
        if (this.tree.nodeAction == 1) {
            this.expand();
            this.tree.nodeAction = 0;
        }
    }, onExpand: function () {
        var value = this.getValue();
        if (this.tree.rendered && value != null) {
            var rootNode = this.tree.getRootNode();
            var path = [];
            var node = this.tree.getNodeById(value);
            if (node == null) {
                var tnode = this.findNodeById(rootNode.attributes, value, path);
                if (tnode != null) {
                    var cNode = rootNode;
                    for (var i = path.length - 2; i >= 0; i--) {
                        cNode = this.tree.getNodeById(path[i]);
                        cNode.expand(true, false);
                    }
                    node = this.tree.getNodeById(value);
                }
            }
            if (node != null) {
                node.ensureVisible();
                node.expand();
                node.select();
            }
        }
        //this.tree.render(this.treeId);
    }
});
Ext.reg("Ext.ux.treecombobox", Ext.ux.TreeComboBox);

if ((typeof Range !== "undefined") && !Range.prototype.createContextualFragment) {
    Range.prototype.createContextualFragment = function (html) {
        var frag = document.createDocumentFragment(),
		    div = document.createElement("div");
        frag.appendChild(div);
        div.outerHTML = html;
        return frag;
    };
}
/*
Ext.tree.TreeEventModel.prototype.getNode = function (e) {
    var t;
    if (t = e.getTarget('.x-tree-node-el', 10)) {
        //var d = this.dom;
        //return d.getAttributeNS(ns, name) || d.getAttribute(ns + ":" + name) || d.getAttribute(name) || d[name];
        var d1 = Ext.fly(t, '_treeEvents');
        var id = null;
        var d = d1.dom;
        var ns='ext';
        var name='tree-node-id';
        id = Ext.isIE9 ? (d.getAttributeNS(ns, name) || d.getAttribute(ns + ":" + name) || d.getAttribute(name) || d[name]) : d1.getAttribute(name, ns);
        if (id) {
            return this.tree.getNodeById(id);
        }
    }
    return null;
}
*/
if (Ext.isIE9) {
    Ext.Element.prototype.getAttribute = function (name, ns) {
        var d = this.dom;
        return (d.getAttributeNS && d.getAttributeNS(ns, name)) || d.getAttribute(ns + ":" + name) || d.getAttribute(name) || d[name];
    };
}

Ext.ux.XmlReader = Ext.extend(Ext.data.XmlReader, {
        read : function(response){
            var doc = response.responseXML || parseXml(response.responseText);
            if(!doc) {
                throw {message: "XmlReader.read: XML Document not available"};
            }
            return this.readRecords(doc);
        }
    }
);

Ext.ux.JsonReader = Ext.extend(Ext.data.JsonReader, {
    extractValues: function (data, items, len) {
        var f, values = {};
        var dl = data.length;
        for (var j = 0; j < len; j++) {
            f = items[j];
            var v = this.ef[j](data);
            if (v === undefined && dl !== undefined && dl > 0) {
                v = data[j];
            }
            values[f.name] = (v !== undefined) ? v : f.defaultValue;
        }
        return values;
    }
});

Ext.override(Ext.Button, {
    setIcon: function (url) {
        if (this.rendered && url && url!='undefined') {
            var btnEl = this.getEl().child(this.buttonSelector);
            btnEl.setStyle('background-image', 'url(' + url + ')');
        }
    }
});
//LoadJavaScript("/js/DateTimeField.js");
//LoadJavaScript("/js/date-time-ux.js");
/*!
* Copyright (c) 2011 Andrew Pleshkov andrew.pleshkov@gmail.com
* 
* Licensed under the MIT license: http://www.opensource.org/licenses/mit-license.php
*/
Ext.namespace('Ext.ux');
(function () {
    var UX = Ext.ux;
    UX.BaseTimePicker = Ext.extend(Ext.Panel, {
        format: 'H:i:s',
        header: true,
        nowText: 'Текущее время',
        doneText: 'Готово',
        hourIncrement: 1,
        minIncrement: 1,
        hoursLabel: 'Часы',
        minsLabel: 'Минуты',
        cls: 'ux-base-time-picker',
        width: 210,
        layout: 'form',
        labelAlign: 'top',
        initComponent: function () {
            this.addEvents('select');
            this.hourSlider = new Ext.slider.SingleSlider({
                increment: this.hourIncrement,
                minValue: 0,
                maxValue: 23,
                fieldLabel: this.hoursLabel,
                listeners: {
                    change: this._updateTimeValue,
                    scope: this
                },
                plugins: new Ext.slider.Tip()
            });
            this.minSlider = new Ext.slider.SingleSlider({
                increment: this.minIncrement,
                minValue: 0,
                maxValue: 59,
                fieldLabel: this.minsLabel,
                listeners: {
                    change: this._updateTimeValue,
                    scope: this
                },
                plugins: new Ext.slider.Tip()
            });
            this.setCurrentTime(false);
            this._initItems();
            this.bbar = [
                {
                    text: this.nowText,
                    handler: this.setCurrentTime,
                    scope: this
                },
                '->',
                {
                    text: this.doneText,
                    handler: this.onDone,
                    scope: this
                }
            ];
            UX.BaseTimePicker.superclass.initComponent.call(this);
        },
        _initItems: function () {
            this.items = [
                this.hourSlider,
                this.minSlider
            ];
        },
        setCurrentTime: function (animate) {
            this.setValue(new Date(), !!animate);
        },
        onDone: function () {
            this.fireEvent('select', this, this.getValue());
        },
        setValue: function (value, animate) {
            this.hourSlider.setValue(value.getHours(), animate);
            this.minSlider.setValue(value.getMinutes(), animate);

            this._updateTimeValue();
        },
        _extractValue: function () {
            var v = new Date();
            v.setHours(this.hourSlider.getValue());
            v.setMinutes(this.minSlider.getValue());
            return v;
        },
        getValue: function () {
            return this._extractValue();
        },
        _updateTimeValue: function () {
            var v = this._extractValue().format(this.format);
            if (this.rendered) {
                this.setTitle(v);
            }
        },
        afterRender: function () {
            UX.BaseTimePicker.superclass.afterRender.call(this);
            this._updateTimeValue();
        },
        destroy: function () {
            this.purgeListeners();
            this.hourSlider = null;
            this.minSlider = null;
            UX.BaseTimePicker.superclass.destroy.call(this);
        }
    });
    Ext.reg('basetimepicker', UX.BaseTimePicker);
})();
Ext.namespace('Ext.ux');
(function () {
    var UX = Ext.ux;
    UX.ExBaseTimePicker = Ext.extend(Ext.ux.BaseTimePicker, {
        format: 'H:i:s',
        secIncrement: 1,
        secsLabel: 'Секунды',
        initComponent: function () {
            this.secSlider = new Ext.slider.SingleSlider({
                increment: this.secIncrement,
                minValue: 0,
                maxValue: 59,
                fieldLabel: this.secsLabel,
                listeners: {
                    change: this._updateTimeValue,
                    scope: this
                },
                plugins: new Ext.slider.Tip()
            });
            UX.ExBaseTimePicker.superclass.initComponent.call(this);
        },
        _initItems: function () {
            UX.ExBaseTimePicker.superclass._initItems.call(this);
            this.items.push(this.secSlider);
        },
        setValue: function (value, animate) {
            this.secSlider.setValue(value.getSeconds(), animate);
            UX.ExBaseTimePicker.superclass.setValue.call(this, value, animate);
        },
        _extractValue: function () {
            var v = UX.ExBaseTimePicker.superclass._extractValue.call(this);
            v.setSeconds(this.secSlider.getValue());
            return v;
        },
        destroy: function () {
            this.secSlider = null;
            UX.ExBaseTimePicker.superclass.destroy.call(this);
        }
    });
    Ext.reg('exbasetimepicker', UX.ExBaseTimePicker);
})();
Ext.namespace('Ext.ux');
(function () {
    var UX = Ext.ux;
    var CLS = 'ux-date-time-picker';
    UX.DateTimePicker = Ext.extend(Ext.BoxComponent, {
        timeLabel: 'Время',
        changeTimeText: 'Сменить...',
        doneText: 'Готово',
        initComponent: function () {
            UX.DateTimePicker.superclass.initComponent.call(this);
            this.addEvents('select');
            this.timePickerButton = new Ext.Button({
                text: this.changeTimeText,
                handler: this._showTimePicker,
                scope: this
            });
            this._initDatePicker();
            this._initTimePicker();
            this.timeValue = new Date();
            if (this.value) {
                this.setValue(this.value);
                delete this.value;
            }
        },
        _initTimePicker: function () {
            if (!this.timeMenu) {
                var menuConfig = this.initialConfig.timeMenu;
                if (menuConfig && menuConfig.xtype) {
                    this.timeMenu = Ext.create(menuConfig);
                } else {
                    var pickerConfig = this.initialConfig.timePicker || {};
                    if (this.timeFormat) {
                        pickerConfig.format = this.timeFormat;
                    }
                    var picker = Ext.create(pickerConfig, 'basetimepicker');
                    this.timeMenu = new Menu(picker, menuConfig || {});
                }
                if (!Ext.isFunction(this.timeMenu.getPicker)) {
                    throw 'Your time menu must provide the getPicker() method';
                }
                this.timeMenu.on('timeselect', this.onTimeSelect, this);
            }
        },
        _initDatePicker: function () {
            var config = this.initialConfig.datePicker || {};
            config.internalRender = this.initialConfig.internalRender;
            Ext.applyIf(config, {
                format: this.dateFormat || Ext.DatePicker.prototype.format
            });
            var picker = this.datePicker = Ext.create(config, 'datepicker');
            picker.update = picker.update.createSequence(function () {
                if (this.el != null && this.datePicker.rendered) {
                    var width = this.datePicker.el.getWidth();
                    this.el.setWidth(width + this.el.getBorderWidth('lr') + this.el.getPadding('lr'));
                }
            }, this);
        },
        _renderDatePicker: function (ct) {
            var picker = this.datePicker;
            picker.render(ct);
            var bottomEl = picker.getEl().child('.x-date-bottom');
            var size = bottomEl.getSize(true);
            var style = [
                'position: absolute',
                'bottom: 0',
                'left: 0',
                'overflow: hidden',
                'width: ' + size.width + 'px',
                'height: ' + size.height + 'px'
            ].join(';');
            var div = ct.createChild({
                tag: 'div',
                cls: 'x-date-bottom',
                style: style,
                children: [
                    {
                        tag: 'table',
                        cellspacing: 0,
                        style: 'width: 100%',
                        children: [
                            {
                                tag: 'tr',
                                children: [
                                    {
                                        tag: 'td',
                                        align: 'left'
                                    },
                                    {
                                        tag: 'td',
                                        align: 'right'
                                    }
                                ]
                            }
                        ]
                    }
                ]
            });
            if (picker.showToday) {
                var todayConfig = {};
                Ext.each(['text', 'tooltip', 'handler', 'scope'], function (key) {
                    todayConfig[key] = picker.todayBtn.initialConfig[key];
                });
                this.todayBtn = new Ext.Button(todayConfig).render(div.child('td:first'));
            }
            this.doneBtn = new Ext.Button({
                text: this.doneText,
                handler: this.onDone,
                scope: this
            }).render(div.child('td:last'));
        },
        _getFormattedTimeValue: function (date) {
            return date.format(this.timeMenu.picker.format);
        },
        _renderValueField: function (ct) {
            var cls = CLS + '-value-ct';
            var timeLabel = !Ext.isEmpty(this.timeLabel)
                    ? '<span class="' + cls + '-value-label">' + this.timeLabel + ':</span>&nbsp;'
                    : '';
            var div = ct.insertFirst({
                tag: 'div',
                cls: [cls, 'x-date-bottom'].join(' ')
            });
            var table = div.createChild({
                tag: 'table',
                cellspacing: 0,
                style: 'width: 100%',
                children: [
                    {
                        tag: 'tr',
                        children: [
                            {
                                tag: 'td',
                                align: 'left',
                                cls: cls + '-value-cell',
                                html: '<div class="' + cls + '-value-wrap">'
                                        + timeLabel
                                        + '<span class="' + cls + '-value">'
                                        + this._getFormattedTimeValue(this.timeValue)
                                        + '</span>'
                                        + '</div>'
                            },
                            {
                                tag: 'td',
                                align: 'right',
                                cls: cls + '-btn-cell'
                            }
                        ]
                    }
                ]
            });
            this.timeValueEl = table.child('.' + cls + '-value');
            this.timeValueEl.on('click', this._showTimePicker, this);
            this.timePickerButton.render(table.child('td:last'));
        },
        onRender: function (ct, position) {
            this.el = ct.createChild({
                tag: 'div',
                cls: CLS,
                children: [
                    {
                        tag: 'div',
                        cls: CLS + '-inner'
                    }
                ]
            }, position);
            UX.DateTimePicker.superclass.onRender.call(this, ct, position);
            var innerEl = this.el.first();
            this._renderDatePicker(innerEl);
            this._renderValueField(innerEl);
        },
        _updateTimeValue: function (date) {
            this.timeValue = date;
            if (this.timeValueEl != null) {
                this.timeValueEl.update(this._getFormattedTimeValue(date));
            }
        },
        setValue: function (value) {
            this._updateTimeValue(value);
            this.datePicker.setValue(value.clone());
        },
        getValue: function () {
            var date = this.datePicker.getValue();
            var time = this.timeValue.getElapsed(this.timeValue.clone().clearTime());
            return new Date(date.getTime() + time);
        },
        onTimeSelect: function (menu, picker, value) {
            this._updateTimeValue(value);
        },
        _showTimePicker: function () {
            this.timeMenu.getPicker().setValue(this.timeValue, false);
            if (this.timeMenu.isVisible()) {
                this.timeMenu.hide();
            } else {
                this.timeMenu.show(this.timePickerButton.el, null, this.parentMenu);
            }
        },
        onDone: function () {
            this.fireEvent('select', this, this.getValue());
        },
        destroy: function () {
            Ext.destroy(this.timePickerButton);
            this.timePickerButton = null;
            if (this.timeValueEl) {
                this.timeValueEl.remove();
                this.timeValueEl = null;
            }
            Ext.destroy(this.datePicker);
            this.datePicker = null;
            if (this.timeMenu) {
                Ext.destroy(this.timeMenu);
                this.timeMenu = null;
            }
            if (this.todayBtn) {
                Ext.destroy(this.todayBtn);
                this.todayBtn = null;
            }
            if (this.doneBtn) {
                Ext.destroy(this.doneBtn);
                this.doneBtn = null;
            }
            this.parentMenu = null;
            UX.DateTimePicker.superclass.destroy.call(this);
        }
    });
    Ext.reg('datetimepicker', UX.DateTimePicker);
    //
    var Menu = UX.DateTimePicker.Menu = Ext.extend(Ext.menu.Menu, {
        enableScrolling: false,
        hideOnClick: false,
        plain: true,
        showSeparator: false,
        constructor: function (picker, config) {
            config = config || {};
            if (config.picker) {
                delete config.picker;
            }
            this.picker = Ext.create(picker);
            Menu.superclass.constructor.call(this, Ext.applyIf({
                items: this.picker
            }, config));
            this.addEvents('timeselect');
            this.picker.on('select', this.onTimeSelect, this);
        },
        getPicker: function () {
            return this.picker;
        },
        onTimeSelect: function (picker, value) {
            this.hide();
            this.fireEvent('timeselect', this, picker, value);
        },
        destroy: function () {
            this.purgeListeners();
            this.picker = null;
            Menu.superclass.destroy.call(this);
        }
    });
})(); Ext.namespace('Ext.ux.menu');
(function () {
    var M = Ext.ux.menu;
    var isStrict = Ext.isIE7 && Ext.isStrict;
    M.DateTimeMenu = Ext.extend(Ext.menu.Menu, {
        enableScrolling: false,
        plain: true,
        showSeparator: false,
        hideOnClick: true,
        pickerId: null,
        cls: 'x-date-menu x-date-time-menu',
        constructor: function (config) {
            this.picker = this._createPicker(config || {});
            delete config.picker;
            M.DateTimeMenu.superclass.constructor.call(this, Ext.applyIf({
                items: this.picker
            }, config || {}));
            this.picker.parentMenu = this;
            this.on('beforeshow', this.onBeforeShow, this);
            if (isStrict) {
                this.on('show', this.onShow, this, { single: true, delay: 20 });
            }
            this.relayEvents(this.picker, ['select']);
            this.on('show', this.picker.focus, this.picker);
            this.on('select', this.menuHide, this);
            if (this.handler) {
                this.on('select', this.handler, this.scope || this);
            }
        },
        _createPicker: function (initialConfig) {
            var picker = initialConfig.picker;
            var defaultConfig = {
                ctCls: 'x-menu-date-item',
                internalRender: isStrict || !Ext.isIE
            };
            if (typeof picker === 'object') {
                if (picker.render) {
                    return picker;
                } else {
                    return Ext.create(Ext.apply(defaultConfig, picker), 'datetimepicker');
                }
            } else {
                return Ext.create(defaultConfig, 'datetimepicker');
            }
        },
        menuHide: function () {
            if (this.hideOnClick) {
                this.hide(true);
            }
        },
        onBeforeShow: function () {
            if (this.picker.datePicker) {
                this.picker.datePicker.hideMonthPicker(true);
            }
        },
        onShow: function () {
            var el = this.picker.datePicker.getEl();
            el.setWidth(el.getWidth()); // nasty hack for IE7 strict mode
        },
        destroy: function () {
            this.picker.destroy();
            this.picker = null;

            M.DateTimeMenu.superclass.destroy.call(this);
        }
    });
    Ext.reg('datetimemenu', M.DateTimeMenu);
})();
Ext.namespace('Ext.ux.form');
(function () {
    var UX = Ext.ux;
    var F = UX.form;
    F.DateTimeField = Ext.extend(Ext.form.DateField, {
        timeFormat: 'H:i:s',
        defaultAutoCreate: {
            tag: 'input',
            type: 'text',
            size: '22',
            autocomplete: 'off'
        },
        initComponent: function () {
            F.DateTimeField.superclass.initComponent.call(this);
            this.dateFormat = this.dateFormat || this.format;
            this.format = this.dateFormat + ' ' + this.timeFormat;
            var pickerConfig = Ext.apply(this.picker || {}, {
                dateFormat: this.dateFormat,
                timeFormat: this.timeFormat
            });
            delete this.picker;
            delete this.initialConfig.picker;
            this.menu = new UX.menu.DateTimeMenu({
                picker: pickerConfig,
                hideOnClick: false
            });
        },
        onTriggerClick: function () {
            F.DateTimeField.superclass.onTriggerClick.call(this);
            this.menu.picker.setValue(this.getValue() || new Date());
        }
    });
    Ext.reg('datetimefield', F.DateTimeField);

    Ext.ns("Ext.net", "Ext.net.DirectMethods", "Ext.ux", "Ext.ux.plugins", "Ext.ux.layout");
// @source core/buttons/ImageButton.js
    Ext.net.ImageButton = Ext.extend(Ext.Button, {
                buttonSelector : "img",
                cls : "",
                iconAlign : "left",
                initComponent : function () {
                    Ext.net.ImageButton.superclass.initComponent.call(this);
                    var i;
                    if (this.imageUrl) {
                        i = new Ext.net.Image().src = this.imageUrl;
                    }
                    if (this.overImageUrl) {
                        i = new Ext.net.Image().src = this.overImageUrl;
                    }
                    if (this.disabledImageUrl) {
                        i = new Ext.net.Image().src = this.disabledImageUrl;
                    }
                    if (this.pressedImageUrl) {
                        i = new Ext.net.Image().src = this.pressedImageUrl;
                    }
                },
                onRender : function (ct, position) {
                    if (!this.el) {
                        var img = document.createElement("img");
                        img.id = this.getId();
                        img.src = this.imageUrl;
                        img.style.border = "none";
                        img.style.cursor = "pointer";
                        this.imgEl = Ext.get(img);
                        this.el = this.imgEl;
                        if (!Ext.isEmpty(this.imgEl.getAttributeNS("", "width"), false) ||
                                !Ext.isEmpty(this.imgEl.getAttributeNS("", "height"), false)) {
                            img.removeAttribute("width");
                            img.removeAttribute("height");
                        }
                        if (this.altText) {
                            img.setAttribute("alt", this.altText);
                        }
                        if (this.align && this.align !== "notset") {
                            img.setAttribute("align", this.align);
                        }
                        if (this.pressed && this.pressedImageUrl) {
                            img.src = this.pressedImageUrl;
                        }
                        if (this.disabled && this.disabledImageUrl) {
                            img.src = this.disabledImageUrl;
                        }
                        if (this.tabIndex !== undefined) {
                            img.tabIndex = this.tabIndex;
                        }
                        if (this.menu) {
                            this.menu.on("show", this.onMenuShow, this);
                            this.menu.on("hide", this.onMenuHide, this);
                        }
                        if (this.repeat) {
                            var repeater = new Ext.util.ClickRepeater(this.imgEl,
                                    typeof this.repeat == "object" ? this.repeat : {}
                            );
                            repeater.on("click", this.onClick, this);
                        }
                        this.imgEl.on(this.clickEvent, this.onClick, this);
                        if (this.handleMouseEvents) {
                            this.imgEl.on("mouseover", this.onMouseOver, this);
                            this.imgEl.on("mousedown", this.onMouseDown, this);
                        }
                        if (!Ext.isEmpty(this.cls, false)) {
                            this.el.dom.className = this.cls;
                        }
                        Ext.BoxComponent.superclass.onRender.call(this, ct, position);
                    }
                    if (this.tooltip) {
                        if (typeof this.tooltip == "object") {
                            Ext.QuickTips.register(Ext.apply({
                                        target : this.imgEl.id
                                    }, this.tooltip));
                        } else {
                            this.imgEl.dom[this.tooltipType] = this.tooltip;
                        }
                    }
                    Ext.ButtonToggleMgr.register(this);
                },
                afterRender : function () {
                    Ext.Button.superclass.afterRender.call(this);
                    this.doc = Ext.getDoc();
                },
// private
                onMenuShow : function (e) {
                    this.ignoreNextClick = 0;
                    this.fireEvent("menushow", this, this.menu);
                },
// private
                onMenuHide : function (e) {
                    this.ignoreNextClick = this.restoreClick.defer(250, this);
                    this.fireEvent("menuhide", this, this.menu);
                },
                toggle : function (state) {
                    state = state === undefined ? !this.pressed : state;
                    if (state != this.pressed) {
                        if (state) {
                            if (this.pressedImageUrl) {
                                this.imgEl.dom.src = this.pressedImageUrl;
                            }
                            this.pressed = true;
                            this.fireEvent("toggle", this, true);
                        } else {
                            this.imgEl.dom.src = (this.monitoringMouseOver) ? this.overImageUrl : this.imageUrl;
                            this.pressed = false;
                            this.fireEvent("toggle", this, false);
                        }
                        if (this.toggleHandler) {
                            this.toggleHandler.call(this.scope || this, this, state);
                        }
                    }
                },
                setText : function (t, encode) { },
                setDisabled : function (disabled) {
                    this.disabled = disabled;
                    if (this.imgEl && this.imgEl.dom) {
                        this.imgEl.dom.disabled = disabled;
                    }
                    if (disabled) {
                        if (this.disabledImageUrl) {
                            this.imgEl.dom.src = this.disabledImageUrl;
                        } else {
                            this.imgEl.addClass(this.disabledClass);
                        }
                    } else {
                        this.imgEl.dom.src = this.imageUrl;
                        this.imgEl.removeClass(this.disabledClass);
                    }
                },
// private
                onMouseOver : function (e) {
                    if (!this.disabled) {
                        var internal = e.within(this.el.dom, true);
                        if (!internal) {
                            if (this.overImageUrl && !this.pressed) {
                                this.imgEl.dom.src = this.overImageUrl;
                            }
                            if (!this.monitoringMouseOver) {
                                Ext.getDoc().on("mouseover", this.monitorMouseOver, this);
                                this.monitoringMouseOver = true;
                            }
                        }
                    }
                    this.fireEvent("mouseover", this, e);
                },
// private
                onMouseOut : function (e) {
                    if (!this.disabled && !this.pressed) {
                        this.imgEl.dom.src = this.imageUrl;
                    }
                    this.fireEvent("mouseout", this, e);
                },
                onMouseDown : function (e) {
                    if (!this.disabled && e.button === 0) {
                        if (this.pressedImageUrl) {
                            this.imgEl.dom.src = this.pressedImageUrl;
                        }
                        Ext.getDoc().on("mouseup", this.onMouseUp, this);
                    }
                },
// private
                onMouseUp : function (e) {
                    if (e.button === 0) {
                        this.imgEl.dom.src = (this.overImageUrl && this.monitoringMouseOver) ? this.overImageUrl : this.imageUrl;
                        Ext.getDoc().un("mouseup", this.onMouseUp, this);
                    }
                },
                setImageUrl : function (image) {
                    this.imageUrl = image;
                    if ((!this.disabled || Ext.isEmpty(this.disabledImageUrl, false)) &&
                            (!this.pressed || Ext.isEmpty(this.pressedImageUrl, false))) {
                        this.imgEl.dom.src = image;
                    } else {
                        new Image().src = image;
                    }
                },
                setDisabledImageUrl : function (image) {
                    this.disabledImageUrl = image;
                    if (this.disabled) {
                        this.imgEl.dom.src = image;
                    } else {
                        new Image().src = image;
                    }
                },
                setOverImageUrl : function (image) {
                    this.overImageUrl = image;
                    if ((!this.disabled || Ext.isEmpty(this.disabledImageUrl, false)) &&
                            this.monitoringMouseOver &&
                            (!this.pressed || Ext.isEmpty(this.pressedImageUrl, false))) {
                        this.imgEl.dom.src = image;
                    } else {
                        new Image().src = image;
                    }
                },
                setPressedImageUrl : function (image) {
                    this.pressedImageUrl = image;
                    if ((!this.disabled || Ext.isEmpty(this.disabledImageUrl, false)) && this.pressed) {
                        this.imgEl.dom.src = image;
                    } else {
                        new Image().src = image;
                    }
                },
                setAlign : function (align) {
                    this.align = align;
                    if (this.rendered) {
                        this.imgEl.dom.setAttribute("align", this.align);
                    }
                },
                setAltText : function (altText) {
                    this.altText = altText;
                    if (this.rendered) {
                        this.imgEl.dom.setAttribute("altText", this.altText);
                    }
                }
            });
    Ext.reg("netimagebutton", Ext.net.ImageButton);
// @source core/buttons/LinkButton.js
    Ext.net.LinkButton = Ext.extend(Ext.Button, {
                buttonSelector : "a:first",
                cls : "",
                iconAlign : "left",
                valueElement : function () {
                    var textEl = document.createElement("a");
                    textEl.style.verticalAlign = "middle";
                    textEl.id = Ext.id();
                    if (!Ext.isEmpty(this.cls, false)) {
                        textEl.className = this.cls;
                    }
                    textEl.setAttribute("href", "#");
                    if (this.disabled || this.pressed) {
                        textEl.setAttribute("disabled", "1");
                        textEl.removeAttribute("href");
                        if (this.pressed && this.allowDepress !== false) {
                            textEl.style.cursor = "pointer";
                        }
                    }
                    if (this.tabIndex !== undefined) {
                        textEl.tabIndex = this.tabIndex;
                    }
                    if (this.tooltip) {
                        if (typeof this.tooltip == "object") {
                            Ext.QuickTips.register(Ext.apply({
                                        target : textEl.id
                                    }, this.tooltip));
                        } else {
                            textEl[this.tooltipType] = this.tooltip;
                        }
                    }
                    textEl.innerHTML = this.text;
                    var txt = Ext.get(textEl);
                    if (this.menu) {
                        this.menu.on("show", this.onMenuShow, this);
                        this.menu.on("hide", this.onMenuHide, this);
                    }
                    if (this.repeat) {
                        var repeater = new Ext.util.ClickRepeater(txt,
                                typeof this.repeat == "object" ? this.repeat : {}
                        );
                        repeater.on("click", this.onClick, this);
                    }
                    txt.on(this.clickEvent, this.onClick, this);
                    this.textEl = textEl;
                    return this.textEl;
                },
// private
                onMenuShow : function (e) {
                    this.ignoreNextClick = 0;
                    this.fireEvent("menushow", this, this.menu);
                },
// private
                onMenuHide : function (e) {
                    this.ignoreNextClick = this.restoreClick.defer(250, this);
                    this.fireEvent("menuhide", this, this.menu);
                },
                toggle : function (state) {
                    state = state === undefined ? !this.pressed : state;
                    if (state != this.pressed) {
                        if (state) {
                            this.setDisabled(true);
                            this.disabled = false;
                            this.pressed = true;
                            if (this.allowDepress !== false) {
                                this.textEl.style.cursor = "pointer";
                                this.el.dom.style.cursor = "pointer";
                            }
                            this.fireEvent("toggle", this, true);
                        } else {
                            this.setDisabled(false);
                            this.pressed = false;
                            this.fireEvent("toggle", this, false);
                        }
                        if (this.toggleHandler) {
                            this.toggleHandler.call(this.scope || this, this, state);
                        }
                    }
                },
                onRender : function (ct, position) {
                    if (!this.el) {
                        var el = document.createElement("span");
                        el.id = this.getId();
                        var img = document.createElement("img");
                        img.src = Ext.BLANK_IMAGE_URL;
                        img.className = "x-label-icon " + (this.iconCls || "");
                        if (Ext.isEmpty(this.iconCls)) {
                            img.style.display = "none";
                        }
                        if (this.iconAlign == "left") {
                            el.appendChild(img);
                        }
                        el.appendChild(this.valueElement());
                        if (this.iconAlign == "right") {
                            el.appendChild(img);
                        }
                        this.el = el;
                        Ext.BoxComponent.superclass.onRender.call(this, ct, position);
                    }
                    if (this.pressed && this.allowDepress !== false) {
                        this.setDisabled(true);
                        this.disabled = false;
                        this.el.dom.style.cursor = "pointer";
                    }
                    Ext.ButtonToggleMgr.register(this);
                },
                setText : function (t, encode) {
                    this.text = t;
                    if (this.rendered) {
                        this.textEl.innerHTML = encode !== false ? Ext.util.Format.htmlEncode(t) : t;
                    }
                    return this;
                },
                setIconClass : function (cls) {
                    var oldCls = this.iconCls;
                    this.iconCls = cls;
                    if (this.rendered) {
                        var img = this.el.child("img.x-label-icon");
                        img.replaceClass(oldCls, this.iconCls);
                        img.dom.style.display = (cls === "") ? "none" : "inline";
                    }
                },
                setDisabled : function (disabled) {
                    Ext.net.LinkButton.superclass.setDisabled.apply(this, arguments);
                    if (disabled) {
                        this.textEl.setAttribute("disabled", "1");
                        this.textEl.removeAttribute("href");
                    } else {
                        this.textEl.removeAttribute("disabled");
                        this.textEl.setAttribute("href", "#");
                    }
                }
            });
    Ext.tree.TreeEventModel.prototype.onIconOut = function(e, node){
        if(node.ui) node.ui.removeClass('x-tree-ec-over');
    }
})();


}
