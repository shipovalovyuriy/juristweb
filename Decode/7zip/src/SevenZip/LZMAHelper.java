package SevenZip;

import java.io.InputStream;
import java.io.OutputStream;

public class LZMAHelper {

    public static void Decompress(InputStream in, OutputStream out) throws Exception {
        int propertiesSize = 5;
        byte[] properties = new byte[propertiesSize];
        if (in.read(properties, 0, propertiesSize) != propertiesSize) {
            throw new Exception("input .lzma file is too short");
        }

        SevenZip.Compression.LZMA.Decoder decoder = new SevenZip.Compression.LZMA.Decoder();
        if (!decoder.SetDecoderProperties(properties)) {
            throw new Exception("Incorrect stream properties");
        }
        long outSize = 0;
        for (int i = 0; i < 8; i++) {
            int v = in.read();
            if (v < 0) {
                throw new Exception("Can't read stream size");
            }
            outSize |= ((long) v) << (8 * i);
        }
        if (!decoder.Code(in, out, outSize)) {
            throw new Exception("Error in data stream");
        }

        out.flush();
        out.close();
        in.close();
    }
}
