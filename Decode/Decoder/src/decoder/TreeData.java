package decoder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Malik.N
 * Date: 27.02.14
 * Time: 13:49
 * Hierarchical data
 */
public class TreeData {
    public long id;
    public long parent_id;
    List<TreeData> children = new ArrayList<TreeData>();
    String data;
    String data2;
    int data3;
}
