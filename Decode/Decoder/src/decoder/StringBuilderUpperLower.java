package decoder;

/**
 * Created with IntelliJ IDEA.
 * User: Malik.N
 * Date: 08.02.13
 * Time: 13:21
 * To change this template use File | Settings | File Templates.
 */
public class StringBuilderUpperLower {
    public static StringBuilder toLowerCase(StringBuilder builder){
        int len = builder.length();
        for (int i = 0; i < len; i++) {
            builder.setCharAt(i, Character.toLowerCase(builder.charAt(i)));
        }
        return builder;
    }
    public static StringBuilder toUpperCase(StringBuilder builder){
        int len = builder.length();
        for (int i = 0; i < len; i++) {
            builder.setCharAt(i, Character.toUpperCase(builder.charAt(i)));
        }
        return builder;
    }
}
