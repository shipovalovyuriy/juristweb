/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package decoder;

import SevenZip.LZMAHelper;

import java.io.*;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author SDenis
 * @author Malik.N
 */
@SuppressWarnings({"ResultOfMethodCallIgnored"})
public class Main {
    String exportDirectory = "";
    String encoding = "utf-8";
    Connection conn;
    Map<Long, Attachment> attachments = new HashMap<Long, Attachment>();
    Map<Long, TreeData> trees;
    List<TreeData> rootNodes;

    private static class ClassificatorInfo {
        String name;
        int idx = 0;
        OutputStream out;
        //docid -> tree_id[]
        Map<Long, List<String>> docs = new HashMap<Long, List<String>>();

        private ClassificatorInfo(String name) {
            this.name = name;
        }
    }

    ClassificatorInfo[] docs_tree = new ClassificatorInfo[]{
            new ClassificatorInfo("Number"),//0 Codes -> номера документов
            new ClassificatorInfo("CLASSES"),//1 Классы
            new ClassificatorInfo("ORGANS"),//2 Источники
            new ClassificatorInfo("KIND"),//3 Типы
            new ClassificatorInfo("SITUATION"),//9 Ситуации
    };

    private static void closeObject(AutoCloseable closeable) {
        try {
            if (closeable != null) {
                closeable.close();
            }
        } catch (Exception ignored) {
        }
    }

    public static void extractDocuments(Connection conn) throws Exception {
        Statement stmt = null;
        ResultSet rs = null;
        try {
            int i = 0;
            int j = 0;
            ByteArrayOutputStream bs = new ByteArrayOutputStream();
            stmt = conn.createStatement();
            //stmt.setFetchSize(Integer.MIN_VALUE);
            // Выберем документ с id = 840
            rs = stmt.executeQuery("SELECT id FROM documents where id not in(select docid from document_body); ");
            while (rs.next()) {
                Integer id = rs.getInt(1);
                Statement stmt2 = conn.createStatement();
                ResultSet rs2 = stmt2.executeQuery("SELECT body FROM documents where id =" + id.toString() + "; ");
                //FileOutputStream fos = new FileOutputStream("840.html");
                if (rs2.next()) {
                    bs.reset();
                    LZMAHelper.Decompress(rs2.getBinaryStream(1), bs);
                    String val = bs.toString("utf-8");
                    //System.out.println(val);
                    PreparedStatement ps = conn.prepareStatement("insert into document_body (docid,body) values(?,?); ");
                    ps.setInt(1, id);
                    StringReader r = new StringReader(val);
                    ps.setClob(2, r);
                    ps.executeUpdate();
                    closeObject(r);
                    closeObject(ps);
                }
                closeObject(rs2);
                closeObject(stmt2);
                //fos.close();
                j++;
                if (++i == 1000) {
                    System.out.println("Documents saved " + j);
                    i = 0;
                    System.gc();
                }
            }
            closeObject(bs);
            System.out.println("Done");

//            // Выберем приложение, например, с id = 595966541
//            rs = stmt.executeQuery("SELECT name, ext, data "
//                    + "FROM attachments WHERE id = 595966541; ");
//            if (rs.next()) {
//                String fileName = String.format("%s.%s", rs.getString("name"),
//                        rs.getString("ext"));
//                FileOutputStream fos = new FileOutputStream(fileName);
//
//                LZMAHelper.Decompress(rs.getBinaryStream("data"), fos);
//                fos.close();
//                System.out.println("Приложение сохранено в файл " + fileName);
//            }
//            rs.close();

        } finally {
            closeObject(rs);
            closeObject(stmt);
        }
    }

    public static void searchDocumentsBrokenLinks(Connection conn) throws Exception {
        String regex = "<a\\s[^>]*href\\s*=\\s*[\"\']?([^\"\' ]*)[\"\']?[^>]*>(.*)</a>";
        Pattern p = Pattern.compile(regex);

        Statement stmt = null;
        ResultSet rs = null;
        try {
            int i = 0;
            HashSet<Long> allIds = getAllDocumentIdentifiers(conn, false);
            ByteArrayOutputStream bs = new ByteArrayOutputStream();
            stmt = conn.createStatement();
            // Выберем документ с id = 840
            rs = stmt.executeQuery("SELECT id FROM documents; ");
            String link;
            String[] linkPref = new String[]{"aps://", "apsfile://", "http://"};
            StringBuilder builder = new StringBuilder(1024 * 1024 * 10);
            long lastDocId = 0;
            while (rs.next()) {
                Integer id = rs.getInt(1);
                Statement stmt2 = conn.createStatement();
                ResultSet rs2 = stmt2.executeQuery("SELECT body FROM documents where id =" + id.toString() + "; ");
                //FileOutputStream fos = new FileOutputStream("840.html");
                if (rs2.next()) {
                    bs.reset();
                    LZMAHelper.Decompress(rs2.getBinaryStream(1), bs);
                    builder.setLength(0);
                    String val = bs.toString("utf-8");
                    builder.append(val);
                    StringBuilderUpperLower.toLowerCase(builder);
                    //System.out.println(val);
                    Matcher m = p.matcher(builder);
                    while (m.find()) {
                        // making case inSensitive
                        link = m.group(1);
                        //removing http and other headers
                        for (String pref : linkPref) {
                            if (link.indexOf(pref) == 0) {
                                link = link.substring(pref.length());
                            }
                        }
                        // removing bookmarks
                        if (link.contains("#")) {
                            link = link.substring(0, link.indexOf("#"));
                        }
                        if (link.contains("/")) {
                            link = link.substring(0, link.indexOf("/"));
                        }
                        // check if its a number, then add to list
                        try {
                            Long newDocId = Long.parseLong(link);
                            if (!allIds.contains(newDocId) && lastDocId != newDocId) {
                                System.out.println(String.format("%d\t\t%d", id, newDocId));
                            }
                            lastDocId = newDocId;
                            //links.add(link);
                        } catch (NumberFormatException e) {
                            // seems external link
                        }
                    }
                }
                closeObject(rs2);
                closeObject(stmt2);
                //fos.close();
                if (++i == 1000) {
                    i = 0;
                    System.gc();
                }
            }
            closeObject(bs);
        } finally {
            closeObject(rs);
            closeObject(stmt);
        }
    }

    private static HashSet<Long> getAllDocumentIdentifiers(Connection conn, boolean ignoreAttachments) throws SQLException {
        int allDocCount = 0;
        // reserving space
        {
            Statement stmt2 = null;
            ResultSet rs2 = null;
            try {
                stmt2 = conn.createStatement();
                String countSql = ignoreAttachments ? "SELECT count(id) cnt FROM documents" : "select sum(cnt) as cnt from ( SELECT count(id) cnt FROM documents union all SELECT count(id) FROM attachments) t";
                rs2 = stmt2.executeQuery(countSql);
                if (rs2.next()) {
                    allDocCount = rs2.getInt(1);
                }
            } catch (Exception ignored) {

            } finally {
                closeObject(rs2);
                closeObject(stmt2);
            }
        }
        HashSet<Long> allIds = new HashSet<Long>(allDocCount);
        // gathering all Ids
        {
            Statement stmt2 = null;
            ResultSet rs2 = null;
            try {
                stmt2 = conn.createStatement();
                String sql = ignoreAttachments ? "SELECT id FROM documents" : "SELECT id FROM documents union all select id from attachments";
                rs2 = stmt2.executeQuery(sql);
                while (rs2.next()) {
                    Long id = rs2.getLong(1);
                    allIds.add(id);
                }

            } catch (Exception ignored) {

            } finally {
                closeObject(rs2);
                closeObject(stmt2);
            }

        }
        return allIds;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        String dbname = "sps2";
        String uname = "test";
        String pwd = "test";
        String host = "localhost";
        if (args.length > 2) {
            dbname = args[0];
            uname = args[1];
            pwd = args[2];
        }
        if (args.length > 3) {
            host = args[3];
        }
        boolean searchBrokenLinks = args.length > 4 && "broken".equals(args[4]);
        String exportDocDir = args.length > 5 && "export".equals(args[4]) ? args[5] : "";
        String encoding = "";
        if (args.length > 6) {
            encoding = args[6];
        }

        Connection conn = DriverManager.getConnection("jdbc:mysql://" + host + "/" + dbname + "?" + "user=" + uname + "&password=" + pwd);
        try {
            if (searchBrokenLinks) {
                searchDocumentsBrokenLinks(conn);
            } else if (!"".equals(exportDocDir)) {
                Main obj = new Main();
                obj.exportDirectory = exportDocDir;
                obj.conn = conn;
                if (!"".equals(encoding)) {
                    obj.encoding = encoding;
                }
                obj.exportDocumentsToDir();
            } else {
                extractDocuments(conn);
            }
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
            ex.printStackTrace();

        } finally {
            closeObject(conn);
        }
    }

    public void writeln(OutputStream out, String msg) throws IOException {
        writeln(out, msg, false);
    }

    public void writeln(OutputStream out, String msg, boolean escapeChars) throws IOException {
        if (escapeChars) {
            msg = msg.replace("\r", "").replace("\n", "");
        }
        out.write(msg.getBytes(encoding));
        out.write("\r\n".getBytes(encoding));
    }

    private void exportDocumentsToDir() {
        new File(exportDirectory).mkdirs();
        exportDictionaries();
        exportAttachments();
        exportDocumentProperties();
        exportDocuments();
    }


    private void exportAttachments() {
        System.out.println("exporting attachments ");
        Statement stmt2 = null;
        ResultSet rs2 = null;
        //ByteArrayOutputStream bs = new ByteArrayOutputStream();
        try {
            //stmt2 = conn.createStatement();
            stmt2 = conn.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
            stmt2.setFetchSize(Integer.MIN_VALUE);
            rs2 = stmt2.executeQuery("SELECT * from attachments");
            while (rs2.next()) {
                Long id = rs2.getLong(1);
                Attachment att = new Attachment();
                att.id = id;
                att.desc = rs2.getString("description");
                att.name = rs2.getString("name");
                String dir = String.format("src/%d", id);
                String ext = rs2.getString("ext");
                if(ext != null && !"".equals(ext) && att.name.toLowerCase().endsWith(ext.toLowerCase())){
                    att.file_name = dir + String.format("/%s.%s", att.name, ext);
                }else{
                    att.file_name = dir + String.format("/%s", att.name);
                }
                File file = new File(exportDirectory + "/" + att.file_name);
                file.getParentFile().mkdirs();
                if (!file.exists()) {
                    FileOutputStream fos = new FileOutputStream(file);
                    try {
                        LZMAHelper.Decompress(rs2.getBinaryStream("data"), fos);
                    } catch (Exception e) {
                        System.out.println(String.format(" error in %d message %s", att.id, e.getMessage()));
                    }
                    closeObject(fos);
                }
                attachments.put(id, att);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeObject(rs2);
            closeObject(stmt2);
        }

    }

    private interface QueryInterface {
        public String getString(ResultSet rs, String val) throws SQLException;

        public void onFinish(OutputStream out);
    }

    //справочники
    private void exportDictionary(String query, String name, String fldPreview, String fldId, QueryInterface callback) {
        System.out.println("exporting dictionary " + name);
        Statement stmt2 = null;
        ResultSet rs2 = null;
        try {
            stmt2 = conn.createStatement();
            rs2 = stmt2.executeQuery(query);
            FileOutputStream out = new FileOutputStream(exportDirectory + "/" + name);
            int sortOrder = -1;
            if (!"".equals(fldId)) {
                ResultSetMetaData md = rs2.getMetaData();
                for (int i = 1; i <= md.getColumnCount(); i++) {
                    if ("sort_order".equals(md.getColumnName(i))) {
                        sortOrder = i;
                        break;
                    }
                }
            }
            while (rs2.next()) {
                String val = rs2.getString(fldPreview);
                if (callback != null) {
                    val = callback.getString(rs2, val);
                }
                if (!"".equals(fldId)) {
                    val += String.format(" <SORTORDER=\"%s\" ID=\"%s\">", sortOrder > 0 ? rs2.getString(sortOrder) : "", rs2.getString(fldId));
                }
                if (!"".equals(val)) {
                    writeln(out, val, true);
                }
            }
            if (callback != null) {
                callback.onFinish(out);
            }
            closeObject(out);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeObject(rs2);
            closeObject(stmt2);
        }
    }

    private void exportDictionaries() {
        exportDictionary("select * from definitions order by word", "definitions.txt", "word", "id", null);
        exportDictionary("select * from classificators order by name", "classificators.txt", "name", "id", null);
        exportDictionary("select * from priorities order by name", "priorities.txt", "name", "priority", null);
        exportDictionary("select * from terms order by word", "terms.txt", "word", "id", new QueryInterface() {
            @Override
            public void onFinish(OutputStream out) {

            }

            @Override
            public String getString(ResultSet rs, String val) throws SQLException {
                return val + " " + rs.getString("definition").replaceAll("\r\n", " ");
            }
        });
        exportTrees();
    }

    private void treeExportNode(TreeData node, String offset, OutputStream out, String idx, String idxClass) {
        int class_id = node.data3;
        final String format = "%s%s %s <SORTORDER=\"%s\" ID=\"%d\" PARENT=\"%d\" CLASS=\"%d\">\r\n";
        String val = String.format(format, offset, idx, node.data, node.data2, node.id, node.parent_id, class_id);
        try {
            byte[] data = val.getBytes(encoding);
            out.write(data);
            if (class_id > 0 && class_id < docs_tree.length && docs_tree[class_id] != null && docs_tree[class_id].out != null) {
                data = String.format(format, offset, idxClass, node.data, node.data2, node.id, node.parent_id, class_id).getBytes(encoding);
                docs_tree[class_id].out.write(data);
            }
            List<TreeData> children = node.children;
            for (int i = 0, childrenSize = children.size(); i < childrenSize; i++) {
                TreeData child = children.get(i);
                treeExportNode(child, offset + "  ", out, idx + "." + (i + 1), idxClass + "." + (i + 1));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void onTreeLoaded(OutputStream out) throws IOException {
        // exclude Number node
        for (int i1 = 1, docs_treeLength = docs_tree.length; i1 < docs_treeLength; i1++) {
            ClassificatorInfo classificatorInfo = docs_tree[i1];
            if (classificatorInfo != null && null != classificatorInfo.name) {
                classificatorInfo.out = new FileOutputStream(exportDirectory + "/" + classificatorInfo.name + ".txt");
            }
        }
        Collection<TreeData> nodes = trees.values();
        // получили отстортированный список узлов дерева
        for (TreeData node : nodes) {
            if (node.parent_id == 0) continue;
            TreeData parent = trees.get(node.parent_id);
            if (parent != null) {
                parent.children.add(node);
            }
        }

        for (int i = 0, rootNodesSize = rootNodes.size(); i < rootNodesSize; i++) {
            TreeData rootNode = rootNodes.get(i);
            ClassificatorInfo info = rootNode.data3 >= docs_tree.length ? null : docs_tree[rootNode.data3];
            treeExportNode(rootNode, "", out, String.valueOf(i + 1), info == null ? "" : String.valueOf(++info.idx));
        }
        for (ClassificatorInfo classificatorInfo : docs_tree) {
            if (classificatorInfo != null && null != classificatorInfo.out) {
                closeObject(classificatorInfo.out);

            }
        }
    }

    private void exportTrees() {
        trees = new HashMap<Long, TreeData>();
        rootNodes = new ArrayList<TreeData>();
        exportDictionary("select * from trees t order by t.parent_id,sort_index,name",
                "trees.txt",
                "name", "", new QueryInterface() {
            @Override
            public String getString(ResultSet rs, String val) throws SQLException {
                TreeData data = new TreeData();
                data.id = rs.getLong("id");
                data.parent_id = rs.getLong("parent_id");
                data.data = rs.getString("name");
                if (data.data != null) {
                    data.data = data.data.replace("\r", "").replace("\n", "");
                }
                data.data2 = rs.getString("sort_index");
                data.data3 = rs.getInt("class_id");
                trees.put(data.id, data);
                if (data.parent_id == 0) {
                    rootNodes.add(data);
                }
                return "";
            }

            @Override
            public void onFinish(OutputStream out) {
                try {
                    onTreeLoaded(out);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        );
    }

    private void exportDocuments() {
        System.out.println("exporting documents ");
        String regex = "<a\\s[^>]*href\\s*=\\s*[\"\']?([^\"\' ]*)[\"\']?[^>]*>";
        Pattern p = Pattern.compile(regex);
        Statement stmt = null;
        ResultSet rs = null;
        try {
            HashSet<Long> allDocuments = getAllDocumentIdentifiers(conn, true);
            int i = 0;
            ByteArrayOutputStream bs = new ByteArrayOutputStream();
            stmt = conn.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
            stmt.setFetchSize(Integer.MIN_VALUE);
            //stmt.setFetchSize(Integer.MIN_VALUE);
            // Выберем документ с id = 840
            rs = stmt.executeQuery("SELECT * FROM documents order by id ");
            String link;
            //List<String> links = new ArrayList<String>();
            //типы ссылок в документе
            String[] linkPref = new String[]{"jl:", "aps://", "apsfile://", "http://"};
            // определяем какого вида ссылки находили
            int usedPref;
            StringBuilder builder = new StringBuilder(1024 * 1024 * 10);
//            long lastDocId = 0;
            while (rs.next()) {
                long id = rs.getInt("id");
                bs.reset();
                LZMAHelper.Decompress(rs.getBinaryStream("body"), bs);
                builder.setLength(0);
                String val = bs.toString("utf-8");
                for (String pref : linkPref) {
                    val = val.replaceAll(pref + id + "/", "");
                    val = val.replaceAll(pref + id, "");
                }
                builder.append(val);
                StringBuilderUpperLower.toLowerCase(builder);
                //System.out.println(val);
                Matcher m = p.matcher(builder);
                while (m.find()) {
                    // making case inSensitive
                    link = m.group(1);
                    //removing http and other headers
                    usedPref = -1;
                    for (int i1 = 0; i1 < linkPref.length; i1++) {
                        String pref = linkPref[i1];
                        if (link.indexOf(pref) == 0) {
                            usedPref = i1;
                            link = link.substring(pref.length());
                        }
                    }
                    // removing bookmarks 840065576
                    if (link.indexOf("#") == 0) {
                        continue;
                    }
                    if (link.contains("#")) {
                        link = link.substring(0, link.indexOf("#"));
                    }
                    if (link.contains("/")) {
                        link = link.substring(0, link.indexOf("/"));
                    }
                    boolean isBrokenLink = 0 == usedPref;
                    // check if its a number, then add to list
                    try {
                        Long newDocId = Long.parseLong(link);
                        Attachment attachment = attachments.get(newDocId);
                        //ignore jl or absent document links
                        if (isBrokenLink || attachment == null && !allDocuments.contains(newDocId)) {
                            val = val.replace(m.group(1), "../700000000/700000000.htm");
                        } else if (val.indexOf("//" + newDocId) > 0 && usedPref >= 0) {
                            //подменяем на локальные ссылки если нашли
                            String linkType = linkPref[usedPref];
                            if (attachment != null) {
                                String newLink = "issp:///%3CExtId%3E::|" + attachment.file_name;
                                val = val.replaceAll(linkType + newDocId + "/", newLink);
                                val = val.replaceAll(linkType + newDocId, newLink);
                            } else {
                                val = val.replaceAll(linkType + newDocId + "/", String.format("../%d/%d.htm", newDocId, newDocId));
                                val = val.replaceAll(linkType + newDocId, String.format("../%d/%d.htm", newDocId, newDocId));
                            }
                            //System.out.println(String.format("%d\t\t%d", id, newDocId));
                        }
//                        lastDocId = newDocId;
                        //links.add(link);
                    } catch (NumberFormatException e) {
                        // seems external link
                        //System.out.println(String.format("%d\t\terror %s", id, link));
                    }
                }
                String file_name = String.format("src/%d/%d.htm", id, id);
                File file = new File(exportDirectory + "/" + file_name);
                file.getParentFile().mkdirs();
                FileOutputStream fos = new FileOutputStream(file);
                val = val.replaceAll("charset=utf-8", "charset=" + encoding);
                writeln(fos, val);
                closeObject(fos);
                if (++i == 1000) {
                    i = 0;
                    System.out.println(id);
                    System.gc();
                    //return;
                }
            }
            closeObject(bs);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeObject(rs);
            closeObject(stmt);
        }
    }


    private void exportNumberCodesProperties() {
        Statement stmt2 = null;
        ResultSet rs2 = null;
        ClassificatorInfo info = docs_tree[0];
        try {
            stmt2 = conn.createStatement();
            rs2 = stmt2.executeQuery("select * from codes order by docId");
            while (rs2.next()) {
                long docId = rs2.getLong("docId");
                String code = rs2.getString("code");
                List<String> trees = info.docs.get(docId);
                if (trees == null) {
                    trees = new ArrayList<String>();
                    info.docs.put(docId, trees);
                }
                trees.add(code);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeObject(rs2);
            closeObject(stmt2);
        }
    }

    private void exportClassificatorProperties() {
        Statement stmt2 = null;
        ResultSet rs2 = null;
        try {
            stmt2 = conn.createStatement();
            rs2 = stmt2.executeQuery("select d.tree_id,d.doc_id, t.class_id\n" +
                    "from tree_document d\n" +
                    "inner join trees t on t.id = d.tree_id\n" +
                    "where t.class_id in (1,2,3)\n" +
                    "order by doc_id, t.class_id");
            while (rs2.next()) {
                int classId = rs2.getInt("class_id");
                ClassificatorInfo info = docs_tree[classId];
                //ignore class_id == 0
                if (info == null || classId == 0) continue;
                List<String> trees = info.docs.get(rs2.getLong("doc_id"));
                if (trees == null) {
                    trees = new ArrayList<String>();
                    info.docs.put(rs2.getLong("doc_id"), trees);
                }
                trees.add(rs2.getString("tree_id"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeObject(rs2);
            closeObject(stmt2);
        }

    }

    private void exportDocumentProperties() {
        exportClassificatorProperties();
        exportNumberCodesProperties();
        System.out.println("exporting document properties ");
        Statement stmt2 = null;
        ResultSet rs2 = null;
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        try {
            FileOutputStream listOut = new FileOutputStream(exportDirectory + "/REQ.DIR");
            stmt2 = conn.createStatement();
            rs2 = stmt2.executeQuery("select * from properties order by id");
            //id, fullName, shortName, publishDate, regDate, regNumber, comment, priority, isDocument, isActive, lastModified, draft, status, price, propsMD5, bodyMD5, treesMD5, codesMD5
            while (rs2.next()) {
                long id = rs2.getLong("id");
                String fileName = String.format("req/%d/%d.req", id, id);
                writeln(listOut, fileName);
                File file = new File(exportDirectory + "/" + fileName);
                // создаем папки если отсутсвуют
                file.getParentFile().mkdirs();
                FileOutputStream out = new FileOutputStream(file);
                try {
                    writeln(out, String.format("[%d]", id));
                    writeln(out, String.format("Id = %d", id));
                    writeln(out, String.format("Title = %s", rs2.getString("fullName")), true);
                    writeln(out, String.format("ShortTitle = %s", rs2.getString("shortName")), true);
                    writeln(out, String.format("Path = src\\%d\\%d.htm", id, id));
                    if (rs2.getLong("comment") > 0) {
                        writeln(out, String.format("Note = src\\%d\\%d.htm", rs2.getLong("comment"), rs2.getLong("comment")));
                    }
                    if (rs2.getDate("publishDate") != null)
                        writeln(out, String.format("PublishDate = %s", df.format(rs2.getDate("publishDate"))));
                    if (rs2.getDate("regDate") != null)
                        writeln(out, String.format("RegDate = %s", df.format(rs2.getDate("regDate"))));

                    if (rs2.getDate("lastModified") != null) {
                        writeln(out, String.format("LastModified = %s", df.format(rs2.getDate("lastModified"))));
                        writeln(out, String.format("SysChangeDate = %s", df.format(rs2.getDate("lastModified"))));
                    }
                    if (!(rs2.getString("regNumber") == null || "".equals(rs2.getString("regNumber")))) {
                        writeln(out, String.format("Number_min = %s", rs2.getString("regNumber")));
                    }
                    writeln(out, String.format("Priority = %s", rs2.getString("priority")));
                    int status = rs2.getInt("status");
                    writeln(out, String.format("Status = %s", status == 0 ? "1" : (status == 1 ? "2" : "4")));
                    writeln(out, String.format("IsActive = %s", rs2.getInt("isActive")));
                    writeln(out, String.format("IsDocument = %s", rs2.getInt("isDocument")));
                    writeln(out, String.format("Draft = %s", rs2.getString("draft")));
                    writeln(out, String.format("Price = %s", rs2.getString("price")));
                    writeln(out, String.format("BodyMD5 = %s", rs2.getString("bodyMD5")));
                    for (ClassificatorInfo info : docs_tree) {
                        if (info == null) continue;
                        List<String> trees = info.docs.get(id);
                        if (trees == null || trees.size() == 0) continue;
                        writeln(out, String.format("%sNUM=%d", info.name, trees.size()));
                        for (int i = 0; i < trees.size(); i++) {
                            String tree = trees.get(i);
                            writeln(out, String.format("%s%02d=%s", info.name, i + 1, tree));
                        }
                    }
                } finally {
                    closeObject(out);
                }
            }
            closeObject(listOut);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeObject(rs2);
            closeObject(stmt2);
        }

    }

}
