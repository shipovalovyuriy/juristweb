@echo off
for /f "tokens=1-4 delims=. " %%a in ('date /t') do (set mydate=%%c%%b%%a)
For /f "tokens=1-4 delims=/:" %%a in ('time /t') do (set mytime=%%a%%b%%c)

set OUTPUT="%mydate%.log"
if "%STDOUT_REDIRECTED%" == "" (
    set STDOUT_REDIRECTED=yes
    cmd.exe /c %0 %* >> %OUTPUT% 2>&1 
    c:\bin\sender\blat.exe c:\bin\sender\1.log -attach %OUTPUT% -s Restore-Interconsult -to malik@novelty.kz
    exit /b %ERRORLEVEL%
)

rem get files list on local ftp
rem FOR /F "delims=|" %%I IN ('DIR "f:\ftp\marina\*.*" /B /O:D') DO SET st=%%I
rem echo "new file name %st%"
rem IF EXIST %~dp0\%st% (
rem  echo "dump already processed %st%"
rem  exit /b 1
rem )
rem copy f:\ftp\marina\%st% %~dp0\

rem get current date formatted yyyymmdd
for /f "tokens=1-4 delims=. " %%a in ('date /t') do (set SCHEME=sps_%%c%%b%%a)
echo "selecting scheme %SCHEME%"

rem IF NOT EXIST %~dp0\%st% (
rem get files list on remote ftp
rem %~dp0\bin\wget ftp://novelty:ghfdjdjqrjycekmnfyn@82.115.35.174:21/ -O 2 --no-remove-listing
%~dp0\bin\wget ftp://novelty:ghfdjdjqrjycekmnfyn@srv.wfin.kz:21/ -O 2 --no-remove-listing
del 2
rem For /f "tokens=9-12 delims=/ " %%a in (.listing) do (set st=%%a)
rem )
set p_file=-rw-r--r--
For /f "tokens=1-12 delims=/ " %%a in (.listing) do (
  IF /i %%a==%p_file% (
    set st=%%i
  )
rem set st=%%a
)


echo "selecting dump %st%"
IF NOT EXIST %~dp0\%st% (
%~dp0\bin\wget ftp://novelty:ghfdjdjqrjycekmnfyn@82.115.35.174:21/%st%
)

del .listing
For /f "tokens=1-4 delims=. " %%a in ("%st%") do (set sql=%%a.sql)
if not exist "%~dp0\%sql%" (
echo extract file
rem %~dp0\bin\tar -xzvf %st%
%~dp0\bin\gzip -dc %st%  | %~dp0\bin\tar xf -
rem get sql file name
if not exist "%~dp0\%sql%" (
 echo "file %sql% not found"
 exit /b 1
)

rem mysql -uroot -p2m1Ggola jurisst -e"select 1;"
rem ERROR 1049 (42000): Unknown database
rem set MYSQL_PATH="C:\Program Files\MySQL\MySQL Server 5.5\bin\"
echo prepare create database
%~dp0\bin\sed -b -e "s/sps_new/%SCHEME%/g" "%~dp0\crDb.sql.template" >%~dp0\crDb.sql
%~dp0\bin\sed -b -e "s/sps_new/%SCHEME%/g" %~dp0\data-config.xml >%~dp0\conf/data-config.xml
echo create database and grant access
mysql.exe -uroot -p2m1Ggola < %~dp0\crdb.sql
echo "restore dump file %sql%"
mysql.exe -uroot -p2m1Ggola %SCHEME% < %sql%
mysql.exe -uroot -p2m1Ggola %SCHEME% -e"alter table documents add changed_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;"
rem mysql -uroot -p2m1Ggola sps_new -e"select * from users"
)
rem echo extract document bodies
rem cmd.exe /c %~dp0\decoder\runDecoder.bat %SCHEME% root 2m1Ggola
rem echo extract document bodies one more time if previous fails
rem cmd.exe /c %~dp0\decoder\runDecoder.bat %SCHEME% root 2m1Ggola
rem echo extract document bodies one more time if previous fails
rem cmd.exe /c %~dp0\decoder\runDecoder.bat %SCHEME% root 2m1Ggola

echo reload configuration for solr index
%~dp0\bin\wget http://127.0.0.1:28010/juristWeb/dataimport?command=reload-config -O 1
echo start indexing
%~dp0\bin\wget http://127.0.0.1:28010/juristWeb/dataimport?command=full-import -O 3
echo view status
rem search for text Indexing completed
FOR /L %%i IN (0 1 2000) DO (
	%~dp0\bin\wget http://127.0.0.1:28010/juristWeb/dataimport?command= -O 4 
	set rs=""
	FOR /F "tokens=*" %%a in ('%~dp0\bin\grep.exe completed 4') do (
		echo "index succeed"
		echo "switching to scheme %SCHEME%"
		%~dp0\bin\sed -b -e "s/sps_new/%SCHEME%/g" "%~dp0\switchScheme.sql.template" >%~dp0\switchScheme.sql
		ping 127.0.0.1 -n 15 >nul
		mysql.exe -uroot -p2m1Ggola < %~dp0\switchScheme.sql
		ping 127.0.0.1 -n 15 >nul
		mysql.exe -uroot -p2m1Ggola < %~dp0\switchScheme.sql
		exit /b 0
		)
	FOR /F "tokens=*" %%a in ('%~dp0\bin\grep.exe failed 4') do (
		echo "index failed"
		exit /b 0
		)
	echo "*** %%i *** sleep 5 seconds"
	ping 127.0.0.1 -n 5 >nul
)

rem Indexing timeout

rem mysql -uroot -p2m1Ggola sps -e"ALTER DATABASE SPS  UPGRADE DATA DIRECTORY sps_20111118;"
rem mysql -uroot -p2m1Ggola sps_new -e"ALTER DATABASE sps UPGRADE DATA DIRECTORY NAME;"
