@echo off


echo shutdowning DB Server
cmd /c %~dp0\test_db\shutdownTestDb.cmd
echo clearing db files
rmdir solr\mysql\data\sps_new /s /q
rmdir solr\mysql\data\jurist /s /q
del solr\mysql\data\ib_logfile0 /s /f /q
del solr\mysql\data\ib_logfile1 /s /f  /q
del solr\mysql\data\ibdata1 /s /f  /q
echo run DB Server
cmd /c %~dp0\test_db\run_testDB.cmd
echo restore Jurist scheme
d:\sps\solr\mysql\bin\mysql -P13306 -utest -ptest2m1Ggola mysql < %~dp0\crDb.sql
echo restore sps_new scheme %1
d:\sps\solr\mysql\bin\mysql -P13306 -utest -ptest2m1Ggola sps_new < %1
d:\sps\solr\mysql\bin\mysql -P13306 -utest -ptest2m1Ggola sps_new -e"alter table documents add changed_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;"
cd d:\sps\

echo generating exe file
copy %~dp0\..\out\artifacts\standAlone\standAlone.jar .\
"c:\Program Files (x86)\Launch4j\launch4jc.exe" %~dp0\..\standAlone\setup\clientExe.xml
"c:\Program Files (x86)\Launch4j\launch4jc.exe" %~dp0\..\standAlone\setup\clientNetExe.xml
echo restore Jurist scheme
cmd /c %~dp0\test_db\restore_jurist.cmd
echo shutdowning DB Server
cmd /c %~dp0\test_db\shutdownTestDb.cmd
cmd /c ping localhost
rmdir solr\data\index /s /q
echo running search index building
cmd /c %~dp0\test_db\run_fullIndex.cmd
del standAlone.jar /s /f /q
del sps0.log /s /f  /q
del sps0.log.lck /s /f /q
echo prepare chrome
rmdir Chromium\Data\Profiles\Default /s /q
mkdir Chromium\Data\Profiles\Default
copy Chromium\Data\Profiles\Preferences Chromium\Data\Profiles\Default
echo running setup build
rem cmd /c %~dp0\test_db\build_install.cmd

rem pause

