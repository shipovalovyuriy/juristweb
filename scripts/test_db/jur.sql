-- MySQL dump 10.13  Distrib 5.5.24, for Win64 (x86)
--
-- Host: localhost    Database: jurist
-- ------------------------------------------------------
-- Server version	5.5.9

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bookmark_docs`
--

DROP TABLE IF EXISTS `bookmark_docs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookmark_docs` (
  `tree_id` bigint(20) NOT NULL,
  `doc_id` bigint(20) NOT NULL,
  PRIMARY KEY (`doc_id`,`tree_id`),
  UNIQUE KEY `fk_bk_tree_doc_ids` (`tree_id`,`doc_id`) USING BTREE,
  CONSTRAINT `fk_bk_tree_id` FOREIGN KEY (`tree_id`) REFERENCES `bookmark_tree` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bookmark_docs`
--

LOCK TABLES `bookmark_docs` WRITE;
/*!40000 ALTER TABLE `bookmark_docs` DISABLE KEYS */;
/*!40000 ALTER TABLE `bookmark_docs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bookmark_tree`
--

DROP TABLE IF EXISTS `bookmark_tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookmark_tree` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `user_id` int(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `fk_bk_users_id` (`user_id`),
  KEY `fk_bk_parent_id` (`parent_id`),
  CONSTRAINT `fk_bk_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `bookmark_tree` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_bk_users_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='дерево закладок ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bookmark_tree`
--

LOCK TABLES `bookmark_tree` WRITE;
/*!40000 ALTER TABLE `bookmark_tree` DISABLE KEYS */;
/*!40000 ALTER TABLE `bookmark_tree` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_trees`
--

DROP TABLE IF EXISTS `role_trees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_trees` (
  `tree_id` bigint(20) NOT NULL DEFAULT '0',
  `role_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tree_id`,`role_id`),
  UNIQUE KEY `idx_role_tree_id` (`tree_id`,`role_id`),
  KEY `fk_tree_role_id` (`role_id`),
  CONSTRAINT `fk_tree_role_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=FIXED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_trees`
--

LOCK TABLES `role_trees` WRITE;
/*!40000 ALTER TABLE `role_trees` DISABLE KEYS */;
INSERT INTO `role_trees` VALUES (1333,7),(1457,7),(1697,7),(1701,7),(1712,7),(1739,7),(1876,7),(1879,7),(1890,7),(1959,7),(1970,7),(1977,7),(1981,7),(1983,7),(2242,7),(2260,7),(2314,7);
/*!40000 ALTER TABLE `role_trees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT 'имя роли',
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `role_UNIQUE` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Роли пользователей';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (7,'Все документы');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `role_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `fk_ur_user_id` (`user_id`),
  KEY `fk_ur_role_id` (`role_id`),
  CONSTRAINT `fk_ur_role_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_ur_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Роли присвоенные пользователям';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES (-1,7);
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_types`
--

DROP TABLE IF EXISTS `user_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_types` (
  `user_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`user_type_id`),
  UNIQUE KEY `user_type_UNIQUE` (`user_type`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_types`
--

LOCK TABLES `user_types` WRITE;
/*!40000 ALTER TABLE `user_types` DISABLE KEYS */;
INSERT INTO `user_types` VALUES (2,'nalogi.kz'),(1,'свои пользователи');
/*!40000 ALTER TABLE `user_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(100) CHARACTER SET utf8 NOT NULL,
  `pwd` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `user_type_id` int(11) NOT NULL COMMENT 'Тип пользователя',
  `email` varchar(255) CHARACTER SET utf8 DEFAULT '' COMMENT 'email',
  `name` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT 'Наименование',
  `valid_from` date DEFAULT NULL,
  `valid_till` date DEFAULT NULL,
  `rnn` varchar(12) CHARACTER SET utf8 DEFAULT NULL,
  `company` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `serial` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `login_UNIQUE` (`login`),
  KEY `fk_u_type_id` (`user_type_id`),
  CONSTRAINT `fk_u_type_id` FOREIGN KEY (`user_type_id`) REFERENCES `user_types` (`user_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='пользователи системы юрист';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (-1,'user',NULL,2,'','','2012-11-29','2013-11-29',NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`test`@`localhost`*/ /*!50003 TRIGGER `jurist`.`trigger1`
BEFORE INSERT ON `jurist`.`users`
FOR EACH ROW
BEGIN 
  if(new.valid_from is NULL or new.valid_from ='')THEN
     set new.valid_from = now();
  end if;
  if(new.valid_till is NULL)THEN
     set new.valid_till = curdate() ;
  end if;
  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Dumping routines for database 'jurist'
--
/*!50003 DROP FUNCTION IF EXISTS `changePassword` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`localhost`*/ /*!50003 FUNCTION `changePassword`(`userId` int,`par` text) RETURNS text CHARSET utf8
BEGIN
	declare cnt int;
	declare id int;
	declare login1 text;
	declare pwd1 text;
	declare newpwd text;
	set login1= ExtractValue(par,'chg/login');
	set pwd1= ExtractValue(par,'chg/pwd');
	set newpwd= ExtractValue(par,'chg/newpwd');
	select count(*),max(u.user_id) into cnt,id from users u where u.user_type_id=1 and u.login=login1 and u.pwd=PASSWORD(pwd1);
	if(cnt=0)THEN
		return '<res><success>0</success></res>';
	end if;
  update users set pwd=PASSWORD(newpwd) where user_type_id=1 and login=login1;
	return '<res><success>1</success></res>';

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getBookmarks` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`localhost`*/ /*!50003 FUNCTION `getBookmarks`(`user_id` int,`xml1` text) RETURNS text CHARSET utf8
BEGIN
	declare cnt int default 0;  
	declare id int default 0;
	declare val1 VARCHAR(100) CHARSET utf8 default "" ;
	declare res text;
	set res="";
	SELECT count(*) as cnt1,GROUP_CONCAT("{id:'",CAST(b.id as char),"',text:'",name,"'}" SEPARATOR ',') as res1  into cnt, res 
		from bookmark_tree b where b.user_id=user_id and ( b.parent_id=xml1 or (xml1='0' and b.parent_id is null));
  if(cnt=0)THEN
    set res = "";
	end if;
	set res = CONCAT("[", res ,"]");
	RETURN res;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getDeniedTreeList` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`localhost`*/ /*!50003 FUNCTION `getDeniedTreeList`(userId int) RETURNS text CHARSET utf8
BEGIN
	declare res text DEFAULT '0';
	declare cnt int;
	select count(*) into cnt from user_roles ur where ur.role_id=1 and ur.user_id=userId;
	if(cnt=0)then

    select DISTINCT GROUP_CONCAT(cast(tree_id as char) ) into res  from role_trees rt where  
		  rt.tree_id not in (select tree_id from role_trees rt1 
          where rt1.role_id in (
            select ur.role_id from user_roles ur where (ur.user_id = userId) 
            union select r.role_id from roles r where r.role_id=5 
            ) 
      );
          

    if(res is null) then
      set res = '0';
    end if;
	end if;
	RETURN res;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getDocumentScheme` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`localhost`*/ /*!50003 FUNCTION `getDocumentScheme`() RETURNS text CHARSET utf8
BEGIN
	
	RETURN "sps_new";
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getGrantedTreeList` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`localhost`*/ /*!50003 FUNCTION `getGrantedTreeList`(userId int) RETURNS text CHARSET utf8
BEGIN
	declare res text DEFAULT '0,1338';
	declare cnt int;
	select count(*) into cnt from user_roles ur where ur.role_id=1 and ur.user_id=userId;
	if(cnt=0)then
	  select DISTINCT GROUP_CONCAT(cast(tree_id as char) ) into res  from role_trees rt
      where rt.role_id in (
            select ur.role_id from user_roles ur where (ur.user_id = userId) 
            union select r.role_id from roles r where r.role_id=5 
            ) ;
     
    if(res is null) then
      set res = '0';
    end if;
	end if;
	RETURN res; 
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getRoles` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`localhost`*/ /*!50003 FUNCTION `getRoles`(`user_id` int) RETURNS text CHARSET utf8
BEGIN  
	declare cnt int default 0;  
	declare id int default 0;
	declare val1 VARCHAR(100) CHARSET utf8 default "" ;
	declare res text;
	set res="";
	SELECT count(*) as cnt1,GROUP_CONCAT("<row><id>",CAST(role_id as char),"</id><value>",role,"</value></row>" SEPARATOR '') as res1  into cnt, res from roles;
	if(cnt=-1)THEN
		BEGIN
			DECLARE done INT DEFAULT 0; 
			DECLARE curRoles CURSOR FOR SELECT role_id,role FROM roles;
			DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
			OPEN curRoles;
			REPEAT
					FETCH curRoles INTO id, val1;
					IF done=0 THEN
					 set res = CONCAT(res ,"<row><id>",id,"</id><value>",val1,"</value></row>");
					END IF;
			UNTIL done=0 END REPEAT;
			CLOSE curRoles;
		end;
	end if;
	set res = CONCAT("<dataset><total>",cnt,"</total>", res ,"</dataset>");
	return res;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getRolesTree` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`localhost`*/ /*!50003 FUNCTION `getRolesTree`(`user_id` int, par text) RETURNS text CHARSET utf8
BEGIN  
	declare cnt int default 0;  
	declare id int default 0;
	declare val1 VARCHAR(100) CHARSET utf8 default "" ;
	declare res text;
	set res="";
	SELECT count(*) as cnt1,GROUP_CONCAT("<row><id>",CAST(role_id as char),"</id><value>",tree_id,"</value></row>" SEPARATOR '') as res1  
				into cnt, res from role_trees where role_id=par;
	if(res is null) then set res =''; end if;
	set res = CONCAT("<dataset><total>",cnt,"</total>", res ,"</dataset>");
	return res;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getUserRoles` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`localhost`*/ /*!50003 FUNCTION `getUserRoles`(userid int, par text) RETURNS text CHARSET utf8
BEGIN 
	declare cnt int default 0;  
	declare id int default 0;
	declare val1 VARCHAR(100) CHARSET utf8 default "" ;
  DECLARE done INT DEFAULT 0; 
	declare res text;
	set res=" ";
	SELECT count(*) as cnt1,GROUP_CONCAT("<row><id>",CAST(u.role_id as char),"</id><value>",CAST(u.role_id as char),"</value></row>" SEPARATOR '') as res1  
			into cnt, res from user_roles u,roles r 
			where u.role_id=r.role_id and u.user_id=par;
	if(cnt=0) THEN
		set res = "";
	end if;
	set res = CONCAT("<dataset><total>",cnt,"</total>", res ,"</dataset>");
	return res;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getUserTypes` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`localhost`*/ /*!50003 FUNCTION `getUserTypes`(`user_id` int) RETURNS text CHARSET utf8
BEGIN 
	declare cnt int default 0;  
	declare id int default 0;
	declare val1 VARCHAR(100) CHARSET utf8 default "" ;
  DECLARE done INT DEFAULT 0; 
	declare res text;
	set res="";
	SELECT count(*) as cnt1,GROUP_CONCAT("<row><id>",CAST(user_type_id as char),"</id><value>",user_type,"</value></row>" SEPARATOR '') as res1  
		into cnt, res from user_types;
	set res = CONCAT("<dataset><total>",cnt,"</total>", res ,"</dataset>");
	return res;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `isGrantedTree` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`localhost`*/ /*!50003 FUNCTION `isGrantedTree`(userId int,treeId int) RETURNS int(11)
BEGIN
	declare res int DEFAULT 1;
  declare txt text;  
	declare cnt int;
	select count(*) into cnt from user_roles ur where ur.role_id=1 and ur.user_id=userId;
	if(cnt=0)then
	  select DISTINCT GROUP_CONCAT(cast(tree_id as char) ) into txt  from role_trees rt
      where rt.tree_id=treeId and rt.role_id in (
            select ur.role_id from user_roles ur where (ur.user_id = userId) 
            union select r.role_id from roles r where r.role_id=5 
            ) ;
     
    if(txt is null) then
      set res = 0;
    end if;
	end if;
	RETURN res; 
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `setRole` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`localhost`*/ /*!50003 FUNCTION `setRole`(userId int,`par` text) RETURNS text CHARSET utf8
    DETERMINISTIC
BEGIN   
	DECLARE xml2 VARCHAR(25) DEFAULT '<obj><count>2</count><parent>2</parent><row><id>0</id><value>2197</value></row><row><id>0</id><value>258</value></row></obj>';
  declare roleId int;
	DECLARE tmp text;
	DECLARE res text;
  set tmp = ExtractValue(par,'obj/row/id');
	set roleId = cast(tmp as UNSIGNED );
	SELECT ExtractValue(par,'obj/row/value') into tmp;
	if ((roleId is null )or (roleId=0) or (tmp = '' )) THEN
		insert into roles (role) VALUES(tmp);
		select role_Id into res from roles where role=tmp;
	ELSE
		update roles set role=tmp where role_id=roleId;
		select roleId into res;
	end if;
	return res;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `setRolesTree` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`localhost`*/ /*!50003 FUNCTION `setRolesTree`(userId int,`par` text) RETURNS text CHARSET utf8
    DETERMINISTIC
BEGIN   
	DECLARE xml2 VARCHAR(25) DEFAULT '<obj><count>2</count><role>2</role><row><id>0</id><value>2197</value></row><row><id>0</id><value>258</value></row></obj>';
  declare cnt int;
  declare roleId int;
  declare i int DEFAULT 0;
	DECLARE tmp text;
	DECLARE res text;
  set tmp = ExtractValue(par,'obj/count');
	set cnt = cast(tmp as UNSIGNED );
  set tmp = ExtractValue(par,'obj/parent');
	set roleId = cast(tmp as UNSIGNED );
	if(roleId is null or roleId=0) THEN
		return 'nothing saved';
  end if;
	delete from role_trees where role_id=roleId;
	WHILE i < cnt DO
		set i=i+1;
		SELECT ExtractValue(par,'obj/row[$i]/value') into tmp;
		insert into role_trees VALUES(tmp,roleId);
 END WHILE;
	select i into res;
	return res;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `setUser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`localhost`*/ /*!50003 FUNCTION `setUser`(userId int,`par` text) RETURNS text CHARSET utf8
    DETERMINISTIC
BEGIN   
	DECLARE xml2 VARCHAR(250) DEFAULT '<obj><count>2</count><parent>2</parent><row><id>0</id><value>2197</value></row><row><id>0</id><value>258</value></row></obj>';
  declare userId int;
	DECLARE tmp text;
	DECLARE tmp1 text;
	DECLARE tmp2 text;
	DECLARE tmp3 text;
	DECLARE tmpValidFrom  text;
	DECLARE tmpValidTill  text;
	DECLARE tmpEmail  text;
	DECLARE tmpName  text;
	DECLARE tmpRnn  text;
	DECLARE tmpCompany  text;    
	DECLARE tmpSerial   text;
	DECLARE res text;
  set tmp = ExtractValue(par,'obj/row/user_id');
  set tmp3= ExtractValue(par,'obj/row/id');
  if(tmp is null or tmp='')then
    set userId = cast(tmp3 as UNSIGNED );
  else 
	  set userId = cast(tmp as UNSIGNED );
  end if;  
	SELECT ExtractValue(par,'obj/row/login') into tmp;
	SELECT ExtractValue(par,'obj/row/pass') into tmp1;
	SELECT ExtractValue(par,'obj/row/user_type_id') into tmp2;
	SELECT ExtractValue(par,'obj/row/valid_from') into tmpValidFrom;
	SELECT ExtractValue(par,'obj/row/valid_till') into tmpValidTill;
	SELECT ExtractValue(par,'obj/row/email') into tmpEmail;
	SELECT ExtractValue(par,'obj/row/name') into tmpName;
	SELECT ExtractValue(par,'obj/row/rnn') into tmpRnn;
	SELECT ExtractValue(par,'obj/row/company') into tmpCompany;
	SELECT ExtractValue(par,'obj/row/serial') into tmpSerial;
    
	if(tmp2 is null  or tmp2='') THEN
		set tmp2 = '1';
	end if;
	if(tmpValidFrom='' or tmpValidFrom='0000-00-00') THEN
		set tmpValidFrom = CURDATE();
	end if;
	if(tmpValidTill='' or tmpValidTill='0000-00-00') THEN
		set tmpValidTill = CURDATE();
	end if;

	if ((userId is null )or (userId=0)) THEN
		insert into users (login,pwd,user_type_id, valid_from, valid_till,email,name, rnn, company, serial) 
        VALUES(tmp,tmp1,tmp2,tmpValidFrom, tmpValidTill,tmpEmail,tmpName,tmpRnn,tmpCompany,tmpSerial);
		select user_Id into userId from users where login=tmp and user_type_id=tmp2;
	ELSE
		update users set login=tmp, valid_from=tmpValidFrom, 
           valid_till=tmpValidTill, email=tmpEmail, serial=tmpSerial,
           name=tmpName, rnn = tmpRnn, company=tmpCompany
    where user_id=userId;
	end if;
	if(tmp1 is null or tmp1='')then
     set tmp1 ='';
  else   
		update users set pwd=PASSWORD(tmp1) where user_id=userId;
	end if;
	select userId into res;
	return res;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `setUserRoles` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`localhost`*/ /*!50003 FUNCTION `setUserRoles`(userId int,`par` text) RETURNS text CHARSET utf8
    DETERMINISTIC
BEGIN   
	DECLARE xml2 VARCHAR(25) DEFAULT '<obj><count>2</count><role>2</role><row><id>0</id><value>2197</value></row><row><id>0</id><value>258</value></row></obj>';
  declare cnt int;
  declare userId int;
  declare i int DEFAULT 0;
	DECLARE tmp text;
	DECLARE res text;
  set tmp = ExtractValue(par,'obj/count');
	set cnt = cast(tmp as UNSIGNED );
  set tmp = ExtractValue(par,'obj/parent');
	set userId = cast(tmp as UNSIGNED );
	if(userId is null or userId=0) THEN
		return 'nothing saved';
  end if;
	delete from user_roles where user_id=userId;
	WHILE i < cnt DO
		set i=i+1;
		SELECT ExtractValue(par,'obj/row[$i]/value') into tmp;
		insert into user_roles(user_id,role_id) VALUES(userId,tmp);
 END WHILE;
	select i into res;
	return res;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `testXml` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`localhost`*/ /*!50003 FUNCTION `testXml`(`xml1` varchar(100)) RETURNS varchar(255) CHARSET utf8
    DETERMINISTIC
BEGIN
	DECLARE xml2 VARCHAR(25) DEFAULT '<a>    <b c="1"><d>X</d></b>    <b c="2"><d>X</d></b>    </a>';
	DECLARE res text;
	set res = ExtractValue(xml1,'a/b/d[../@c="1"]');
	SET res = '1';
	set res = getRoles(0);

	return res;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `addBookmark` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`localhost`*/ /*!50003 PROCEDURE `addBookmark`(`user_id` int,`xml1` text,INOUT `res` text)
BEGIN
  if xml1 ='0' THEN
   set xml1 = null;
  end if;
	insert into bookmark_tree(name,parent_id, user_id) values ('����� �����',xml1,user_id);
	set res  = "1";
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `bookmarkDocument` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`localhost`*/ /*!50003 PROCEDURE `bookmarkDocument`(`user_id` int,`par` text,INOUT `res` text)
BEGIN
	DECLARE tmp text;
	DECLARE tmp1 text;
	DECLARE tmp2 text;
	DECLARE tmp3 text;
  set tmp1 = ExtractValue(par,'obj/id');
  set tmp3 = ExtractValue(par,'obj/docid');
  if(tmp3 = '0')THEN
   set tmp3 = null;
  end if;
  if(tmp1 = '0')THEN
   set tmp1 = null;
  end if;
 if(tmp1 is not null and tmp3 is not null) then begin
	 INSERT into bookmark_docs (tree_id, doc_id) VALUES(tmp1, tmp3);
	 set res = '1';
  end;
 else 
   set res = '�������� �� �� ������������ ��� �� ������� �����';
 end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `clearFullTree` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`localhost`*/ /*!50003 PROCEDURE `clearFullTree`(ids text)
BEGIN
    declare res text;
    declare sch text;
    select getDocumentScheme() into sch;
    SELECT @indexes := COUNT(*) FROM INFORMATION_SCHEMA.STATISTICS
        WHERE table_schema = sch
        AND table_name = 'tree_document'
        AND index_name = 'tree_doc_doc';
    if(@indexes = 0)then
    SET @sql_cmd := CONCAT( 'ALTER TABLE ', sch, '.tree_document ADD INDEX tree_doc_doc (doc_id ASC), ADD INDEX tree_doc_tree (tree_id ASC)');
        SELECT @sql_cmd;
        PREPARE stmt FROM @sql_cmd;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
    end if;
    call fillFullTreeList(ids, res);
    SET @s = CONCAT('DROP TABLE IF EXISTS ', sch,'.tmp_docs');
    PREPARE stm FROM @s;
    EXECUTE stm;
    DEALLOCATE PREPARE stm;
    set @s = concat('CREATE TABLE ', sch, '.tmp_docs (doc_id BIGINT(20),KEY(doc_id)) select distinct doc_id from ', sch,'.tree_document tr where tr.tree_id in  (',res,')');
    PREPARE stm FROM @s;
    EXECUTE stm;
    DEALLOCATE PREPARE stm;
    
    SET @s = CONCAT('delete from ', sch,'.tmp_docs where doc_id in (select tr.doc_id from ', 
            sch,'.tree_document tr,', sch,'.trees t where t.id=tr.tree_id and t.class_id=1 and  tr.tree_id not in (', res, '))');
    PREPARE stm FROM @s;
    EXECUTE stm;
    DEALLOCATE PREPARE stm;
    
    SET @s = CONCAT('delete from ', sch,'.documents where id in(select doc_id from ', sch,'.tmp_docs )');
    PREPARE stm FROM @s;
    EXECUTE stm;
    DEALLOCATE PREPARE stm;
    
    SET @s = CONCAT('delete from ', sch,'.properties where id in(select doc_id from ', sch,'.tmp_docs )');
    PREPARE stm FROM @s;
    EXECUTE stm;
    DEALLOCATE PREPARE stm;
    
    SET @s = CONCAT('delete from ', sch,'.tree_document where doc_id in(select doc_id from ', sch,'.tmp_docs )');
    PREPARE stm FROM @s;
    EXECUTE stm;
    DEALLOCATE PREPARE stm;
    
    SET @s = CONCAT('DROP TABLE IF EXISTS ', sch,'.tmp_docs');
    PREPARE stm FROM @s;
    EXECUTE stm;
    DEALLOCATE PREPARE stm;
    
    SET @s = CONCAT('delete from ', sch,'.trees where id in (', res, ')');
    PREPARE stm FROM @s;
    EXECUTE stm;
    DEALLOCATE PREPARE stm;
    
    
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `clearTreeNotIn` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`localhost`*/ /*!50003 PROCEDURE `clearTreeNotIn`(ids text)
BEGIN
    declare res text;
    declare sch text;
    select getDocumentScheme() into sch;
    SELECT @indexes := COUNT(*) FROM INFORMATION_SCHEMA.STATISTICS
        WHERE table_schema = sch
        AND table_name = 'tree_document'
        AND index_name = 'tree_doc_doc';
    if(@indexes = 0)then
    SET @sql_cmd := CONCAT( 'ALTER TABLE ', sch, '.tree_document ADD INDEX tree_doc_doc (doc_id ASC), ADD INDEX tree_doc_tree (tree_id ASC)');
        SELECT @sql_cmd;
        PREPARE stmt FROM @sql_cmd;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
    end if;
    call fillFullTreeList(ids, res);
    set res = concat(res,',1333');
    SET @s = CONCAT('DROP TABLE IF EXISTS ', sch,'.tmp_docs');
    PREPARE stm FROM @s;
    EXECUTE stm;
    DEALLOCATE PREPARE stm;
    set @s1 = concat('CREATE TABLE ', sch, '.tmp_docs ( doc_id BIGINT(20),KEY(doc_id) ) select distinct doc_id from ', sch,'.tree_document tr ');
    PREPARE stm FROM @s1;
    EXECUTE stm;
    DEALLOCATE PREPARE stm;
    
    
    SET @s2 = CONCAT('delete from ', sch,'.tmp_docs where doc_id in (select tr.doc_id from ', 
            sch,'.tree_document tr,', sch,'.trees t where t.id=tr.tree_id and t.class_id=1 and  tr.tree_id in (', res, '1) )');
    PREPARE stm FROM @s2;
    EXECUTE stm;
    DEALLOCATE PREPARE stm;
    
    
    SET @s3 = CONCAT('delete from ', sch,'.documents where id in(select doc_id from ', sch,'.tmp_docs )');
    PREPARE stm FROM @s3;
    EXECUTE stm;
    DEALLOCATE PREPARE stm;
    
    SET @s4 = CONCAT('delete from ', sch,'.properties where id in(select doc_id from ', sch,'.tmp_docs )');
    PREPARE stm FROM @s4;
    EXECUTE stm;
    DEALLOCATE PREPARE stm;
    
    SET @s5 = CONCAT('delete from ', sch,'.tree_document where doc_id in(select doc_id from ', sch,'.tmp_docs )');
    PREPARE stm FROM @s5;
    EXECUTE stm;
    DEALLOCATE PREPARE stm;
    
    SET @s6 = CONCAT('DROP TABLE IF EXISTS ', sch,'.tmp_docs');
    PREPARE stm FROM @s6;
    EXECUTE stm;
    DEALLOCATE PREPARE stm;
    
    
    SET @s7 = CONCAT('delete from ', sch,'.trees where class_id = 1 and id not in (', res, ')');
    PREPARE stm FROM @s7;
    EXECUTE stm;
    DEALLOCATE PREPARE stm;
    
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `doLogin` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`localhost`*/ /*!50003 PROCEDURE `doLogin`(IN `login` text,IN `pwd` text,INOUT `userId` int)
BEGIN 
	declare cnt int;
	declare id int;
	select count(*),max(u.user_id) into cnt,id from users u where u.user_type_id=1 and u.login=login and u.pwd=PASSWORD(pwd) and now() between u.valid_from and u.valid_till;
	if(cnt>0)THEN
		set userId = id;
	end if;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `editBookmark` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`localhost`*/ /*!50003 PROCEDURE `editBookmark`(`user_id` int,`par` text,INOUT `res` text)
BEGIN
	DECLARE tmp text;
	DECLARE tmp1 text;
	DECLARE tmp2 text;
	DECLARE tmp3 text;
  set tmp1 = ExtractValue(par,'obj/id');
  set tmp2 = ExtractValue(par,'obj/name');
  set tmp3 = ExtractValue(par,'obj/parent');
  if(tmp3 = '0')THEN
   set tmp3 = null;
  end if;
  update bookmark_tree b set b.parent_id=tmp3 , 	b.name=tmp2 where b.id=tmp1;
  set res = '1';
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `execute_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`localhost`*/ /*!50003 PROCEDURE `execute_proc`(IN user_id int, IN reqType text, IN par text, INOUT res text)
BEGIN
		if (reqType="getRoles") THEN
			set res = getRoles(user_id);
		elseif (reqType = "getUserTypes") then
			set res = getUserTypes(user_id);
		elseif (reqType = "getUserRoles") then
			set res = getUserRoles(user_id,par);
		elseif (reqType = "getUsers") then
			call getUsers(user_id,par, res);
		elseif (reqType = "changePassword") then
			set res = changePassword(user_id,par);
		elseif (reqType = "getRolesTree") then
			set res = getRolesTree(user_id,par);
		elseif (reqType = "setRolesTree") then
			set res = setRolesTree(user_id,par);
		elseif (reqType = "setRole") then
			set res = setRole(user_id,par);
		elseif (reqType = "setUserRoles") then
			set res = setUserRoles(user_id,par);
		elseif (reqType = "setUser") then
			set res = setUser(user_id,par);
		elseif (reqType = "isTreeDenied") then
			call isTreeDenied(user_id, par, res);
		elseif (reqType = "getGrantedTreeList") then
			set res = getGrantedTreeList(user_id);
		elseif (reqType = "isDocumentDenied") then
			call isDocumentDenied(user_id, par, res);
		elseif (reqType = "addBookmark") then
			call addBookmark(user_id, par, res);
		elseif (reqType = "getBookmarks") then
			set res = getBookmarks(user_id, par);
		elseif (reqType = "editBookmark") then
			call editBookmark(user_id, par,res );
		elseif (reqType = "bookmarkDocument") then
			call bookmarkDocument(user_id, par, res );
		elseif (reqType = "getDefaultPage") then
			call getDefaultPage(user_id, res );
    else  
        set res = concat('unknown request ',reqType);
		end if;
		if(res is null)then
			set res='';
		end if;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fillFullTreeList` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`localhost`*/ /*!50003 PROCEDURE `fillFullTreeList`(in ids text, out res text)
BEGIN
	declare sch text;
	declare childTrees text;
	declare tmp text;
  set max_sp_recursion_depth=30000;
	select getDocumentScheme() into sch;
	set @sql1 = concat('select GROUP_CONCAT(cast(id as char) ) into @cid from ',sch,'.trees where parent_id in(',ids,')');
	PREPARE stmt FROM @sql1;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
  set childTrees = @cid;
	if((childTrees is null) or (childTrees =''))THEN
  	set res = ids;
  else
    call fillFullTreeList(childTrees,tmp);
    if(not (tmp is null or tmp = '' ))then
            set res = concat(ids,',',tmp);
    end if;
	end if;
  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getDefaultPage` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`localhost`*/ /*!50003 PROCEDURE `getDefaultPage`(IN userId int, OUT `aBody` text)
BEGIN
  declare sch text;
	select getDocumentScheme() into sch;
	
	set @sql1 = concat('select d.body into @aBody from  ',sch,'.document_body d where d.docid=840065639');
  
	PREPARE stmt FROM @sql1;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
  set aBody = @aBody;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getUsers` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`localhost`*/ /*!50003 PROCEDURE `getUsers`(`userId` int, par text, out res longtext)
BEGIN
	declare cnt int default 0;  
	declare fcnt int default 0;  
	declare id int default 0;
	declare i int default 0;
	declare val1 VARCHAR(100) CHARSET utf8 default "" ;
	declare pname VARCHAR(100) CHARSET utf8 default "" ;
	declare pcmp VARCHAR(100) CHARSET utf8 default "" ;
	declare cmp VARCHAR(100) CHARSET utf8 default "" ;
	declare ptype VARCHAR(100) CHARSET utf8 default "" ;
	declare pval VARCHAR(100) CHARSET utf8 default "" ;
	declare sort1 VARCHAR(100) CHARSET utf8 default " login " ;
	declare dir1 VARCHAR(100) CHARSET utf8 default " asc " ;
	declare orderBy VARCHAR(100) CHARSET utf8 default " " ;
	declare where1 text CHARSET utf8 default " where user_type_id is not null " ;
	SELECT ExtractValue(par,'filter/start') into @start1;
	SELECT ExtractValue(par,'filter/limit') into @limit1;
	SELECT ExtractValue(par,'filter/cnt') into fcnt;
	SELECT ExtractValue(par,'filter/sort') into sort1;
	SELECT ExtractValue(par,'filter/dir') into dir1;
  IF(sort1='pass')THEN
				set sort1 = 'pwd';
  elseif (sort1 is null) then              
				set sort1 = 'login';
  END IF;                
	set orderBy = CONCAT(" ORDER BY ",sort1," ",dir1);
  SET SESSION group_concat_max_len = 20000;
	while i<fcnt DO
		BEGIN
			set i=i+1;
			SELECT ExtractValue(par,'filter/p[$i]/@name') into pname;
			SELECT ExtractValue(par,'filter/p[$i]/@cmp') into pcmp;
			SELECT ExtractValue(par,'filter/p[$i]') into pval;
			SELECT ExtractValue(par,'filter/p[$i]/@type') into ptype;
			set  cmp = ' = ';
			IF(pcmp='lt')THEN
				set cmp = ' < ';
			ELSEIF (pcmp='gt')THEN
				set cmp = ' > ';
			ELSEIF (pcmp='like')THEN
				set cmp = ' like ';
			END IF;
			if(ptype='string' or ptype='date')THEN
				set pval = CONCAT('''',REPLACE(pval,'''',''),'''');
			end if;
			set where1 = CONCAT(where1,' and ',pname, cmp, pval);
		END;
	END WHILE;
	set res="";
	set @sql1 = concat('SELECT GROUP_CONCAT("<row><u>",/*CAST(*/user_id /*as char)*/,
							"</u><l>",login,"</l><ut>",
							user_type_id,"</ut><vf>",
							IFNULL(valid_from,""),"</vf><vt>",
							IFNULL(valid_till,""),"</vt><e>",
							IFNULL(email,""),"</e><n>",
							IFNULL(name, ""),"</n><r>",
							IFNULL(rnn, ""),"</r><c>",
							IFNULL(company, ""),"</c><s>",
							IFNULL(serial, ""),"</s></row>" SEPARATOR "") as res1  ,
				(select count(*) from users',where1,' )as cnt1 into @res,@cnt from(select t.* from users t',where1,orderBy,' limit ?,? ) k');
	PREPARE stmt FROM @sql1;
	EXECUTE stmt USING @start1,@limit1;
	DEALLOCATE PREPARE stmt;
	set res = @res;
	set cnt = @cnt;
	set res = CONCAT("<dataset><total>",cnt,"</total>", res ,"</dataset>");
	
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `isDocumentDenied` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`localhost`*/ /*!50003 PROCEDURE `isDocumentDenied`(IN userId int,IN par text,OUT res text)
BEGIN
	declare sch text;
	declare deniedTrees text;
	declare cnt BIGINT;
	declare grantedCnt BIGINT;
	declare tid BIGINT default 0;
  declare treeRes text;
  DECLARE i INT DEFAULT 0;
	select getDocumentScheme() into sch;
	select getDeniedTreeList(userId) into deniedTrees;
	set res = '0';
	set @sql1 = concat('select count(*) into @cnt from ',sch,'.tree_document where doc_id=?');
  set @par  = par;
	PREPARE stmt FROM @sql1;
	EXECUTE stmt USING @par;
  set cnt = @cnt;
	DEALLOCATE PREPARE stmt;
	set @sql1 = concat('select count(*) into @cnt from ',sch,'.tree_document where doc_id=?  and 
     tree_id in (select tree_id from role_trees rt1 
          where rt1.role_id in (
            select ur.role_id from user_roles ur where (ur.user_id = ?) 
            union select r.role_id from roles r where r.role_id=5 
            ) 
      )');
  set @par  = par;
  set @userId  = userId;
	PREPARE stmt FROM @sql1;
	EXECUTE stmt USING @par,@userId;
  set grantedCnt= @cnt;
	DEALLOCATE PREPARE stmt;
	if(cnt is null or cnt=0 or grantedCnt > 0)THEN
		set res = '0';
  else
    set res = '1';
	end if;
	if(cnt>0 and res='1')then
		set res = '888';
    set i=0;
    set tid=0;
    WHILE(i<cnt and res='888')do
  		BEGIN
  	    set @sql2 = concat(
            'select tree_id into @cid2  from ',
            sch,
            '.tree_document where doc_id=? and tree_id > ? order by tree_id  LIMIT 0, 1');
        set @cid2=null;
        SET @iter = tid;
  			PREPARE stmt1 FROM @sql2;
  			EXECUTE stmt1 USING @par,@iter;
        set tid=@cid2;
  			DEALLOCATE PREPARE stmt1;
        set treeRes = '';
        call isTreeDenied(userId,tid,treeRes);
  			if(treeRes ='0' )THEN
  				set res = '0';
      	end if;
        set i=i+1;
  		END;
  	END WHILE;
	END if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `isTreeDenied` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`test`@`localhost`*/ /*!50003 PROCEDURE `isTreeDenied`(IN userId int,IN par text,OUT res text)
BEGIN
	declare sch text;
	declare deniedTrees text;
	declare grantedTree int;
	declare cid BIGINT;
	declare pid BIGINT;
	select getDocumentScheme() into sch;
	select getDeniedTreeList(userId) into deniedTrees;
	select isGrantedTree(userId,par) into grantedTree;
	set res = '0';
	set @sql1 = concat('select id ,parent_id into @cid, @pid  from ',sch,'.trees where id=? and id not in(',deniedTrees,')');
  set @par  = par;
	PREPARE stmt FROM @sql1;
	EXECUTE stmt USING @par;
	DEALLOCATE PREPARE stmt;
  set cid = @cid;
  set pid = @pid;
	if(cid is null)THEN
		set res = '1';
	end if;
	WHILE pid is not null and pid!=0 do
		BEGIN
	    set @sql2 = concat('select id ,parent_id into @cid2, @pid2  from ',sch,'.trees where id=? and id not in(',deniedTrees,')');
      set @pid1=pid;
      set @cid2=null;
      set @pid2=null;
			PREPARE stmt1 FROM @sql2;
			EXECUTE stmt1 USING @pid1;
			DEALLOCATE PREPARE stmt1;
      set cid = @cid2;
      set pid = @pid2;
			if(cid is null)THEN
				set res = '1';
			end if;
		END;
	END WHILE;
  if(grantedTree=1) then
   set res='0';
  end if;
    
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-12-27 11:56:04
