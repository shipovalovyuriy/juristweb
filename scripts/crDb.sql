DROP DATABASE IF EXISTS sps_new;
create database sps_new;
grant all on sps_new.* to 'test'@localhost identified by 'test2m1Ggola';
CREATE TABLE sps_new.document_body (
  `docid` int(11) NOT NULL,
  `body` longtext,
  `changed_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`docid`)
)