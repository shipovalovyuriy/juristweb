#!/bin/bash
echo "$0"
dirname "$0"
MY_PATH=`dirname "$0"`
SCHEME=`date +"sps_%Y%m%d"`

TPL=$(echo "$MY_PATH/crDb.sql.template")

echo $TPL

sed -b -e "s/sps_new/$SCHEME/g" "$TPL" >$MY_PATH/crDb.sql
sed -b -e "s/sps_new/$SCHEME/g" $MY_PATH/data-config.xml >$MY_PATH/conf/data-config.xml

mysql -uroot -p2m1Ggola < $MY_PATH/crDb.sql
echo restore dump
mysql -uroot -p2m1Ggola $SCHEME < ${1}
# mysql -uroot -p2m1Ggola sps_new -e"select * from users"
echo extract document bodies
$MY_PATH/decoder/runDecoder.bat $SCHEME root 2m1Ggola
#java -jar $MY_PATH/decoder/Decoder.jar $SCHEME root 2m1Ggola
echo reload configuration for solr index
echo wget http://127.0.0.1:28010/juristWeb/dataimport?command=reload-config -O 1
echo start indexing
echo wget http://127.0.0.1:28010/juristWeb/dataimport?command=full-import -O 3
echo view status
echo wget http://127.0.0.1:28010/juristWeb/dataimport?command= -O 4 


