package kz.novelty.jurist;

import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;
import com.mysql.management.MysqldResource;
import com.mysql.management.MysqldResourceI;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.io.FileInputStream;
import java.io.PrintStream;
import java.util.*;
import java.util.logging.*;

public class SpsStandAlone {
    public static String juristHome = null;
    Object mysqldResource = null;
    int dbPort = 13306;
    int webPort = 8080;
    String[] args;

    public SpsStandAlone(String[] args) {
        this.args = args;
        if(isUpdateBuilderMode()){
            this.webPort = 8090;
        }
        if(isUpdateServerMode()){
            this.webPort = 9090;
        }
    }

    public Object startDatabase(String databaseDir, int port,
                                       String userName, String password) {
        MysqldResource mysqldResource = new MysqldResource(new File(databaseDir));
        //?createDatabaseIfNotExist=true&amp;server.datadir=mysql_data&amp;characterEncoding=UTF-8&amp;server.default-storage-engine=INNODB&amp;innodb_file_per_table
        Map database_options = new HashMap();
        database_options.put(MysqldResourceI.PORT, Integer.toString(port));
        database_options.put(MysqldResourceI.INITIALIZE_USER, "true");
        database_options.put(MysqldResourceI.INITIALIZE_USER_NAME, userName);
        database_options.put(MysqldResourceI.INITIALIZE_PASSWORD, password);
        //database_options.put("innodb_file_per_table", null);
        database_options.put("server.default-storage-engine", "INNODB");
        database_options.put("character-set-server", "utf8");
        database_options.put("max_allowed_packet", "104857600");

        mysqldResource.start("sps-mysqld-thread", database_options);
        if (!mysqldResource.isRunning()) {
            throw new RuntimeException("MySQL did not start.");
        }
        System.out.println("MySQL is running.");
        return mysqldResource;
    }

    Vector<String> messages;

    public void declareForWar() {
        try {
            InitialContext ctx = null;
            ctx = (InitialContext) new InitialContext();
            MysqlConnectionPoolDataSource ds = new MysqlConnectionPoolDataSource();
            String dbAlias = "NoveltyDBJurist";
            File clientProp = new File("client.properties");
            if (allowUpdate() && clientProp.exists()) {
                try {
                    Properties properties = new Properties();
                    properties.load(new FileInputStream(clientProp));
                    System.setProperty("sps.update.url", properties.getProperty("sps.update.url"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            File updateServerProp = new File(isUpdateBuilderMode() ? "builder.properties" : "update_server.properties");
            if ((isUpdateBuilderMode() || isUpdateServerMode()) && updateServerProp.exists()) {
                try {
                    Properties properties = new Properties();
                    properties.load(new FileInputStream(updateServerProp));
                    ds.setUrl(properties.getProperty("db.url"));
                    ds.setUser(properties.getProperty("db.user"));
                    ds.setPassword(properties.getProperty("db.pass"));
                    System.setProperty("cert.alias", properties.getProperty("cert.alias"));
                    System.setProperty("cert.file", properties.getProperty("cert.file"));
                    if(isUpdateServerMode()){
                        dbAlias = "UpdateDBJurist";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    shutdown();
                }
            }else{
                ds.setUrl("jdbc:mysql://localhost:"+dbPort+"/jurist");
                ds.setUser("test");
                ds.setPassword("test2m1Ggola");
            }
            Context jdbcCtx = null;
            try {
                jdbcCtx = ctx.doLookup("jdbc");
            } catch (NamingException e) {
                jdbcCtx = ctx.createSubcontext("jdbc");
            }
            jdbcCtx.bind(dbAlias, ds);
            ctx.bind("embeddedMode", System.getProperty("interconsult.mode","LOC"));
            SysInfo info = new SysInfo();
            info.fillSystemInfo();
            System.out.println("SysInfo " + info.getItems().size());
            ctx.bind("sys_info", info.getItems());
            ctx.bind("embeddedDrives", getCdDrivesLabel());
            messages = new Vector<String>(){
                @Override
                public synchronized boolean add(String s) {
                    if("inc_ind".equals(s)){
                        doIndexIncremental(false);
                    }else if("exit".equals(s)){
                        shutdown();
                    }else {
                        SpsTrayMenu.showMessagge("Info", s);
                    }
                    return true;
                }
            };
            ctx.bind("tray_messages", messages);
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    public boolean isUpdateBuilderMode(){
        return "builder".equals(System.getProperty("interconsult.mode"));
    }

    public void initDB() {
//            org.eclipse.jetty.plus.jndi.Resource s = new Resource("jdbc/NoveltyDBJurist", ds);
        //juristHome = "solr";//(String) ctx.lookup("java:comp/env/jurist.home");
        if (!isUpdateBuilderMode() && !isUpdateServerMode() && juristHome != null && !"".equals(juristHome)) {
            mysqldResource = startDatabase(juristHome + "/mysql", dbPort, "test", "test2m1Ggola");
        }
        declareForWar();
    }

    public void shutdown() {
        stopDB();
        System.exit(0);
    }

    public void stopDB() {
        if (mysqldResource != null) {
//        	ServerLauncherSocketFactory.shutdown(juristHome + "/mysql", null);
            try {
                ((MysqldResource) mysqldResource).shutdown();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public Map<String, String> getCdDrivesLabel() {
        Map<String, String> res = new HashMap<String, String>();
        List<File> files = Arrays.asList(File.listRoots());
        for (File f : files) {
            String s1 = FileSystemView.getFileSystemView().getSystemDisplayName(f);
            String s2 = FileSystemView.getFileSystemView().getSystemTypeDescription(f);
            System.out.println("getSystemDisplayName : " + s1);
            System.out.println("getSystemTypeDescription : " + s2);
            if (s2 != null && (s2.indexOf("CD ") >= 0 || s2.indexOf("CD-") >= 0  || s2.indexOf("CD-") >= 0 || s2.indexOf("DVD ") >= 0 || s2.indexOf("DVD-") >= 0)) {
                String drive = f.getPath();
                drive = drive.substring(0, drive.length() - 1);
                String label = s1;
                int idx = s1.indexOf(drive);
                if (idx >= 0) {
                    label = label.substring(idx + 3).trim();
                }
                if("".equals(label) && (idx > 0)){
                    label = s1.substring(0,idx - 2).trim();
                }
                System.out.println("label : " + label + " drive : " + drive);
                if (label == null || "".equals(label)) continue;
                System.out.println("getSystemDisplayName : " + label);
                System.out.println("path : " + drive);
                res.put(drive, label);
            }
        }
        return res;
    }

    public void initServer() throws Exception {
        // для компиляции jsp файлов
        System.setProperty("org.apache.jasper.compiler.disablejsr199","true");
//        System.setProperty("org.eclipse.jetty.server.webapp.ContainerIncludeJarPattern",".*/.*jsp-api-[^/]*\\.jar$|.*/.*jsp-[^/]*\\.jar$|.*/.*taglibs[^/]*\\.jar$");
        System.setProperty("DEBUG", "true");
        Server server = new Server(webPort);
        if(isUpdateBuilderMode()){
            juristHome = System.getProperty("java.io.tmpdir")+"/solr";
        }else{
            juristHome = System.getProperty("jetty.home",".")+"/solr";
        }
        System.setProperty("solr.solr.home", juristHome);
        WebAppContext webapp = new WebAppContext();
        webapp.setContextPath("/");
        if(!isUpdateServerMode()){
            webapp.setWar("juristWeb.war");
        }else{
            webapp.setWar("updateServer.war");
        }
        server.setHandler(webapp);
        // start db
        initDB();
        server.start();
    }

    // процес полной индексации
    public void doIndexFull(boolean exitOnFinish){
        perfromIndexOperation("full-import", exitOnFinish);
    }
    // процес инкрементальной индексации
    public void doIndexIncremental(boolean exitOnFinish){
        perfromIndexOperation("delta-import", exitOnFinish);
    }

    public void perfromUpdateOperation(final boolean exitOnFinish){
        new Thread(new Runnable() {
            public void run() {
                String type="";
                if(exitOnFinish){
                    type = "&t=exit";
                }
                byte[] res = URLRequester.executeRequestAsByte("http://localhost:" + webPort + "/RequestHandler?requestType=applyUpdates" + type, null, false);

            }
        }).start();
    }
    /**
     * Начать индексацию поиска
     * @param indexMode - режим индексации
     * @param exitOnFinish - выйти их приложения при завершении индексации
     */
    public void perfromIndexOperation(String indexMode, boolean exitOnFinish){

        SpsTrayMenu.showMessagge("Правовой Консультант","Начат процесс индексации");
        try {
            String res = URLRequester.executeRequest("localhost:"+webPort, "dataimport?command=" + indexMode, "/", null);
            System.out.println(res);
            while (true){
                String xml=URLRequester.executeRequest("localhost:"+webPort, "dataimport?command=", "/", null);
                if(xml != null && xml.indexOf("failed")>=0) {
                    SpsTrayMenu.showMessagge("Правовой Консультант","Процесс индексации завершен с ошибкой");
                    break;
                }else if(xml != null && xml.indexOf("<str name=\"status\">idle</str>")>=0) {
                    SpsTrayMenu.showMessagge("Правовой Консультант","Завершен процесс индексации");
                    break;
                }
                Thread.sleep(120000);//2 minutes
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        // выходим если запущено из командной строки
        if(exitOnFinish){
            messages.add("exit");
        }
    }

    /**
     * Проверка параметров командой строки
     * @param args список аргументов
     * @param param название параметра
     * @return
     */
    public static boolean isCmdParamExist(String[] args, String param){
        for (int i = 0; i < args.length; i++) {
            if(param.equals(args[i])) return true;
        }
        return false;
    }

    public static void main(String[] args) {
        try {
            SpsStandAlone app  = new SpsStandAlone(args);
            // если просят не выводить в лог, значит не выводим
            if( !isCmdParamExist(args, "no-log-redirect") ){
                app.initLogging();
            }
            app.initServer();
            SpsTrayMenu.app = app;
            SpsTrayMenu.startUI("http://localhost:"+app.webPort+"/", !(isCmdParamExist(args, "full-index") || isCmdParamExist(args, "delta-index") || isCmdParamExist(args, "update")));
            // режим индексации
            if( isCmdParamExist(args, "full-index") ){
                app.doIndexFull(true);
            }
            if( isCmdParamExist(args, "delta-index") ){
                app.doIndexIncremental(true);
            }
            if( isCmdParamExist(args, "update") ){
                app.perfromUpdateOperation(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getLogFileName(){
        if(isUpdateServerMode()){
            return "sps_srv%g.log";
        }
        if(isUpdateBuilderMode()){
            return "sps_bld%g.log";
        }
        return "sps%g.log";
    }

    private void initLogging() throws Exception {
        // initialize logging to go to rolling log file
        LogManager logManager = LogManager.getLogManager();
        logManager.reset();

        // log file max size 1M, 3 rolling files, append-on-open
        Handler fileHandler = new FileHandler( getLogFileName(), 1000000, 10, true);
        fileHandler.setFormatter(new LogFormatter());
        Logger.getLogger("").addHandler(fileHandler);

        // preserve old stdout/stderr streams in case they might be useful
        PrintStream stdout = System.out;
        PrintStream stderr = System.err;

        // now rebind stdout/stderr to logger
        Logger logger;
        LoggingOutputStream los;

        logger = Logger.getLogger("stdout");
        los = new LoggingOutputStream(logger, StdOutErrLevel.STDOUT);
        System.setOut(new PrintStream(los, true));

        logger = Logger.getLogger("stderr");
        los = new LoggingOutputStream(logger, StdOutErrLevel.STDERR);
        System.setErr(new PrintStream(los, true));

        stdout.println("Output Redirected to log");

    }

    public boolean isUpdateServerMode() {
        return "update_server".equals(System.getProperty("interconsult.mode"));
    }

    public boolean allowUpdate() {
        return !isUpdateBuilderMode() && !isUpdateServerMode() && !"CD".equals(System.getProperty("interconsult.mode"));
    }
}
