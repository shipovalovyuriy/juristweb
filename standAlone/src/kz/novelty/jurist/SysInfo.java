package kz.novelty.jurist;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class SysInfo {
    private ArrayList<String> items = new ArrayList<String>();


    public ArrayList<String> getItems() {
        return items;
    }

    //check if item exist
    public boolean isItemExist(String name, String value) {
        String search = name+"="+value;
        return isItemExist(search);
    }
    public boolean isItemExist(String search) {
        for (int i = 0; i < items.size(); i++) {
            String item = items.get(i);
            if (item.equals(search)) return true;
        }
        return false;
    }

    // check if all item exists
    public boolean isAllItemsExist(List<String> list) {
        boolean exist = true;
        for (int i = 0; i < list.size(); i++) {
            String item = list.get(i);
            exist = exist && isItemExist(item);
            if (!exist) return exist;
        }
        return exist;
    }

    private void processOutput(String prefix, List<String> out) {
        for (int i = 0; i < out.size(); i++) {
            String line = out.get(i);
            if ("".equals(line)) continue;
            String[] prop = line.split("=");
            if (prop == null || prop.length < 2) continue;
            items.add(prefix + line);
        }
    }

    public void fillSystemInfo() {
        items.clear();
        String os = System.getProperty("os.name");
        if (os.toLowerCase().indexOf("win") < 0) return;
        final String command = "wmic %s get %s /value";
        String[][] cmds = new String[][]{
                new String[]{"nic", "nic", "macaddress"},
                new String[]{"bios", "bios", "SerialNumber"},
                new String[]{"hdd", "diskdrive", "Serialnumber,model"},
                new String[]{"win", "os", "Serialnumber,csName,BootDevice,BuildNumber,Caption,CodeSet," +
                        "InstallDate,MUILanguages,OSArchitecture,OSLanguage,OSProductSuite,OSType,Version," +
                        "ServicePackMajorVersion,ServicePackMinorVersion"},
                new String[]{"mb", "baseboard", "SerialNumber"}
        };
        List<String> out = new ArrayList<String>();
        String line;
        Process p = null;
        for (int i = 0; i < cmds.length; i++) {
            String[] c = cmds[i];
            out.clear();
            try {
                p = Runtime.getRuntime().exec(String.format(command, c[1], c[2]));
                p.getOutputStream().close();
                BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
                while ((line = input.readLine()) != null) {
                    out.add(line);
                }
                input.close();
                processOutput(c[0]+".", out);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }
}
